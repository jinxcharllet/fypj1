﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// ColorCycle
struct ColorCycle_t3175142491;
// DG.Tweening.Sequence
struct Sequence_t2050373119;
// DG.Tweening.Tween
struct Tween_t2342918553;
// DGADGameLogic
struct DGADGameLogic_t3582670137;
// FloatReference
struct FloatReference_t1374219879;
// FloatVariable
struct FloatVariable_t761998486;
// GDTGameLogic
struct GDTGameLogic_t4243140947;
// GameEvent
struct GameEvent_t4069578135;
// GameObjectList
struct GameObjectList_t328636558;
// IGameLogic
struct IGameLogic_t3343192399;
// IntVariable
struct IntVariable_t1706759505;
// MatwColorList
struct MatwColorList_t3252287760;
// MeshHolderList
struct MeshHolderList_t1122110985;
// ObjectData
struct ObjectData_t3315742923;
// ObjectState
struct ObjectState_t1202103565;
// RandomTextData
struct RandomTextData_t1210110652;
// System.Collections.Generic.Dictionary`2<System.Int32,System.Collections.Generic.List`1<UnityEngine.GameObject>>
struct Dictionary_2_t1474424692;
// System.Collections.Generic.List`1<GameEvent>
struct List_1_t1246685581;
// System.Collections.Generic.List`1<MaterialwColorHolder>
struct List_1_t3892189426;
// System.Collections.Generic.List`1<MeshHolder>
struct List_1_t1776565326;
// System.Collections.Generic.List`1<StateWithMaterials>
struct List_1_t284606406;
// System.Collections.Generic.List`1<System.String>
struct List_1_t3319525431;
// System.Collections.Generic.List`1<UnityEngine.Events.UnityEvent>
struct List_1_t4053343389;
// System.Collections.Generic.List`1<UnityEngine.GameObject>
struct List_1_t2585711361;
// System.Collections.Generic.List`1<UnityEngine.ScriptableObject>
struct List_1_t4000433264;
// System.Collections.Generic.List`1<UnityEngine.UI.Image>
struct List_1_t4142344393;
// System.Collections.Generic.List`1<UnityEngine.Vector3>
struct List_1_t899420910;
// System.String
struct String_t;
// System.Void
struct Void_t1185182177;
// TMPro.TextMeshProUGUI
struct TextMeshProUGUI_t529313277;
// UintVariable
struct UintVariable_t3613798951;
// UnityEngine.Animator
struct Animator_t434523843;
// UnityEngine.Audio.AudioMixer
struct AudioMixer_t3521020193;
// UnityEngine.Audio.AudioMixerGroup
struct AudioMixerGroup_t2743564464;
// UnityEngine.AudioClip
struct AudioClip_t3680889665;
// UnityEngine.AudioSource
struct AudioSource_t3935305588;
// UnityEngine.CanvasGroup
struct CanvasGroup_t4083511760;
// UnityEngine.GameObject
struct GameObject_t1113636619;
// UnityEngine.MeshCollider
struct MeshCollider_t903564387;
// UnityEngine.MeshFilter
struct MeshFilter_t3523625662;
// UnityEngine.MeshRenderer
struct MeshRenderer_t587009260;
// UnityEngine.RectTransform
struct RectTransform_t3704657025;
// UnityEngine.Renderer
struct Renderer_t2627027031;
// UnityEngine.Rigidbody
struct Rigidbody_t3916780224;
// UnityEngine.Rigidbody2D
struct Rigidbody2D_t939494601;
// UnityEngine.Sprite
struct Sprite_t280657092;
// UnityEngine.SpriteRenderer
struct SpriteRenderer_t3235626157;
// UnityEngine.Transform
struct Transform_t3600365921;
// UnityEngine.UI.Graphic
struct Graphic_t1660335611;
// UnityEngine.UI.GridLayoutGroup
struct GridLayoutGroup_t3046220461;
// UnityEngine.UI.Image
struct Image_t2670269651;
// UnityEngine.UI.LayoutElement
struct LayoutElement_t1785403678;
// UnityEngine.UI.Outline
struct Outline_t2536100125;
// UnityEngine.UI.ScrollRect
struct ScrollRect_t4137855814;
// UnityEngine.UI.Slider
struct Slider_t3903728902;
// UnityEngine.UI.Text
struct Text_t1901882714;




#ifndef U3CMODULEU3E_T692745555_H
#define U3CMODULEU3E_T692745555_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t692745555 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T692745555_H
#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef DOTWEENMODULEAUDIO_T3475130407_H
#define DOTWEENMODULEAUDIO_T3475130407_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.DOTweenModuleAudio
struct  DOTweenModuleAudio_t3475130407  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DOTWEENMODULEAUDIO_T3475130407_H
#ifndef U3CDOFADEU3EC__ANONSTOREY0_T2614714771_H
#define U3CDOFADEU3EC__ANONSTOREY0_T2614714771_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.DOTweenModuleAudio/<DOFade>c__AnonStorey0
struct  U3CDOFadeU3Ec__AnonStorey0_t2614714771  : public RuntimeObject
{
public:
	// UnityEngine.AudioSource DG.Tweening.DOTweenModuleAudio/<DOFade>c__AnonStorey0::target
	AudioSource_t3935305588 * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CDOFadeU3Ec__AnonStorey0_t2614714771, ___target_0)); }
	inline AudioSource_t3935305588 * get_target_0() const { return ___target_0; }
	inline AudioSource_t3935305588 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(AudioSource_t3935305588 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((&___target_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CDOFADEU3EC__ANONSTOREY0_T2614714771_H
#ifndef U3CDOPITCHU3EC__ANONSTOREY1_T3536537354_H
#define U3CDOPITCHU3EC__ANONSTOREY1_T3536537354_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.DOTweenModuleAudio/<DOPitch>c__AnonStorey1
struct  U3CDOPitchU3Ec__AnonStorey1_t3536537354  : public RuntimeObject
{
public:
	// UnityEngine.AudioSource DG.Tweening.DOTweenModuleAudio/<DOPitch>c__AnonStorey1::target
	AudioSource_t3935305588 * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CDOPitchU3Ec__AnonStorey1_t3536537354, ___target_0)); }
	inline AudioSource_t3935305588 * get_target_0() const { return ___target_0; }
	inline AudioSource_t3935305588 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(AudioSource_t3935305588 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((&___target_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CDOPITCHU3EC__ANONSTOREY1_T3536537354_H
#ifndef U3CDOSETFLOATU3EC__ANONSTOREY2_T1929304968_H
#define U3CDOSETFLOATU3EC__ANONSTOREY2_T1929304968_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.DOTweenModuleAudio/<DOSetFloat>c__AnonStorey2
struct  U3CDOSetFloatU3Ec__AnonStorey2_t1929304968  : public RuntimeObject
{
public:
	// UnityEngine.Audio.AudioMixer DG.Tweening.DOTweenModuleAudio/<DOSetFloat>c__AnonStorey2::target
	AudioMixer_t3521020193 * ___target_0;
	// System.String DG.Tweening.DOTweenModuleAudio/<DOSetFloat>c__AnonStorey2::floatName
	String_t* ___floatName_1;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CDOSetFloatU3Ec__AnonStorey2_t1929304968, ___target_0)); }
	inline AudioMixer_t3521020193 * get_target_0() const { return ___target_0; }
	inline AudioMixer_t3521020193 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(AudioMixer_t3521020193 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((&___target_0), value);
	}

	inline static int32_t get_offset_of_floatName_1() { return static_cast<int32_t>(offsetof(U3CDOSetFloatU3Ec__AnonStorey2_t1929304968, ___floatName_1)); }
	inline String_t* get_floatName_1() const { return ___floatName_1; }
	inline String_t** get_address_of_floatName_1() { return &___floatName_1; }
	inline void set_floatName_1(String_t* value)
	{
		___floatName_1 = value;
		Il2CppCodeGenWriteBarrier((&___floatName_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CDOSETFLOATU3EC__ANONSTOREY2_T1929304968_H
#ifndef DOTWEENMODULEPHYSICS_T1584043136_H
#define DOTWEENMODULEPHYSICS_T1584043136_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.DOTweenModulePhysics
struct  DOTweenModulePhysics_t1584043136  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DOTWEENMODULEPHYSICS_T1584043136_H
#ifndef U3CDOLOCALPATHU3EC__ANONSTOREY8_T3330346749_H
#define U3CDOLOCALPATHU3EC__ANONSTOREY8_T3330346749_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.DOTweenModulePhysics/<DOLocalPath>c__AnonStorey8
struct  U3CDOLocalPathU3Ec__AnonStorey8_t3330346749  : public RuntimeObject
{
public:
	// UnityEngine.Transform DG.Tweening.DOTweenModulePhysics/<DOLocalPath>c__AnonStorey8::trans
	Transform_t3600365921 * ___trans_0;
	// UnityEngine.Rigidbody DG.Tweening.DOTweenModulePhysics/<DOLocalPath>c__AnonStorey8::target
	Rigidbody_t3916780224 * ___target_1;

public:
	inline static int32_t get_offset_of_trans_0() { return static_cast<int32_t>(offsetof(U3CDOLocalPathU3Ec__AnonStorey8_t3330346749, ___trans_0)); }
	inline Transform_t3600365921 * get_trans_0() const { return ___trans_0; }
	inline Transform_t3600365921 ** get_address_of_trans_0() { return &___trans_0; }
	inline void set_trans_0(Transform_t3600365921 * value)
	{
		___trans_0 = value;
		Il2CppCodeGenWriteBarrier((&___trans_0), value);
	}

	inline static int32_t get_offset_of_target_1() { return static_cast<int32_t>(offsetof(U3CDOLocalPathU3Ec__AnonStorey8_t3330346749, ___target_1)); }
	inline Rigidbody_t3916780224 * get_target_1() const { return ___target_1; }
	inline Rigidbody_t3916780224 ** get_address_of_target_1() { return &___target_1; }
	inline void set_target_1(Rigidbody_t3916780224 * value)
	{
		___target_1 = value;
		Il2CppCodeGenWriteBarrier((&___target_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CDOLOCALPATHU3EC__ANONSTOREY8_T3330346749_H
#ifndef U3CDOLOCALPATHU3EC__ANONSTOREYA_T3757313789_H
#define U3CDOLOCALPATHU3EC__ANONSTOREYA_T3757313789_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.DOTweenModulePhysics/<DOLocalPath>c__AnonStoreyA
struct  U3CDOLocalPathU3Ec__AnonStoreyA_t3757313789  : public RuntimeObject
{
public:
	// UnityEngine.Transform DG.Tweening.DOTweenModulePhysics/<DOLocalPath>c__AnonStoreyA::trans
	Transform_t3600365921 * ___trans_0;
	// UnityEngine.Rigidbody DG.Tweening.DOTweenModulePhysics/<DOLocalPath>c__AnonStoreyA::target
	Rigidbody_t3916780224 * ___target_1;

public:
	inline static int32_t get_offset_of_trans_0() { return static_cast<int32_t>(offsetof(U3CDOLocalPathU3Ec__AnonStoreyA_t3757313789, ___trans_0)); }
	inline Transform_t3600365921 * get_trans_0() const { return ___trans_0; }
	inline Transform_t3600365921 ** get_address_of_trans_0() { return &___trans_0; }
	inline void set_trans_0(Transform_t3600365921 * value)
	{
		___trans_0 = value;
		Il2CppCodeGenWriteBarrier((&___trans_0), value);
	}

	inline static int32_t get_offset_of_target_1() { return static_cast<int32_t>(offsetof(U3CDOLocalPathU3Ec__AnonStoreyA_t3757313789, ___target_1)); }
	inline Rigidbody_t3916780224 * get_target_1() const { return ___target_1; }
	inline Rigidbody_t3916780224 ** get_address_of_target_1() { return &___target_1; }
	inline void set_target_1(Rigidbody_t3916780224 * value)
	{
		___target_1 = value;
		Il2CppCodeGenWriteBarrier((&___target_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CDOLOCALPATHU3EC__ANONSTOREYA_T3757313789_H
#ifndef U3CDOLOOKATU3EC__ANONSTOREY5_T2052975667_H
#define U3CDOLOOKATU3EC__ANONSTOREY5_T2052975667_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.DOTweenModulePhysics/<DOLookAt>c__AnonStorey5
struct  U3CDOLookAtU3Ec__AnonStorey5_t2052975667  : public RuntimeObject
{
public:
	// UnityEngine.Rigidbody DG.Tweening.DOTweenModulePhysics/<DOLookAt>c__AnonStorey5::target
	Rigidbody_t3916780224 * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CDOLookAtU3Ec__AnonStorey5_t2052975667, ___target_0)); }
	inline Rigidbody_t3916780224 * get_target_0() const { return ___target_0; }
	inline Rigidbody_t3916780224 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(Rigidbody_t3916780224 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((&___target_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CDOLOOKATU3EC__ANONSTOREY5_T2052975667_H
#ifndef U3CDOMOVEU3EC__ANONSTOREY0_T4100116351_H
#define U3CDOMOVEU3EC__ANONSTOREY0_T4100116351_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.DOTweenModulePhysics/<DOMove>c__AnonStorey0
struct  U3CDOMoveU3Ec__AnonStorey0_t4100116351  : public RuntimeObject
{
public:
	// UnityEngine.Rigidbody DG.Tweening.DOTweenModulePhysics/<DOMove>c__AnonStorey0::target
	Rigidbody_t3916780224 * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CDOMoveU3Ec__AnonStorey0_t4100116351, ___target_0)); }
	inline Rigidbody_t3916780224 * get_target_0() const { return ___target_0; }
	inline Rigidbody_t3916780224 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(Rigidbody_t3916780224 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((&___target_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CDOMOVEU3EC__ANONSTOREY0_T4100116351_H
#ifndef U3CDOMOVEXU3EC__ANONSTOREY1_T4110287298_H
#define U3CDOMOVEXU3EC__ANONSTOREY1_T4110287298_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.DOTweenModulePhysics/<DOMoveX>c__AnonStorey1
struct  U3CDOMoveXU3Ec__AnonStorey1_t4110287298  : public RuntimeObject
{
public:
	// UnityEngine.Rigidbody DG.Tweening.DOTweenModulePhysics/<DOMoveX>c__AnonStorey1::target
	Rigidbody_t3916780224 * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CDOMoveXU3Ec__AnonStorey1_t4110287298, ___target_0)); }
	inline Rigidbody_t3916780224 * get_target_0() const { return ___target_0; }
	inline Rigidbody_t3916780224 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(Rigidbody_t3916780224 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((&___target_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CDOMOVEXU3EC__ANONSTOREY1_T4110287298_H
#ifndef U3CDOMOVEYU3EC__ANONSTOREY2_T1388526791_H
#define U3CDOMOVEYU3EC__ANONSTOREY2_T1388526791_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.DOTweenModulePhysics/<DOMoveY>c__AnonStorey2
struct  U3CDOMoveYU3Ec__AnonStorey2_t1388526791  : public RuntimeObject
{
public:
	// UnityEngine.Rigidbody DG.Tweening.DOTweenModulePhysics/<DOMoveY>c__AnonStorey2::target
	Rigidbody_t3916780224 * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CDOMoveYU3Ec__AnonStorey2_t1388526791, ___target_0)); }
	inline Rigidbody_t3916780224 * get_target_0() const { return ___target_0; }
	inline Rigidbody_t3916780224 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(Rigidbody_t3916780224 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((&___target_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CDOMOVEYU3EC__ANONSTOREY2_T1388526791_H
#ifndef U3CDOMOVEZU3EC__ANONSTOREY3_T3727291876_H
#define U3CDOMOVEZU3EC__ANONSTOREY3_T3727291876_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.DOTweenModulePhysics/<DOMoveZ>c__AnonStorey3
struct  U3CDOMoveZU3Ec__AnonStorey3_t3727291876  : public RuntimeObject
{
public:
	// UnityEngine.Rigidbody DG.Tweening.DOTweenModulePhysics/<DOMoveZ>c__AnonStorey3::target
	Rigidbody_t3916780224 * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CDOMoveZU3Ec__AnonStorey3_t3727291876, ___target_0)); }
	inline Rigidbody_t3916780224 * get_target_0() const { return ___target_0; }
	inline Rigidbody_t3916780224 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(Rigidbody_t3916780224 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((&___target_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CDOMOVEZU3EC__ANONSTOREY3_T3727291876_H
#ifndef U3CDOPATHU3EC__ANONSTOREY7_T510934631_H
#define U3CDOPATHU3EC__ANONSTOREY7_T510934631_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.DOTweenModulePhysics/<DOPath>c__AnonStorey7
struct  U3CDOPathU3Ec__AnonStorey7_t510934631  : public RuntimeObject
{
public:
	// UnityEngine.Rigidbody DG.Tweening.DOTweenModulePhysics/<DOPath>c__AnonStorey7::target
	Rigidbody_t3916780224 * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CDOPathU3Ec__AnonStorey7_t510934631, ___target_0)); }
	inline Rigidbody_t3916780224 * get_target_0() const { return ___target_0; }
	inline Rigidbody_t3916780224 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(Rigidbody_t3916780224 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((&___target_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CDOPATHU3EC__ANONSTOREY7_T510934631_H
#ifndef U3CDOPATHU3EC__ANONSTOREY9_T3643102513_H
#define U3CDOPATHU3EC__ANONSTOREY9_T3643102513_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.DOTweenModulePhysics/<DOPath>c__AnonStorey9
struct  U3CDOPathU3Ec__AnonStorey9_t3643102513  : public RuntimeObject
{
public:
	// UnityEngine.Rigidbody DG.Tweening.DOTweenModulePhysics/<DOPath>c__AnonStorey9::target
	Rigidbody_t3916780224 * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CDOPathU3Ec__AnonStorey9_t3643102513, ___target_0)); }
	inline Rigidbody_t3916780224 * get_target_0() const { return ___target_0; }
	inline Rigidbody_t3916780224 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(Rigidbody_t3916780224 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((&___target_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CDOPATHU3EC__ANONSTOREY9_T3643102513_H
#ifndef U3CDOROTATEU3EC__ANONSTOREY4_T2104260424_H
#define U3CDOROTATEU3EC__ANONSTOREY4_T2104260424_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.DOTweenModulePhysics/<DORotate>c__AnonStorey4
struct  U3CDORotateU3Ec__AnonStorey4_t2104260424  : public RuntimeObject
{
public:
	// UnityEngine.Rigidbody DG.Tweening.DOTweenModulePhysics/<DORotate>c__AnonStorey4::target
	Rigidbody_t3916780224 * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CDORotateU3Ec__AnonStorey4_t2104260424, ___target_0)); }
	inline Rigidbody_t3916780224 * get_target_0() const { return ___target_0; }
	inline Rigidbody_t3916780224 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(Rigidbody_t3916780224 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((&___target_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CDOROTATEU3EC__ANONSTOREY4_T2104260424_H
#ifndef DOTWEENMODULEPHYSICS2D_T3356628190_H
#define DOTWEENMODULEPHYSICS2D_T3356628190_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.DOTweenModulePhysics2D
struct  DOTweenModulePhysics2D_t3356628190  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DOTWEENMODULEPHYSICS2D_T3356628190_H
#ifndef U3CDOMOVEU3EC__ANONSTOREY0_T2044744932_H
#define U3CDOMOVEU3EC__ANONSTOREY0_T2044744932_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.DOTweenModulePhysics2D/<DOMove>c__AnonStorey0
struct  U3CDOMoveU3Ec__AnonStorey0_t2044744932  : public RuntimeObject
{
public:
	// UnityEngine.Rigidbody2D DG.Tweening.DOTweenModulePhysics2D/<DOMove>c__AnonStorey0::target
	Rigidbody2D_t939494601 * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CDOMoveU3Ec__AnonStorey0_t2044744932, ___target_0)); }
	inline Rigidbody2D_t939494601 * get_target_0() const { return ___target_0; }
	inline Rigidbody2D_t939494601 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(Rigidbody2D_t939494601 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((&___target_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CDOMOVEU3EC__ANONSTOREY0_T2044744932_H
#ifndef U3CDOMOVEXU3EC__ANONSTOREY1_T79866776_H
#define U3CDOMOVEXU3EC__ANONSTOREY1_T79866776_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.DOTweenModulePhysics2D/<DOMoveX>c__AnonStorey1
struct  U3CDOMoveXU3Ec__AnonStorey1_t79866776  : public RuntimeObject
{
public:
	// UnityEngine.Rigidbody2D DG.Tweening.DOTweenModulePhysics2D/<DOMoveX>c__AnonStorey1::target
	Rigidbody2D_t939494601 * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CDOMoveXU3Ec__AnonStorey1_t79866776, ___target_0)); }
	inline Rigidbody2D_t939494601 * get_target_0() const { return ___target_0; }
	inline Rigidbody2D_t939494601 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(Rigidbody2D_t939494601 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((&___target_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CDOMOVEXU3EC__ANONSTOREY1_T79866776_H
#ifndef U3CDOMOVEYU3EC__ANONSTOREY2_T1894716487_H
#define U3CDOMOVEYU3EC__ANONSTOREY2_T1894716487_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.DOTweenModulePhysics2D/<DOMoveY>c__AnonStorey2
struct  U3CDOMoveYU3Ec__AnonStorey2_t1894716487  : public RuntimeObject
{
public:
	// UnityEngine.Rigidbody2D DG.Tweening.DOTweenModulePhysics2D/<DOMoveY>c__AnonStorey2::target
	Rigidbody2D_t939494601 * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CDOMoveYU3Ec__AnonStorey2_t1894716487, ___target_0)); }
	inline Rigidbody2D_t939494601 * get_target_0() const { return ___target_0; }
	inline Rigidbody2D_t939494601 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(Rigidbody2D_t939494601 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((&___target_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CDOMOVEYU3EC__ANONSTOREY2_T1894716487_H
#ifndef U3CDOROTATEU3EC__ANONSTOREY3_T307238404_H
#define U3CDOROTATEU3EC__ANONSTOREY3_T307238404_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.DOTweenModulePhysics2D/<DORotate>c__AnonStorey3
struct  U3CDORotateU3Ec__AnonStorey3_t307238404  : public RuntimeObject
{
public:
	// UnityEngine.Rigidbody2D DG.Tweening.DOTweenModulePhysics2D/<DORotate>c__AnonStorey3::target
	Rigidbody2D_t939494601 * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CDORotateU3Ec__AnonStorey3_t307238404, ___target_0)); }
	inline Rigidbody2D_t939494601 * get_target_0() const { return ___target_0; }
	inline Rigidbody2D_t939494601 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(Rigidbody2D_t939494601 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((&___target_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CDOROTATEU3EC__ANONSTOREY3_T307238404_H
#ifndef DOTWEENMODULESPRITE_T688046044_H
#define DOTWEENMODULESPRITE_T688046044_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.DOTweenModuleSprite
struct  DOTweenModuleSprite_t688046044  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DOTWEENMODULESPRITE_T688046044_H
#ifndef U3CDOCOLORU3EC__ANONSTOREY0_T2678535312_H
#define U3CDOCOLORU3EC__ANONSTOREY0_T2678535312_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.DOTweenModuleSprite/<DOColor>c__AnonStorey0
struct  U3CDOColorU3Ec__AnonStorey0_t2678535312  : public RuntimeObject
{
public:
	// UnityEngine.SpriteRenderer DG.Tweening.DOTweenModuleSprite/<DOColor>c__AnonStorey0::target
	SpriteRenderer_t3235626157 * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CDOColorU3Ec__AnonStorey0_t2678535312, ___target_0)); }
	inline SpriteRenderer_t3235626157 * get_target_0() const { return ___target_0; }
	inline SpriteRenderer_t3235626157 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(SpriteRenderer_t3235626157 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((&___target_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CDOCOLORU3EC__ANONSTOREY0_T2678535312_H
#ifndef U3CDOFADEU3EC__ANONSTOREY1_T108519314_H
#define U3CDOFADEU3EC__ANONSTOREY1_T108519314_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.DOTweenModuleSprite/<DOFade>c__AnonStorey1
struct  U3CDOFadeU3Ec__AnonStorey1_t108519314  : public RuntimeObject
{
public:
	// UnityEngine.SpriteRenderer DG.Tweening.DOTweenModuleSprite/<DOFade>c__AnonStorey1::target
	SpriteRenderer_t3235626157 * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CDOFadeU3Ec__AnonStorey1_t108519314, ___target_0)); }
	inline SpriteRenderer_t3235626157 * get_target_0() const { return ___target_0; }
	inline SpriteRenderer_t3235626157 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(SpriteRenderer_t3235626157 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((&___target_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CDOFADEU3EC__ANONSTOREY1_T108519314_H
#ifndef DOTWEENMODULEUI_T3477205437_H
#define DOTWEENMODULEUI_T3477205437_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.DOTweenModuleUI
struct  DOTweenModuleUI_t3477205437  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DOTWEENMODULEUI_T3477205437_H
#ifndef U3CDOANCHORMAXU3EC__ANONSTOREY13_T3164487871_H
#define U3CDOANCHORMAXU3EC__ANONSTOREY13_T3164487871_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.DOTweenModuleUI/<DOAnchorMax>c__AnonStorey13
struct  U3CDOAnchorMaxU3Ec__AnonStorey13_t3164487871  : public RuntimeObject
{
public:
	// UnityEngine.RectTransform DG.Tweening.DOTweenModuleUI/<DOAnchorMax>c__AnonStorey13::target
	RectTransform_t3704657025 * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CDOAnchorMaxU3Ec__AnonStorey13_t3164487871, ___target_0)); }
	inline RectTransform_t3704657025 * get_target_0() const { return ___target_0; }
	inline RectTransform_t3704657025 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(RectTransform_t3704657025 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((&___target_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CDOANCHORMAXU3EC__ANONSTOREY13_T3164487871_H
#ifndef U3CDOANCHORMINU3EC__ANONSTOREY14_T308074497_H
#define U3CDOANCHORMINU3EC__ANONSTOREY14_T308074497_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.DOTweenModuleUI/<DOAnchorMin>c__AnonStorey14
struct  U3CDOAnchorMinU3Ec__AnonStorey14_t308074497  : public RuntimeObject
{
public:
	// UnityEngine.RectTransform DG.Tweening.DOTweenModuleUI/<DOAnchorMin>c__AnonStorey14::target
	RectTransform_t3704657025 * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CDOAnchorMinU3Ec__AnonStorey14_t308074497, ___target_0)); }
	inline RectTransform_t3704657025 * get_target_0() const { return ___target_0; }
	inline RectTransform_t3704657025 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(RectTransform_t3704657025 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((&___target_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CDOANCHORMINU3EC__ANONSTOREY14_T308074497_H
#ifndef U3CDOANCHORPOS3DU3EC__ANONSTOREYF_T916713582_H
#define U3CDOANCHORPOS3DU3EC__ANONSTOREYF_T916713582_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.DOTweenModuleUI/<DOAnchorPos3D>c__AnonStoreyF
struct  U3CDOAnchorPos3DU3Ec__AnonStoreyF_t916713582  : public RuntimeObject
{
public:
	// UnityEngine.RectTransform DG.Tweening.DOTweenModuleUI/<DOAnchorPos3D>c__AnonStoreyF::target
	RectTransform_t3704657025 * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CDOAnchorPos3DU3Ec__AnonStoreyF_t916713582, ___target_0)); }
	inline RectTransform_t3704657025 * get_target_0() const { return ___target_0; }
	inline RectTransform_t3704657025 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(RectTransform_t3704657025 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((&___target_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CDOANCHORPOS3DU3EC__ANONSTOREYF_T916713582_H
#ifndef U3CDOANCHORPOS3DXU3EC__ANONSTOREY10_T692117743_H
#define U3CDOANCHORPOS3DXU3EC__ANONSTOREY10_T692117743_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.DOTweenModuleUI/<DOAnchorPos3DX>c__AnonStorey10
struct  U3CDOAnchorPos3DXU3Ec__AnonStorey10_t692117743  : public RuntimeObject
{
public:
	// UnityEngine.RectTransform DG.Tweening.DOTweenModuleUI/<DOAnchorPos3DX>c__AnonStorey10::target
	RectTransform_t3704657025 * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CDOAnchorPos3DXU3Ec__AnonStorey10_t692117743, ___target_0)); }
	inline RectTransform_t3704657025 * get_target_0() const { return ___target_0; }
	inline RectTransform_t3704657025 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(RectTransform_t3704657025 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((&___target_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CDOANCHORPOS3DXU3EC__ANONSTOREY10_T692117743_H
#ifndef U3CDOANCHORPOS3DYU3EC__ANONSTOREY11_T1739080443_H
#define U3CDOANCHORPOS3DYU3EC__ANONSTOREY11_T1739080443_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.DOTweenModuleUI/<DOAnchorPos3DY>c__AnonStorey11
struct  U3CDOAnchorPos3DYU3Ec__AnonStorey11_t1739080443  : public RuntimeObject
{
public:
	// UnityEngine.RectTransform DG.Tweening.DOTweenModuleUI/<DOAnchorPos3DY>c__AnonStorey11::target
	RectTransform_t3704657025 * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CDOAnchorPos3DYU3Ec__AnonStorey11_t1739080443, ___target_0)); }
	inline RectTransform_t3704657025 * get_target_0() const { return ___target_0; }
	inline RectTransform_t3704657025 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(RectTransform_t3704657025 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((&___target_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CDOANCHORPOS3DYU3EC__ANONSTOREY11_T1739080443_H
#ifndef U3CDOANCHORPOS3DZU3EC__ANONSTOREY12_T1172413799_H
#define U3CDOANCHORPOS3DZU3EC__ANONSTOREY12_T1172413799_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.DOTweenModuleUI/<DOAnchorPos3DZ>c__AnonStorey12
struct  U3CDOAnchorPos3DZU3Ec__AnonStorey12_t1172413799  : public RuntimeObject
{
public:
	// UnityEngine.RectTransform DG.Tweening.DOTweenModuleUI/<DOAnchorPos3DZ>c__AnonStorey12::target
	RectTransform_t3704657025 * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CDOAnchorPos3DZU3Ec__AnonStorey12_t1172413799, ___target_0)); }
	inline RectTransform_t3704657025 * get_target_0() const { return ___target_0; }
	inline RectTransform_t3704657025 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(RectTransform_t3704657025 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((&___target_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CDOANCHORPOS3DZU3EC__ANONSTOREY12_T1172413799_H
#ifndef U3CDOANCHORPOSU3EC__ANONSTOREYC_T3922317631_H
#define U3CDOANCHORPOSU3EC__ANONSTOREYC_T3922317631_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.DOTweenModuleUI/<DOAnchorPos>c__AnonStoreyC
struct  U3CDOAnchorPosU3Ec__AnonStoreyC_t3922317631  : public RuntimeObject
{
public:
	// UnityEngine.RectTransform DG.Tweening.DOTweenModuleUI/<DOAnchorPos>c__AnonStoreyC::target
	RectTransform_t3704657025 * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CDOAnchorPosU3Ec__AnonStoreyC_t3922317631, ___target_0)); }
	inline RectTransform_t3704657025 * get_target_0() const { return ___target_0; }
	inline RectTransform_t3704657025 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(RectTransform_t3704657025 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((&___target_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CDOANCHORPOSU3EC__ANONSTOREYC_T3922317631_H
#ifndef U3CDOANCHORPOSXU3EC__ANONSTOREYD_T1583578289_H
#define U3CDOANCHORPOSXU3EC__ANONSTOREYD_T1583578289_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.DOTweenModuleUI/<DOAnchorPosX>c__AnonStoreyD
struct  U3CDOAnchorPosXU3Ec__AnonStoreyD_t1583578289  : public RuntimeObject
{
public:
	// UnityEngine.RectTransform DG.Tweening.DOTweenModuleUI/<DOAnchorPosX>c__AnonStoreyD::target
	RectTransform_t3704657025 * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CDOAnchorPosXU3Ec__AnonStoreyD_t1583578289, ___target_0)); }
	inline RectTransform_t3704657025 * get_target_0() const { return ___target_0; }
	inline RectTransform_t3704657025 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(RectTransform_t3704657025 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((&___target_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CDOANCHORPOSXU3EC__ANONSTOREYD_T1583578289_H
#ifndef U3CDOANCHORPOSYU3EC__ANONSTOREYE_T3922517944_H
#define U3CDOANCHORPOSYU3EC__ANONSTOREYE_T3922517944_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.DOTweenModuleUI/<DOAnchorPosY>c__AnonStoreyE
struct  U3CDOAnchorPosYU3Ec__AnonStoreyE_t3922517944  : public RuntimeObject
{
public:
	// UnityEngine.RectTransform DG.Tweening.DOTweenModuleUI/<DOAnchorPosY>c__AnonStoreyE::target
	RectTransform_t3704657025 * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CDOAnchorPosYU3Ec__AnonStoreyE_t3922517944, ___target_0)); }
	inline RectTransform_t3704657025 * get_target_0() const { return ___target_0; }
	inline RectTransform_t3704657025 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(RectTransform_t3704657025 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((&___target_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CDOANCHORPOSYU3EC__ANONSTOREYE_T3922517944_H
#ifndef U3CDOCOLORU3EC__ANONSTOREY1_T292238278_H
#define U3CDOCOLORU3EC__ANONSTOREY1_T292238278_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.DOTweenModuleUI/<DOColor>c__AnonStorey1
struct  U3CDOColorU3Ec__AnonStorey1_t292238278  : public RuntimeObject
{
public:
	// UnityEngine.UI.Graphic DG.Tweening.DOTweenModuleUI/<DOColor>c__AnonStorey1::target
	Graphic_t1660335611 * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CDOColorU3Ec__AnonStorey1_t292238278, ___target_0)); }
	inline Graphic_t1660335611 * get_target_0() const { return ___target_0; }
	inline Graphic_t1660335611 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(Graphic_t1660335611 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((&___target_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CDOCOLORU3EC__ANONSTOREY1_T292238278_H
#ifndef U3CDOCOLORU3EC__ANONSTOREY21_T2960703275_H
#define U3CDOCOLORU3EC__ANONSTOREY21_T2960703275_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.DOTweenModuleUI/<DOColor>c__AnonStorey21
struct  U3CDOColorU3Ec__AnonStorey21_t2960703275  : public RuntimeObject
{
public:
	// UnityEngine.UI.Text DG.Tweening.DOTweenModuleUI/<DOColor>c__AnonStorey21::target
	Text_t1901882714 * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CDOColorU3Ec__AnonStorey21_t2960703275, ___target_0)); }
	inline Text_t1901882714 * get_target_0() const { return ___target_0; }
	inline Text_t1901882714 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(Text_t1901882714 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((&___target_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CDOCOLORU3EC__ANONSTOREY21_T2960703275_H
#ifndef U3CDOCOLORU3EC__ANONSTOREY3_T3424406160_H
#define U3CDOCOLORU3EC__ANONSTOREY3_T3424406160_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.DOTweenModuleUI/<DOColor>c__AnonStorey3
struct  U3CDOColorU3Ec__AnonStorey3_t3424406160  : public RuntimeObject
{
public:
	// UnityEngine.UI.Image DG.Tweening.DOTweenModuleUI/<DOColor>c__AnonStorey3::target
	Image_t2670269651 * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CDOColorU3Ec__AnonStorey3_t3424406160, ___target_0)); }
	inline Image_t2670269651 * get_target_0() const { return ___target_0; }
	inline Image_t2670269651 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(Image_t2670269651 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((&___target_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CDOCOLORU3EC__ANONSTOREY3_T3424406160_H
#ifndef U3CDOCOLORU3EC__ANONSTOREY9_T648468638_H
#define U3CDOCOLORU3EC__ANONSTOREY9_T648468638_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.DOTweenModuleUI/<DOColor>c__AnonStorey9
struct  U3CDOColorU3Ec__AnonStorey9_t648468638  : public RuntimeObject
{
public:
	// UnityEngine.UI.Outline DG.Tweening.DOTweenModuleUI/<DOColor>c__AnonStorey9::target
	Outline_t2536100125 * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CDOColorU3Ec__AnonStorey9_t648468638, ___target_0)); }
	inline Outline_t2536100125 * get_target_0() const { return ___target_0; }
	inline Outline_t2536100125 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(Outline_t2536100125 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((&___target_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CDOCOLORU3EC__ANONSTOREY9_T648468638_H
#ifndef U3CDOFADEU3EC__ANONSTOREY0_T3571959874_H
#define U3CDOFADEU3EC__ANONSTOREY0_T3571959874_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.DOTweenModuleUI/<DOFade>c__AnonStorey0
struct  U3CDOFadeU3Ec__AnonStorey0_t3571959874  : public RuntimeObject
{
public:
	// UnityEngine.CanvasGroup DG.Tweening.DOTweenModuleUI/<DOFade>c__AnonStorey0::target
	CanvasGroup_t4083511760 * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CDOFadeU3Ec__AnonStorey0_t3571959874, ___target_0)); }
	inline CanvasGroup_t4083511760 * get_target_0() const { return ___target_0; }
	inline CanvasGroup_t4083511760 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(CanvasGroup_t4083511760 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((&___target_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CDOFADEU3EC__ANONSTOREY0_T3571959874_H
#ifndef U3CDOFADEU3EC__ANONSTOREY2_T3572090946_H
#define U3CDOFADEU3EC__ANONSTOREY2_T3572090946_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.DOTweenModuleUI/<DOFade>c__AnonStorey2
struct  U3CDOFadeU3Ec__AnonStorey2_t3572090946  : public RuntimeObject
{
public:
	// UnityEngine.UI.Graphic DG.Tweening.DOTweenModuleUI/<DOFade>c__AnonStorey2::target
	Graphic_t1660335611 * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CDOFadeU3Ec__AnonStorey2_t3572090946, ___target_0)); }
	inline Graphic_t1660335611 * get_target_0() const { return ___target_0; }
	inline Graphic_t1660335611 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(Graphic_t1660335611 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((&___target_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CDOFADEU3EC__ANONSTOREY2_T3572090946_H
#ifndef U3CDOFADEU3EC__ANONSTOREY22_T4148204238_H
#define U3CDOFADEU3EC__ANONSTOREY22_T4148204238_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.DOTweenModuleUI/<DOFade>c__AnonStorey22
struct  U3CDOFadeU3Ec__AnonStorey22_t4148204238  : public RuntimeObject
{
public:
	// UnityEngine.UI.Text DG.Tweening.DOTweenModuleUI/<DOFade>c__AnonStorey22::target
	Text_t1901882714 * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CDOFadeU3Ec__AnonStorey22_t4148204238, ___target_0)); }
	inline Text_t1901882714 * get_target_0() const { return ___target_0; }
	inline Text_t1901882714 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(Text_t1901882714 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((&___target_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CDOFADEU3EC__ANONSTOREY22_T4148204238_H
#ifndef U3CDOFADEU3EC__ANONSTOREY4_T3571697730_H
#define U3CDOFADEU3EC__ANONSTOREY4_T3571697730_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.DOTweenModuleUI/<DOFade>c__AnonStorey4
struct  U3CDOFadeU3Ec__AnonStorey4_t3571697730  : public RuntimeObject
{
public:
	// UnityEngine.UI.Image DG.Tweening.DOTweenModuleUI/<DOFade>c__AnonStorey4::target
	Image_t2670269651 * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CDOFadeU3Ec__AnonStorey4_t3571697730, ___target_0)); }
	inline Image_t2670269651 * get_target_0() const { return ___target_0; }
	inline Image_t2670269651 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(Image_t2670269651 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((&___target_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CDOFADEU3EC__ANONSTOREY4_T3571697730_H
#ifndef U3CDOFADEU3EC__ANONSTOREYA_T3566651458_H
#define U3CDOFADEU3EC__ANONSTOREYA_T3566651458_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.DOTweenModuleUI/<DOFade>c__AnonStoreyA
struct  U3CDOFadeU3Ec__AnonStoreyA_t3566651458  : public RuntimeObject
{
public:
	// UnityEngine.UI.Outline DG.Tweening.DOTweenModuleUI/<DOFade>c__AnonStoreyA::target
	Outline_t2536100125 * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CDOFadeU3Ec__AnonStoreyA_t3566651458, ___target_0)); }
	inline Outline_t2536100125 * get_target_0() const { return ___target_0; }
	inline Outline_t2536100125 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(Outline_t2536100125 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((&___target_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CDOFADEU3EC__ANONSTOREYA_T3566651458_H
#ifndef U3CDOFILLAMOUNTU3EC__ANONSTOREY5_T4285147863_H
#define U3CDOFILLAMOUNTU3EC__ANONSTOREY5_T4285147863_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.DOTweenModuleUI/<DOFillAmount>c__AnonStorey5
struct  U3CDOFillAmountU3Ec__AnonStorey5_t4285147863  : public RuntimeObject
{
public:
	// UnityEngine.UI.Image DG.Tweening.DOTweenModuleUI/<DOFillAmount>c__AnonStorey5::target
	Image_t2670269651 * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CDOFillAmountU3Ec__AnonStorey5_t4285147863, ___target_0)); }
	inline Image_t2670269651 * get_target_0() const { return ___target_0; }
	inline Image_t2670269651 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(Image_t2670269651 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((&___target_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CDOFILLAMOUNTU3EC__ANONSTOREY5_T4285147863_H
#ifndef U3CDOFLEXIBLESIZEU3EC__ANONSTOREY6_T3266349678_H
#define U3CDOFLEXIBLESIZEU3EC__ANONSTOREY6_T3266349678_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.DOTweenModuleUI/<DOFlexibleSize>c__AnonStorey6
struct  U3CDOFlexibleSizeU3Ec__AnonStorey6_t3266349678  : public RuntimeObject
{
public:
	// UnityEngine.UI.LayoutElement DG.Tweening.DOTweenModuleUI/<DOFlexibleSize>c__AnonStorey6::target
	LayoutElement_t1785403678 * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CDOFlexibleSizeU3Ec__AnonStorey6_t3266349678, ___target_0)); }
	inline LayoutElement_t1785403678 * get_target_0() const { return ___target_0; }
	inline LayoutElement_t1785403678 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(LayoutElement_t1785403678 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((&___target_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CDOFLEXIBLESIZEU3EC__ANONSTOREY6_T3266349678_H
#ifndef U3CDOHORIZONTALNORMALIZEDPOSU3EC__ANONSTOREY1E_T1691945541_H
#define U3CDOHORIZONTALNORMALIZEDPOSU3EC__ANONSTOREY1E_T1691945541_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.DOTweenModuleUI/<DOHorizontalNormalizedPos>c__AnonStorey1E
struct  U3CDOHorizontalNormalizedPosU3Ec__AnonStorey1E_t1691945541  : public RuntimeObject
{
public:
	// UnityEngine.UI.ScrollRect DG.Tweening.DOTweenModuleUI/<DOHorizontalNormalizedPos>c__AnonStorey1E::target
	ScrollRect_t4137855814 * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CDOHorizontalNormalizedPosU3Ec__AnonStorey1E_t1691945541, ___target_0)); }
	inline ScrollRect_t4137855814 * get_target_0() const { return ___target_0; }
	inline ScrollRect_t4137855814 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(ScrollRect_t4137855814 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((&___target_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CDOHORIZONTALNORMALIZEDPOSU3EC__ANONSTOREY1E_T1691945541_H
#ifndef U3CDOMINSIZEU3EC__ANONSTOREY7_T309622074_H
#define U3CDOMINSIZEU3EC__ANONSTOREY7_T309622074_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.DOTweenModuleUI/<DOMinSize>c__AnonStorey7
struct  U3CDOMinSizeU3Ec__AnonStorey7_t309622074  : public RuntimeObject
{
public:
	// UnityEngine.UI.LayoutElement DG.Tweening.DOTweenModuleUI/<DOMinSize>c__AnonStorey7::target
	LayoutElement_t1785403678 * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CDOMinSizeU3Ec__AnonStorey7_t309622074, ___target_0)); }
	inline LayoutElement_t1785403678 * get_target_0() const { return ___target_0; }
	inline LayoutElement_t1785403678 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(LayoutElement_t1785403678 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((&___target_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CDOMINSIZEU3EC__ANONSTOREY7_T309622074_H
#ifndef U3CDONORMALIZEDPOSU3EC__ANONSTOREY1D_T4252408738_H
#define U3CDONORMALIZEDPOSU3EC__ANONSTOREY1D_T4252408738_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.DOTweenModuleUI/<DONormalizedPos>c__AnonStorey1D
struct  U3CDONormalizedPosU3Ec__AnonStorey1D_t4252408738  : public RuntimeObject
{
public:
	// UnityEngine.UI.ScrollRect DG.Tweening.DOTweenModuleUI/<DONormalizedPos>c__AnonStorey1D::target
	ScrollRect_t4137855814 * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CDONormalizedPosU3Ec__AnonStorey1D_t4252408738, ___target_0)); }
	inline ScrollRect_t4137855814 * get_target_0() const { return ___target_0; }
	inline ScrollRect_t4137855814 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(ScrollRect_t4137855814 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((&___target_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CDONORMALIZEDPOSU3EC__ANONSTOREY1D_T4252408738_H
#ifndef U3CDOPIVOTU3EC__ANONSTOREY15_T2354936221_H
#define U3CDOPIVOTU3EC__ANONSTOREY15_T2354936221_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.DOTweenModuleUI/<DOPivot>c__AnonStorey15
struct  U3CDOPivotU3Ec__AnonStorey15_t2354936221  : public RuntimeObject
{
public:
	// UnityEngine.RectTransform DG.Tweening.DOTweenModuleUI/<DOPivot>c__AnonStorey15::target
	RectTransform_t3704657025 * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CDOPivotU3Ec__AnonStorey15_t2354936221, ___target_0)); }
	inline RectTransform_t3704657025 * get_target_0() const { return ___target_0; }
	inline RectTransform_t3704657025 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(RectTransform_t3704657025 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((&___target_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CDOPIVOTU3EC__ANONSTOREY15_T2354936221_H
#ifndef U3CDOPIVOTXU3EC__ANONSTOREY16_T963667313_H
#define U3CDOPIVOTXU3EC__ANONSTOREY16_T963667313_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.DOTweenModuleUI/<DOPivotX>c__AnonStorey16
struct  U3CDOPivotXU3Ec__AnonStorey16_t963667313  : public RuntimeObject
{
public:
	// UnityEngine.RectTransform DG.Tweening.DOTweenModuleUI/<DOPivotX>c__AnonStorey16::target
	RectTransform_t3704657025 * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CDOPivotXU3Ec__AnonStorey16_t963667313, ___target_0)); }
	inline RectTransform_t3704657025 * get_target_0() const { return ___target_0; }
	inline RectTransform_t3704657025 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(RectTransform_t3704657025 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((&___target_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CDOPIVOTXU3EC__ANONSTOREY16_T963667313_H
#ifndef U3CDOPIVOTYU3EC__ANONSTOREY17_T970927219_H
#define U3CDOPIVOTYU3EC__ANONSTOREY17_T970927219_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.DOTweenModuleUI/<DOPivotY>c__AnonStorey17
struct  U3CDOPivotYU3Ec__AnonStorey17_t970927219  : public RuntimeObject
{
public:
	// UnityEngine.RectTransform DG.Tweening.DOTweenModuleUI/<DOPivotY>c__AnonStorey17::target
	RectTransform_t3704657025 * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CDOPivotYU3Ec__AnonStorey17_t970927219, ___target_0)); }
	inline RectTransform_t3704657025 * get_target_0() const { return ___target_0; }
	inline RectTransform_t3704657025 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(RectTransform_t3704657025 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((&___target_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CDOPIVOTYU3EC__ANONSTOREY17_T970927219_H
#ifndef U3CDOPREFERREDSIZEU3EC__ANONSTOREY8_T4005645670_H
#define U3CDOPREFERREDSIZEU3EC__ANONSTOREY8_T4005645670_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.DOTweenModuleUI/<DOPreferredSize>c__AnonStorey8
struct  U3CDOPreferredSizeU3Ec__AnonStorey8_t4005645670  : public RuntimeObject
{
public:
	// UnityEngine.UI.LayoutElement DG.Tweening.DOTweenModuleUI/<DOPreferredSize>c__AnonStorey8::target
	LayoutElement_t1785403678 * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CDOPreferredSizeU3Ec__AnonStorey8_t4005645670, ___target_0)); }
	inline LayoutElement_t1785403678 * get_target_0() const { return ___target_0; }
	inline LayoutElement_t1785403678 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(LayoutElement_t1785403678 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((&___target_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CDOPREFERREDSIZEU3EC__ANONSTOREY8_T4005645670_H
#ifndef U3CDOPUNCHANCHORPOSU3EC__ANONSTOREY19_T1651748926_H
#define U3CDOPUNCHANCHORPOSU3EC__ANONSTOREY19_T1651748926_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.DOTweenModuleUI/<DOPunchAnchorPos>c__AnonStorey19
struct  U3CDOPunchAnchorPosU3Ec__AnonStorey19_t1651748926  : public RuntimeObject
{
public:
	// UnityEngine.RectTransform DG.Tweening.DOTweenModuleUI/<DOPunchAnchorPos>c__AnonStorey19::target
	RectTransform_t3704657025 * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CDOPunchAnchorPosU3Ec__AnonStorey19_t1651748926, ___target_0)); }
	inline RectTransform_t3704657025 * get_target_0() const { return ___target_0; }
	inline RectTransform_t3704657025 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(RectTransform_t3704657025 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((&___target_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CDOPUNCHANCHORPOSU3EC__ANONSTOREY19_T1651748926_H
#ifndef U3CDOSCALEU3EC__ANONSTOREYB_T4203681660_H
#define U3CDOSCALEU3EC__ANONSTOREYB_T4203681660_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.DOTweenModuleUI/<DOScale>c__AnonStoreyB
struct  U3CDOScaleU3Ec__AnonStoreyB_t4203681660  : public RuntimeObject
{
public:
	// UnityEngine.UI.Outline DG.Tweening.DOTweenModuleUI/<DOScale>c__AnonStoreyB::target
	Outline_t2536100125 * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CDOScaleU3Ec__AnonStoreyB_t4203681660, ___target_0)); }
	inline Outline_t2536100125 * get_target_0() const { return ___target_0; }
	inline Outline_t2536100125 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(Outline_t2536100125 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((&___target_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CDOSCALEU3EC__ANONSTOREYB_T4203681660_H
#ifndef U3CDOSHAKEANCHORPOSU3EC__ANONSTOREY1A_T1681899448_H
#define U3CDOSHAKEANCHORPOSU3EC__ANONSTOREY1A_T1681899448_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.DOTweenModuleUI/<DOShakeAnchorPos>c__AnonStorey1A
struct  U3CDOShakeAnchorPosU3Ec__AnonStorey1A_t1681899448  : public RuntimeObject
{
public:
	// UnityEngine.RectTransform DG.Tweening.DOTweenModuleUI/<DOShakeAnchorPos>c__AnonStorey1A::target
	RectTransform_t3704657025 * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CDOShakeAnchorPosU3Ec__AnonStorey1A_t1681899448, ___target_0)); }
	inline RectTransform_t3704657025 * get_target_0() const { return ___target_0; }
	inline RectTransform_t3704657025 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(RectTransform_t3704657025 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((&___target_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CDOSHAKEANCHORPOSU3EC__ANONSTOREY1A_T1681899448_H
#ifndef U3CDOSHAKEANCHORPOSU3EC__ANONSTOREY1B_T1681899451_H
#define U3CDOSHAKEANCHORPOSU3EC__ANONSTOREY1B_T1681899451_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.DOTweenModuleUI/<DOShakeAnchorPos>c__AnonStorey1B
struct  U3CDOShakeAnchorPosU3Ec__AnonStorey1B_t1681899451  : public RuntimeObject
{
public:
	// UnityEngine.RectTransform DG.Tweening.DOTweenModuleUI/<DOShakeAnchorPos>c__AnonStorey1B::target
	RectTransform_t3704657025 * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CDOShakeAnchorPosU3Ec__AnonStorey1B_t1681899451, ___target_0)); }
	inline RectTransform_t3704657025 * get_target_0() const { return ___target_0; }
	inline RectTransform_t3704657025 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(RectTransform_t3704657025 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((&___target_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CDOSHAKEANCHORPOSU3EC__ANONSTOREY1B_T1681899451_H
#ifndef U3CDOSIZEDELTAU3EC__ANONSTOREY18_T85417030_H
#define U3CDOSIZEDELTAU3EC__ANONSTOREY18_T85417030_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.DOTweenModuleUI/<DOSizeDelta>c__AnonStorey18
struct  U3CDOSizeDeltaU3Ec__AnonStorey18_t85417030  : public RuntimeObject
{
public:
	// UnityEngine.RectTransform DG.Tweening.DOTweenModuleUI/<DOSizeDelta>c__AnonStorey18::target
	RectTransform_t3704657025 * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CDOSizeDeltaU3Ec__AnonStorey18_t85417030, ___target_0)); }
	inline RectTransform_t3704657025 * get_target_0() const { return ___target_0; }
	inline RectTransform_t3704657025 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(RectTransform_t3704657025 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((&___target_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CDOSIZEDELTAU3EC__ANONSTOREY18_T85417030_H
#ifndef U3CDOTEXTU3EC__ANONSTOREY23_T630041313_H
#define U3CDOTEXTU3EC__ANONSTOREY23_T630041313_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.DOTweenModuleUI/<DOText>c__AnonStorey23
struct  U3CDOTextU3Ec__AnonStorey23_t630041313  : public RuntimeObject
{
public:
	// UnityEngine.UI.Text DG.Tweening.DOTweenModuleUI/<DOText>c__AnonStorey23::target
	Text_t1901882714 * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CDOTextU3Ec__AnonStorey23_t630041313, ___target_0)); }
	inline Text_t1901882714 * get_target_0() const { return ___target_0; }
	inline Text_t1901882714 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(Text_t1901882714 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((&___target_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CDOTEXTU3EC__ANONSTOREY23_T630041313_H
#ifndef U3CDOVALUEU3EC__ANONSTOREY20_T1335203578_H
#define U3CDOVALUEU3EC__ANONSTOREY20_T1335203578_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.DOTweenModuleUI/<DOValue>c__AnonStorey20
struct  U3CDOValueU3Ec__AnonStorey20_t1335203578  : public RuntimeObject
{
public:
	// UnityEngine.UI.Slider DG.Tweening.DOTweenModuleUI/<DOValue>c__AnonStorey20::target
	Slider_t3903728902 * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CDOValueU3Ec__AnonStorey20_t1335203578, ___target_0)); }
	inline Slider_t3903728902 * get_target_0() const { return ___target_0; }
	inline Slider_t3903728902 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(Slider_t3903728902 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((&___target_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CDOVALUEU3EC__ANONSTOREY20_T1335203578_H
#ifndef U3CDOVERTICALNORMALIZEDPOSU3EC__ANONSTOREY1F_T3179021983_H
#define U3CDOVERTICALNORMALIZEDPOSU3EC__ANONSTOREY1F_T3179021983_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.DOTweenModuleUI/<DOVerticalNormalizedPos>c__AnonStorey1F
struct  U3CDOVerticalNormalizedPosU3Ec__AnonStorey1F_t3179021983  : public RuntimeObject
{
public:
	// UnityEngine.UI.ScrollRect DG.Tweening.DOTweenModuleUI/<DOVerticalNormalizedPos>c__AnonStorey1F::target
	ScrollRect_t4137855814 * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CDOVerticalNormalizedPosU3Ec__AnonStorey1F_t3179021983, ___target_0)); }
	inline ScrollRect_t4137855814 * get_target_0() const { return ___target_0; }
	inline ScrollRect_t4137855814 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(ScrollRect_t4137855814 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((&___target_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CDOVERTICALNORMALIZEDPOSU3EC__ANONSTOREY1F_T3179021983_H
#ifndef UTILS_T544397632_H
#define UTILS_T544397632_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.DOTweenModuleUI/Utils
struct  Utils_t544397632  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UTILS_T544397632_H
#ifndef SOUND_T3007421746_H
#define SOUND_T3007421746_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sound
struct  Sound_t3007421746  : public RuntimeObject
{
public:
	// System.String Sound::name
	String_t* ___name_0;
	// UnityEngine.AudioClip Sound::clip
	AudioClip_t3680889665 * ___clip_1;
	// System.Single Sound::volume
	float ___volume_2;
	// System.Single Sound::volumeVariance
	float ___volumeVariance_3;
	// System.Single Sound::pitch
	float ___pitch_4;
	// System.Single Sound::pitchVariance
	float ___pitchVariance_5;
	// System.Boolean Sound::loop
	bool ___loop_6;
	// UnityEngine.Audio.AudioMixerGroup Sound::mixerGroup
	AudioMixerGroup_t2743564464 * ___mixerGroup_7;
	// UnityEngine.AudioSource Sound::source
	AudioSource_t3935305588 * ___source_8;

public:
	inline static int32_t get_offset_of_name_0() { return static_cast<int32_t>(offsetof(Sound_t3007421746, ___name_0)); }
	inline String_t* get_name_0() const { return ___name_0; }
	inline String_t** get_address_of_name_0() { return &___name_0; }
	inline void set_name_0(String_t* value)
	{
		___name_0 = value;
		Il2CppCodeGenWriteBarrier((&___name_0), value);
	}

	inline static int32_t get_offset_of_clip_1() { return static_cast<int32_t>(offsetof(Sound_t3007421746, ___clip_1)); }
	inline AudioClip_t3680889665 * get_clip_1() const { return ___clip_1; }
	inline AudioClip_t3680889665 ** get_address_of_clip_1() { return &___clip_1; }
	inline void set_clip_1(AudioClip_t3680889665 * value)
	{
		___clip_1 = value;
		Il2CppCodeGenWriteBarrier((&___clip_1), value);
	}

	inline static int32_t get_offset_of_volume_2() { return static_cast<int32_t>(offsetof(Sound_t3007421746, ___volume_2)); }
	inline float get_volume_2() const { return ___volume_2; }
	inline float* get_address_of_volume_2() { return &___volume_2; }
	inline void set_volume_2(float value)
	{
		___volume_2 = value;
	}

	inline static int32_t get_offset_of_volumeVariance_3() { return static_cast<int32_t>(offsetof(Sound_t3007421746, ___volumeVariance_3)); }
	inline float get_volumeVariance_3() const { return ___volumeVariance_3; }
	inline float* get_address_of_volumeVariance_3() { return &___volumeVariance_3; }
	inline void set_volumeVariance_3(float value)
	{
		___volumeVariance_3 = value;
	}

	inline static int32_t get_offset_of_pitch_4() { return static_cast<int32_t>(offsetof(Sound_t3007421746, ___pitch_4)); }
	inline float get_pitch_4() const { return ___pitch_4; }
	inline float* get_address_of_pitch_4() { return &___pitch_4; }
	inline void set_pitch_4(float value)
	{
		___pitch_4 = value;
	}

	inline static int32_t get_offset_of_pitchVariance_5() { return static_cast<int32_t>(offsetof(Sound_t3007421746, ___pitchVariance_5)); }
	inline float get_pitchVariance_5() const { return ___pitchVariance_5; }
	inline float* get_address_of_pitchVariance_5() { return &___pitchVariance_5; }
	inline void set_pitchVariance_5(float value)
	{
		___pitchVariance_5 = value;
	}

	inline static int32_t get_offset_of_loop_6() { return static_cast<int32_t>(offsetof(Sound_t3007421746, ___loop_6)); }
	inline bool get_loop_6() const { return ___loop_6; }
	inline bool* get_address_of_loop_6() { return &___loop_6; }
	inline void set_loop_6(bool value)
	{
		___loop_6 = value;
	}

	inline static int32_t get_offset_of_mixerGroup_7() { return static_cast<int32_t>(offsetof(Sound_t3007421746, ___mixerGroup_7)); }
	inline AudioMixerGroup_t2743564464 * get_mixerGroup_7() const { return ___mixerGroup_7; }
	inline AudioMixerGroup_t2743564464 ** get_address_of_mixerGroup_7() { return &___mixerGroup_7; }
	inline void set_mixerGroup_7(AudioMixerGroup_t2743564464 * value)
	{
		___mixerGroup_7 = value;
		Il2CppCodeGenWriteBarrier((&___mixerGroup_7), value);
	}

	inline static int32_t get_offset_of_source_8() { return static_cast<int32_t>(offsetof(Sound_t3007421746, ___source_8)); }
	inline AudioSource_t3935305588 * get_source_8() const { return ___source_8; }
	inline AudioSource_t3935305588 ** get_address_of_source_8() { return &___source_8; }
	inline void set_source_8(AudioSource_t3935305588 * value)
	{
		___source_8 = value;
		Il2CppCodeGenWriteBarrier((&___source_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SOUND_T3007421746_H
#ifndef VALUETYPE_T3640485471_H
#define VALUETYPE_T3640485471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3640485471  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_com
{
};
#endif // VALUETYPE_T3640485471_H
#ifndef HSBCOLOR_T1247197400_H
#define HSBCOLOR_T1247197400_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HSBColor
struct  HSBColor_t1247197400 
{
public:
	// System.Single HSBColor::h
	float ___h_0;
	// System.Single HSBColor::s
	float ___s_1;
	// System.Single HSBColor::b
	float ___b_2;
	// System.Single HSBColor::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_h_0() { return static_cast<int32_t>(offsetof(HSBColor_t1247197400, ___h_0)); }
	inline float get_h_0() const { return ___h_0; }
	inline float* get_address_of_h_0() { return &___h_0; }
	inline void set_h_0(float value)
	{
		___h_0 = value;
	}

	inline static int32_t get_offset_of_s_1() { return static_cast<int32_t>(offsetof(HSBColor_t1247197400, ___s_1)); }
	inline float get_s_1() const { return ___s_1; }
	inline float* get_address_of_s_1() { return &___s_1; }
	inline void set_s_1(float value)
	{
		___s_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(HSBColor_t1247197400, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(HSBColor_t1247197400, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HSBCOLOR_T1247197400_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef COLOR_T2555686324_H
#define COLOR_T2555686324_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color
struct  Color_t2555686324 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR_T2555686324_H
#ifndef VECTOR2_T2156229523_H
#define VECTOR2_T2156229523_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector2
struct  Vector2_t2156229523 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_t2156229523, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_t2156229523, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_t2156229523_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_t2156229523  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_t2156229523  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_t2156229523  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_t2156229523  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_t2156229523  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_t2156229523  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_t2156229523  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_t2156229523  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___zeroVector_2)); }
	inline Vector2_t2156229523  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_t2156229523 * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_t2156229523  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___oneVector_3)); }
	inline Vector2_t2156229523  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_t2156229523 * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_t2156229523  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___upVector_4)); }
	inline Vector2_t2156229523  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_t2156229523 * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_t2156229523  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___downVector_5)); }
	inline Vector2_t2156229523  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_t2156229523 * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_t2156229523  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___leftVector_6)); }
	inline Vector2_t2156229523  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_t2156229523 * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_t2156229523  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___rightVector_7)); }
	inline Vector2_t2156229523  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_t2156229523 * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_t2156229523  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_t2156229523  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_t2156229523 * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_t2156229523  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_t2156229523  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_t2156229523 * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_t2156229523  value)
	{
		___negativeInfinityVector_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2_T2156229523_H
#ifndef VECTOR3_T3722313464_H
#define VECTOR3_T3722313464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector3
struct  Vector3_t3722313464 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_2;
	// System.Single UnityEngine.Vector3::y
	float ___y_3;
	// System.Single UnityEngine.Vector3::z
	float ___z_4;

public:
	inline static int32_t get_offset_of_x_2() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___x_2)); }
	inline float get_x_2() const { return ___x_2; }
	inline float* get_address_of_x_2() { return &___x_2; }
	inline void set_x_2(float value)
	{
		___x_2 = value;
	}

	inline static int32_t get_offset_of_y_3() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___y_3)); }
	inline float get_y_3() const { return ___y_3; }
	inline float* get_address_of_y_3() { return &___y_3; }
	inline void set_y_3(float value)
	{
		___y_3 = value;
	}

	inline static int32_t get_offset_of_z_4() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___z_4)); }
	inline float get_z_4() const { return ___z_4; }
	inline float* get_address_of_z_4() { return &___z_4; }
	inline void set_z_4(float value)
	{
		___z_4 = value;
	}
};

struct Vector3_t3722313464_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_t3722313464  ___zeroVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_t3722313464  ___oneVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_t3722313464  ___upVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_t3722313464  ___downVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_t3722313464  ___leftVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_t3722313464  ___rightVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_t3722313464  ___forwardVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_t3722313464  ___backVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_t3722313464  ___positiveInfinityVector_13;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_t3722313464  ___negativeInfinityVector_14;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___zeroVector_5)); }
	inline Vector3_t3722313464  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector3_t3722313464 * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector3_t3722313464  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___oneVector_6)); }
	inline Vector3_t3722313464  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector3_t3722313464 * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector3_t3722313464  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_upVector_7() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___upVector_7)); }
	inline Vector3_t3722313464  get_upVector_7() const { return ___upVector_7; }
	inline Vector3_t3722313464 * get_address_of_upVector_7() { return &___upVector_7; }
	inline void set_upVector_7(Vector3_t3722313464  value)
	{
		___upVector_7 = value;
	}

	inline static int32_t get_offset_of_downVector_8() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___downVector_8)); }
	inline Vector3_t3722313464  get_downVector_8() const { return ___downVector_8; }
	inline Vector3_t3722313464 * get_address_of_downVector_8() { return &___downVector_8; }
	inline void set_downVector_8(Vector3_t3722313464  value)
	{
		___downVector_8 = value;
	}

	inline static int32_t get_offset_of_leftVector_9() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___leftVector_9)); }
	inline Vector3_t3722313464  get_leftVector_9() const { return ___leftVector_9; }
	inline Vector3_t3722313464 * get_address_of_leftVector_9() { return &___leftVector_9; }
	inline void set_leftVector_9(Vector3_t3722313464  value)
	{
		___leftVector_9 = value;
	}

	inline static int32_t get_offset_of_rightVector_10() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___rightVector_10)); }
	inline Vector3_t3722313464  get_rightVector_10() const { return ___rightVector_10; }
	inline Vector3_t3722313464 * get_address_of_rightVector_10() { return &___rightVector_10; }
	inline void set_rightVector_10(Vector3_t3722313464  value)
	{
		___rightVector_10 = value;
	}

	inline static int32_t get_offset_of_forwardVector_11() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___forwardVector_11)); }
	inline Vector3_t3722313464  get_forwardVector_11() const { return ___forwardVector_11; }
	inline Vector3_t3722313464 * get_address_of_forwardVector_11() { return &___forwardVector_11; }
	inline void set_forwardVector_11(Vector3_t3722313464  value)
	{
		___forwardVector_11 = value;
	}

	inline static int32_t get_offset_of_backVector_12() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___backVector_12)); }
	inline Vector3_t3722313464  get_backVector_12() const { return ___backVector_12; }
	inline Vector3_t3722313464 * get_address_of_backVector_12() { return &___backVector_12; }
	inline void set_backVector_12(Vector3_t3722313464  value)
	{
		___backVector_12 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___positiveInfinityVector_13)); }
	inline Vector3_t3722313464  get_positiveInfinityVector_13() const { return ___positiveInfinityVector_13; }
	inline Vector3_t3722313464 * get_address_of_positiveInfinityVector_13() { return &___positiveInfinityVector_13; }
	inline void set_positiveInfinityVector_13(Vector3_t3722313464  value)
	{
		___positiveInfinityVector_13 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_14() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___negativeInfinityVector_14)); }
	inline Vector3_t3722313464  get_negativeInfinityVector_14() const { return ___negativeInfinityVector_14; }
	inline Vector3_t3722313464 * get_address_of_negativeInfinityVector_14() { return &___negativeInfinityVector_14; }
	inline void set_negativeInfinityVector_14(Vector3_t3722313464  value)
	{
		___negativeInfinityVector_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_T3722313464_H
#ifndef U3CDOJUMPU3EC__ANONSTOREY6_T2908495474_H
#define U3CDOJUMPU3EC__ANONSTOREY6_T2908495474_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.DOTweenModulePhysics/<DOJump>c__AnonStorey6
struct  U3CDOJumpU3Ec__AnonStorey6_t2908495474  : public RuntimeObject
{
public:
	// UnityEngine.Rigidbody DG.Tweening.DOTweenModulePhysics/<DOJump>c__AnonStorey6::target
	Rigidbody_t3916780224 * ___target_0;
	// System.Single DG.Tweening.DOTweenModulePhysics/<DOJump>c__AnonStorey6::startPosY
	float ___startPosY_1;
	// System.Boolean DG.Tweening.DOTweenModulePhysics/<DOJump>c__AnonStorey6::offsetYSet
	bool ___offsetYSet_2;
	// DG.Tweening.Sequence DG.Tweening.DOTweenModulePhysics/<DOJump>c__AnonStorey6::s
	Sequence_t2050373119 * ___s_3;
	// UnityEngine.Vector3 DG.Tweening.DOTweenModulePhysics/<DOJump>c__AnonStorey6::endValue
	Vector3_t3722313464  ___endValue_4;
	// System.Single DG.Tweening.DOTweenModulePhysics/<DOJump>c__AnonStorey6::offsetY
	float ___offsetY_5;
	// DG.Tweening.Tween DG.Tweening.DOTweenModulePhysics/<DOJump>c__AnonStorey6::yTween
	Tween_t2342918553 * ___yTween_6;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CDOJumpU3Ec__AnonStorey6_t2908495474, ___target_0)); }
	inline Rigidbody_t3916780224 * get_target_0() const { return ___target_0; }
	inline Rigidbody_t3916780224 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(Rigidbody_t3916780224 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((&___target_0), value);
	}

	inline static int32_t get_offset_of_startPosY_1() { return static_cast<int32_t>(offsetof(U3CDOJumpU3Ec__AnonStorey6_t2908495474, ___startPosY_1)); }
	inline float get_startPosY_1() const { return ___startPosY_1; }
	inline float* get_address_of_startPosY_1() { return &___startPosY_1; }
	inline void set_startPosY_1(float value)
	{
		___startPosY_1 = value;
	}

	inline static int32_t get_offset_of_offsetYSet_2() { return static_cast<int32_t>(offsetof(U3CDOJumpU3Ec__AnonStorey6_t2908495474, ___offsetYSet_2)); }
	inline bool get_offsetYSet_2() const { return ___offsetYSet_2; }
	inline bool* get_address_of_offsetYSet_2() { return &___offsetYSet_2; }
	inline void set_offsetYSet_2(bool value)
	{
		___offsetYSet_2 = value;
	}

	inline static int32_t get_offset_of_s_3() { return static_cast<int32_t>(offsetof(U3CDOJumpU3Ec__AnonStorey6_t2908495474, ___s_3)); }
	inline Sequence_t2050373119 * get_s_3() const { return ___s_3; }
	inline Sequence_t2050373119 ** get_address_of_s_3() { return &___s_3; }
	inline void set_s_3(Sequence_t2050373119 * value)
	{
		___s_3 = value;
		Il2CppCodeGenWriteBarrier((&___s_3), value);
	}

	inline static int32_t get_offset_of_endValue_4() { return static_cast<int32_t>(offsetof(U3CDOJumpU3Ec__AnonStorey6_t2908495474, ___endValue_4)); }
	inline Vector3_t3722313464  get_endValue_4() const { return ___endValue_4; }
	inline Vector3_t3722313464 * get_address_of_endValue_4() { return &___endValue_4; }
	inline void set_endValue_4(Vector3_t3722313464  value)
	{
		___endValue_4 = value;
	}

	inline static int32_t get_offset_of_offsetY_5() { return static_cast<int32_t>(offsetof(U3CDOJumpU3Ec__AnonStorey6_t2908495474, ___offsetY_5)); }
	inline float get_offsetY_5() const { return ___offsetY_5; }
	inline float* get_address_of_offsetY_5() { return &___offsetY_5; }
	inline void set_offsetY_5(float value)
	{
		___offsetY_5 = value;
	}

	inline static int32_t get_offset_of_yTween_6() { return static_cast<int32_t>(offsetof(U3CDOJumpU3Ec__AnonStorey6_t2908495474, ___yTween_6)); }
	inline Tween_t2342918553 * get_yTween_6() const { return ___yTween_6; }
	inline Tween_t2342918553 ** get_address_of_yTween_6() { return &___yTween_6; }
	inline void set_yTween_6(Tween_t2342918553 * value)
	{
		___yTween_6 = value;
		Il2CppCodeGenWriteBarrier((&___yTween_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CDOJUMPU3EC__ANONSTOREY6_T2908495474_H
#ifndef U3CDOJUMPU3EC__ANONSTOREY4_T1356384432_H
#define U3CDOJUMPU3EC__ANONSTOREY4_T1356384432_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.DOTweenModulePhysics2D/<DOJump>c__AnonStorey4
struct  U3CDOJumpU3Ec__AnonStorey4_t1356384432  : public RuntimeObject
{
public:
	// UnityEngine.Rigidbody2D DG.Tweening.DOTweenModulePhysics2D/<DOJump>c__AnonStorey4::target
	Rigidbody2D_t939494601 * ___target_0;
	// System.Single DG.Tweening.DOTweenModulePhysics2D/<DOJump>c__AnonStorey4::startPosY
	float ___startPosY_1;
	// System.Boolean DG.Tweening.DOTweenModulePhysics2D/<DOJump>c__AnonStorey4::offsetYSet
	bool ___offsetYSet_2;
	// DG.Tweening.Sequence DG.Tweening.DOTweenModulePhysics2D/<DOJump>c__AnonStorey4::s
	Sequence_t2050373119 * ___s_3;
	// UnityEngine.Vector2 DG.Tweening.DOTweenModulePhysics2D/<DOJump>c__AnonStorey4::endValue
	Vector2_t2156229523  ___endValue_4;
	// System.Single DG.Tweening.DOTweenModulePhysics2D/<DOJump>c__AnonStorey4::offsetY
	float ___offsetY_5;
	// DG.Tweening.Tween DG.Tweening.DOTweenModulePhysics2D/<DOJump>c__AnonStorey4::yTween
	Tween_t2342918553 * ___yTween_6;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CDOJumpU3Ec__AnonStorey4_t1356384432, ___target_0)); }
	inline Rigidbody2D_t939494601 * get_target_0() const { return ___target_0; }
	inline Rigidbody2D_t939494601 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(Rigidbody2D_t939494601 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((&___target_0), value);
	}

	inline static int32_t get_offset_of_startPosY_1() { return static_cast<int32_t>(offsetof(U3CDOJumpU3Ec__AnonStorey4_t1356384432, ___startPosY_1)); }
	inline float get_startPosY_1() const { return ___startPosY_1; }
	inline float* get_address_of_startPosY_1() { return &___startPosY_1; }
	inline void set_startPosY_1(float value)
	{
		___startPosY_1 = value;
	}

	inline static int32_t get_offset_of_offsetYSet_2() { return static_cast<int32_t>(offsetof(U3CDOJumpU3Ec__AnonStorey4_t1356384432, ___offsetYSet_2)); }
	inline bool get_offsetYSet_2() const { return ___offsetYSet_2; }
	inline bool* get_address_of_offsetYSet_2() { return &___offsetYSet_2; }
	inline void set_offsetYSet_2(bool value)
	{
		___offsetYSet_2 = value;
	}

	inline static int32_t get_offset_of_s_3() { return static_cast<int32_t>(offsetof(U3CDOJumpU3Ec__AnonStorey4_t1356384432, ___s_3)); }
	inline Sequence_t2050373119 * get_s_3() const { return ___s_3; }
	inline Sequence_t2050373119 ** get_address_of_s_3() { return &___s_3; }
	inline void set_s_3(Sequence_t2050373119 * value)
	{
		___s_3 = value;
		Il2CppCodeGenWriteBarrier((&___s_3), value);
	}

	inline static int32_t get_offset_of_endValue_4() { return static_cast<int32_t>(offsetof(U3CDOJumpU3Ec__AnonStorey4_t1356384432, ___endValue_4)); }
	inline Vector2_t2156229523  get_endValue_4() const { return ___endValue_4; }
	inline Vector2_t2156229523 * get_address_of_endValue_4() { return &___endValue_4; }
	inline void set_endValue_4(Vector2_t2156229523  value)
	{
		___endValue_4 = value;
	}

	inline static int32_t get_offset_of_offsetY_5() { return static_cast<int32_t>(offsetof(U3CDOJumpU3Ec__AnonStorey4_t1356384432, ___offsetY_5)); }
	inline float get_offsetY_5() const { return ___offsetY_5; }
	inline float* get_address_of_offsetY_5() { return &___offsetY_5; }
	inline void set_offsetY_5(float value)
	{
		___offsetY_5 = value;
	}

	inline static int32_t get_offset_of_yTween_6() { return static_cast<int32_t>(offsetof(U3CDOJumpU3Ec__AnonStorey4_t1356384432, ___yTween_6)); }
	inline Tween_t2342918553 * get_yTween_6() const { return ___yTween_6; }
	inline Tween_t2342918553 ** get_address_of_yTween_6() { return &___yTween_6; }
	inline void set_yTween_6(Tween_t2342918553 * value)
	{
		___yTween_6 = value;
		Il2CppCodeGenWriteBarrier((&___yTween_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CDOJUMPU3EC__ANONSTOREY4_T1356384432_H
#ifndef U3CDOBLENDABLECOLORU3EC__ANONSTOREY2_T4261762257_H
#define U3CDOBLENDABLECOLORU3EC__ANONSTOREY2_T4261762257_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.DOTweenModuleSprite/<DOBlendableColor>c__AnonStorey2
struct  U3CDOBlendableColorU3Ec__AnonStorey2_t4261762257  : public RuntimeObject
{
public:
	// UnityEngine.Color DG.Tweening.DOTweenModuleSprite/<DOBlendableColor>c__AnonStorey2::to
	Color_t2555686324  ___to_0;
	// UnityEngine.SpriteRenderer DG.Tweening.DOTweenModuleSprite/<DOBlendableColor>c__AnonStorey2::target
	SpriteRenderer_t3235626157 * ___target_1;

public:
	inline static int32_t get_offset_of_to_0() { return static_cast<int32_t>(offsetof(U3CDOBlendableColorU3Ec__AnonStorey2_t4261762257, ___to_0)); }
	inline Color_t2555686324  get_to_0() const { return ___to_0; }
	inline Color_t2555686324 * get_address_of_to_0() { return &___to_0; }
	inline void set_to_0(Color_t2555686324  value)
	{
		___to_0 = value;
	}

	inline static int32_t get_offset_of_target_1() { return static_cast<int32_t>(offsetof(U3CDOBlendableColorU3Ec__AnonStorey2_t4261762257, ___target_1)); }
	inline SpriteRenderer_t3235626157 * get_target_1() const { return ___target_1; }
	inline SpriteRenderer_t3235626157 ** get_address_of_target_1() { return &___target_1; }
	inline void set_target_1(SpriteRenderer_t3235626157 * value)
	{
		___target_1 = value;
		Il2CppCodeGenWriteBarrier((&___target_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CDOBLENDABLECOLORU3EC__ANONSTOREY2_T4261762257_H
#ifndef U3CDOBLENDABLECOLORU3EC__ANONSTOREY24_T2770270651_H
#define U3CDOBLENDABLECOLORU3EC__ANONSTOREY24_T2770270651_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.DOTweenModuleUI/<DOBlendableColor>c__AnonStorey24
struct  U3CDOBlendableColorU3Ec__AnonStorey24_t2770270651  : public RuntimeObject
{
public:
	// UnityEngine.Color DG.Tweening.DOTweenModuleUI/<DOBlendableColor>c__AnonStorey24::to
	Color_t2555686324  ___to_0;
	// UnityEngine.UI.Graphic DG.Tweening.DOTweenModuleUI/<DOBlendableColor>c__AnonStorey24::target
	Graphic_t1660335611 * ___target_1;

public:
	inline static int32_t get_offset_of_to_0() { return static_cast<int32_t>(offsetof(U3CDOBlendableColorU3Ec__AnonStorey24_t2770270651, ___to_0)); }
	inline Color_t2555686324  get_to_0() const { return ___to_0; }
	inline Color_t2555686324 * get_address_of_to_0() { return &___to_0; }
	inline void set_to_0(Color_t2555686324  value)
	{
		___to_0 = value;
	}

	inline static int32_t get_offset_of_target_1() { return static_cast<int32_t>(offsetof(U3CDOBlendableColorU3Ec__AnonStorey24_t2770270651, ___target_1)); }
	inline Graphic_t1660335611 * get_target_1() const { return ___target_1; }
	inline Graphic_t1660335611 ** get_address_of_target_1() { return &___target_1; }
	inline void set_target_1(Graphic_t1660335611 * value)
	{
		___target_1 = value;
		Il2CppCodeGenWriteBarrier((&___target_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CDOBLENDABLECOLORU3EC__ANONSTOREY24_T2770270651_H
#ifndef U3CDOJUMPANCHORPOSU3EC__ANONSTOREY1C_T2532437241_H
#define U3CDOJUMPANCHORPOSU3EC__ANONSTOREY1C_T2532437241_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.DOTweenModuleUI/<DOJumpAnchorPos>c__AnonStorey1C
struct  U3CDOJumpAnchorPosU3Ec__AnonStorey1C_t2532437241  : public RuntimeObject
{
public:
	// UnityEngine.RectTransform DG.Tweening.DOTweenModuleUI/<DOJumpAnchorPos>c__AnonStorey1C::target
	RectTransform_t3704657025 * ___target_0;
	// System.Single DG.Tweening.DOTweenModuleUI/<DOJumpAnchorPos>c__AnonStorey1C::startPosY
	float ___startPosY_1;
	// System.Boolean DG.Tweening.DOTweenModuleUI/<DOJumpAnchorPos>c__AnonStorey1C::offsetYSet
	bool ___offsetYSet_2;
	// DG.Tweening.Sequence DG.Tweening.DOTweenModuleUI/<DOJumpAnchorPos>c__AnonStorey1C::s
	Sequence_t2050373119 * ___s_3;
	// UnityEngine.Vector2 DG.Tweening.DOTweenModuleUI/<DOJumpAnchorPos>c__AnonStorey1C::endValue
	Vector2_t2156229523  ___endValue_4;
	// System.Single DG.Tweening.DOTweenModuleUI/<DOJumpAnchorPos>c__AnonStorey1C::offsetY
	float ___offsetY_5;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CDOJumpAnchorPosU3Ec__AnonStorey1C_t2532437241, ___target_0)); }
	inline RectTransform_t3704657025 * get_target_0() const { return ___target_0; }
	inline RectTransform_t3704657025 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(RectTransform_t3704657025 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((&___target_0), value);
	}

	inline static int32_t get_offset_of_startPosY_1() { return static_cast<int32_t>(offsetof(U3CDOJumpAnchorPosU3Ec__AnonStorey1C_t2532437241, ___startPosY_1)); }
	inline float get_startPosY_1() const { return ___startPosY_1; }
	inline float* get_address_of_startPosY_1() { return &___startPosY_1; }
	inline void set_startPosY_1(float value)
	{
		___startPosY_1 = value;
	}

	inline static int32_t get_offset_of_offsetYSet_2() { return static_cast<int32_t>(offsetof(U3CDOJumpAnchorPosU3Ec__AnonStorey1C_t2532437241, ___offsetYSet_2)); }
	inline bool get_offsetYSet_2() const { return ___offsetYSet_2; }
	inline bool* get_address_of_offsetYSet_2() { return &___offsetYSet_2; }
	inline void set_offsetYSet_2(bool value)
	{
		___offsetYSet_2 = value;
	}

	inline static int32_t get_offset_of_s_3() { return static_cast<int32_t>(offsetof(U3CDOJumpAnchorPosU3Ec__AnonStorey1C_t2532437241, ___s_3)); }
	inline Sequence_t2050373119 * get_s_3() const { return ___s_3; }
	inline Sequence_t2050373119 ** get_address_of_s_3() { return &___s_3; }
	inline void set_s_3(Sequence_t2050373119 * value)
	{
		___s_3 = value;
		Il2CppCodeGenWriteBarrier((&___s_3), value);
	}

	inline static int32_t get_offset_of_endValue_4() { return static_cast<int32_t>(offsetof(U3CDOJumpAnchorPosU3Ec__AnonStorey1C_t2532437241, ___endValue_4)); }
	inline Vector2_t2156229523  get_endValue_4() const { return ___endValue_4; }
	inline Vector2_t2156229523 * get_address_of_endValue_4() { return &___endValue_4; }
	inline void set_endValue_4(Vector2_t2156229523  value)
	{
		___endValue_4 = value;
	}

	inline static int32_t get_offset_of_offsetY_5() { return static_cast<int32_t>(offsetof(U3CDOJumpAnchorPosU3Ec__AnonStorey1C_t2532437241, ___offsetY_5)); }
	inline float get_offsetY_5() const { return ___offsetY_5; }
	inline float* get_address_of_offsetY_5() { return &___offsetY_5; }
	inline void set_offsetY_5(float value)
	{
		___offsetY_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CDOJUMPANCHORPOSU3EC__ANONSTOREY1C_T2532437241_H
#ifndef OBJECT_T631007953_H
#define OBJECT_T631007953_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_t631007953  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_t631007953, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_t631007953_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_t631007953_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_T631007953_H
#ifndef COMPONENT_T1923634451_H
#define COMPONENT_T1923634451_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t1923634451  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T1923634451_H
#ifndef SCRIPTABLEOBJECT_T2528358522_H
#define SCRIPTABLEOBJECT_T2528358522_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.ScriptableObject
struct  ScriptableObject_t2528358522  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_t2528358522_marshaled_pinvoke : public Object_t631007953_marshaled_pinvoke
{
};
// Native definition for COM marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_t2528358522_marshaled_com : public Object_t631007953_marshaled_com
{
};
#endif // SCRIPTABLEOBJECT_T2528358522_H
#ifndef GENERICLIST_1_T1718585186_H
#define GENERICLIST_1_T1718585186_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GenericList`1<MaterialwColorHolder>
struct  GenericList_1_t1718585186  : public ScriptableObject_t2528358522
{
public:
	// System.Collections.Generic.List`1<T> GenericList`1::m_value
	List_1_t3892189426 * ___m_value_4;

public:
	inline static int32_t get_offset_of_m_value_4() { return static_cast<int32_t>(offsetof(GenericList_1_t1718585186, ___m_value_4)); }
	inline List_1_t3892189426 * get_m_value_4() const { return ___m_value_4; }
	inline List_1_t3892189426 ** get_address_of_m_value_4() { return &___m_value_4; }
	inline void set_m_value_4(List_1_t3892189426 * value)
	{
		___m_value_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_value_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GENERICLIST_1_T1718585186_H
#ifndef GENERICLIST_1_T3897928382_H
#define GENERICLIST_1_T3897928382_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GenericList`1<MeshHolder>
struct  GenericList_1_t3897928382  : public ScriptableObject_t2528358522
{
public:
	// System.Collections.Generic.List`1<T> GenericList`1::m_value
	List_1_t1776565326 * ___m_value_4;

public:
	inline static int32_t get_offset_of_m_value_4() { return static_cast<int32_t>(offsetof(GenericList_1_t3897928382, ___m_value_4)); }
	inline List_1_t1776565326 * get_m_value_4() const { return ___m_value_4; }
	inline List_1_t1776565326 ** get_address_of_m_value_4() { return &___m_value_4; }
	inline void set_m_value_4(List_1_t1776565326 * value)
	{
		___m_value_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_value_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GENERICLIST_1_T3897928382_H
#ifndef GENERICLIST_1_T1826829024_H
#define GENERICLIST_1_T1826829024_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GenericList`1<UnityEngine.ScriptableObject>
struct  GenericList_1_t1826829024  : public ScriptableObject_t2528358522
{
public:
	// System.Collections.Generic.List`1<T> GenericList`1::m_value
	List_1_t4000433264 * ___m_value_4;

public:
	inline static int32_t get_offset_of_m_value_4() { return static_cast<int32_t>(offsetof(GenericList_1_t1826829024, ___m_value_4)); }
	inline List_1_t4000433264 * get_m_value_4() const { return ___m_value_4; }
	inline List_1_t4000433264 ** get_address_of_m_value_4() { return &___m_value_4; }
	inline void set_m_value_4(List_1_t4000433264 * value)
	{
		___m_value_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_value_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GENERICLIST_1_T1826829024_H
#ifndef GENERICLIST_1_T3020783966_H
#define GENERICLIST_1_T3020783966_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GenericList`1<UnityEngine.Vector3>
struct  GenericList_1_t3020783966  : public ScriptableObject_t2528358522
{
public:
	// System.Collections.Generic.List`1<T> GenericList`1::m_value
	List_1_t899420910 * ___m_value_4;

public:
	inline static int32_t get_offset_of_m_value_4() { return static_cast<int32_t>(offsetof(GenericList_1_t3020783966, ___m_value_4)); }
	inline List_1_t899420910 * get_m_value_4() const { return ___m_value_4; }
	inline List_1_t899420910 ** get_address_of_m_value_4() { return &___m_value_4; }
	inline void set_m_value_4(List_1_t899420910 * value)
	{
		___m_value_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_value_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GENERICLIST_1_T3020783966_H
#ifndef GENERICVARIABLE_1_T111255121_H
#define GENERICVARIABLE_1_T111255121_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GenericVariable`1<System.Int16>
struct  GenericVariable_1_t111255121  : public ScriptableObject_t2528358522
{
public:
	// T GenericVariable`1::m_value
	int16_t ___m_value_4;

public:
	inline static int32_t get_offset_of_m_value_4() { return static_cast<int32_t>(offsetof(GenericVariable_1_t111255121, ___m_value_4)); }
	inline int16_t get_m_value_4() const { return ___m_value_4; }
	inline int16_t* get_address_of_m_value_4() { return &___m_value_4; }
	inline void set_m_value_4(int16_t value)
	{
		___m_value_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GENERICVARIABLE_1_T111255121_H
#ifndef GENERICVARIABLE_1_T118496712_H
#define GENERICVARIABLE_1_T118496712_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GenericVariable`1<System.UInt32>
struct  GenericVariable_1_t118496712  : public ScriptableObject_t2528358522
{
public:
	// T GenericVariable`1::m_value
	uint32_t ___m_value_4;

public:
	inline static int32_t get_offset_of_m_value_4() { return static_cast<int32_t>(offsetof(GenericVariable_1_t118496712, ___m_value_4)); }
	inline uint32_t get_m_value_4() const { return ___m_value_4; }
	inline uint32_t* get_address_of_m_value_4() { return &___m_value_4; }
	inline void set_m_value_4(uint32_t value)
	{
		___m_value_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GENERICVARIABLE_1_T118496712_H
#ifndef OBJECTDATA_T3315742923_H
#define OBJECTDATA_T3315742923_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ObjectData
struct  ObjectData_t3315742923  : public ScriptableObject_t2528358522
{
public:
	// System.Collections.Generic.List`1<StateWithMaterials> ObjectData::m_dataList
	List_1_t284606406 * ___m_dataList_4;

public:
	inline static int32_t get_offset_of_m_dataList_4() { return static_cast<int32_t>(offsetof(ObjectData_t3315742923, ___m_dataList_4)); }
	inline List_1_t284606406 * get_m_dataList_4() const { return ___m_dataList_4; }
	inline List_1_t284606406 ** get_address_of_m_dataList_4() { return &___m_dataList_4; }
	inline void set_m_dataList_4(List_1_t284606406 * value)
	{
		___m_dataList_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_dataList_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OBJECTDATA_T3315742923_H
#ifndef RANDOMTEXTDATA_T1210110652_H
#define RANDOMTEXTDATA_T1210110652_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RandomTextData
struct  RandomTextData_t1210110652  : public ScriptableObject_t2528358522
{
public:
	// System.Collections.Generic.List`1<System.String> RandomTextData::textDatabase
	List_1_t3319525431 * ___textDatabase_4;

public:
	inline static int32_t get_offset_of_textDatabase_4() { return static_cast<int32_t>(offsetof(RandomTextData_t1210110652, ___textDatabase_4)); }
	inline List_1_t3319525431 * get_textDatabase_4() const { return ___textDatabase_4; }
	inline List_1_t3319525431 ** get_address_of_textDatabase_4() { return &___textDatabase_4; }
	inline void set_textDatabase_4(List_1_t3319525431 * value)
	{
		___textDatabase_4 = value;
		Il2CppCodeGenWriteBarrier((&___textDatabase_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RANDOMTEXTDATA_T1210110652_H
#ifndef BEHAVIOUR_T1437897464_H
#define BEHAVIOUR_T1437897464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_t1437897464  : public Component_t1923634451
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_T1437897464_H
#ifndef MATWCOLORLIST_T3252287760_H
#define MATWCOLORLIST_T3252287760_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MatwColorList
struct  MatwColorList_t3252287760  : public GenericList_1_t1718585186
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MATWCOLORLIST_T3252287760_H
#ifndef MESHHOLDERLIST_T1122110985_H
#define MESHHOLDERLIST_T1122110985_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MeshHolderList
struct  MeshHolderList_t1122110985  : public GenericList_1_t3897928382
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MESHHOLDERLIST_T1122110985_H
#ifndef SCRIPTABLEOBJECTLIST_T1377352194_H
#define SCRIPTABLEOBJECTLIST_T1377352194_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ScriptableObjectList
struct  ScriptableObjectList_t1377352194  : public GenericList_1_t1826829024
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCRIPTABLEOBJECTLIST_T1377352194_H
#ifndef SHORTVARIABLE_T886426368_H
#define SHORTVARIABLE_T886426368_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ShortVariable
struct  ShortVariable_t886426368  : public GenericVariable_1_t111255121
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SHORTVARIABLE_T886426368_H
#ifndef UINTVARIABLE_T3613798951_H
#define UINTVARIABLE_T3613798951_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UintVariable
struct  UintVariable_t3613798951  : public GenericVariable_1_t118496712
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UINTVARIABLE_T3613798951_H
#ifndef MONOBEHAVIOUR_T3962482529_H
#define MONOBEHAVIOUR_T3962482529_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t3962482529  : public Behaviour_t1437897464
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T3962482529_H
#ifndef VECTOR3LIST_T3337973461_H
#define VECTOR3LIST_T3337973461_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vector3List
struct  Vector3List_t3337973461  : public GenericList_1_t3020783966
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3LIST_T3337973461_H
#ifndef AMMOTOGGLE_T520557478_H
#define AMMOTOGGLE_T520557478_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AmmoToggle
struct  AmmoToggle_t520557478  : public MonoBehaviour_t3962482529
{
public:
	// GameEvent AmmoToggle::toggleAmmo
	GameEvent_t4069578135 * ___toggleAmmo_4;
	// FloatVariable AmmoToggle::currEnergy
	FloatVariable_t761998486 * ___currEnergy_5;
	// FloatVariable AmmoToggle::maxEnergy
	FloatVariable_t761998486 * ___maxEnergy_6;
	// UnityEngine.GameObject AmmoToggle::enemyAmmo
	GameObject_t1113636619 * ___enemyAmmo_7;
	// UnityEngine.GameObject AmmoToggle::energyAmmo
	GameObject_t1113636619 * ___energyAmmo_8;
	// UnityEngine.GameObject AmmoToggle::enemyAmmoCrosshair
	GameObject_t1113636619 * ___enemyAmmoCrosshair_9;
	// UnityEngine.GameObject AmmoToggle::energyAmmoCrosshair
	GameObject_t1113636619 * ___energyAmmoCrosshair_10;

public:
	inline static int32_t get_offset_of_toggleAmmo_4() { return static_cast<int32_t>(offsetof(AmmoToggle_t520557478, ___toggleAmmo_4)); }
	inline GameEvent_t4069578135 * get_toggleAmmo_4() const { return ___toggleAmmo_4; }
	inline GameEvent_t4069578135 ** get_address_of_toggleAmmo_4() { return &___toggleAmmo_4; }
	inline void set_toggleAmmo_4(GameEvent_t4069578135 * value)
	{
		___toggleAmmo_4 = value;
		Il2CppCodeGenWriteBarrier((&___toggleAmmo_4), value);
	}

	inline static int32_t get_offset_of_currEnergy_5() { return static_cast<int32_t>(offsetof(AmmoToggle_t520557478, ___currEnergy_5)); }
	inline FloatVariable_t761998486 * get_currEnergy_5() const { return ___currEnergy_5; }
	inline FloatVariable_t761998486 ** get_address_of_currEnergy_5() { return &___currEnergy_5; }
	inline void set_currEnergy_5(FloatVariable_t761998486 * value)
	{
		___currEnergy_5 = value;
		Il2CppCodeGenWriteBarrier((&___currEnergy_5), value);
	}

	inline static int32_t get_offset_of_maxEnergy_6() { return static_cast<int32_t>(offsetof(AmmoToggle_t520557478, ___maxEnergy_6)); }
	inline FloatVariable_t761998486 * get_maxEnergy_6() const { return ___maxEnergy_6; }
	inline FloatVariable_t761998486 ** get_address_of_maxEnergy_6() { return &___maxEnergy_6; }
	inline void set_maxEnergy_6(FloatVariable_t761998486 * value)
	{
		___maxEnergy_6 = value;
		Il2CppCodeGenWriteBarrier((&___maxEnergy_6), value);
	}

	inline static int32_t get_offset_of_enemyAmmo_7() { return static_cast<int32_t>(offsetof(AmmoToggle_t520557478, ___enemyAmmo_7)); }
	inline GameObject_t1113636619 * get_enemyAmmo_7() const { return ___enemyAmmo_7; }
	inline GameObject_t1113636619 ** get_address_of_enemyAmmo_7() { return &___enemyAmmo_7; }
	inline void set_enemyAmmo_7(GameObject_t1113636619 * value)
	{
		___enemyAmmo_7 = value;
		Il2CppCodeGenWriteBarrier((&___enemyAmmo_7), value);
	}

	inline static int32_t get_offset_of_energyAmmo_8() { return static_cast<int32_t>(offsetof(AmmoToggle_t520557478, ___energyAmmo_8)); }
	inline GameObject_t1113636619 * get_energyAmmo_8() const { return ___energyAmmo_8; }
	inline GameObject_t1113636619 ** get_address_of_energyAmmo_8() { return &___energyAmmo_8; }
	inline void set_energyAmmo_8(GameObject_t1113636619 * value)
	{
		___energyAmmo_8 = value;
		Il2CppCodeGenWriteBarrier((&___energyAmmo_8), value);
	}

	inline static int32_t get_offset_of_enemyAmmoCrosshair_9() { return static_cast<int32_t>(offsetof(AmmoToggle_t520557478, ___enemyAmmoCrosshair_9)); }
	inline GameObject_t1113636619 * get_enemyAmmoCrosshair_9() const { return ___enemyAmmoCrosshair_9; }
	inline GameObject_t1113636619 ** get_address_of_enemyAmmoCrosshair_9() { return &___enemyAmmoCrosshair_9; }
	inline void set_enemyAmmoCrosshair_9(GameObject_t1113636619 * value)
	{
		___enemyAmmoCrosshair_9 = value;
		Il2CppCodeGenWriteBarrier((&___enemyAmmoCrosshair_9), value);
	}

	inline static int32_t get_offset_of_energyAmmoCrosshair_10() { return static_cast<int32_t>(offsetof(AmmoToggle_t520557478, ___energyAmmoCrosshair_10)); }
	inline GameObject_t1113636619 * get_energyAmmoCrosshair_10() const { return ___energyAmmoCrosshair_10; }
	inline GameObject_t1113636619 ** get_address_of_energyAmmoCrosshair_10() { return &___energyAmmoCrosshair_10; }
	inline void set_energyAmmoCrosshair_10(GameObject_t1113636619 * value)
	{
		___energyAmmoCrosshair_10 = value;
		Il2CppCodeGenWriteBarrier((&___energyAmmoCrosshair_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AMMOTOGGLE_T520557478_H
#ifndef BASESPAWNER_T2872291831_H
#define BASESPAWNER_T2872291831_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BaseSpawner
struct  BaseSpawner_t2872291831  : public MonoBehaviour_t3962482529
{
public:
	// ObjectData BaseSpawner::m_data
	ObjectData_t3315742923 * ___m_data_4;
	// UnityEngine.GameObject BaseSpawner::m_objectToSpawn
	GameObject_t1113636619 * ___m_objectToSpawn_5;
	// UnityEngine.GameObject BaseSpawner::m_spawnPoint
	GameObject_t1113636619 * ___m_spawnPoint_6;

public:
	inline static int32_t get_offset_of_m_data_4() { return static_cast<int32_t>(offsetof(BaseSpawner_t2872291831, ___m_data_4)); }
	inline ObjectData_t3315742923 * get_m_data_4() const { return ___m_data_4; }
	inline ObjectData_t3315742923 ** get_address_of_m_data_4() { return &___m_data_4; }
	inline void set_m_data_4(ObjectData_t3315742923 * value)
	{
		___m_data_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_data_4), value);
	}

	inline static int32_t get_offset_of_m_objectToSpawn_5() { return static_cast<int32_t>(offsetof(BaseSpawner_t2872291831, ___m_objectToSpawn_5)); }
	inline GameObject_t1113636619 * get_m_objectToSpawn_5() const { return ___m_objectToSpawn_5; }
	inline GameObject_t1113636619 ** get_address_of_m_objectToSpawn_5() { return &___m_objectToSpawn_5; }
	inline void set_m_objectToSpawn_5(GameObject_t1113636619 * value)
	{
		___m_objectToSpawn_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_objectToSpawn_5), value);
	}

	inline static int32_t get_offset_of_m_spawnPoint_6() { return static_cast<int32_t>(offsetof(BaseSpawner_t2872291831, ___m_spawnPoint_6)); }
	inline GameObject_t1113636619 * get_m_spawnPoint_6() const { return ___m_spawnPoint_6; }
	inline GameObject_t1113636619 ** get_address_of_m_spawnPoint_6() { return &___m_spawnPoint_6; }
	inline void set_m_spawnPoint_6(GameObject_t1113636619 * value)
	{
		___m_spawnPoint_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_spawnPoint_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BASESPAWNER_T2872291831_H
#ifndef BUTTONFIREEVENT_T125960542_H
#define BUTTONFIREEVENT_T125960542_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ButtonFireEvent
struct  ButtonFireEvent_t125960542  : public MonoBehaviour_t3962482529
{
public:
	// System.Collections.Generic.List`1<GameEvent> ButtonFireEvent::gameEventsToFire
	List_1_t1246685581 * ___gameEventsToFire_4;

public:
	inline static int32_t get_offset_of_gameEventsToFire_4() { return static_cast<int32_t>(offsetof(ButtonFireEvent_t125960542, ___gameEventsToFire_4)); }
	inline List_1_t1246685581 * get_gameEventsToFire_4() const { return ___gameEventsToFire_4; }
	inline List_1_t1246685581 ** get_address_of_gameEventsToFire_4() { return &___gameEventsToFire_4; }
	inline void set_gameEventsToFire_4(List_1_t1246685581 * value)
	{
		___gameEventsToFire_4 = value;
		Il2CppCodeGenWriteBarrier((&___gameEventsToFire_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BUTTONFIREEVENT_T125960542_H
#ifndef COLORCYCLE_T3175142491_H
#define COLORCYCLE_T3175142491_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ColorCycle
struct  ColorCycle_t3175142491  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.MeshRenderer ColorCycle::meshRenderer
	MeshRenderer_t587009260 * ___meshRenderer_4;
	// System.Single ColorCycle::speed
	float ___speed_5;
	// UnityEngine.Color ColorCycle::color
	Color_t2555686324  ___color_6;
	// System.Boolean ColorCycle::fixedColor
	bool ___fixedColor_7;

public:
	inline static int32_t get_offset_of_meshRenderer_4() { return static_cast<int32_t>(offsetof(ColorCycle_t3175142491, ___meshRenderer_4)); }
	inline MeshRenderer_t587009260 * get_meshRenderer_4() const { return ___meshRenderer_4; }
	inline MeshRenderer_t587009260 ** get_address_of_meshRenderer_4() { return &___meshRenderer_4; }
	inline void set_meshRenderer_4(MeshRenderer_t587009260 * value)
	{
		___meshRenderer_4 = value;
		Il2CppCodeGenWriteBarrier((&___meshRenderer_4), value);
	}

	inline static int32_t get_offset_of_speed_5() { return static_cast<int32_t>(offsetof(ColorCycle_t3175142491, ___speed_5)); }
	inline float get_speed_5() const { return ___speed_5; }
	inline float* get_address_of_speed_5() { return &___speed_5; }
	inline void set_speed_5(float value)
	{
		___speed_5 = value;
	}

	inline static int32_t get_offset_of_color_6() { return static_cast<int32_t>(offsetof(ColorCycle_t3175142491, ___color_6)); }
	inline Color_t2555686324  get_color_6() const { return ___color_6; }
	inline Color_t2555686324 * get_address_of_color_6() { return &___color_6; }
	inline void set_color_6(Color_t2555686324  value)
	{
		___color_6 = value;
	}

	inline static int32_t get_offset_of_fixedColor_7() { return static_cast<int32_t>(offsetof(ColorCycle_t3175142491, ___fixedColor_7)); }
	inline bool get_fixedColor_7() const { return ___fixedColor_7; }
	inline bool* get_address_of_fixedColor_7() { return &___fixedColor_7; }
	inline void set_fixedColor_7(bool value)
	{
		___fixedColor_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLORCYCLE_T3175142491_H
#ifndef CROSSHAIRTOGGLE_T3349641956_H
#define CROSSHAIRTOGGLE_T3349641956_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CrosshairToggle
struct  CrosshairToggle_t3349641956  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.GameObject CrosshairToggle::energyCrosshair
	GameObject_t1113636619 * ___energyCrosshair_4;
	// UnityEngine.GameObject CrosshairToggle::gunCrosshair
	GameObject_t1113636619 * ___gunCrosshair_5;

public:
	inline static int32_t get_offset_of_energyCrosshair_4() { return static_cast<int32_t>(offsetof(CrosshairToggle_t3349641956, ___energyCrosshair_4)); }
	inline GameObject_t1113636619 * get_energyCrosshair_4() const { return ___energyCrosshair_4; }
	inline GameObject_t1113636619 ** get_address_of_energyCrosshair_4() { return &___energyCrosshair_4; }
	inline void set_energyCrosshair_4(GameObject_t1113636619 * value)
	{
		___energyCrosshair_4 = value;
		Il2CppCodeGenWriteBarrier((&___energyCrosshair_4), value);
	}

	inline static int32_t get_offset_of_gunCrosshair_5() { return static_cast<int32_t>(offsetof(CrosshairToggle_t3349641956, ___gunCrosshair_5)); }
	inline GameObject_t1113636619 * get_gunCrosshair_5() const { return ___gunCrosshair_5; }
	inline GameObject_t1113636619 ** get_address_of_gunCrosshair_5() { return &___gunCrosshair_5; }
	inline void set_gunCrosshair_5(GameObject_t1113636619 * value)
	{
		___gunCrosshair_5 = value;
		Il2CppCodeGenWriteBarrier((&___gunCrosshair_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CROSSHAIRTOGGLE_T3349641956_H
#ifndef DGADASSIGNMENT_T2646297173_H
#define DGADASSIGNMENT_T2646297173_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DGADAssignment
struct  DGADAssignment_t2646297173  : public MonoBehaviour_t3962482529
{
public:
	// MeshHolderList DGADAssignment::m_meshList
	MeshHolderList_t1122110985 * ___m_meshList_4;
	// UnityEngine.GameObject DGADAssignment::m_assignmentUpParticle
	GameObject_t1113636619 * ___m_assignmentUpParticle_5;
	// FloatVariable DGADAssignment::m_assignmentProgress
	FloatVariable_t761998486 * ___m_assignmentProgress_6;
	// System.Int32 DGADAssignment::m_energyIncrement
	int32_t ___m_energyIncrement_7;
	// System.Single DGADAssignment::m_totalParticleTime
	float ___m_totalParticleTime_8;
	// UnityEngine.MeshFilter DGADAssignment::m_meshFilter
	MeshFilter_t3523625662 * ___m_meshFilter_9;
	// UnityEngine.MeshRenderer DGADAssignment::m_meshRenderer
	MeshRenderer_t587009260 * ___m_meshRenderer_10;
	// UnityEngine.MeshCollider DGADAssignment::m_meshCollider
	MeshCollider_t903564387 * ___m_meshCollider_11;
	// IntVariable DGADAssignment::m_stage
	IntVariable_t1706759505 * ___m_stage_12;
	// GameEvent DGADAssignment::m_winEvent
	GameEvent_t4069578135 * ___m_winEvent_13;
	// GameEvent DGADAssignment::m_killAllEnemy
	GameEvent_t4069578135 * ___m_killAllEnemy_14;
	// DGADGameLogic DGADAssignment::m_dgadLogic
	DGADGameLogic_t3582670137 * ___m_dgadLogic_15;

public:
	inline static int32_t get_offset_of_m_meshList_4() { return static_cast<int32_t>(offsetof(DGADAssignment_t2646297173, ___m_meshList_4)); }
	inline MeshHolderList_t1122110985 * get_m_meshList_4() const { return ___m_meshList_4; }
	inline MeshHolderList_t1122110985 ** get_address_of_m_meshList_4() { return &___m_meshList_4; }
	inline void set_m_meshList_4(MeshHolderList_t1122110985 * value)
	{
		___m_meshList_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_meshList_4), value);
	}

	inline static int32_t get_offset_of_m_assignmentUpParticle_5() { return static_cast<int32_t>(offsetof(DGADAssignment_t2646297173, ___m_assignmentUpParticle_5)); }
	inline GameObject_t1113636619 * get_m_assignmentUpParticle_5() const { return ___m_assignmentUpParticle_5; }
	inline GameObject_t1113636619 ** get_address_of_m_assignmentUpParticle_5() { return &___m_assignmentUpParticle_5; }
	inline void set_m_assignmentUpParticle_5(GameObject_t1113636619 * value)
	{
		___m_assignmentUpParticle_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_assignmentUpParticle_5), value);
	}

	inline static int32_t get_offset_of_m_assignmentProgress_6() { return static_cast<int32_t>(offsetof(DGADAssignment_t2646297173, ___m_assignmentProgress_6)); }
	inline FloatVariable_t761998486 * get_m_assignmentProgress_6() const { return ___m_assignmentProgress_6; }
	inline FloatVariable_t761998486 ** get_address_of_m_assignmentProgress_6() { return &___m_assignmentProgress_6; }
	inline void set_m_assignmentProgress_6(FloatVariable_t761998486 * value)
	{
		___m_assignmentProgress_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_assignmentProgress_6), value);
	}

	inline static int32_t get_offset_of_m_energyIncrement_7() { return static_cast<int32_t>(offsetof(DGADAssignment_t2646297173, ___m_energyIncrement_7)); }
	inline int32_t get_m_energyIncrement_7() const { return ___m_energyIncrement_7; }
	inline int32_t* get_address_of_m_energyIncrement_7() { return &___m_energyIncrement_7; }
	inline void set_m_energyIncrement_7(int32_t value)
	{
		___m_energyIncrement_7 = value;
	}

	inline static int32_t get_offset_of_m_totalParticleTime_8() { return static_cast<int32_t>(offsetof(DGADAssignment_t2646297173, ___m_totalParticleTime_8)); }
	inline float get_m_totalParticleTime_8() const { return ___m_totalParticleTime_8; }
	inline float* get_address_of_m_totalParticleTime_8() { return &___m_totalParticleTime_8; }
	inline void set_m_totalParticleTime_8(float value)
	{
		___m_totalParticleTime_8 = value;
	}

	inline static int32_t get_offset_of_m_meshFilter_9() { return static_cast<int32_t>(offsetof(DGADAssignment_t2646297173, ___m_meshFilter_9)); }
	inline MeshFilter_t3523625662 * get_m_meshFilter_9() const { return ___m_meshFilter_9; }
	inline MeshFilter_t3523625662 ** get_address_of_m_meshFilter_9() { return &___m_meshFilter_9; }
	inline void set_m_meshFilter_9(MeshFilter_t3523625662 * value)
	{
		___m_meshFilter_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_meshFilter_9), value);
	}

	inline static int32_t get_offset_of_m_meshRenderer_10() { return static_cast<int32_t>(offsetof(DGADAssignment_t2646297173, ___m_meshRenderer_10)); }
	inline MeshRenderer_t587009260 * get_m_meshRenderer_10() const { return ___m_meshRenderer_10; }
	inline MeshRenderer_t587009260 ** get_address_of_m_meshRenderer_10() { return &___m_meshRenderer_10; }
	inline void set_m_meshRenderer_10(MeshRenderer_t587009260 * value)
	{
		___m_meshRenderer_10 = value;
		Il2CppCodeGenWriteBarrier((&___m_meshRenderer_10), value);
	}

	inline static int32_t get_offset_of_m_meshCollider_11() { return static_cast<int32_t>(offsetof(DGADAssignment_t2646297173, ___m_meshCollider_11)); }
	inline MeshCollider_t903564387 * get_m_meshCollider_11() const { return ___m_meshCollider_11; }
	inline MeshCollider_t903564387 ** get_address_of_m_meshCollider_11() { return &___m_meshCollider_11; }
	inline void set_m_meshCollider_11(MeshCollider_t903564387 * value)
	{
		___m_meshCollider_11 = value;
		Il2CppCodeGenWriteBarrier((&___m_meshCollider_11), value);
	}

	inline static int32_t get_offset_of_m_stage_12() { return static_cast<int32_t>(offsetof(DGADAssignment_t2646297173, ___m_stage_12)); }
	inline IntVariable_t1706759505 * get_m_stage_12() const { return ___m_stage_12; }
	inline IntVariable_t1706759505 ** get_address_of_m_stage_12() { return &___m_stage_12; }
	inline void set_m_stage_12(IntVariable_t1706759505 * value)
	{
		___m_stage_12 = value;
		Il2CppCodeGenWriteBarrier((&___m_stage_12), value);
	}

	inline static int32_t get_offset_of_m_winEvent_13() { return static_cast<int32_t>(offsetof(DGADAssignment_t2646297173, ___m_winEvent_13)); }
	inline GameEvent_t4069578135 * get_m_winEvent_13() const { return ___m_winEvent_13; }
	inline GameEvent_t4069578135 ** get_address_of_m_winEvent_13() { return &___m_winEvent_13; }
	inline void set_m_winEvent_13(GameEvent_t4069578135 * value)
	{
		___m_winEvent_13 = value;
		Il2CppCodeGenWriteBarrier((&___m_winEvent_13), value);
	}

	inline static int32_t get_offset_of_m_killAllEnemy_14() { return static_cast<int32_t>(offsetof(DGADAssignment_t2646297173, ___m_killAllEnemy_14)); }
	inline GameEvent_t4069578135 * get_m_killAllEnemy_14() const { return ___m_killAllEnemy_14; }
	inline GameEvent_t4069578135 ** get_address_of_m_killAllEnemy_14() { return &___m_killAllEnemy_14; }
	inline void set_m_killAllEnemy_14(GameEvent_t4069578135 * value)
	{
		___m_killAllEnemy_14 = value;
		Il2CppCodeGenWriteBarrier((&___m_killAllEnemy_14), value);
	}

	inline static int32_t get_offset_of_m_dgadLogic_15() { return static_cast<int32_t>(offsetof(DGADAssignment_t2646297173, ___m_dgadLogic_15)); }
	inline DGADGameLogic_t3582670137 * get_m_dgadLogic_15() const { return ___m_dgadLogic_15; }
	inline DGADGameLogic_t3582670137 ** get_address_of_m_dgadLogic_15() { return &___m_dgadLogic_15; }
	inline void set_m_dgadLogic_15(DGADGameLogic_t3582670137 * value)
	{
		___m_dgadLogic_15 = value;
		Il2CppCodeGenWriteBarrier((&___m_dgadLogic_15), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DGADASSIGNMENT_T2646297173_H
#ifndef DGADENEMY_T110213370_H
#define DGADENEMY_T110213370_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DGADEnemy
struct  DGADEnemy_t110213370  : public MonoBehaviour_t3962482529
{
public:
	// MatwColorList DGADEnemy::m_matList
	MatwColorList_t3252287760 * ___m_matList_4;
	// UnityEngine.GameObject DGADEnemy::m_deadParticle
	GameObject_t1113636619 * ___m_deadParticle_5;
	// DGADGameLogic DGADEnemy::m_dgadGameLogic
	DGADGameLogic_t3582670137 * ___m_dgadGameLogic_6;
	// UnityEngine.MeshRenderer DGADEnemy::m_meshRenderer
	MeshRenderer_t587009260 * ___m_meshRenderer_7;
	// UnityEngine.Rigidbody DGADEnemy::m_rb
	Rigidbody_t3916780224 * ___m_rb_8;
	// ColorCycle DGADEnemy::m_colCycle
	ColorCycle_t3175142491 * ___m_colCycle_9;
	// UnityEngine.Vector3 DGADEnemy::m_endPoint
	Vector3_t3722313464  ___m_endPoint_10;
	// GameEvent DGADEnemy::m_hurt
	GameEvent_t4069578135 * ___m_hurt_11;
	// FloatReference DGADEnemy::m_speed
	FloatReference_t1374219879 * ___m_speed_12;
	// System.Single DGADEnemy::m_totalParticleTime
	float ___m_totalParticleTime_13;

public:
	inline static int32_t get_offset_of_m_matList_4() { return static_cast<int32_t>(offsetof(DGADEnemy_t110213370, ___m_matList_4)); }
	inline MatwColorList_t3252287760 * get_m_matList_4() const { return ___m_matList_4; }
	inline MatwColorList_t3252287760 ** get_address_of_m_matList_4() { return &___m_matList_4; }
	inline void set_m_matList_4(MatwColorList_t3252287760 * value)
	{
		___m_matList_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_matList_4), value);
	}

	inline static int32_t get_offset_of_m_deadParticle_5() { return static_cast<int32_t>(offsetof(DGADEnemy_t110213370, ___m_deadParticle_5)); }
	inline GameObject_t1113636619 * get_m_deadParticle_5() const { return ___m_deadParticle_5; }
	inline GameObject_t1113636619 ** get_address_of_m_deadParticle_5() { return &___m_deadParticle_5; }
	inline void set_m_deadParticle_5(GameObject_t1113636619 * value)
	{
		___m_deadParticle_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_deadParticle_5), value);
	}

	inline static int32_t get_offset_of_m_dgadGameLogic_6() { return static_cast<int32_t>(offsetof(DGADEnemy_t110213370, ___m_dgadGameLogic_6)); }
	inline DGADGameLogic_t3582670137 * get_m_dgadGameLogic_6() const { return ___m_dgadGameLogic_6; }
	inline DGADGameLogic_t3582670137 ** get_address_of_m_dgadGameLogic_6() { return &___m_dgadGameLogic_6; }
	inline void set_m_dgadGameLogic_6(DGADGameLogic_t3582670137 * value)
	{
		___m_dgadGameLogic_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_dgadGameLogic_6), value);
	}

	inline static int32_t get_offset_of_m_meshRenderer_7() { return static_cast<int32_t>(offsetof(DGADEnemy_t110213370, ___m_meshRenderer_7)); }
	inline MeshRenderer_t587009260 * get_m_meshRenderer_7() const { return ___m_meshRenderer_7; }
	inline MeshRenderer_t587009260 ** get_address_of_m_meshRenderer_7() { return &___m_meshRenderer_7; }
	inline void set_m_meshRenderer_7(MeshRenderer_t587009260 * value)
	{
		___m_meshRenderer_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_meshRenderer_7), value);
	}

	inline static int32_t get_offset_of_m_rb_8() { return static_cast<int32_t>(offsetof(DGADEnemy_t110213370, ___m_rb_8)); }
	inline Rigidbody_t3916780224 * get_m_rb_8() const { return ___m_rb_8; }
	inline Rigidbody_t3916780224 ** get_address_of_m_rb_8() { return &___m_rb_8; }
	inline void set_m_rb_8(Rigidbody_t3916780224 * value)
	{
		___m_rb_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_rb_8), value);
	}

	inline static int32_t get_offset_of_m_colCycle_9() { return static_cast<int32_t>(offsetof(DGADEnemy_t110213370, ___m_colCycle_9)); }
	inline ColorCycle_t3175142491 * get_m_colCycle_9() const { return ___m_colCycle_9; }
	inline ColorCycle_t3175142491 ** get_address_of_m_colCycle_9() { return &___m_colCycle_9; }
	inline void set_m_colCycle_9(ColorCycle_t3175142491 * value)
	{
		___m_colCycle_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_colCycle_9), value);
	}

	inline static int32_t get_offset_of_m_endPoint_10() { return static_cast<int32_t>(offsetof(DGADEnemy_t110213370, ___m_endPoint_10)); }
	inline Vector3_t3722313464  get_m_endPoint_10() const { return ___m_endPoint_10; }
	inline Vector3_t3722313464 * get_address_of_m_endPoint_10() { return &___m_endPoint_10; }
	inline void set_m_endPoint_10(Vector3_t3722313464  value)
	{
		___m_endPoint_10 = value;
	}

	inline static int32_t get_offset_of_m_hurt_11() { return static_cast<int32_t>(offsetof(DGADEnemy_t110213370, ___m_hurt_11)); }
	inline GameEvent_t4069578135 * get_m_hurt_11() const { return ___m_hurt_11; }
	inline GameEvent_t4069578135 ** get_address_of_m_hurt_11() { return &___m_hurt_11; }
	inline void set_m_hurt_11(GameEvent_t4069578135 * value)
	{
		___m_hurt_11 = value;
		Il2CppCodeGenWriteBarrier((&___m_hurt_11), value);
	}

	inline static int32_t get_offset_of_m_speed_12() { return static_cast<int32_t>(offsetof(DGADEnemy_t110213370, ___m_speed_12)); }
	inline FloatReference_t1374219879 * get_m_speed_12() const { return ___m_speed_12; }
	inline FloatReference_t1374219879 ** get_address_of_m_speed_12() { return &___m_speed_12; }
	inline void set_m_speed_12(FloatReference_t1374219879 * value)
	{
		___m_speed_12 = value;
		Il2CppCodeGenWriteBarrier((&___m_speed_12), value);
	}

	inline static int32_t get_offset_of_m_totalParticleTime_13() { return static_cast<int32_t>(offsetof(DGADEnemy_t110213370, ___m_totalParticleTime_13)); }
	inline float get_m_totalParticleTime_13() const { return ___m_totalParticleTime_13; }
	inline float* get_address_of_m_totalParticleTime_13() { return &___m_totalParticleTime_13; }
	inline void set_m_totalParticleTime_13(float value)
	{
		___m_totalParticleTime_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DGADENEMY_T110213370_H
#ifndef GDTBUBBLE_T1793342389_H
#define GDTBUBBLE_T1793342389_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GDTBubble
struct  GDTBubble_t1793342389  : public MonoBehaviour_t3962482529
{
public:
	// ObjectState GDTBubble::m_state
	ObjectState_t1202103565 * ___m_state_4;
	// UnityEngine.GameObject GDTBubble::m_iconObject
	GameObject_t1113636619 * ___m_iconObject_5;
	// ObjectData GDTBubble::m_iconMat
	ObjectData_t3315742923 * ___m_iconMat_6;
	// FloatReference GDTBubble::m_maxTimer
	FloatReference_t1374219879 * ___m_maxTimer_7;
	// FloatReference GDTBubble::m_minTimer
	FloatReference_t1374219879 * ___m_minTimer_8;
	// GDTGameLogic GDTBubble::m_gdtDataReference
	GDTGameLogic_t4243140947 * ___m_gdtDataReference_9;
	// UnityEngine.Rigidbody GDTBubble::m_rb
	Rigidbody_t3916780224 * ___m_rb_10;
	// System.Single GDTBubble::m_timer
	float ___m_timer_11;

public:
	inline static int32_t get_offset_of_m_state_4() { return static_cast<int32_t>(offsetof(GDTBubble_t1793342389, ___m_state_4)); }
	inline ObjectState_t1202103565 * get_m_state_4() const { return ___m_state_4; }
	inline ObjectState_t1202103565 ** get_address_of_m_state_4() { return &___m_state_4; }
	inline void set_m_state_4(ObjectState_t1202103565 * value)
	{
		___m_state_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_state_4), value);
	}

	inline static int32_t get_offset_of_m_iconObject_5() { return static_cast<int32_t>(offsetof(GDTBubble_t1793342389, ___m_iconObject_5)); }
	inline GameObject_t1113636619 * get_m_iconObject_5() const { return ___m_iconObject_5; }
	inline GameObject_t1113636619 ** get_address_of_m_iconObject_5() { return &___m_iconObject_5; }
	inline void set_m_iconObject_5(GameObject_t1113636619 * value)
	{
		___m_iconObject_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_iconObject_5), value);
	}

	inline static int32_t get_offset_of_m_iconMat_6() { return static_cast<int32_t>(offsetof(GDTBubble_t1793342389, ___m_iconMat_6)); }
	inline ObjectData_t3315742923 * get_m_iconMat_6() const { return ___m_iconMat_6; }
	inline ObjectData_t3315742923 ** get_address_of_m_iconMat_6() { return &___m_iconMat_6; }
	inline void set_m_iconMat_6(ObjectData_t3315742923 * value)
	{
		___m_iconMat_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_iconMat_6), value);
	}

	inline static int32_t get_offset_of_m_maxTimer_7() { return static_cast<int32_t>(offsetof(GDTBubble_t1793342389, ___m_maxTimer_7)); }
	inline FloatReference_t1374219879 * get_m_maxTimer_7() const { return ___m_maxTimer_7; }
	inline FloatReference_t1374219879 ** get_address_of_m_maxTimer_7() { return &___m_maxTimer_7; }
	inline void set_m_maxTimer_7(FloatReference_t1374219879 * value)
	{
		___m_maxTimer_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_maxTimer_7), value);
	}

	inline static int32_t get_offset_of_m_minTimer_8() { return static_cast<int32_t>(offsetof(GDTBubble_t1793342389, ___m_minTimer_8)); }
	inline FloatReference_t1374219879 * get_m_minTimer_8() const { return ___m_minTimer_8; }
	inline FloatReference_t1374219879 ** get_address_of_m_minTimer_8() { return &___m_minTimer_8; }
	inline void set_m_minTimer_8(FloatReference_t1374219879 * value)
	{
		___m_minTimer_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_minTimer_8), value);
	}

	inline static int32_t get_offset_of_m_gdtDataReference_9() { return static_cast<int32_t>(offsetof(GDTBubble_t1793342389, ___m_gdtDataReference_9)); }
	inline GDTGameLogic_t4243140947 * get_m_gdtDataReference_9() const { return ___m_gdtDataReference_9; }
	inline GDTGameLogic_t4243140947 ** get_address_of_m_gdtDataReference_9() { return &___m_gdtDataReference_9; }
	inline void set_m_gdtDataReference_9(GDTGameLogic_t4243140947 * value)
	{
		___m_gdtDataReference_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_gdtDataReference_9), value);
	}

	inline static int32_t get_offset_of_m_rb_10() { return static_cast<int32_t>(offsetof(GDTBubble_t1793342389, ___m_rb_10)); }
	inline Rigidbody_t3916780224 * get_m_rb_10() const { return ___m_rb_10; }
	inline Rigidbody_t3916780224 ** get_address_of_m_rb_10() { return &___m_rb_10; }
	inline void set_m_rb_10(Rigidbody_t3916780224 * value)
	{
		___m_rb_10 = value;
		Il2CppCodeGenWriteBarrier((&___m_rb_10), value);
	}

	inline static int32_t get_offset_of_m_timer_11() { return static_cast<int32_t>(offsetof(GDTBubble_t1793342389, ___m_timer_11)); }
	inline float get_m_timer_11() const { return ___m_timer_11; }
	inline float* get_address_of_m_timer_11() { return &___m_timer_11; }
	inline void set_m_timer_11(float value)
	{
		___m_timer_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GDTBUBBLE_T1793342389_H
#ifndef GDTCOMBODISPLAY_T4242164989_H
#define GDTCOMBODISPLAY_T4242164989_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GDTComboDisplay
struct  GDTComboDisplay_t4242164989  : public MonoBehaviour_t3962482529
{
public:
	// UintVariable GDTComboDisplay::currCombo
	UintVariable_t3613798951 * ___currCombo_4;
	// TMPro.TextMeshProUGUI GDTComboDisplay::comboText
	TextMeshProUGUI_t529313277 * ___comboText_5;

public:
	inline static int32_t get_offset_of_currCombo_4() { return static_cast<int32_t>(offsetof(GDTComboDisplay_t4242164989, ___currCombo_4)); }
	inline UintVariable_t3613798951 * get_currCombo_4() const { return ___currCombo_4; }
	inline UintVariable_t3613798951 ** get_address_of_currCombo_4() { return &___currCombo_4; }
	inline void set_currCombo_4(UintVariable_t3613798951 * value)
	{
		___currCombo_4 = value;
		Il2CppCodeGenWriteBarrier((&___currCombo_4), value);
	}

	inline static int32_t get_offset_of_comboText_5() { return static_cast<int32_t>(offsetof(GDTComboDisplay_t4242164989, ___comboText_5)); }
	inline TextMeshProUGUI_t529313277 * get_comboText_5() const { return ___comboText_5; }
	inline TextMeshProUGUI_t529313277 ** get_address_of_comboText_5() { return &___comboText_5; }
	inline void set_comboText_5(TextMeshProUGUI_t529313277 * value)
	{
		___comboText_5 = value;
		Il2CppCodeGenWriteBarrier((&___comboText_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GDTCOMBODISPLAY_T4242164989_H
#ifndef GDTFEVERDISPLAY_T4005242174_H
#define GDTFEVERDISPLAY_T4005242174_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GDTFeverDisplay
struct  GDTFeverDisplay_t4005242174  : public MonoBehaviour_t3962482529
{
public:
	// FloatReference GDTFeverDisplay::currFever
	FloatReference_t1374219879 * ___currFever_4;
	// FloatReference GDTFeverDisplay::maxFever
	FloatReference_t1374219879 * ___maxFever_5;
	// System.Collections.Generic.List`1<UnityEngine.GameObject> GDTFeverDisplay::feverBars
	List_1_t2585711361 * ___feverBars_6;
	// System.Collections.Generic.List`1<UnityEngine.UI.Image> GDTFeverDisplay::feverBarImage
	List_1_t4142344393 * ___feverBarImage_7;

public:
	inline static int32_t get_offset_of_currFever_4() { return static_cast<int32_t>(offsetof(GDTFeverDisplay_t4005242174, ___currFever_4)); }
	inline FloatReference_t1374219879 * get_currFever_4() const { return ___currFever_4; }
	inline FloatReference_t1374219879 ** get_address_of_currFever_4() { return &___currFever_4; }
	inline void set_currFever_4(FloatReference_t1374219879 * value)
	{
		___currFever_4 = value;
		Il2CppCodeGenWriteBarrier((&___currFever_4), value);
	}

	inline static int32_t get_offset_of_maxFever_5() { return static_cast<int32_t>(offsetof(GDTFeverDisplay_t4005242174, ___maxFever_5)); }
	inline FloatReference_t1374219879 * get_maxFever_5() const { return ___maxFever_5; }
	inline FloatReference_t1374219879 ** get_address_of_maxFever_5() { return &___maxFever_5; }
	inline void set_maxFever_5(FloatReference_t1374219879 * value)
	{
		___maxFever_5 = value;
		Il2CppCodeGenWriteBarrier((&___maxFever_5), value);
	}

	inline static int32_t get_offset_of_feverBars_6() { return static_cast<int32_t>(offsetof(GDTFeverDisplay_t4005242174, ___feverBars_6)); }
	inline List_1_t2585711361 * get_feverBars_6() const { return ___feverBars_6; }
	inline List_1_t2585711361 ** get_address_of_feverBars_6() { return &___feverBars_6; }
	inline void set_feverBars_6(List_1_t2585711361 * value)
	{
		___feverBars_6 = value;
		Il2CppCodeGenWriteBarrier((&___feverBars_6), value);
	}

	inline static int32_t get_offset_of_feverBarImage_7() { return static_cast<int32_t>(offsetof(GDTFeverDisplay_t4005242174, ___feverBarImage_7)); }
	inline List_1_t4142344393 * get_feverBarImage_7() const { return ___feverBarImage_7; }
	inline List_1_t4142344393 ** get_address_of_feverBarImage_7() { return &___feverBarImage_7; }
	inline void set_feverBarImage_7(List_1_t4142344393 * value)
	{
		___feverBarImage_7 = value;
		Il2CppCodeGenWriteBarrier((&___feverBarImage_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GDTFEVERDISPLAY_T4005242174_H
#ifndef GDTHEARTDISPLAY_T943292325_H
#define GDTHEARTDISPLAY_T943292325_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GDTHeartDisplay
struct  GDTHeartDisplay_t943292325  : public MonoBehaviour_t3962482529
{
public:
	// UintVariable GDTHeartDisplay::currHealth
	UintVariable_t3613798951 * ___currHealth_4;
	// UintVariable GDTHeartDisplay::maxHealth
	UintVariable_t3613798951 * ___maxHealth_5;
	// System.Collections.Generic.List`1<UnityEngine.UI.Image> GDTHeartDisplay::heartSprites
	List_1_t4142344393 * ___heartSprites_6;
	// UnityEngine.UI.GridLayoutGroup GDTHeartDisplay::heartLayout
	GridLayoutGroup_t3046220461 * ___heartLayout_7;
	// UnityEngine.Sprite GDTHeartDisplay::filledHeart
	Sprite_t280657092 * ___filledHeart_8;
	// UnityEngine.Sprite GDTHeartDisplay::emptyHeart
	Sprite_t280657092 * ___emptyHeart_9;

public:
	inline static int32_t get_offset_of_currHealth_4() { return static_cast<int32_t>(offsetof(GDTHeartDisplay_t943292325, ___currHealth_4)); }
	inline UintVariable_t3613798951 * get_currHealth_4() const { return ___currHealth_4; }
	inline UintVariable_t3613798951 ** get_address_of_currHealth_4() { return &___currHealth_4; }
	inline void set_currHealth_4(UintVariable_t3613798951 * value)
	{
		___currHealth_4 = value;
		Il2CppCodeGenWriteBarrier((&___currHealth_4), value);
	}

	inline static int32_t get_offset_of_maxHealth_5() { return static_cast<int32_t>(offsetof(GDTHeartDisplay_t943292325, ___maxHealth_5)); }
	inline UintVariable_t3613798951 * get_maxHealth_5() const { return ___maxHealth_5; }
	inline UintVariable_t3613798951 ** get_address_of_maxHealth_5() { return &___maxHealth_5; }
	inline void set_maxHealth_5(UintVariable_t3613798951 * value)
	{
		___maxHealth_5 = value;
		Il2CppCodeGenWriteBarrier((&___maxHealth_5), value);
	}

	inline static int32_t get_offset_of_heartSprites_6() { return static_cast<int32_t>(offsetof(GDTHeartDisplay_t943292325, ___heartSprites_6)); }
	inline List_1_t4142344393 * get_heartSprites_6() const { return ___heartSprites_6; }
	inline List_1_t4142344393 ** get_address_of_heartSprites_6() { return &___heartSprites_6; }
	inline void set_heartSprites_6(List_1_t4142344393 * value)
	{
		___heartSprites_6 = value;
		Il2CppCodeGenWriteBarrier((&___heartSprites_6), value);
	}

	inline static int32_t get_offset_of_heartLayout_7() { return static_cast<int32_t>(offsetof(GDTHeartDisplay_t943292325, ___heartLayout_7)); }
	inline GridLayoutGroup_t3046220461 * get_heartLayout_7() const { return ___heartLayout_7; }
	inline GridLayoutGroup_t3046220461 ** get_address_of_heartLayout_7() { return &___heartLayout_7; }
	inline void set_heartLayout_7(GridLayoutGroup_t3046220461 * value)
	{
		___heartLayout_7 = value;
		Il2CppCodeGenWriteBarrier((&___heartLayout_7), value);
	}

	inline static int32_t get_offset_of_filledHeart_8() { return static_cast<int32_t>(offsetof(GDTHeartDisplay_t943292325, ___filledHeart_8)); }
	inline Sprite_t280657092 * get_filledHeart_8() const { return ___filledHeart_8; }
	inline Sprite_t280657092 ** get_address_of_filledHeart_8() { return &___filledHeart_8; }
	inline void set_filledHeart_8(Sprite_t280657092 * value)
	{
		___filledHeart_8 = value;
		Il2CppCodeGenWriteBarrier((&___filledHeart_8), value);
	}

	inline static int32_t get_offset_of_emptyHeart_9() { return static_cast<int32_t>(offsetof(GDTHeartDisplay_t943292325, ___emptyHeart_9)); }
	inline Sprite_t280657092 * get_emptyHeart_9() const { return ___emptyHeart_9; }
	inline Sprite_t280657092 ** get_address_of_emptyHeart_9() { return &___emptyHeart_9; }
	inline void set_emptyHeart_9(Sprite_t280657092 * value)
	{
		___emptyHeart_9 = value;
		Il2CppCodeGenWriteBarrier((&___emptyHeart_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GDTHEARTDISPLAY_T943292325_H
#ifndef GDTPOWERUP_T3548236120_H
#define GDTPOWERUP_T3548236120_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GDTPowerUp
struct  GDTPowerUp_t3548236120  : public MonoBehaviour_t3962482529
{
public:
	// GDTGameLogic GDTPowerUp::m_gdtDataReference
	GDTGameLogic_t4243140947 * ___m_gdtDataReference_4;
	// UnityEngine.Rigidbody GDTPowerUp::m_rb
	Rigidbody_t3916780224 * ___m_rb_5;

public:
	inline static int32_t get_offset_of_m_gdtDataReference_4() { return static_cast<int32_t>(offsetof(GDTPowerUp_t3548236120, ___m_gdtDataReference_4)); }
	inline GDTGameLogic_t4243140947 * get_m_gdtDataReference_4() const { return ___m_gdtDataReference_4; }
	inline GDTGameLogic_t4243140947 ** get_address_of_m_gdtDataReference_4() { return &___m_gdtDataReference_4; }
	inline void set_m_gdtDataReference_4(GDTGameLogic_t4243140947 * value)
	{
		___m_gdtDataReference_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_gdtDataReference_4), value);
	}

	inline static int32_t get_offset_of_m_rb_5() { return static_cast<int32_t>(offsetof(GDTPowerUp_t3548236120, ___m_rb_5)); }
	inline Rigidbody_t3916780224 * get_m_rb_5() const { return ___m_rb_5; }
	inline Rigidbody_t3916780224 ** get_address_of_m_rb_5() { return &___m_rb_5; }
	inline void set_m_rb_5(Rigidbody_t3916780224 * value)
	{
		___m_rb_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_rb_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GDTPOWERUP_T3548236120_H
#ifndef GDTTEXTDISPLAY_T2902361552_H
#define GDTTEXTDISPLAY_T2902361552_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GDTTextDisplay
struct  GDTTextDisplay_t2902361552  : public MonoBehaviour_t3962482529
{
public:
	// UintVariable GDTTextDisplay::Variable
	UintVariable_t3613798951 * ___Variable_4;
	// System.String GDTTextDisplay::prefix
	String_t* ___prefix_5;
	// System.String GDTTextDisplay::postfix
	String_t* ___postfix_6;
	// TMPro.TextMeshProUGUI GDTTextDisplay::textMeshPro
	TextMeshProUGUI_t529313277 * ___textMeshPro_7;

public:
	inline static int32_t get_offset_of_Variable_4() { return static_cast<int32_t>(offsetof(GDTTextDisplay_t2902361552, ___Variable_4)); }
	inline UintVariable_t3613798951 * get_Variable_4() const { return ___Variable_4; }
	inline UintVariable_t3613798951 ** get_address_of_Variable_4() { return &___Variable_4; }
	inline void set_Variable_4(UintVariable_t3613798951 * value)
	{
		___Variable_4 = value;
		Il2CppCodeGenWriteBarrier((&___Variable_4), value);
	}

	inline static int32_t get_offset_of_prefix_5() { return static_cast<int32_t>(offsetof(GDTTextDisplay_t2902361552, ___prefix_5)); }
	inline String_t* get_prefix_5() const { return ___prefix_5; }
	inline String_t** get_address_of_prefix_5() { return &___prefix_5; }
	inline void set_prefix_5(String_t* value)
	{
		___prefix_5 = value;
		Il2CppCodeGenWriteBarrier((&___prefix_5), value);
	}

	inline static int32_t get_offset_of_postfix_6() { return static_cast<int32_t>(offsetof(GDTTextDisplay_t2902361552, ___postfix_6)); }
	inline String_t* get_postfix_6() const { return ___postfix_6; }
	inline String_t** get_address_of_postfix_6() { return &___postfix_6; }
	inline void set_postfix_6(String_t* value)
	{
		___postfix_6 = value;
		Il2CppCodeGenWriteBarrier((&___postfix_6), value);
	}

	inline static int32_t get_offset_of_textMeshPro_7() { return static_cast<int32_t>(offsetof(GDTTextDisplay_t2902361552, ___textMeshPro_7)); }
	inline TextMeshProUGUI_t529313277 * get_textMeshPro_7() const { return ___textMeshPro_7; }
	inline TextMeshProUGUI_t529313277 ** get_address_of_textMeshPro_7() { return &___textMeshPro_7; }
	inline void set_textMeshPro_7(TextMeshProUGUI_t529313277 * value)
	{
		___textMeshPro_7 = value;
		Il2CppCodeGenWriteBarrier((&___textMeshPro_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GDTTEXTDISPLAY_T2902361552_H
#ifndef GDTTIMERDISPLAY_T2542997772_H
#define GDTTIMERDISPLAY_T2542997772_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GDTTimerDisplay
struct  GDTTimerDisplay_t2542997772  : public MonoBehaviour_t3962482529
{
public:
	// FloatVariable GDTTimerDisplay::currTimer
	FloatVariable_t761998486 * ___currTimer_4;
	// FloatVariable GDTTimerDisplay::maxTimer
	FloatVariable_t761998486 * ___maxTimer_5;
	// UnityEngine.UI.Image GDTTimerDisplay::timerBar
	Image_t2670269651 * ___timerBar_6;
	// TMPro.TextMeshProUGUI GDTTimerDisplay::timerText
	TextMeshProUGUI_t529313277 * ___timerText_7;

public:
	inline static int32_t get_offset_of_currTimer_4() { return static_cast<int32_t>(offsetof(GDTTimerDisplay_t2542997772, ___currTimer_4)); }
	inline FloatVariable_t761998486 * get_currTimer_4() const { return ___currTimer_4; }
	inline FloatVariable_t761998486 ** get_address_of_currTimer_4() { return &___currTimer_4; }
	inline void set_currTimer_4(FloatVariable_t761998486 * value)
	{
		___currTimer_4 = value;
		Il2CppCodeGenWriteBarrier((&___currTimer_4), value);
	}

	inline static int32_t get_offset_of_maxTimer_5() { return static_cast<int32_t>(offsetof(GDTTimerDisplay_t2542997772, ___maxTimer_5)); }
	inline FloatVariable_t761998486 * get_maxTimer_5() const { return ___maxTimer_5; }
	inline FloatVariable_t761998486 ** get_address_of_maxTimer_5() { return &___maxTimer_5; }
	inline void set_maxTimer_5(FloatVariable_t761998486 * value)
	{
		___maxTimer_5 = value;
		Il2CppCodeGenWriteBarrier((&___maxTimer_5), value);
	}

	inline static int32_t get_offset_of_timerBar_6() { return static_cast<int32_t>(offsetof(GDTTimerDisplay_t2542997772, ___timerBar_6)); }
	inline Image_t2670269651 * get_timerBar_6() const { return ___timerBar_6; }
	inline Image_t2670269651 ** get_address_of_timerBar_6() { return &___timerBar_6; }
	inline void set_timerBar_6(Image_t2670269651 * value)
	{
		___timerBar_6 = value;
		Il2CppCodeGenWriteBarrier((&___timerBar_6), value);
	}

	inline static int32_t get_offset_of_timerText_7() { return static_cast<int32_t>(offsetof(GDTTimerDisplay_t2542997772, ___timerText_7)); }
	inline TextMeshProUGUI_t529313277 * get_timerText_7() const { return ___timerText_7; }
	inline TextMeshProUGUI_t529313277 ** get_address_of_timerText_7() { return &___timerText_7; }
	inline void set_timerText_7(TextMeshProUGUI_t529313277 * value)
	{
		___timerText_7 = value;
		Il2CppCodeGenWriteBarrier((&___timerText_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GDTTIMERDISPLAY_T2542997772_H
#ifndef INTERACTIONSHAKE_T1542128478_H
#define INTERACTIONSHAKE_T1542128478_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// InteractionShake
struct  InteractionShake_t1542128478  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Renderer InteractionShake::renderer
	Renderer_t2627027031 * ___renderer_4;
	// System.Boolean InteractionShake::isShaking
	bool ___isShaking_5;
	// System.Single InteractionShake::defaultSpeed
	float ___defaultSpeed_6;
	// System.Single InteractionShake::shakeSpeed
	float ___shakeSpeed_7;
	// System.Single InteractionShake::shakeDuration
	float ___shakeDuration_8;
	// System.Single InteractionShake::shakeTimer
	float ___shakeTimer_9;

public:
	inline static int32_t get_offset_of_renderer_4() { return static_cast<int32_t>(offsetof(InteractionShake_t1542128478, ___renderer_4)); }
	inline Renderer_t2627027031 * get_renderer_4() const { return ___renderer_4; }
	inline Renderer_t2627027031 ** get_address_of_renderer_4() { return &___renderer_4; }
	inline void set_renderer_4(Renderer_t2627027031 * value)
	{
		___renderer_4 = value;
		Il2CppCodeGenWriteBarrier((&___renderer_4), value);
	}

	inline static int32_t get_offset_of_isShaking_5() { return static_cast<int32_t>(offsetof(InteractionShake_t1542128478, ___isShaking_5)); }
	inline bool get_isShaking_5() const { return ___isShaking_5; }
	inline bool* get_address_of_isShaking_5() { return &___isShaking_5; }
	inline void set_isShaking_5(bool value)
	{
		___isShaking_5 = value;
	}

	inline static int32_t get_offset_of_defaultSpeed_6() { return static_cast<int32_t>(offsetof(InteractionShake_t1542128478, ___defaultSpeed_6)); }
	inline float get_defaultSpeed_6() const { return ___defaultSpeed_6; }
	inline float* get_address_of_defaultSpeed_6() { return &___defaultSpeed_6; }
	inline void set_defaultSpeed_6(float value)
	{
		___defaultSpeed_6 = value;
	}

	inline static int32_t get_offset_of_shakeSpeed_7() { return static_cast<int32_t>(offsetof(InteractionShake_t1542128478, ___shakeSpeed_7)); }
	inline float get_shakeSpeed_7() const { return ___shakeSpeed_7; }
	inline float* get_address_of_shakeSpeed_7() { return &___shakeSpeed_7; }
	inline void set_shakeSpeed_7(float value)
	{
		___shakeSpeed_7 = value;
	}

	inline static int32_t get_offset_of_shakeDuration_8() { return static_cast<int32_t>(offsetof(InteractionShake_t1542128478, ___shakeDuration_8)); }
	inline float get_shakeDuration_8() const { return ___shakeDuration_8; }
	inline float* get_address_of_shakeDuration_8() { return &___shakeDuration_8; }
	inline void set_shakeDuration_8(float value)
	{
		___shakeDuration_8 = value;
	}

	inline static int32_t get_offset_of_shakeTimer_9() { return static_cast<int32_t>(offsetof(InteractionShake_t1542128478, ___shakeTimer_9)); }
	inline float get_shakeTimer_9() const { return ___shakeTimer_9; }
	inline float* get_address_of_shakeTimer_9() { return &___shakeTimer_9; }
	inline void set_shakeTimer_9(float value)
	{
		___shakeTimer_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTERACTIONSHAKE_T1542128478_H
#ifndef INTERACTIVEOBJECT_T2206068412_H
#define INTERACTIVEOBJECT_T2206068412_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// InteractiveObject
struct  InteractiveObject_t2206068412  : public MonoBehaviour_t3962482529
{
public:
	// GameEvent InteractiveObject::interactionEvent
	GameEvent_t4069578135 * ___interactionEvent_4;
	// System.Boolean InteractiveObject::isSpecific
	bool ___isSpecific_5;
	// System.Collections.Generic.List`1<UnityEngine.Events.UnityEvent> InteractiveObject::eventListeners
	List_1_t4053343389 * ___eventListeners_6;
	// System.Int32 InteractiveObject::clicksToActivate
	int32_t ___clicksToActivate_7;
	// System.Int32 InteractiveObject::accumulatedClicks
	int32_t ___accumulatedClicks_8;

public:
	inline static int32_t get_offset_of_interactionEvent_4() { return static_cast<int32_t>(offsetof(InteractiveObject_t2206068412, ___interactionEvent_4)); }
	inline GameEvent_t4069578135 * get_interactionEvent_4() const { return ___interactionEvent_4; }
	inline GameEvent_t4069578135 ** get_address_of_interactionEvent_4() { return &___interactionEvent_4; }
	inline void set_interactionEvent_4(GameEvent_t4069578135 * value)
	{
		___interactionEvent_4 = value;
		Il2CppCodeGenWriteBarrier((&___interactionEvent_4), value);
	}

	inline static int32_t get_offset_of_isSpecific_5() { return static_cast<int32_t>(offsetof(InteractiveObject_t2206068412, ___isSpecific_5)); }
	inline bool get_isSpecific_5() const { return ___isSpecific_5; }
	inline bool* get_address_of_isSpecific_5() { return &___isSpecific_5; }
	inline void set_isSpecific_5(bool value)
	{
		___isSpecific_5 = value;
	}

	inline static int32_t get_offset_of_eventListeners_6() { return static_cast<int32_t>(offsetof(InteractiveObject_t2206068412, ___eventListeners_6)); }
	inline List_1_t4053343389 * get_eventListeners_6() const { return ___eventListeners_6; }
	inline List_1_t4053343389 ** get_address_of_eventListeners_6() { return &___eventListeners_6; }
	inline void set_eventListeners_6(List_1_t4053343389 * value)
	{
		___eventListeners_6 = value;
		Il2CppCodeGenWriteBarrier((&___eventListeners_6), value);
	}

	inline static int32_t get_offset_of_clicksToActivate_7() { return static_cast<int32_t>(offsetof(InteractiveObject_t2206068412, ___clicksToActivate_7)); }
	inline int32_t get_clicksToActivate_7() const { return ___clicksToActivate_7; }
	inline int32_t* get_address_of_clicksToActivate_7() { return &___clicksToActivate_7; }
	inline void set_clicksToActivate_7(int32_t value)
	{
		___clicksToActivate_7 = value;
	}

	inline static int32_t get_offset_of_accumulatedClicks_8() { return static_cast<int32_t>(offsetof(InteractiveObject_t2206068412, ___accumulatedClicks_8)); }
	inline int32_t get_accumulatedClicks_8() const { return ___accumulatedClicks_8; }
	inline int32_t* get_address_of_accumulatedClicks_8() { return &___accumulatedClicks_8; }
	inline void set_accumulatedClicks_8(int32_t value)
	{
		___accumulatedClicks_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTERACTIVEOBJECT_T2206068412_H
#ifndef MINIGAMEMANAGER_T325152534_H
#define MINIGAMEMANAGER_T325152534_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MiniGameManager
struct  MiniGameManager_t325152534  : public MonoBehaviour_t3962482529
{
public:
	// IGameLogic MiniGameManager::gameLogic
	IGameLogic_t3343192399 * ___gameLogic_4;
	// GameEvent MiniGameManager::gameIdle
	GameEvent_t4069578135 * ___gameIdle_5;
	// GameEvent MiniGameManager::gameStart
	GameEvent_t4069578135 * ___gameStart_6;
	// GameEvent MiniGameManager::gamePlay
	GameEvent_t4069578135 * ___gamePlay_7;
	// GameEvent MiniGameManager::gameOver
	GameEvent_t4069578135 * ___gameOver_8;
	// GameEvent MiniGameManager::gameEnd
	GameEvent_t4069578135 * ___gameEnd_9;
	// GameEvent MiniGameManager::gamePause
	GameEvent_t4069578135 * ___gamePause_10;
	// GameEvent MiniGameManager::gameResume
	GameEvent_t4069578135 * ___gameResume_11;
	// GameEvent MiniGameManager::gameStateChanged
	GameEvent_t4069578135 * ___gameStateChanged_12;
	// System.Collections.Generic.List`1<UnityEngine.GameObject> MiniGameManager::GDTFlowers
	List_1_t2585711361 * ___GDTFlowers_13;
	// System.Collections.Generic.List`1<UnityEngine.GameObject> MiniGameManager::GDTPowerUpFlowers
	List_1_t2585711361 * ___GDTPowerUpFlowers_14;
	// System.Collections.Generic.List`1<UnityEngine.GameObject> MiniGameManager::DGADSpawnpoints
	List_1_t2585711361 * ___DGADSpawnpoints_15;

public:
	inline static int32_t get_offset_of_gameLogic_4() { return static_cast<int32_t>(offsetof(MiniGameManager_t325152534, ___gameLogic_4)); }
	inline IGameLogic_t3343192399 * get_gameLogic_4() const { return ___gameLogic_4; }
	inline IGameLogic_t3343192399 ** get_address_of_gameLogic_4() { return &___gameLogic_4; }
	inline void set_gameLogic_4(IGameLogic_t3343192399 * value)
	{
		___gameLogic_4 = value;
		Il2CppCodeGenWriteBarrier((&___gameLogic_4), value);
	}

	inline static int32_t get_offset_of_gameIdle_5() { return static_cast<int32_t>(offsetof(MiniGameManager_t325152534, ___gameIdle_5)); }
	inline GameEvent_t4069578135 * get_gameIdle_5() const { return ___gameIdle_5; }
	inline GameEvent_t4069578135 ** get_address_of_gameIdle_5() { return &___gameIdle_5; }
	inline void set_gameIdle_5(GameEvent_t4069578135 * value)
	{
		___gameIdle_5 = value;
		Il2CppCodeGenWriteBarrier((&___gameIdle_5), value);
	}

	inline static int32_t get_offset_of_gameStart_6() { return static_cast<int32_t>(offsetof(MiniGameManager_t325152534, ___gameStart_6)); }
	inline GameEvent_t4069578135 * get_gameStart_6() const { return ___gameStart_6; }
	inline GameEvent_t4069578135 ** get_address_of_gameStart_6() { return &___gameStart_6; }
	inline void set_gameStart_6(GameEvent_t4069578135 * value)
	{
		___gameStart_6 = value;
		Il2CppCodeGenWriteBarrier((&___gameStart_6), value);
	}

	inline static int32_t get_offset_of_gamePlay_7() { return static_cast<int32_t>(offsetof(MiniGameManager_t325152534, ___gamePlay_7)); }
	inline GameEvent_t4069578135 * get_gamePlay_7() const { return ___gamePlay_7; }
	inline GameEvent_t4069578135 ** get_address_of_gamePlay_7() { return &___gamePlay_7; }
	inline void set_gamePlay_7(GameEvent_t4069578135 * value)
	{
		___gamePlay_7 = value;
		Il2CppCodeGenWriteBarrier((&___gamePlay_7), value);
	}

	inline static int32_t get_offset_of_gameOver_8() { return static_cast<int32_t>(offsetof(MiniGameManager_t325152534, ___gameOver_8)); }
	inline GameEvent_t4069578135 * get_gameOver_8() const { return ___gameOver_8; }
	inline GameEvent_t4069578135 ** get_address_of_gameOver_8() { return &___gameOver_8; }
	inline void set_gameOver_8(GameEvent_t4069578135 * value)
	{
		___gameOver_8 = value;
		Il2CppCodeGenWriteBarrier((&___gameOver_8), value);
	}

	inline static int32_t get_offset_of_gameEnd_9() { return static_cast<int32_t>(offsetof(MiniGameManager_t325152534, ___gameEnd_9)); }
	inline GameEvent_t4069578135 * get_gameEnd_9() const { return ___gameEnd_9; }
	inline GameEvent_t4069578135 ** get_address_of_gameEnd_9() { return &___gameEnd_9; }
	inline void set_gameEnd_9(GameEvent_t4069578135 * value)
	{
		___gameEnd_9 = value;
		Il2CppCodeGenWriteBarrier((&___gameEnd_9), value);
	}

	inline static int32_t get_offset_of_gamePause_10() { return static_cast<int32_t>(offsetof(MiniGameManager_t325152534, ___gamePause_10)); }
	inline GameEvent_t4069578135 * get_gamePause_10() const { return ___gamePause_10; }
	inline GameEvent_t4069578135 ** get_address_of_gamePause_10() { return &___gamePause_10; }
	inline void set_gamePause_10(GameEvent_t4069578135 * value)
	{
		___gamePause_10 = value;
		Il2CppCodeGenWriteBarrier((&___gamePause_10), value);
	}

	inline static int32_t get_offset_of_gameResume_11() { return static_cast<int32_t>(offsetof(MiniGameManager_t325152534, ___gameResume_11)); }
	inline GameEvent_t4069578135 * get_gameResume_11() const { return ___gameResume_11; }
	inline GameEvent_t4069578135 ** get_address_of_gameResume_11() { return &___gameResume_11; }
	inline void set_gameResume_11(GameEvent_t4069578135 * value)
	{
		___gameResume_11 = value;
		Il2CppCodeGenWriteBarrier((&___gameResume_11), value);
	}

	inline static int32_t get_offset_of_gameStateChanged_12() { return static_cast<int32_t>(offsetof(MiniGameManager_t325152534, ___gameStateChanged_12)); }
	inline GameEvent_t4069578135 * get_gameStateChanged_12() const { return ___gameStateChanged_12; }
	inline GameEvent_t4069578135 ** get_address_of_gameStateChanged_12() { return &___gameStateChanged_12; }
	inline void set_gameStateChanged_12(GameEvent_t4069578135 * value)
	{
		___gameStateChanged_12 = value;
		Il2CppCodeGenWriteBarrier((&___gameStateChanged_12), value);
	}

	inline static int32_t get_offset_of_GDTFlowers_13() { return static_cast<int32_t>(offsetof(MiniGameManager_t325152534, ___GDTFlowers_13)); }
	inline List_1_t2585711361 * get_GDTFlowers_13() const { return ___GDTFlowers_13; }
	inline List_1_t2585711361 ** get_address_of_GDTFlowers_13() { return &___GDTFlowers_13; }
	inline void set_GDTFlowers_13(List_1_t2585711361 * value)
	{
		___GDTFlowers_13 = value;
		Il2CppCodeGenWriteBarrier((&___GDTFlowers_13), value);
	}

	inline static int32_t get_offset_of_GDTPowerUpFlowers_14() { return static_cast<int32_t>(offsetof(MiniGameManager_t325152534, ___GDTPowerUpFlowers_14)); }
	inline List_1_t2585711361 * get_GDTPowerUpFlowers_14() const { return ___GDTPowerUpFlowers_14; }
	inline List_1_t2585711361 ** get_address_of_GDTPowerUpFlowers_14() { return &___GDTPowerUpFlowers_14; }
	inline void set_GDTPowerUpFlowers_14(List_1_t2585711361 * value)
	{
		___GDTPowerUpFlowers_14 = value;
		Il2CppCodeGenWriteBarrier((&___GDTPowerUpFlowers_14), value);
	}

	inline static int32_t get_offset_of_DGADSpawnpoints_15() { return static_cast<int32_t>(offsetof(MiniGameManager_t325152534, ___DGADSpawnpoints_15)); }
	inline List_1_t2585711361 * get_DGADSpawnpoints_15() const { return ___DGADSpawnpoints_15; }
	inline List_1_t2585711361 ** get_address_of_DGADSpawnpoints_15() { return &___DGADSpawnpoints_15; }
	inline void set_DGADSpawnpoints_15(List_1_t2585711361 * value)
	{
		___DGADSpawnpoints_15 = value;
		Il2CppCodeGenWriteBarrier((&___DGADSpawnpoints_15), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MINIGAMEMANAGER_T325152534_H
#ifndef OBJECTPOOL_T251773294_H
#define OBJECTPOOL_T251773294_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ObjectPool
struct  ObjectPool_t251773294  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Transform ObjectPool::m_GDTParent
	Transform_t3600365921 * ___m_GDTParent_5;
	// UnityEngine.Transform ObjectPool::m_DGADParent
	Transform_t3600365921 * ___m_DGADParent_6;
	// System.UInt32 ObjectPool::m_amountToSpawn
	uint32_t ___m_amountToSpawn_7;
	// GameObjectList ObjectPool::m_prefabList
	GameObjectList_t328636558 * ___m_prefabList_8;
	// System.Collections.Generic.Dictionary`2<System.Int32,System.Collections.Generic.List`1<UnityEngine.GameObject>> ObjectPool::m_objectDic
	Dictionary_2_t1474424692 * ___m_objectDic_9;

public:
	inline static int32_t get_offset_of_m_GDTParent_5() { return static_cast<int32_t>(offsetof(ObjectPool_t251773294, ___m_GDTParent_5)); }
	inline Transform_t3600365921 * get_m_GDTParent_5() const { return ___m_GDTParent_5; }
	inline Transform_t3600365921 ** get_address_of_m_GDTParent_5() { return &___m_GDTParent_5; }
	inline void set_m_GDTParent_5(Transform_t3600365921 * value)
	{
		___m_GDTParent_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_GDTParent_5), value);
	}

	inline static int32_t get_offset_of_m_DGADParent_6() { return static_cast<int32_t>(offsetof(ObjectPool_t251773294, ___m_DGADParent_6)); }
	inline Transform_t3600365921 * get_m_DGADParent_6() const { return ___m_DGADParent_6; }
	inline Transform_t3600365921 ** get_address_of_m_DGADParent_6() { return &___m_DGADParent_6; }
	inline void set_m_DGADParent_6(Transform_t3600365921 * value)
	{
		___m_DGADParent_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_DGADParent_6), value);
	}

	inline static int32_t get_offset_of_m_amountToSpawn_7() { return static_cast<int32_t>(offsetof(ObjectPool_t251773294, ___m_amountToSpawn_7)); }
	inline uint32_t get_m_amountToSpawn_7() const { return ___m_amountToSpawn_7; }
	inline uint32_t* get_address_of_m_amountToSpawn_7() { return &___m_amountToSpawn_7; }
	inline void set_m_amountToSpawn_7(uint32_t value)
	{
		___m_amountToSpawn_7 = value;
	}

	inline static int32_t get_offset_of_m_prefabList_8() { return static_cast<int32_t>(offsetof(ObjectPool_t251773294, ___m_prefabList_8)); }
	inline GameObjectList_t328636558 * get_m_prefabList_8() const { return ___m_prefabList_8; }
	inline GameObjectList_t328636558 ** get_address_of_m_prefabList_8() { return &___m_prefabList_8; }
	inline void set_m_prefabList_8(GameObjectList_t328636558 * value)
	{
		___m_prefabList_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_prefabList_8), value);
	}

	inline static int32_t get_offset_of_m_objectDic_9() { return static_cast<int32_t>(offsetof(ObjectPool_t251773294, ___m_objectDic_9)); }
	inline Dictionary_2_t1474424692 * get_m_objectDic_9() const { return ___m_objectDic_9; }
	inline Dictionary_2_t1474424692 ** get_address_of_m_objectDic_9() { return &___m_objectDic_9; }
	inline void set_m_objectDic_9(Dictionary_2_t1474424692 * value)
	{
		___m_objectDic_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_objectDic_9), value);
	}
};

struct ObjectPool_t251773294_StaticFields
{
public:
	// ObjectPool ObjectPool::m_instance
	ObjectPool_t251773294 * ___m_instance_4;

public:
	inline static int32_t get_offset_of_m_instance_4() { return static_cast<int32_t>(offsetof(ObjectPool_t251773294_StaticFields, ___m_instance_4)); }
	inline ObjectPool_t251773294 * get_m_instance_4() const { return ___m_instance_4; }
	inline ObjectPool_t251773294 ** get_address_of_m_instance_4() { return &___m_instance_4; }
	inline void set_m_instance_4(ObjectPool_t251773294 * value)
	{
		___m_instance_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_instance_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OBJECTPOOL_T251773294_H
#ifndef OPENWEBSITE_T1870304370_H
#define OPENWEBSITE_T1870304370_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// OpenWebsite
struct  OpenWebsite_t1870304370  : public MonoBehaviour_t3962482529
{
public:
	// System.String OpenWebsite::url
	String_t* ___url_4;

public:
	inline static int32_t get_offset_of_url_4() { return static_cast<int32_t>(offsetof(OpenWebsite_t1870304370, ___url_4)); }
	inline String_t* get_url_4() const { return ___url_4; }
	inline String_t** get_address_of_url_4() { return &___url_4; }
	inline void set_url_4(String_t* value)
	{
		___url_4 = value;
		Il2CppCodeGenWriteBarrier((&___url_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OPENWEBSITE_T1870304370_H
#ifndef RAINBOWCYCLE_T3082184821_H
#define RAINBOWCYCLE_T3082184821_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RainbowCycle
struct  RainbowCycle_t3082184821  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.UI.Image RainbowCycle::m_image
	Image_t2670269651 * ___m_image_4;
	// UnityEngine.Vector3 RainbowCycle::m_rainbowColor
	Vector3_t3722313464  ___m_rainbowColor_5;
	// System.Boolean RainbowCycle::m_changeR
	bool ___m_changeR_6;
	// System.Boolean RainbowCycle::m_changeG
	bool ___m_changeG_7;
	// System.Boolean RainbowCycle::m_changeB
	bool ___m_changeB_8;
	// System.Single RainbowCycle::m_speed
	float ___m_speed_9;

public:
	inline static int32_t get_offset_of_m_image_4() { return static_cast<int32_t>(offsetof(RainbowCycle_t3082184821, ___m_image_4)); }
	inline Image_t2670269651 * get_m_image_4() const { return ___m_image_4; }
	inline Image_t2670269651 ** get_address_of_m_image_4() { return &___m_image_4; }
	inline void set_m_image_4(Image_t2670269651 * value)
	{
		___m_image_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_image_4), value);
	}

	inline static int32_t get_offset_of_m_rainbowColor_5() { return static_cast<int32_t>(offsetof(RainbowCycle_t3082184821, ___m_rainbowColor_5)); }
	inline Vector3_t3722313464  get_m_rainbowColor_5() const { return ___m_rainbowColor_5; }
	inline Vector3_t3722313464 * get_address_of_m_rainbowColor_5() { return &___m_rainbowColor_5; }
	inline void set_m_rainbowColor_5(Vector3_t3722313464  value)
	{
		___m_rainbowColor_5 = value;
	}

	inline static int32_t get_offset_of_m_changeR_6() { return static_cast<int32_t>(offsetof(RainbowCycle_t3082184821, ___m_changeR_6)); }
	inline bool get_m_changeR_6() const { return ___m_changeR_6; }
	inline bool* get_address_of_m_changeR_6() { return &___m_changeR_6; }
	inline void set_m_changeR_6(bool value)
	{
		___m_changeR_6 = value;
	}

	inline static int32_t get_offset_of_m_changeG_7() { return static_cast<int32_t>(offsetof(RainbowCycle_t3082184821, ___m_changeG_7)); }
	inline bool get_m_changeG_7() const { return ___m_changeG_7; }
	inline bool* get_address_of_m_changeG_7() { return &___m_changeG_7; }
	inline void set_m_changeG_7(bool value)
	{
		___m_changeG_7 = value;
	}

	inline static int32_t get_offset_of_m_changeB_8() { return static_cast<int32_t>(offsetof(RainbowCycle_t3082184821, ___m_changeB_8)); }
	inline bool get_m_changeB_8() const { return ___m_changeB_8; }
	inline bool* get_address_of_m_changeB_8() { return &___m_changeB_8; }
	inline void set_m_changeB_8(bool value)
	{
		___m_changeB_8 = value;
	}

	inline static int32_t get_offset_of_m_speed_9() { return static_cast<int32_t>(offsetof(RainbowCycle_t3082184821, ___m_speed_9)); }
	inline float get_m_speed_9() const { return ___m_speed_9; }
	inline float* get_address_of_m_speed_9() { return &___m_speed_9; }
	inline void set_m_speed_9(float value)
	{
		___m_speed_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RAINBOWCYCLE_T3082184821_H
#ifndef RANDOMTEXT_T1675857450_H
#define RANDOMTEXT_T1675857450_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RandomText
struct  RandomText_t1675857450  : public MonoBehaviour_t3962482529
{
public:
	// TMPro.TextMeshProUGUI RandomText::textTarget
	TextMeshProUGUI_t529313277 * ___textTarget_4;
	// RandomTextData RandomText::textDatabase
	RandomTextData_t1210110652 * ___textDatabase_5;
	// System.Int32 RandomText::currIndex
	int32_t ___currIndex_6;

public:
	inline static int32_t get_offset_of_textTarget_4() { return static_cast<int32_t>(offsetof(RandomText_t1675857450, ___textTarget_4)); }
	inline TextMeshProUGUI_t529313277 * get_textTarget_4() const { return ___textTarget_4; }
	inline TextMeshProUGUI_t529313277 ** get_address_of_textTarget_4() { return &___textTarget_4; }
	inline void set_textTarget_4(TextMeshProUGUI_t529313277 * value)
	{
		___textTarget_4 = value;
		Il2CppCodeGenWriteBarrier((&___textTarget_4), value);
	}

	inline static int32_t get_offset_of_textDatabase_5() { return static_cast<int32_t>(offsetof(RandomText_t1675857450, ___textDatabase_5)); }
	inline RandomTextData_t1210110652 * get_textDatabase_5() const { return ___textDatabase_5; }
	inline RandomTextData_t1210110652 ** get_address_of_textDatabase_5() { return &___textDatabase_5; }
	inline void set_textDatabase_5(RandomTextData_t1210110652 * value)
	{
		___textDatabase_5 = value;
		Il2CppCodeGenWriteBarrier((&___textDatabase_5), value);
	}

	inline static int32_t get_offset_of_currIndex_6() { return static_cast<int32_t>(offsetof(RandomText_t1675857450, ___currIndex_6)); }
	inline int32_t get_currIndex_6() const { return ___currIndex_6; }
	inline int32_t* get_address_of_currIndex_6() { return &___currIndex_6; }
	inline void set_currIndex_6(int32_t value)
	{
		___currIndex_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RANDOMTEXT_T1675857450_H
#ifndef SPLASHCONTROLLER_T66433326_H
#define SPLASHCONTROLLER_T66433326_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SplashController
struct  SplashController_t66433326  : public MonoBehaviour_t3962482529
{
public:
	// System.Collections.Generic.List`1<UnityEngine.GameObject> SplashController::splashEffects
	List_1_t2585711361 * ___splashEffects_4;

public:
	inline static int32_t get_offset_of_splashEffects_4() { return static_cast<int32_t>(offsetof(SplashController_t66433326, ___splashEffects_4)); }
	inline List_1_t2585711361 * get_splashEffects_4() const { return ___splashEffects_4; }
	inline List_1_t2585711361 ** get_address_of_splashEffects_4() { return &___splashEffects_4; }
	inline void set_splashEffects_4(List_1_t2585711361 * value)
	{
		___splashEffects_4 = value;
		Il2CppCodeGenWriteBarrier((&___splashEffects_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPLASHCONTROLLER_T66433326_H
#ifndef DGADSPAWNER_T314780093_H
#define DGADSPAWNER_T314780093_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DGADSpawner
struct  DGADSpawner_t314780093  : public BaseSpawner_t2872291831
{
public:
	// UnityEngine.Vector3 DGADSpawner::m_maxArea
	Vector3_t3722313464  ___m_maxArea_7;
	// UnityEngine.Transform DGADSpawner::m_endPoint
	Transform_t3600365921 * ___m_endPoint_8;

public:
	inline static int32_t get_offset_of_m_maxArea_7() { return static_cast<int32_t>(offsetof(DGADSpawner_t314780093, ___m_maxArea_7)); }
	inline Vector3_t3722313464  get_m_maxArea_7() const { return ___m_maxArea_7; }
	inline Vector3_t3722313464 * get_address_of_m_maxArea_7() { return &___m_maxArea_7; }
	inline void set_m_maxArea_7(Vector3_t3722313464  value)
	{
		___m_maxArea_7 = value;
	}

	inline static int32_t get_offset_of_m_endPoint_8() { return static_cast<int32_t>(offsetof(DGADSpawner_t314780093, ___m_endPoint_8)); }
	inline Transform_t3600365921 * get_m_endPoint_8() const { return ___m_endPoint_8; }
	inline Transform_t3600365921 ** get_address_of_m_endPoint_8() { return &___m_endPoint_8; }
	inline void set_m_endPoint_8(Transform_t3600365921 * value)
	{
		___m_endPoint_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_endPoint_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DGADSPAWNER_T314780093_H
#ifndef GDTSPAWNER_T632591313_H
#define GDTSPAWNER_T632591313_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GDTSpawner
struct  GDTSpawner_t632591313  : public BaseSpawner_t2872291831
{
public:
	// FloatReference GDTSpawner::m_shootForceMax
	FloatReference_t1374219879 * ___m_shootForceMax_7;
	// FloatReference GDTSpawner::m_shootForceMin
	FloatReference_t1374219879 * ___m_shootForceMin_8;
	// FloatReference GDTSpawner::m_riggedChance
	FloatReference_t1374219879 * ___m_riggedChance_9;
	// UnityEngine.Animator GDTSpawner::m_animator
	Animator_t434523843 * ___m_animator_10;
	// System.UInt32 GDTSpawner::m_amountToShoot
	uint32_t ___m_amountToShoot_11;
	// System.Boolean GDTSpawner::m_isFever
	bool ___m_isFever_12;
	// System.Boolean GDTSpawner::m_isPowerUp
	bool ___m_isPowerUp_13;

public:
	inline static int32_t get_offset_of_m_shootForceMax_7() { return static_cast<int32_t>(offsetof(GDTSpawner_t632591313, ___m_shootForceMax_7)); }
	inline FloatReference_t1374219879 * get_m_shootForceMax_7() const { return ___m_shootForceMax_7; }
	inline FloatReference_t1374219879 ** get_address_of_m_shootForceMax_7() { return &___m_shootForceMax_7; }
	inline void set_m_shootForceMax_7(FloatReference_t1374219879 * value)
	{
		___m_shootForceMax_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_shootForceMax_7), value);
	}

	inline static int32_t get_offset_of_m_shootForceMin_8() { return static_cast<int32_t>(offsetof(GDTSpawner_t632591313, ___m_shootForceMin_8)); }
	inline FloatReference_t1374219879 * get_m_shootForceMin_8() const { return ___m_shootForceMin_8; }
	inline FloatReference_t1374219879 ** get_address_of_m_shootForceMin_8() { return &___m_shootForceMin_8; }
	inline void set_m_shootForceMin_8(FloatReference_t1374219879 * value)
	{
		___m_shootForceMin_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_shootForceMin_8), value);
	}

	inline static int32_t get_offset_of_m_riggedChance_9() { return static_cast<int32_t>(offsetof(GDTSpawner_t632591313, ___m_riggedChance_9)); }
	inline FloatReference_t1374219879 * get_m_riggedChance_9() const { return ___m_riggedChance_9; }
	inline FloatReference_t1374219879 ** get_address_of_m_riggedChance_9() { return &___m_riggedChance_9; }
	inline void set_m_riggedChance_9(FloatReference_t1374219879 * value)
	{
		___m_riggedChance_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_riggedChance_9), value);
	}

	inline static int32_t get_offset_of_m_animator_10() { return static_cast<int32_t>(offsetof(GDTSpawner_t632591313, ___m_animator_10)); }
	inline Animator_t434523843 * get_m_animator_10() const { return ___m_animator_10; }
	inline Animator_t434523843 ** get_address_of_m_animator_10() { return &___m_animator_10; }
	inline void set_m_animator_10(Animator_t434523843 * value)
	{
		___m_animator_10 = value;
		Il2CppCodeGenWriteBarrier((&___m_animator_10), value);
	}

	inline static int32_t get_offset_of_m_amountToShoot_11() { return static_cast<int32_t>(offsetof(GDTSpawner_t632591313, ___m_amountToShoot_11)); }
	inline uint32_t get_m_amountToShoot_11() const { return ___m_amountToShoot_11; }
	inline uint32_t* get_address_of_m_amountToShoot_11() { return &___m_amountToShoot_11; }
	inline void set_m_amountToShoot_11(uint32_t value)
	{
		___m_amountToShoot_11 = value;
	}

	inline static int32_t get_offset_of_m_isFever_12() { return static_cast<int32_t>(offsetof(GDTSpawner_t632591313, ___m_isFever_12)); }
	inline bool get_m_isFever_12() const { return ___m_isFever_12; }
	inline bool* get_address_of_m_isFever_12() { return &___m_isFever_12; }
	inline void set_m_isFever_12(bool value)
	{
		___m_isFever_12 = value;
	}

	inline static int32_t get_offset_of_m_isPowerUp_13() { return static_cast<int32_t>(offsetof(GDTSpawner_t632591313, ___m_isPowerUp_13)); }
	inline bool get_m_isPowerUp_13() const { return ___m_isPowerUp_13; }
	inline bool* get_address_of_m_isPowerUp_13() { return &___m_isPowerUp_13; }
	inline void set_m_isPowerUp_13(bool value)
	{
		___m_isPowerUp_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GDTSPAWNER_T632591313_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2900 = { sizeof (MatwColorList_t3252287760), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2901 = { sizeof (MeshHolderList_t1122110985), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2902 = { sizeof (ScriptableObjectList_t1377352194), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2903 = { sizeof (ShortVariable_t886426368), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2904 = { sizeof (UintVariable_t3613798951), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2905 = { sizeof (Vector3List_t3337973461), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2906 = { sizeof (InteractiveObject_t2206068412), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2906[5] = 
{
	InteractiveObject_t2206068412::get_offset_of_interactionEvent_4(),
	InteractiveObject_t2206068412::get_offset_of_isSpecific_5(),
	InteractiveObject_t2206068412::get_offset_of_eventListeners_6(),
	InteractiveObject_t2206068412::get_offset_of_clicksToActivate_7(),
	InteractiveObject_t2206068412::get_offset_of_accumulatedClicks_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2907 = { sizeof (MiniGameManager_t325152534), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2907[12] = 
{
	MiniGameManager_t325152534::get_offset_of_gameLogic_4(),
	MiniGameManager_t325152534::get_offset_of_gameIdle_5(),
	MiniGameManager_t325152534::get_offset_of_gameStart_6(),
	MiniGameManager_t325152534::get_offset_of_gamePlay_7(),
	MiniGameManager_t325152534::get_offset_of_gameOver_8(),
	MiniGameManager_t325152534::get_offset_of_gameEnd_9(),
	MiniGameManager_t325152534::get_offset_of_gamePause_10(),
	MiniGameManager_t325152534::get_offset_of_gameResume_11(),
	MiniGameManager_t325152534::get_offset_of_gameStateChanged_12(),
	MiniGameManager_t325152534::get_offset_of_GDTFlowers_13(),
	MiniGameManager_t325152534::get_offset_of_GDTPowerUpFlowers_14(),
	MiniGameManager_t325152534::get_offset_of_DGADSpawnpoints_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2908 = { sizeof (BaseSpawner_t2872291831), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2908[3] = 
{
	BaseSpawner_t2872291831::get_offset_of_m_data_4(),
	BaseSpawner_t2872291831::get_offset_of_m_objectToSpawn_5(),
	BaseSpawner_t2872291831::get_offset_of_m_spawnPoint_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2909 = { sizeof (DGADAssignment_t2646297173), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2909[12] = 
{
	DGADAssignment_t2646297173::get_offset_of_m_meshList_4(),
	DGADAssignment_t2646297173::get_offset_of_m_assignmentUpParticle_5(),
	DGADAssignment_t2646297173::get_offset_of_m_assignmentProgress_6(),
	DGADAssignment_t2646297173::get_offset_of_m_energyIncrement_7(),
	DGADAssignment_t2646297173::get_offset_of_m_totalParticleTime_8(),
	DGADAssignment_t2646297173::get_offset_of_m_meshFilter_9(),
	DGADAssignment_t2646297173::get_offset_of_m_meshRenderer_10(),
	DGADAssignment_t2646297173::get_offset_of_m_meshCollider_11(),
	DGADAssignment_t2646297173::get_offset_of_m_stage_12(),
	DGADAssignment_t2646297173::get_offset_of_m_winEvent_13(),
	DGADAssignment_t2646297173::get_offset_of_m_killAllEnemy_14(),
	DGADAssignment_t2646297173::get_offset_of_m_dgadLogic_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2910 = { sizeof (DGADEnemy_t110213370), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2910[10] = 
{
	DGADEnemy_t110213370::get_offset_of_m_matList_4(),
	DGADEnemy_t110213370::get_offset_of_m_deadParticle_5(),
	DGADEnemy_t110213370::get_offset_of_m_dgadGameLogic_6(),
	DGADEnemy_t110213370::get_offset_of_m_meshRenderer_7(),
	DGADEnemy_t110213370::get_offset_of_m_rb_8(),
	DGADEnemy_t110213370::get_offset_of_m_colCycle_9(),
	DGADEnemy_t110213370::get_offset_of_m_endPoint_10(),
	DGADEnemy_t110213370::get_offset_of_m_hurt_11(),
	DGADEnemy_t110213370::get_offset_of_m_speed_12(),
	DGADEnemy_t110213370::get_offset_of_m_totalParticleTime_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2911 = { sizeof (DGADSpawner_t314780093), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2911[2] = 
{
	DGADSpawner_t314780093::get_offset_of_m_maxArea_7(),
	DGADSpawner_t314780093::get_offset_of_m_endPoint_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2912 = { sizeof (GDTBubble_t1793342389), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2912[8] = 
{
	GDTBubble_t1793342389::get_offset_of_m_state_4(),
	GDTBubble_t1793342389::get_offset_of_m_iconObject_5(),
	GDTBubble_t1793342389::get_offset_of_m_iconMat_6(),
	GDTBubble_t1793342389::get_offset_of_m_maxTimer_7(),
	GDTBubble_t1793342389::get_offset_of_m_minTimer_8(),
	GDTBubble_t1793342389::get_offset_of_m_gdtDataReference_9(),
	GDTBubble_t1793342389::get_offset_of_m_rb_10(),
	GDTBubble_t1793342389::get_offset_of_m_timer_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2913 = { sizeof (GDTPowerUp_t3548236120), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2913[2] = 
{
	GDTPowerUp_t3548236120::get_offset_of_m_gdtDataReference_4(),
	GDTPowerUp_t3548236120::get_offset_of_m_rb_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2914 = { sizeof (GDTSpawner_t632591313), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2914[7] = 
{
	GDTSpawner_t632591313::get_offset_of_m_shootForceMax_7(),
	GDTSpawner_t632591313::get_offset_of_m_shootForceMin_8(),
	GDTSpawner_t632591313::get_offset_of_m_riggedChance_9(),
	GDTSpawner_t632591313::get_offset_of_m_animator_10(),
	GDTSpawner_t632591313::get_offset_of_m_amountToShoot_11(),
	GDTSpawner_t632591313::get_offset_of_m_isFever_12(),
	GDTSpawner_t632591313::get_offset_of_m_isPowerUp_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2915 = { sizeof (ObjectData_t3315742923), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2915[1] = 
{
	ObjectData_t3315742923::get_offset_of_m_dataList_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2916 = { sizeof (ObjectPool_t251773294), -1, sizeof(ObjectPool_t251773294_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2916[6] = 
{
	ObjectPool_t251773294_StaticFields::get_offset_of_m_instance_4(),
	ObjectPool_t251773294::get_offset_of_m_GDTParent_5(),
	ObjectPool_t251773294::get_offset_of_m_DGADParent_6(),
	ObjectPool_t251773294::get_offset_of_m_amountToSpawn_7(),
	ObjectPool_t251773294::get_offset_of_m_prefabList_8(),
	ObjectPool_t251773294::get_offset_of_m_objectDic_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2917 = { sizeof (Sound_t3007421746), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2917[9] = 
{
	Sound_t3007421746::get_offset_of_name_0(),
	Sound_t3007421746::get_offset_of_clip_1(),
	Sound_t3007421746::get_offset_of_volume_2(),
	Sound_t3007421746::get_offset_of_volumeVariance_3(),
	Sound_t3007421746::get_offset_of_pitch_4(),
	Sound_t3007421746::get_offset_of_pitchVariance_5(),
	Sound_t3007421746::get_offset_of_loop_6(),
	Sound_t3007421746::get_offset_of_mixerGroup_7(),
	Sound_t3007421746::get_offset_of_source_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2918 = { sizeof (SplashController_t66433326), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2918[1] = 
{
	SplashController_t66433326::get_offset_of_splashEffects_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2919 = { sizeof (ButtonFireEvent_t125960542), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2919[1] = 
{
	ButtonFireEvent_t125960542::get_offset_of_gameEventsToFire_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2920 = { sizeof (ColorCycle_t3175142491), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2920[4] = 
{
	ColorCycle_t3175142491::get_offset_of_meshRenderer_4(),
	ColorCycle_t3175142491::get_offset_of_speed_5(),
	ColorCycle_t3175142491::get_offset_of_color_6(),
	ColorCycle_t3175142491::get_offset_of_fixedColor_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2921 = { sizeof (AmmoToggle_t520557478), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2921[7] = 
{
	AmmoToggle_t520557478::get_offset_of_toggleAmmo_4(),
	AmmoToggle_t520557478::get_offset_of_currEnergy_5(),
	AmmoToggle_t520557478::get_offset_of_maxEnergy_6(),
	AmmoToggle_t520557478::get_offset_of_enemyAmmo_7(),
	AmmoToggle_t520557478::get_offset_of_energyAmmo_8(),
	AmmoToggle_t520557478::get_offset_of_enemyAmmoCrosshair_9(),
	AmmoToggle_t520557478::get_offset_of_energyAmmoCrosshair_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2922 = { sizeof (CrosshairToggle_t3349641956), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2922[2] = 
{
	CrosshairToggle_t3349641956::get_offset_of_energyCrosshair_4(),
	CrosshairToggle_t3349641956::get_offset_of_gunCrosshair_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2923 = { sizeof (GDTComboDisplay_t4242164989), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2923[2] = 
{
	GDTComboDisplay_t4242164989::get_offset_of_currCombo_4(),
	GDTComboDisplay_t4242164989::get_offset_of_comboText_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2924 = { sizeof (GDTFeverDisplay_t4005242174), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2924[4] = 
{
	GDTFeverDisplay_t4005242174::get_offset_of_currFever_4(),
	GDTFeverDisplay_t4005242174::get_offset_of_maxFever_5(),
	GDTFeverDisplay_t4005242174::get_offset_of_feverBars_6(),
	GDTFeverDisplay_t4005242174::get_offset_of_feverBarImage_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2925 = { sizeof (GDTHeartDisplay_t943292325), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2925[6] = 
{
	GDTHeartDisplay_t943292325::get_offset_of_currHealth_4(),
	GDTHeartDisplay_t943292325::get_offset_of_maxHealth_5(),
	GDTHeartDisplay_t943292325::get_offset_of_heartSprites_6(),
	GDTHeartDisplay_t943292325::get_offset_of_heartLayout_7(),
	GDTHeartDisplay_t943292325::get_offset_of_filledHeart_8(),
	GDTHeartDisplay_t943292325::get_offset_of_emptyHeart_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2926 = { sizeof (GDTTextDisplay_t2902361552), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2926[4] = 
{
	GDTTextDisplay_t2902361552::get_offset_of_Variable_4(),
	GDTTextDisplay_t2902361552::get_offset_of_prefix_5(),
	GDTTextDisplay_t2902361552::get_offset_of_postfix_6(),
	GDTTextDisplay_t2902361552::get_offset_of_textMeshPro_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2927 = { sizeof (GDTTimerDisplay_t2542997772), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2927[4] = 
{
	GDTTimerDisplay_t2542997772::get_offset_of_currTimer_4(),
	GDTTimerDisplay_t2542997772::get_offset_of_maxTimer_5(),
	GDTTimerDisplay_t2542997772::get_offset_of_timerBar_6(),
	GDTTimerDisplay_t2542997772::get_offset_of_timerText_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2928 = { sizeof (InteractionShake_t1542128478), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2928[6] = 
{
	InteractionShake_t1542128478::get_offset_of_renderer_4(),
	InteractionShake_t1542128478::get_offset_of_isShaking_5(),
	InteractionShake_t1542128478::get_offset_of_defaultSpeed_6(),
	InteractionShake_t1542128478::get_offset_of_shakeSpeed_7(),
	InteractionShake_t1542128478::get_offset_of_shakeDuration_8(),
	InteractionShake_t1542128478::get_offset_of_shakeTimer_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2929 = { sizeof (HSBColor_t1247197400)+ sizeof (RuntimeObject), sizeof(HSBColor_t1247197400 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2929[4] = 
{
	HSBColor_t1247197400::get_offset_of_h_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	HSBColor_t1247197400::get_offset_of_s_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	HSBColor_t1247197400::get_offset_of_b_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	HSBColor_t1247197400::get_offset_of_a_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2930 = { sizeof (OpenWebsite_t1870304370), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2930[1] = 
{
	OpenWebsite_t1870304370::get_offset_of_url_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2931 = { sizeof (RainbowCycle_t3082184821), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2931[6] = 
{
	RainbowCycle_t3082184821::get_offset_of_m_image_4(),
	RainbowCycle_t3082184821::get_offset_of_m_rainbowColor_5(),
	RainbowCycle_t3082184821::get_offset_of_m_changeR_6(),
	RainbowCycle_t3082184821::get_offset_of_m_changeG_7(),
	RainbowCycle_t3082184821::get_offset_of_m_changeB_8(),
	RainbowCycle_t3082184821::get_offset_of_m_speed_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2932 = { sizeof (RandomText_t1675857450), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2932[3] = 
{
	RandomText_t1675857450::get_offset_of_textTarget_4(),
	RandomText_t1675857450::get_offset_of_textDatabase_5(),
	RandomText_t1675857450::get_offset_of_currIndex_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2933 = { sizeof (RandomTextData_t1210110652), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2933[1] = 
{
	RandomTextData_t1210110652::get_offset_of_textDatabase_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2934 = { sizeof (U3CModuleU3E_t692745555), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2935 = { sizeof (DOTweenModuleAudio_t3475130407), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2936 = { sizeof (U3CDOFadeU3Ec__AnonStorey0_t2614714771), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2936[1] = 
{
	U3CDOFadeU3Ec__AnonStorey0_t2614714771::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2937 = { sizeof (U3CDOPitchU3Ec__AnonStorey1_t3536537354), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2937[1] = 
{
	U3CDOPitchU3Ec__AnonStorey1_t3536537354::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2938 = { sizeof (U3CDOSetFloatU3Ec__AnonStorey2_t1929304968), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2938[2] = 
{
	U3CDOSetFloatU3Ec__AnonStorey2_t1929304968::get_offset_of_target_0(),
	U3CDOSetFloatU3Ec__AnonStorey2_t1929304968::get_offset_of_floatName_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2939 = { sizeof (DOTweenModulePhysics_t1584043136), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2940 = { sizeof (U3CDOMoveU3Ec__AnonStorey0_t4100116351), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2940[1] = 
{
	U3CDOMoveU3Ec__AnonStorey0_t4100116351::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2941 = { sizeof (U3CDOMoveXU3Ec__AnonStorey1_t4110287298), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2941[1] = 
{
	U3CDOMoveXU3Ec__AnonStorey1_t4110287298::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2942 = { sizeof (U3CDOMoveYU3Ec__AnonStorey2_t1388526791), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2942[1] = 
{
	U3CDOMoveYU3Ec__AnonStorey2_t1388526791::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2943 = { sizeof (U3CDOMoveZU3Ec__AnonStorey3_t3727291876), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2943[1] = 
{
	U3CDOMoveZU3Ec__AnonStorey3_t3727291876::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2944 = { sizeof (U3CDORotateU3Ec__AnonStorey4_t2104260424), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2944[1] = 
{
	U3CDORotateU3Ec__AnonStorey4_t2104260424::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2945 = { sizeof (U3CDOLookAtU3Ec__AnonStorey5_t2052975667), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2945[1] = 
{
	U3CDOLookAtU3Ec__AnonStorey5_t2052975667::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2946 = { sizeof (U3CDOJumpU3Ec__AnonStorey6_t2908495474), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2946[7] = 
{
	U3CDOJumpU3Ec__AnonStorey6_t2908495474::get_offset_of_target_0(),
	U3CDOJumpU3Ec__AnonStorey6_t2908495474::get_offset_of_startPosY_1(),
	U3CDOJumpU3Ec__AnonStorey6_t2908495474::get_offset_of_offsetYSet_2(),
	U3CDOJumpU3Ec__AnonStorey6_t2908495474::get_offset_of_s_3(),
	U3CDOJumpU3Ec__AnonStorey6_t2908495474::get_offset_of_endValue_4(),
	U3CDOJumpU3Ec__AnonStorey6_t2908495474::get_offset_of_offsetY_5(),
	U3CDOJumpU3Ec__AnonStorey6_t2908495474::get_offset_of_yTween_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2947 = { sizeof (U3CDOPathU3Ec__AnonStorey7_t510934631), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2947[1] = 
{
	U3CDOPathU3Ec__AnonStorey7_t510934631::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2948 = { sizeof (U3CDOLocalPathU3Ec__AnonStorey8_t3330346749), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2948[2] = 
{
	U3CDOLocalPathU3Ec__AnonStorey8_t3330346749::get_offset_of_trans_0(),
	U3CDOLocalPathU3Ec__AnonStorey8_t3330346749::get_offset_of_target_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2949 = { sizeof (U3CDOPathU3Ec__AnonStorey9_t3643102513), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2949[1] = 
{
	U3CDOPathU3Ec__AnonStorey9_t3643102513::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2950 = { sizeof (U3CDOLocalPathU3Ec__AnonStoreyA_t3757313789), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2950[2] = 
{
	U3CDOLocalPathU3Ec__AnonStoreyA_t3757313789::get_offset_of_trans_0(),
	U3CDOLocalPathU3Ec__AnonStoreyA_t3757313789::get_offset_of_target_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2951 = { sizeof (DOTweenModulePhysics2D_t3356628190), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2952 = { sizeof (U3CDOMoveU3Ec__AnonStorey0_t2044744932), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2952[1] = 
{
	U3CDOMoveU3Ec__AnonStorey0_t2044744932::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2953 = { sizeof (U3CDOMoveXU3Ec__AnonStorey1_t79866776), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2953[1] = 
{
	U3CDOMoveXU3Ec__AnonStorey1_t79866776::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2954 = { sizeof (U3CDOMoveYU3Ec__AnonStorey2_t1894716487), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2954[1] = 
{
	U3CDOMoveYU3Ec__AnonStorey2_t1894716487::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2955 = { sizeof (U3CDORotateU3Ec__AnonStorey3_t307238404), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2955[1] = 
{
	U3CDORotateU3Ec__AnonStorey3_t307238404::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2956 = { sizeof (U3CDOJumpU3Ec__AnonStorey4_t1356384432), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2956[7] = 
{
	U3CDOJumpU3Ec__AnonStorey4_t1356384432::get_offset_of_target_0(),
	U3CDOJumpU3Ec__AnonStorey4_t1356384432::get_offset_of_startPosY_1(),
	U3CDOJumpU3Ec__AnonStorey4_t1356384432::get_offset_of_offsetYSet_2(),
	U3CDOJumpU3Ec__AnonStorey4_t1356384432::get_offset_of_s_3(),
	U3CDOJumpU3Ec__AnonStorey4_t1356384432::get_offset_of_endValue_4(),
	U3CDOJumpU3Ec__AnonStorey4_t1356384432::get_offset_of_offsetY_5(),
	U3CDOJumpU3Ec__AnonStorey4_t1356384432::get_offset_of_yTween_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2957 = { sizeof (DOTweenModuleSprite_t688046044), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2958 = { sizeof (U3CDOColorU3Ec__AnonStorey0_t2678535312), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2958[1] = 
{
	U3CDOColorU3Ec__AnonStorey0_t2678535312::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2959 = { sizeof (U3CDOFadeU3Ec__AnonStorey1_t108519314), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2959[1] = 
{
	U3CDOFadeU3Ec__AnonStorey1_t108519314::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2960 = { sizeof (U3CDOBlendableColorU3Ec__AnonStorey2_t4261762257), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2960[2] = 
{
	U3CDOBlendableColorU3Ec__AnonStorey2_t4261762257::get_offset_of_to_0(),
	U3CDOBlendableColorU3Ec__AnonStorey2_t4261762257::get_offset_of_target_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2961 = { sizeof (DOTweenModuleUI_t3477205437), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2962 = { sizeof (Utils_t544397632), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2963 = { sizeof (U3CDOFadeU3Ec__AnonStorey0_t3571959874), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2963[1] = 
{
	U3CDOFadeU3Ec__AnonStorey0_t3571959874::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2964 = { sizeof (U3CDOColorU3Ec__AnonStorey1_t292238278), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2964[1] = 
{
	U3CDOColorU3Ec__AnonStorey1_t292238278::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2965 = { sizeof (U3CDOFadeU3Ec__AnonStorey2_t3572090946), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2965[1] = 
{
	U3CDOFadeU3Ec__AnonStorey2_t3572090946::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2966 = { sizeof (U3CDOColorU3Ec__AnonStorey3_t3424406160), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2966[1] = 
{
	U3CDOColorU3Ec__AnonStorey3_t3424406160::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2967 = { sizeof (U3CDOFadeU3Ec__AnonStorey4_t3571697730), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2967[1] = 
{
	U3CDOFadeU3Ec__AnonStorey4_t3571697730::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2968 = { sizeof (U3CDOFillAmountU3Ec__AnonStorey5_t4285147863), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2968[1] = 
{
	U3CDOFillAmountU3Ec__AnonStorey5_t4285147863::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2969 = { sizeof (U3CDOFlexibleSizeU3Ec__AnonStorey6_t3266349678), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2969[1] = 
{
	U3CDOFlexibleSizeU3Ec__AnonStorey6_t3266349678::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2970 = { sizeof (U3CDOMinSizeU3Ec__AnonStorey7_t309622074), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2970[1] = 
{
	U3CDOMinSizeU3Ec__AnonStorey7_t309622074::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2971 = { sizeof (U3CDOPreferredSizeU3Ec__AnonStorey8_t4005645670), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2971[1] = 
{
	U3CDOPreferredSizeU3Ec__AnonStorey8_t4005645670::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2972 = { sizeof (U3CDOColorU3Ec__AnonStorey9_t648468638), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2972[1] = 
{
	U3CDOColorU3Ec__AnonStorey9_t648468638::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2973 = { sizeof (U3CDOFadeU3Ec__AnonStoreyA_t3566651458), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2973[1] = 
{
	U3CDOFadeU3Ec__AnonStoreyA_t3566651458::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2974 = { sizeof (U3CDOScaleU3Ec__AnonStoreyB_t4203681660), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2974[1] = 
{
	U3CDOScaleU3Ec__AnonStoreyB_t4203681660::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2975 = { sizeof (U3CDOAnchorPosU3Ec__AnonStoreyC_t3922317631), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2975[1] = 
{
	U3CDOAnchorPosU3Ec__AnonStoreyC_t3922317631::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2976 = { sizeof (U3CDOAnchorPosXU3Ec__AnonStoreyD_t1583578289), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2976[1] = 
{
	U3CDOAnchorPosXU3Ec__AnonStoreyD_t1583578289::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2977 = { sizeof (U3CDOAnchorPosYU3Ec__AnonStoreyE_t3922517944), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2977[1] = 
{
	U3CDOAnchorPosYU3Ec__AnonStoreyE_t3922517944::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2978 = { sizeof (U3CDOAnchorPos3DU3Ec__AnonStoreyF_t916713582), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2978[1] = 
{
	U3CDOAnchorPos3DU3Ec__AnonStoreyF_t916713582::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2979 = { sizeof (U3CDOAnchorPos3DXU3Ec__AnonStorey10_t692117743), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2979[1] = 
{
	U3CDOAnchorPos3DXU3Ec__AnonStorey10_t692117743::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2980 = { sizeof (U3CDOAnchorPos3DYU3Ec__AnonStorey11_t1739080443), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2980[1] = 
{
	U3CDOAnchorPos3DYU3Ec__AnonStorey11_t1739080443::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2981 = { sizeof (U3CDOAnchorPos3DZU3Ec__AnonStorey12_t1172413799), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2981[1] = 
{
	U3CDOAnchorPos3DZU3Ec__AnonStorey12_t1172413799::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2982 = { sizeof (U3CDOAnchorMaxU3Ec__AnonStorey13_t3164487871), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2982[1] = 
{
	U3CDOAnchorMaxU3Ec__AnonStorey13_t3164487871::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2983 = { sizeof (U3CDOAnchorMinU3Ec__AnonStorey14_t308074497), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2983[1] = 
{
	U3CDOAnchorMinU3Ec__AnonStorey14_t308074497::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2984 = { sizeof (U3CDOPivotU3Ec__AnonStorey15_t2354936221), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2984[1] = 
{
	U3CDOPivotU3Ec__AnonStorey15_t2354936221::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2985 = { sizeof (U3CDOPivotXU3Ec__AnonStorey16_t963667313), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2985[1] = 
{
	U3CDOPivotXU3Ec__AnonStorey16_t963667313::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2986 = { sizeof (U3CDOPivotYU3Ec__AnonStorey17_t970927219), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2986[1] = 
{
	U3CDOPivotYU3Ec__AnonStorey17_t970927219::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2987 = { sizeof (U3CDOSizeDeltaU3Ec__AnonStorey18_t85417030), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2987[1] = 
{
	U3CDOSizeDeltaU3Ec__AnonStorey18_t85417030::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2988 = { sizeof (U3CDOPunchAnchorPosU3Ec__AnonStorey19_t1651748926), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2988[1] = 
{
	U3CDOPunchAnchorPosU3Ec__AnonStorey19_t1651748926::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2989 = { sizeof (U3CDOShakeAnchorPosU3Ec__AnonStorey1A_t1681899448), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2989[1] = 
{
	U3CDOShakeAnchorPosU3Ec__AnonStorey1A_t1681899448::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2990 = { sizeof (U3CDOShakeAnchorPosU3Ec__AnonStorey1B_t1681899451), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2990[1] = 
{
	U3CDOShakeAnchorPosU3Ec__AnonStorey1B_t1681899451::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2991 = { sizeof (U3CDOJumpAnchorPosU3Ec__AnonStorey1C_t2532437241), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2991[6] = 
{
	U3CDOJumpAnchorPosU3Ec__AnonStorey1C_t2532437241::get_offset_of_target_0(),
	U3CDOJumpAnchorPosU3Ec__AnonStorey1C_t2532437241::get_offset_of_startPosY_1(),
	U3CDOJumpAnchorPosU3Ec__AnonStorey1C_t2532437241::get_offset_of_offsetYSet_2(),
	U3CDOJumpAnchorPosU3Ec__AnonStorey1C_t2532437241::get_offset_of_s_3(),
	U3CDOJumpAnchorPosU3Ec__AnonStorey1C_t2532437241::get_offset_of_endValue_4(),
	U3CDOJumpAnchorPosU3Ec__AnonStorey1C_t2532437241::get_offset_of_offsetY_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2992 = { sizeof (U3CDONormalizedPosU3Ec__AnonStorey1D_t4252408738), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2992[1] = 
{
	U3CDONormalizedPosU3Ec__AnonStorey1D_t4252408738::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2993 = { sizeof (U3CDOHorizontalNormalizedPosU3Ec__AnonStorey1E_t1691945541), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2993[1] = 
{
	U3CDOHorizontalNormalizedPosU3Ec__AnonStorey1E_t1691945541::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2994 = { sizeof (U3CDOVerticalNormalizedPosU3Ec__AnonStorey1F_t3179021983), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2994[1] = 
{
	U3CDOVerticalNormalizedPosU3Ec__AnonStorey1F_t3179021983::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2995 = { sizeof (U3CDOValueU3Ec__AnonStorey20_t1335203578), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2995[1] = 
{
	U3CDOValueU3Ec__AnonStorey20_t1335203578::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2996 = { sizeof (U3CDOColorU3Ec__AnonStorey21_t2960703275), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2996[1] = 
{
	U3CDOColorU3Ec__AnonStorey21_t2960703275::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2997 = { sizeof (U3CDOFadeU3Ec__AnonStorey22_t4148204238), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2997[1] = 
{
	U3CDOFadeU3Ec__AnonStorey22_t4148204238::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2998 = { sizeof (U3CDOTextU3Ec__AnonStorey23_t630041313), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2998[1] = 
{
	U3CDOTextU3Ec__AnonStorey23_t630041313::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2999 = { sizeof (U3CDOBlendableColorU3Ec__AnonStorey24_t2770270651), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2999[2] = 
{
	U3CDOBlendableColorU3Ec__AnonStorey24_t2770270651::get_offset_of_to_0(),
	U3CDOBlendableColorU3Ec__AnonStorey24_t2770270651::get_offset_of_target_1(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
