﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// DG.Tweening.Tween
struct Tween_t2342918553;
// IngameDebugConsole.DebugLogConsole/ParseFunction
struct ParseFunction_t3373928245;
// IngameDebugConsole.DebugLogEntry
struct DebugLogEntry_t3243893538;
// IngameDebugConsole.DebugLogIndexList
struct DebugLogIndexList_t3544812386;
// IngameDebugConsole.DebugLogItem
struct DebugLogItem_t1132107439;
// IngameDebugConsole.DebugLogManager
struct DebugLogManager_t637049758;
// IngameDebugConsole.DebugLogPopup
struct DebugLogPopup_t3403515216;
// IngameDebugConsole.DebugLogRecycledListView
struct DebugLogRecycledListView_t3570785762;
// System.Action`4<DG.Tweening.Plugins.Options.PathOptions,DG.Tweening.Tween,UnityEngine.Quaternion,UnityEngine.Transform>
struct Action_4_t2634302908;
// System.AsyncCallback
struct AsyncCallback_t3962456242;
// System.Char[]
struct CharU5BU5D_t3528271667;
// System.Collections.Generic.Dictionary`2<IngameDebugConsole.DebugLogEntry,System.Int32>
struct Dictionary_2_t663053759;
// System.Collections.Generic.Dictionary`2<System.Int32,IngameDebugConsole.DebugLogItem>
struct Dictionary_2_t20820770;
// System.Collections.Generic.Dictionary`2<System.String,IngameDebugConsole.ConsoleMethodInfo>
struct Dictionary_2_t3346014491;
// System.Collections.Generic.Dictionary`2<System.Type,IngameDebugConsole.DebugLogConsole/ParseFunction>
struct Dictionary_2_t1523308013;
// System.Collections.Generic.Dictionary`2<System.Type,System.String>
struct Dictionary_2_t4291797753;
// System.Collections.Generic.Dictionary`2<UnityEngine.LogType,UnityEngine.Sprite>
struct Dictionary_2_t3851943986;
// System.Collections.Generic.List`1<IngameDebugConsole.DebugLogEntry>
struct List_1_t421000984;
// System.Collections.Generic.List`1<IngameDebugConsole.DebugLogItem>
struct List_1_t2604182181;
// System.Collections.Generic.List`1<System.String>
struct List_1_t3319525431;
// System.Collections.IEnumerator
struct IEnumerator_t1853284238;
// System.DelegateData
struct DelegateData_t1677132599;
// System.IAsyncResult
struct IAsyncResult_t767004451;
// System.Int32[]
struct Int32U5BU5D_t385246372;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.String
struct String_t;
// System.String[]
struct StringU5BU5D_t1281789340;
// System.Type[]
struct TypeU5BU5D_t3940880105;
// System.Void
struct Void_t1185182177;
// UnityEngine.CanvasGroup
struct CanvasGroup_t4083511760;
// UnityEngine.EventSystems.PointerEventData
struct PointerEventData_t3807901092;
// UnityEngine.GameObject
struct GameObject_t1113636619;
// UnityEngine.RectTransform
struct RectTransform_t3704657025;
// UnityEngine.Sprite
struct Sprite_t280657092;
// UnityEngine.UI.Image
struct Image_t2670269651;
// UnityEngine.UI.InputField
struct InputField_t3762917431;
// UnityEngine.UI.ScrollRect
struct ScrollRect_t4137855814;
// UnityEngine.UI.Text
struct Text_t1901882714;




#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef DOTWEENCYINSTRUCTION_T3873353372_H
#define DOTWEENCYINSTRUCTION_T3873353372_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.DOTweenCYInstruction
struct  DOTweenCYInstruction_t3873353372  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DOTWEENCYINSTRUCTION_T3873353372_H
#ifndef DOTWEENMODULEUNITYVERSION_T1642064347_H
#define DOTWEENMODULEUNITYVERSION_T1642064347_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.DOTweenModuleUnityVersion
struct  DOTweenModuleUnityVersion_t1642064347  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DOTWEENMODULEUNITYVERSION_T1642064347_H
#ifndef DOTWEENMODULEUTILS_T382849935_H
#define DOTWEENMODULEUTILS_T382849935_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.DOTweenModuleUtils
struct  DOTweenModuleUtils_t382849935  : public RuntimeObject
{
public:

public:
};

struct DOTweenModuleUtils_t382849935_StaticFields
{
public:
	// System.Boolean DG.Tweening.DOTweenModuleUtils::_initialized
	bool ____initialized_0;
	// System.Action`4<DG.Tweening.Plugins.Options.PathOptions,DG.Tweening.Tween,UnityEngine.Quaternion,UnityEngine.Transform> DG.Tweening.DOTweenModuleUtils::<>f__mg$cache0
	Action_4_t2634302908 * ___U3CU3Ef__mgU24cache0_1;

public:
	inline static int32_t get_offset_of__initialized_0() { return static_cast<int32_t>(offsetof(DOTweenModuleUtils_t382849935_StaticFields, ____initialized_0)); }
	inline bool get__initialized_0() const { return ____initialized_0; }
	inline bool* get_address_of__initialized_0() { return &____initialized_0; }
	inline void set__initialized_0(bool value)
	{
		____initialized_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache0_1() { return static_cast<int32_t>(offsetof(DOTweenModuleUtils_t382849935_StaticFields, ___U3CU3Ef__mgU24cache0_1)); }
	inline Action_4_t2634302908 * get_U3CU3Ef__mgU24cache0_1() const { return ___U3CU3Ef__mgU24cache0_1; }
	inline Action_4_t2634302908 ** get_address_of_U3CU3Ef__mgU24cache0_1() { return &___U3CU3Ef__mgU24cache0_1; }
	inline void set_U3CU3Ef__mgU24cache0_1(Action_4_t2634302908 * value)
	{
		___U3CU3Ef__mgU24cache0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache0_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DOTWEENMODULEUTILS_T382849935_H
#ifndef PHYSICS_T3684376097_H
#define PHYSICS_T3684376097_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.DOTweenModuleUtils/Physics
struct  Physics_t3684376097  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PHYSICS_T3684376097_H
#ifndef CONSOLEMETHODINFO_T3560758192_H
#define CONSOLEMETHODINFO_T3560758192_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IngameDebugConsole.ConsoleMethodInfo
struct  ConsoleMethodInfo_t3560758192  : public RuntimeObject
{
public:
	// System.Reflection.MethodInfo IngameDebugConsole.ConsoleMethodInfo::method
	MethodInfo_t * ___method_0;
	// System.Type[] IngameDebugConsole.ConsoleMethodInfo::parameterTypes
	TypeU5BU5D_t3940880105* ___parameterTypes_1;
	// System.Object IngameDebugConsole.ConsoleMethodInfo::instance
	RuntimeObject * ___instance_2;
	// System.String IngameDebugConsole.ConsoleMethodInfo::signature
	String_t* ___signature_3;

public:
	inline static int32_t get_offset_of_method_0() { return static_cast<int32_t>(offsetof(ConsoleMethodInfo_t3560758192, ___method_0)); }
	inline MethodInfo_t * get_method_0() const { return ___method_0; }
	inline MethodInfo_t ** get_address_of_method_0() { return &___method_0; }
	inline void set_method_0(MethodInfo_t * value)
	{
		___method_0 = value;
		Il2CppCodeGenWriteBarrier((&___method_0), value);
	}

	inline static int32_t get_offset_of_parameterTypes_1() { return static_cast<int32_t>(offsetof(ConsoleMethodInfo_t3560758192, ___parameterTypes_1)); }
	inline TypeU5BU5D_t3940880105* get_parameterTypes_1() const { return ___parameterTypes_1; }
	inline TypeU5BU5D_t3940880105** get_address_of_parameterTypes_1() { return &___parameterTypes_1; }
	inline void set_parameterTypes_1(TypeU5BU5D_t3940880105* value)
	{
		___parameterTypes_1 = value;
		Il2CppCodeGenWriteBarrier((&___parameterTypes_1), value);
	}

	inline static int32_t get_offset_of_instance_2() { return static_cast<int32_t>(offsetof(ConsoleMethodInfo_t3560758192, ___instance_2)); }
	inline RuntimeObject * get_instance_2() const { return ___instance_2; }
	inline RuntimeObject ** get_address_of_instance_2() { return &___instance_2; }
	inline void set_instance_2(RuntimeObject * value)
	{
		___instance_2 = value;
		Il2CppCodeGenWriteBarrier((&___instance_2), value);
	}

	inline static int32_t get_offset_of_signature_3() { return static_cast<int32_t>(offsetof(ConsoleMethodInfo_t3560758192, ___signature_3)); }
	inline String_t* get_signature_3() const { return ___signature_3; }
	inline String_t** get_address_of_signature_3() { return &___signature_3; }
	inline void set_signature_3(String_t* value)
	{
		___signature_3 = value;
		Il2CppCodeGenWriteBarrier((&___signature_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONSOLEMETHODINFO_T3560758192_H
#ifndef DEBUGLOGCONSOLE_T1447296567_H
#define DEBUGLOGCONSOLE_T1447296567_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IngameDebugConsole.DebugLogConsole
struct  DebugLogConsole_t1447296567  : public RuntimeObject
{
public:

public:
};

struct DebugLogConsole_t1447296567_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,IngameDebugConsole.ConsoleMethodInfo> IngameDebugConsole.DebugLogConsole::methods
	Dictionary_2_t3346014491 * ___methods_0;
	// System.Collections.Generic.Dictionary`2<System.Type,IngameDebugConsole.DebugLogConsole/ParseFunction> IngameDebugConsole.DebugLogConsole::parseFunctions
	Dictionary_2_t1523308013 * ___parseFunctions_1;
	// System.Collections.Generic.Dictionary`2<System.Type,System.String> IngameDebugConsole.DebugLogConsole::typeReadableNames
	Dictionary_2_t4291797753 * ___typeReadableNames_2;
	// System.Collections.Generic.List`1<System.String> IngameDebugConsole.DebugLogConsole::commandArguments
	List_1_t3319525431 * ___commandArguments_3;
	// System.String[] IngameDebugConsole.DebugLogConsole::inputDelimiters
	StringU5BU5D_t1281789340* ___inputDelimiters_4;
	// IngameDebugConsole.DebugLogConsole/ParseFunction IngameDebugConsole.DebugLogConsole::<>f__mg$cache0
	ParseFunction_t3373928245 * ___U3CU3Ef__mgU24cache0_5;
	// IngameDebugConsole.DebugLogConsole/ParseFunction IngameDebugConsole.DebugLogConsole::<>f__mg$cache1
	ParseFunction_t3373928245 * ___U3CU3Ef__mgU24cache1_6;
	// IngameDebugConsole.DebugLogConsole/ParseFunction IngameDebugConsole.DebugLogConsole::<>f__mg$cache2
	ParseFunction_t3373928245 * ___U3CU3Ef__mgU24cache2_7;
	// IngameDebugConsole.DebugLogConsole/ParseFunction IngameDebugConsole.DebugLogConsole::<>f__mg$cache3
	ParseFunction_t3373928245 * ___U3CU3Ef__mgU24cache3_8;
	// IngameDebugConsole.DebugLogConsole/ParseFunction IngameDebugConsole.DebugLogConsole::<>f__mg$cache4
	ParseFunction_t3373928245 * ___U3CU3Ef__mgU24cache4_9;
	// IngameDebugConsole.DebugLogConsole/ParseFunction IngameDebugConsole.DebugLogConsole::<>f__mg$cache5
	ParseFunction_t3373928245 * ___U3CU3Ef__mgU24cache5_10;
	// IngameDebugConsole.DebugLogConsole/ParseFunction IngameDebugConsole.DebugLogConsole::<>f__mg$cache6
	ParseFunction_t3373928245 * ___U3CU3Ef__mgU24cache6_11;
	// IngameDebugConsole.DebugLogConsole/ParseFunction IngameDebugConsole.DebugLogConsole::<>f__mg$cache7
	ParseFunction_t3373928245 * ___U3CU3Ef__mgU24cache7_12;
	// IngameDebugConsole.DebugLogConsole/ParseFunction IngameDebugConsole.DebugLogConsole::<>f__mg$cache8
	ParseFunction_t3373928245 * ___U3CU3Ef__mgU24cache8_13;
	// IngameDebugConsole.DebugLogConsole/ParseFunction IngameDebugConsole.DebugLogConsole::<>f__mg$cache9
	ParseFunction_t3373928245 * ___U3CU3Ef__mgU24cache9_14;
	// IngameDebugConsole.DebugLogConsole/ParseFunction IngameDebugConsole.DebugLogConsole::<>f__mg$cacheA
	ParseFunction_t3373928245 * ___U3CU3Ef__mgU24cacheA_15;
	// IngameDebugConsole.DebugLogConsole/ParseFunction IngameDebugConsole.DebugLogConsole::<>f__mg$cacheB
	ParseFunction_t3373928245 * ___U3CU3Ef__mgU24cacheB_16;
	// IngameDebugConsole.DebugLogConsole/ParseFunction IngameDebugConsole.DebugLogConsole::<>f__mg$cacheC
	ParseFunction_t3373928245 * ___U3CU3Ef__mgU24cacheC_17;
	// IngameDebugConsole.DebugLogConsole/ParseFunction IngameDebugConsole.DebugLogConsole::<>f__mg$cacheD
	ParseFunction_t3373928245 * ___U3CU3Ef__mgU24cacheD_18;
	// IngameDebugConsole.DebugLogConsole/ParseFunction IngameDebugConsole.DebugLogConsole::<>f__mg$cacheE
	ParseFunction_t3373928245 * ___U3CU3Ef__mgU24cacheE_19;
	// IngameDebugConsole.DebugLogConsole/ParseFunction IngameDebugConsole.DebugLogConsole::<>f__mg$cacheF
	ParseFunction_t3373928245 * ___U3CU3Ef__mgU24cacheF_20;
	// IngameDebugConsole.DebugLogConsole/ParseFunction IngameDebugConsole.DebugLogConsole::<>f__mg$cache10
	ParseFunction_t3373928245 * ___U3CU3Ef__mgU24cache10_21;
	// IngameDebugConsole.DebugLogConsole/ParseFunction IngameDebugConsole.DebugLogConsole::<>f__mg$cache11
	ParseFunction_t3373928245 * ___U3CU3Ef__mgU24cache11_22;

public:
	inline static int32_t get_offset_of_methods_0() { return static_cast<int32_t>(offsetof(DebugLogConsole_t1447296567_StaticFields, ___methods_0)); }
	inline Dictionary_2_t3346014491 * get_methods_0() const { return ___methods_0; }
	inline Dictionary_2_t3346014491 ** get_address_of_methods_0() { return &___methods_0; }
	inline void set_methods_0(Dictionary_2_t3346014491 * value)
	{
		___methods_0 = value;
		Il2CppCodeGenWriteBarrier((&___methods_0), value);
	}

	inline static int32_t get_offset_of_parseFunctions_1() { return static_cast<int32_t>(offsetof(DebugLogConsole_t1447296567_StaticFields, ___parseFunctions_1)); }
	inline Dictionary_2_t1523308013 * get_parseFunctions_1() const { return ___parseFunctions_1; }
	inline Dictionary_2_t1523308013 ** get_address_of_parseFunctions_1() { return &___parseFunctions_1; }
	inline void set_parseFunctions_1(Dictionary_2_t1523308013 * value)
	{
		___parseFunctions_1 = value;
		Il2CppCodeGenWriteBarrier((&___parseFunctions_1), value);
	}

	inline static int32_t get_offset_of_typeReadableNames_2() { return static_cast<int32_t>(offsetof(DebugLogConsole_t1447296567_StaticFields, ___typeReadableNames_2)); }
	inline Dictionary_2_t4291797753 * get_typeReadableNames_2() const { return ___typeReadableNames_2; }
	inline Dictionary_2_t4291797753 ** get_address_of_typeReadableNames_2() { return &___typeReadableNames_2; }
	inline void set_typeReadableNames_2(Dictionary_2_t4291797753 * value)
	{
		___typeReadableNames_2 = value;
		Il2CppCodeGenWriteBarrier((&___typeReadableNames_2), value);
	}

	inline static int32_t get_offset_of_commandArguments_3() { return static_cast<int32_t>(offsetof(DebugLogConsole_t1447296567_StaticFields, ___commandArguments_3)); }
	inline List_1_t3319525431 * get_commandArguments_3() const { return ___commandArguments_3; }
	inline List_1_t3319525431 ** get_address_of_commandArguments_3() { return &___commandArguments_3; }
	inline void set_commandArguments_3(List_1_t3319525431 * value)
	{
		___commandArguments_3 = value;
		Il2CppCodeGenWriteBarrier((&___commandArguments_3), value);
	}

	inline static int32_t get_offset_of_inputDelimiters_4() { return static_cast<int32_t>(offsetof(DebugLogConsole_t1447296567_StaticFields, ___inputDelimiters_4)); }
	inline StringU5BU5D_t1281789340* get_inputDelimiters_4() const { return ___inputDelimiters_4; }
	inline StringU5BU5D_t1281789340** get_address_of_inputDelimiters_4() { return &___inputDelimiters_4; }
	inline void set_inputDelimiters_4(StringU5BU5D_t1281789340* value)
	{
		___inputDelimiters_4 = value;
		Il2CppCodeGenWriteBarrier((&___inputDelimiters_4), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache0_5() { return static_cast<int32_t>(offsetof(DebugLogConsole_t1447296567_StaticFields, ___U3CU3Ef__mgU24cache0_5)); }
	inline ParseFunction_t3373928245 * get_U3CU3Ef__mgU24cache0_5() const { return ___U3CU3Ef__mgU24cache0_5; }
	inline ParseFunction_t3373928245 ** get_address_of_U3CU3Ef__mgU24cache0_5() { return &___U3CU3Ef__mgU24cache0_5; }
	inline void set_U3CU3Ef__mgU24cache0_5(ParseFunction_t3373928245 * value)
	{
		___U3CU3Ef__mgU24cache0_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache0_5), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache1_6() { return static_cast<int32_t>(offsetof(DebugLogConsole_t1447296567_StaticFields, ___U3CU3Ef__mgU24cache1_6)); }
	inline ParseFunction_t3373928245 * get_U3CU3Ef__mgU24cache1_6() const { return ___U3CU3Ef__mgU24cache1_6; }
	inline ParseFunction_t3373928245 ** get_address_of_U3CU3Ef__mgU24cache1_6() { return &___U3CU3Ef__mgU24cache1_6; }
	inline void set_U3CU3Ef__mgU24cache1_6(ParseFunction_t3373928245 * value)
	{
		___U3CU3Ef__mgU24cache1_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache1_6), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache2_7() { return static_cast<int32_t>(offsetof(DebugLogConsole_t1447296567_StaticFields, ___U3CU3Ef__mgU24cache2_7)); }
	inline ParseFunction_t3373928245 * get_U3CU3Ef__mgU24cache2_7() const { return ___U3CU3Ef__mgU24cache2_7; }
	inline ParseFunction_t3373928245 ** get_address_of_U3CU3Ef__mgU24cache2_7() { return &___U3CU3Ef__mgU24cache2_7; }
	inline void set_U3CU3Ef__mgU24cache2_7(ParseFunction_t3373928245 * value)
	{
		___U3CU3Ef__mgU24cache2_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache2_7), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache3_8() { return static_cast<int32_t>(offsetof(DebugLogConsole_t1447296567_StaticFields, ___U3CU3Ef__mgU24cache3_8)); }
	inline ParseFunction_t3373928245 * get_U3CU3Ef__mgU24cache3_8() const { return ___U3CU3Ef__mgU24cache3_8; }
	inline ParseFunction_t3373928245 ** get_address_of_U3CU3Ef__mgU24cache3_8() { return &___U3CU3Ef__mgU24cache3_8; }
	inline void set_U3CU3Ef__mgU24cache3_8(ParseFunction_t3373928245 * value)
	{
		___U3CU3Ef__mgU24cache3_8 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache3_8), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache4_9() { return static_cast<int32_t>(offsetof(DebugLogConsole_t1447296567_StaticFields, ___U3CU3Ef__mgU24cache4_9)); }
	inline ParseFunction_t3373928245 * get_U3CU3Ef__mgU24cache4_9() const { return ___U3CU3Ef__mgU24cache4_9; }
	inline ParseFunction_t3373928245 ** get_address_of_U3CU3Ef__mgU24cache4_9() { return &___U3CU3Ef__mgU24cache4_9; }
	inline void set_U3CU3Ef__mgU24cache4_9(ParseFunction_t3373928245 * value)
	{
		___U3CU3Ef__mgU24cache4_9 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache4_9), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache5_10() { return static_cast<int32_t>(offsetof(DebugLogConsole_t1447296567_StaticFields, ___U3CU3Ef__mgU24cache5_10)); }
	inline ParseFunction_t3373928245 * get_U3CU3Ef__mgU24cache5_10() const { return ___U3CU3Ef__mgU24cache5_10; }
	inline ParseFunction_t3373928245 ** get_address_of_U3CU3Ef__mgU24cache5_10() { return &___U3CU3Ef__mgU24cache5_10; }
	inline void set_U3CU3Ef__mgU24cache5_10(ParseFunction_t3373928245 * value)
	{
		___U3CU3Ef__mgU24cache5_10 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache5_10), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache6_11() { return static_cast<int32_t>(offsetof(DebugLogConsole_t1447296567_StaticFields, ___U3CU3Ef__mgU24cache6_11)); }
	inline ParseFunction_t3373928245 * get_U3CU3Ef__mgU24cache6_11() const { return ___U3CU3Ef__mgU24cache6_11; }
	inline ParseFunction_t3373928245 ** get_address_of_U3CU3Ef__mgU24cache6_11() { return &___U3CU3Ef__mgU24cache6_11; }
	inline void set_U3CU3Ef__mgU24cache6_11(ParseFunction_t3373928245 * value)
	{
		___U3CU3Ef__mgU24cache6_11 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache6_11), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache7_12() { return static_cast<int32_t>(offsetof(DebugLogConsole_t1447296567_StaticFields, ___U3CU3Ef__mgU24cache7_12)); }
	inline ParseFunction_t3373928245 * get_U3CU3Ef__mgU24cache7_12() const { return ___U3CU3Ef__mgU24cache7_12; }
	inline ParseFunction_t3373928245 ** get_address_of_U3CU3Ef__mgU24cache7_12() { return &___U3CU3Ef__mgU24cache7_12; }
	inline void set_U3CU3Ef__mgU24cache7_12(ParseFunction_t3373928245 * value)
	{
		___U3CU3Ef__mgU24cache7_12 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache7_12), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache8_13() { return static_cast<int32_t>(offsetof(DebugLogConsole_t1447296567_StaticFields, ___U3CU3Ef__mgU24cache8_13)); }
	inline ParseFunction_t3373928245 * get_U3CU3Ef__mgU24cache8_13() const { return ___U3CU3Ef__mgU24cache8_13; }
	inline ParseFunction_t3373928245 ** get_address_of_U3CU3Ef__mgU24cache8_13() { return &___U3CU3Ef__mgU24cache8_13; }
	inline void set_U3CU3Ef__mgU24cache8_13(ParseFunction_t3373928245 * value)
	{
		___U3CU3Ef__mgU24cache8_13 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache8_13), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache9_14() { return static_cast<int32_t>(offsetof(DebugLogConsole_t1447296567_StaticFields, ___U3CU3Ef__mgU24cache9_14)); }
	inline ParseFunction_t3373928245 * get_U3CU3Ef__mgU24cache9_14() const { return ___U3CU3Ef__mgU24cache9_14; }
	inline ParseFunction_t3373928245 ** get_address_of_U3CU3Ef__mgU24cache9_14() { return &___U3CU3Ef__mgU24cache9_14; }
	inline void set_U3CU3Ef__mgU24cache9_14(ParseFunction_t3373928245 * value)
	{
		___U3CU3Ef__mgU24cache9_14 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache9_14), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cacheA_15() { return static_cast<int32_t>(offsetof(DebugLogConsole_t1447296567_StaticFields, ___U3CU3Ef__mgU24cacheA_15)); }
	inline ParseFunction_t3373928245 * get_U3CU3Ef__mgU24cacheA_15() const { return ___U3CU3Ef__mgU24cacheA_15; }
	inline ParseFunction_t3373928245 ** get_address_of_U3CU3Ef__mgU24cacheA_15() { return &___U3CU3Ef__mgU24cacheA_15; }
	inline void set_U3CU3Ef__mgU24cacheA_15(ParseFunction_t3373928245 * value)
	{
		___U3CU3Ef__mgU24cacheA_15 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cacheA_15), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cacheB_16() { return static_cast<int32_t>(offsetof(DebugLogConsole_t1447296567_StaticFields, ___U3CU3Ef__mgU24cacheB_16)); }
	inline ParseFunction_t3373928245 * get_U3CU3Ef__mgU24cacheB_16() const { return ___U3CU3Ef__mgU24cacheB_16; }
	inline ParseFunction_t3373928245 ** get_address_of_U3CU3Ef__mgU24cacheB_16() { return &___U3CU3Ef__mgU24cacheB_16; }
	inline void set_U3CU3Ef__mgU24cacheB_16(ParseFunction_t3373928245 * value)
	{
		___U3CU3Ef__mgU24cacheB_16 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cacheB_16), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cacheC_17() { return static_cast<int32_t>(offsetof(DebugLogConsole_t1447296567_StaticFields, ___U3CU3Ef__mgU24cacheC_17)); }
	inline ParseFunction_t3373928245 * get_U3CU3Ef__mgU24cacheC_17() const { return ___U3CU3Ef__mgU24cacheC_17; }
	inline ParseFunction_t3373928245 ** get_address_of_U3CU3Ef__mgU24cacheC_17() { return &___U3CU3Ef__mgU24cacheC_17; }
	inline void set_U3CU3Ef__mgU24cacheC_17(ParseFunction_t3373928245 * value)
	{
		___U3CU3Ef__mgU24cacheC_17 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cacheC_17), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cacheD_18() { return static_cast<int32_t>(offsetof(DebugLogConsole_t1447296567_StaticFields, ___U3CU3Ef__mgU24cacheD_18)); }
	inline ParseFunction_t3373928245 * get_U3CU3Ef__mgU24cacheD_18() const { return ___U3CU3Ef__mgU24cacheD_18; }
	inline ParseFunction_t3373928245 ** get_address_of_U3CU3Ef__mgU24cacheD_18() { return &___U3CU3Ef__mgU24cacheD_18; }
	inline void set_U3CU3Ef__mgU24cacheD_18(ParseFunction_t3373928245 * value)
	{
		___U3CU3Ef__mgU24cacheD_18 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cacheD_18), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cacheE_19() { return static_cast<int32_t>(offsetof(DebugLogConsole_t1447296567_StaticFields, ___U3CU3Ef__mgU24cacheE_19)); }
	inline ParseFunction_t3373928245 * get_U3CU3Ef__mgU24cacheE_19() const { return ___U3CU3Ef__mgU24cacheE_19; }
	inline ParseFunction_t3373928245 ** get_address_of_U3CU3Ef__mgU24cacheE_19() { return &___U3CU3Ef__mgU24cacheE_19; }
	inline void set_U3CU3Ef__mgU24cacheE_19(ParseFunction_t3373928245 * value)
	{
		___U3CU3Ef__mgU24cacheE_19 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cacheE_19), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cacheF_20() { return static_cast<int32_t>(offsetof(DebugLogConsole_t1447296567_StaticFields, ___U3CU3Ef__mgU24cacheF_20)); }
	inline ParseFunction_t3373928245 * get_U3CU3Ef__mgU24cacheF_20() const { return ___U3CU3Ef__mgU24cacheF_20; }
	inline ParseFunction_t3373928245 ** get_address_of_U3CU3Ef__mgU24cacheF_20() { return &___U3CU3Ef__mgU24cacheF_20; }
	inline void set_U3CU3Ef__mgU24cacheF_20(ParseFunction_t3373928245 * value)
	{
		___U3CU3Ef__mgU24cacheF_20 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cacheF_20), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache10_21() { return static_cast<int32_t>(offsetof(DebugLogConsole_t1447296567_StaticFields, ___U3CU3Ef__mgU24cache10_21)); }
	inline ParseFunction_t3373928245 * get_U3CU3Ef__mgU24cache10_21() const { return ___U3CU3Ef__mgU24cache10_21; }
	inline ParseFunction_t3373928245 ** get_address_of_U3CU3Ef__mgU24cache10_21() { return &___U3CU3Ef__mgU24cache10_21; }
	inline void set_U3CU3Ef__mgU24cache10_21(ParseFunction_t3373928245 * value)
	{
		___U3CU3Ef__mgU24cache10_21 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache10_21), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache11_22() { return static_cast<int32_t>(offsetof(DebugLogConsole_t1447296567_StaticFields, ___U3CU3Ef__mgU24cache11_22)); }
	inline ParseFunction_t3373928245 * get_U3CU3Ef__mgU24cache11_22() const { return ___U3CU3Ef__mgU24cache11_22; }
	inline ParseFunction_t3373928245 ** get_address_of_U3CU3Ef__mgU24cache11_22() { return &___U3CU3Ef__mgU24cache11_22; }
	inline void set_U3CU3Ef__mgU24cache11_22(ParseFunction_t3373928245 * value)
	{
		___U3CU3Ef__mgU24cache11_22 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache11_22), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEBUGLOGCONSOLE_T1447296567_H
#ifndef DEBUGLOGENTRY_T3243893538_H
#define DEBUGLOGENTRY_T3243893538_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IngameDebugConsole.DebugLogEntry
struct  DebugLogEntry_t3243893538  : public RuntimeObject
{
public:
	// System.String IngameDebugConsole.DebugLogEntry::logString
	String_t* ___logString_1;
	// System.String IngameDebugConsole.DebugLogEntry::stackTrace
	String_t* ___stackTrace_2;
	// System.String IngameDebugConsole.DebugLogEntry::completeLog
	String_t* ___completeLog_3;
	// UnityEngine.Sprite IngameDebugConsole.DebugLogEntry::logTypeSpriteRepresentation
	Sprite_t280657092 * ___logTypeSpriteRepresentation_4;
	// System.Int32 IngameDebugConsole.DebugLogEntry::count
	int32_t ___count_5;
	// System.Int32 IngameDebugConsole.DebugLogEntry::hashValue
	int32_t ___hashValue_6;

public:
	inline static int32_t get_offset_of_logString_1() { return static_cast<int32_t>(offsetof(DebugLogEntry_t3243893538, ___logString_1)); }
	inline String_t* get_logString_1() const { return ___logString_1; }
	inline String_t** get_address_of_logString_1() { return &___logString_1; }
	inline void set_logString_1(String_t* value)
	{
		___logString_1 = value;
		Il2CppCodeGenWriteBarrier((&___logString_1), value);
	}

	inline static int32_t get_offset_of_stackTrace_2() { return static_cast<int32_t>(offsetof(DebugLogEntry_t3243893538, ___stackTrace_2)); }
	inline String_t* get_stackTrace_2() const { return ___stackTrace_2; }
	inline String_t** get_address_of_stackTrace_2() { return &___stackTrace_2; }
	inline void set_stackTrace_2(String_t* value)
	{
		___stackTrace_2 = value;
		Il2CppCodeGenWriteBarrier((&___stackTrace_2), value);
	}

	inline static int32_t get_offset_of_completeLog_3() { return static_cast<int32_t>(offsetof(DebugLogEntry_t3243893538, ___completeLog_3)); }
	inline String_t* get_completeLog_3() const { return ___completeLog_3; }
	inline String_t** get_address_of_completeLog_3() { return &___completeLog_3; }
	inline void set_completeLog_3(String_t* value)
	{
		___completeLog_3 = value;
		Il2CppCodeGenWriteBarrier((&___completeLog_3), value);
	}

	inline static int32_t get_offset_of_logTypeSpriteRepresentation_4() { return static_cast<int32_t>(offsetof(DebugLogEntry_t3243893538, ___logTypeSpriteRepresentation_4)); }
	inline Sprite_t280657092 * get_logTypeSpriteRepresentation_4() const { return ___logTypeSpriteRepresentation_4; }
	inline Sprite_t280657092 ** get_address_of_logTypeSpriteRepresentation_4() { return &___logTypeSpriteRepresentation_4; }
	inline void set_logTypeSpriteRepresentation_4(Sprite_t280657092 * value)
	{
		___logTypeSpriteRepresentation_4 = value;
		Il2CppCodeGenWriteBarrier((&___logTypeSpriteRepresentation_4), value);
	}

	inline static int32_t get_offset_of_count_5() { return static_cast<int32_t>(offsetof(DebugLogEntry_t3243893538, ___count_5)); }
	inline int32_t get_count_5() const { return ___count_5; }
	inline int32_t* get_address_of_count_5() { return &___count_5; }
	inline void set_count_5(int32_t value)
	{
		___count_5 = value;
	}

	inline static int32_t get_offset_of_hashValue_6() { return static_cast<int32_t>(offsetof(DebugLogEntry_t3243893538, ___hashValue_6)); }
	inline int32_t get_hashValue_6() const { return ___hashValue_6; }
	inline int32_t* get_address_of_hashValue_6() { return &___hashValue_6; }
	inline void set_hashValue_6(int32_t value)
	{
		___hashValue_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEBUGLOGENTRY_T3243893538_H
#ifndef DEBUGLOGINDEXLIST_T3544812386_H
#define DEBUGLOGINDEXLIST_T3544812386_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IngameDebugConsole.DebugLogIndexList
struct  DebugLogIndexList_t3544812386  : public RuntimeObject
{
public:
	// System.Int32[] IngameDebugConsole.DebugLogIndexList::indices
	Int32U5BU5D_t385246372* ___indices_0;
	// System.Int32 IngameDebugConsole.DebugLogIndexList::size
	int32_t ___size_1;

public:
	inline static int32_t get_offset_of_indices_0() { return static_cast<int32_t>(offsetof(DebugLogIndexList_t3544812386, ___indices_0)); }
	inline Int32U5BU5D_t385246372* get_indices_0() const { return ___indices_0; }
	inline Int32U5BU5D_t385246372** get_address_of_indices_0() { return &___indices_0; }
	inline void set_indices_0(Int32U5BU5D_t385246372* value)
	{
		___indices_0 = value;
		Il2CppCodeGenWriteBarrier((&___indices_0), value);
	}

	inline static int32_t get_offset_of_size_1() { return static_cast<int32_t>(offsetof(DebugLogIndexList_t3544812386, ___size_1)); }
	inline int32_t get_size_1() const { return ___size_1; }
	inline int32_t* get_address_of_size_1() { return &___size_1; }
	inline void set_size_1(int32_t value)
	{
		___size_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEBUGLOGINDEXLIST_T3544812386_H
#ifndef ATTRIBUTE_T861562559_H
#define ATTRIBUTE_T861562559_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Attribute
struct  Attribute_t861562559  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ATTRIBUTE_T861562559_H
#ifndef VALUETYPE_T3640485471_H
#define VALUETYPE_T3640485471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3640485471  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_com
{
};
#endif // VALUETYPE_T3640485471_H
#ifndef CUSTOMYIELDINSTRUCTION_T1895667560_H
#define CUSTOMYIELDINSTRUCTION_T1895667560_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.CustomYieldInstruction
struct  CustomYieldInstruction_t1895667560  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CUSTOMYIELDINSTRUCTION_T1895667560_H
#ifndef WAITFORCOMPLETION_T2334662098_H
#define WAITFORCOMPLETION_T2334662098_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.DOTweenCYInstruction/WaitForCompletion
struct  WaitForCompletion_t2334662098  : public CustomYieldInstruction_t1895667560
{
public:
	// DG.Tweening.Tween DG.Tweening.DOTweenCYInstruction/WaitForCompletion::t
	Tween_t2342918553 * ___t_0;

public:
	inline static int32_t get_offset_of_t_0() { return static_cast<int32_t>(offsetof(WaitForCompletion_t2334662098, ___t_0)); }
	inline Tween_t2342918553 * get_t_0() const { return ___t_0; }
	inline Tween_t2342918553 ** get_address_of_t_0() { return &___t_0; }
	inline void set_t_0(Tween_t2342918553 * value)
	{
		___t_0 = value;
		Il2CppCodeGenWriteBarrier((&___t_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WAITFORCOMPLETION_T2334662098_H
#ifndef WAITFORELAPSEDLOOPS_T268076146_H
#define WAITFORELAPSEDLOOPS_T268076146_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.DOTweenCYInstruction/WaitForElapsedLoops
struct  WaitForElapsedLoops_t268076146  : public CustomYieldInstruction_t1895667560
{
public:
	// DG.Tweening.Tween DG.Tweening.DOTweenCYInstruction/WaitForElapsedLoops::t
	Tween_t2342918553 * ___t_0;
	// System.Int32 DG.Tweening.DOTweenCYInstruction/WaitForElapsedLoops::elapsedLoops
	int32_t ___elapsedLoops_1;

public:
	inline static int32_t get_offset_of_t_0() { return static_cast<int32_t>(offsetof(WaitForElapsedLoops_t268076146, ___t_0)); }
	inline Tween_t2342918553 * get_t_0() const { return ___t_0; }
	inline Tween_t2342918553 ** get_address_of_t_0() { return &___t_0; }
	inline void set_t_0(Tween_t2342918553 * value)
	{
		___t_0 = value;
		Il2CppCodeGenWriteBarrier((&___t_0), value);
	}

	inline static int32_t get_offset_of_elapsedLoops_1() { return static_cast<int32_t>(offsetof(WaitForElapsedLoops_t268076146, ___elapsedLoops_1)); }
	inline int32_t get_elapsedLoops_1() const { return ___elapsedLoops_1; }
	inline int32_t* get_address_of_elapsedLoops_1() { return &___elapsedLoops_1; }
	inline void set_elapsedLoops_1(int32_t value)
	{
		___elapsedLoops_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WAITFORELAPSEDLOOPS_T268076146_H
#ifndef WAITFORKILL_T3778347930_H
#define WAITFORKILL_T3778347930_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.DOTweenCYInstruction/WaitForKill
struct  WaitForKill_t3778347930  : public CustomYieldInstruction_t1895667560
{
public:
	// DG.Tweening.Tween DG.Tweening.DOTweenCYInstruction/WaitForKill::t
	Tween_t2342918553 * ___t_0;

public:
	inline static int32_t get_offset_of_t_0() { return static_cast<int32_t>(offsetof(WaitForKill_t3778347930, ___t_0)); }
	inline Tween_t2342918553 * get_t_0() const { return ___t_0; }
	inline Tween_t2342918553 ** get_address_of_t_0() { return &___t_0; }
	inline void set_t_0(Tween_t2342918553 * value)
	{
		___t_0 = value;
		Il2CppCodeGenWriteBarrier((&___t_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WAITFORKILL_T3778347930_H
#ifndef WAITFORPOSITION_T3077499975_H
#define WAITFORPOSITION_T3077499975_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.DOTweenCYInstruction/WaitForPosition
struct  WaitForPosition_t3077499975  : public CustomYieldInstruction_t1895667560
{
public:
	// DG.Tweening.Tween DG.Tweening.DOTweenCYInstruction/WaitForPosition::t
	Tween_t2342918553 * ___t_0;
	// System.Single DG.Tweening.DOTweenCYInstruction/WaitForPosition::position
	float ___position_1;

public:
	inline static int32_t get_offset_of_t_0() { return static_cast<int32_t>(offsetof(WaitForPosition_t3077499975, ___t_0)); }
	inline Tween_t2342918553 * get_t_0() const { return ___t_0; }
	inline Tween_t2342918553 ** get_address_of_t_0() { return &___t_0; }
	inline void set_t_0(Tween_t2342918553 * value)
	{
		___t_0 = value;
		Il2CppCodeGenWriteBarrier((&___t_0), value);
	}

	inline static int32_t get_offset_of_position_1() { return static_cast<int32_t>(offsetof(WaitForPosition_t3077499975, ___position_1)); }
	inline float get_position_1() const { return ___position_1; }
	inline float* get_address_of_position_1() { return &___position_1; }
	inline void set_position_1(float value)
	{
		___position_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WAITFORPOSITION_T3077499975_H
#ifndef WAITFORREWIND_T1388328290_H
#define WAITFORREWIND_T1388328290_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.DOTweenCYInstruction/WaitForRewind
struct  WaitForRewind_t1388328290  : public CustomYieldInstruction_t1895667560
{
public:
	// DG.Tweening.Tween DG.Tweening.DOTweenCYInstruction/WaitForRewind::t
	Tween_t2342918553 * ___t_0;

public:
	inline static int32_t get_offset_of_t_0() { return static_cast<int32_t>(offsetof(WaitForRewind_t1388328290, ___t_0)); }
	inline Tween_t2342918553 * get_t_0() const { return ___t_0; }
	inline Tween_t2342918553 ** get_address_of_t_0() { return &___t_0; }
	inline void set_t_0(Tween_t2342918553 * value)
	{
		___t_0 = value;
		Il2CppCodeGenWriteBarrier((&___t_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WAITFORREWIND_T1388328290_H
#ifndef WAITFORSTART_T1801061045_H
#define WAITFORSTART_T1801061045_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.DOTweenCYInstruction/WaitForStart
struct  WaitForStart_t1801061045  : public CustomYieldInstruction_t1895667560
{
public:
	// DG.Tweening.Tween DG.Tweening.DOTweenCYInstruction/WaitForStart::t
	Tween_t2342918553 * ___t_0;

public:
	inline static int32_t get_offset_of_t_0() { return static_cast<int32_t>(offsetof(WaitForStart_t1801061045, ___t_0)); }
	inline Tween_t2342918553 * get_t_0() const { return ___t_0; }
	inline Tween_t2342918553 ** get_address_of_t_0() { return &___t_0; }
	inline void set_t_0(Tween_t2342918553 * value)
	{
		___t_0 = value;
		Il2CppCodeGenWriteBarrier((&___t_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WAITFORSTART_T1801061045_H
#ifndef CONSOLEMETHODATTRIBUTE_T1602398842_H
#define CONSOLEMETHODATTRIBUTE_T1602398842_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IngameDebugConsole.ConsoleMethodAttribute
struct  ConsoleMethodAttribute_t1602398842  : public Attribute_t861562559
{
public:
	// System.String IngameDebugConsole.ConsoleMethodAttribute::m_command
	String_t* ___m_command_0;
	// System.String IngameDebugConsole.ConsoleMethodAttribute::m_description
	String_t* ___m_description_1;

public:
	inline static int32_t get_offset_of_m_command_0() { return static_cast<int32_t>(offsetof(ConsoleMethodAttribute_t1602398842, ___m_command_0)); }
	inline String_t* get_m_command_0() const { return ___m_command_0; }
	inline String_t** get_address_of_m_command_0() { return &___m_command_0; }
	inline void set_m_command_0(String_t* value)
	{
		___m_command_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_command_0), value);
	}

	inline static int32_t get_offset_of_m_description_1() { return static_cast<int32_t>(offsetof(ConsoleMethodAttribute_t1602398842, ___m_description_1)); }
	inline String_t* get_m_description_1() const { return ___m_description_1; }
	inline String_t** get_address_of_m_description_1() { return &___m_description_1; }
	inline void set_m_description_1(String_t* value)
	{
		___m_description_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_description_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONSOLEMETHODATTRIBUTE_T1602398842_H
#ifndef ENUM_T4135868527_H
#define ENUM_T4135868527_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t4135868527  : public ValueType_t3640485471
{
public:

public:
};

struct Enum_t4135868527_StaticFields
{
public:
	// System.Char[] System.Enum::split_char
	CharU5BU5D_t3528271667* ___split_char_0;

public:
	inline static int32_t get_offset_of_split_char_0() { return static_cast<int32_t>(offsetof(Enum_t4135868527_StaticFields, ___split_char_0)); }
	inline CharU5BU5D_t3528271667* get_split_char_0() const { return ___split_char_0; }
	inline CharU5BU5D_t3528271667** get_address_of_split_char_0() { return &___split_char_0; }
	inline void set_split_char_0(CharU5BU5D_t3528271667* value)
	{
		___split_char_0 = value;
		Il2CppCodeGenWriteBarrier((&___split_char_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t4135868527_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t4135868527_marshaled_com
{
};
#endif // ENUM_T4135868527_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef COLOR_T2555686324_H
#define COLOR_T2555686324_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color
struct  Color_t2555686324 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR_T2555686324_H
#ifndef VECTOR2_T2156229523_H
#define VECTOR2_T2156229523_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector2
struct  Vector2_t2156229523 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_t2156229523, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_t2156229523, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_t2156229523_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_t2156229523  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_t2156229523  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_t2156229523  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_t2156229523  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_t2156229523  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_t2156229523  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_t2156229523  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_t2156229523  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___zeroVector_2)); }
	inline Vector2_t2156229523  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_t2156229523 * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_t2156229523  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___oneVector_3)); }
	inline Vector2_t2156229523  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_t2156229523 * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_t2156229523  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___upVector_4)); }
	inline Vector2_t2156229523  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_t2156229523 * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_t2156229523  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___downVector_5)); }
	inline Vector2_t2156229523  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_t2156229523 * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_t2156229523  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___leftVector_6)); }
	inline Vector2_t2156229523  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_t2156229523 * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_t2156229523  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___rightVector_7)); }
	inline Vector2_t2156229523  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_t2156229523 * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_t2156229523  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_t2156229523  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_t2156229523 * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_t2156229523  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_t2156229523  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_t2156229523 * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_t2156229523  value)
	{
		___negativeInfinityVector_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2_T2156229523_H
#ifndef VECTOR3_T3722313464_H
#define VECTOR3_T3722313464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector3
struct  Vector3_t3722313464 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_2;
	// System.Single UnityEngine.Vector3::y
	float ___y_3;
	// System.Single UnityEngine.Vector3::z
	float ___z_4;

public:
	inline static int32_t get_offset_of_x_2() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___x_2)); }
	inline float get_x_2() const { return ___x_2; }
	inline float* get_address_of_x_2() { return &___x_2; }
	inline void set_x_2(float value)
	{
		___x_2 = value;
	}

	inline static int32_t get_offset_of_y_3() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___y_3)); }
	inline float get_y_3() const { return ___y_3; }
	inline float* get_address_of_y_3() { return &___y_3; }
	inline void set_y_3(float value)
	{
		___y_3 = value;
	}

	inline static int32_t get_offset_of_z_4() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___z_4)); }
	inline float get_z_4() const { return ___z_4; }
	inline float* get_address_of_z_4() { return &___z_4; }
	inline void set_z_4(float value)
	{
		___z_4 = value;
	}
};

struct Vector3_t3722313464_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_t3722313464  ___zeroVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_t3722313464  ___oneVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_t3722313464  ___upVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_t3722313464  ___downVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_t3722313464  ___leftVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_t3722313464  ___rightVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_t3722313464  ___forwardVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_t3722313464  ___backVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_t3722313464  ___positiveInfinityVector_13;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_t3722313464  ___negativeInfinityVector_14;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___zeroVector_5)); }
	inline Vector3_t3722313464  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector3_t3722313464 * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector3_t3722313464  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___oneVector_6)); }
	inline Vector3_t3722313464  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector3_t3722313464 * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector3_t3722313464  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_upVector_7() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___upVector_7)); }
	inline Vector3_t3722313464  get_upVector_7() const { return ___upVector_7; }
	inline Vector3_t3722313464 * get_address_of_upVector_7() { return &___upVector_7; }
	inline void set_upVector_7(Vector3_t3722313464  value)
	{
		___upVector_7 = value;
	}

	inline static int32_t get_offset_of_downVector_8() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___downVector_8)); }
	inline Vector3_t3722313464  get_downVector_8() const { return ___downVector_8; }
	inline Vector3_t3722313464 * get_address_of_downVector_8() { return &___downVector_8; }
	inline void set_downVector_8(Vector3_t3722313464  value)
	{
		___downVector_8 = value;
	}

	inline static int32_t get_offset_of_leftVector_9() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___leftVector_9)); }
	inline Vector3_t3722313464  get_leftVector_9() const { return ___leftVector_9; }
	inline Vector3_t3722313464 * get_address_of_leftVector_9() { return &___leftVector_9; }
	inline void set_leftVector_9(Vector3_t3722313464  value)
	{
		___leftVector_9 = value;
	}

	inline static int32_t get_offset_of_rightVector_10() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___rightVector_10)); }
	inline Vector3_t3722313464  get_rightVector_10() const { return ___rightVector_10; }
	inline Vector3_t3722313464 * get_address_of_rightVector_10() { return &___rightVector_10; }
	inline void set_rightVector_10(Vector3_t3722313464  value)
	{
		___rightVector_10 = value;
	}

	inline static int32_t get_offset_of_forwardVector_11() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___forwardVector_11)); }
	inline Vector3_t3722313464  get_forwardVector_11() const { return ___forwardVector_11; }
	inline Vector3_t3722313464 * get_address_of_forwardVector_11() { return &___forwardVector_11; }
	inline void set_forwardVector_11(Vector3_t3722313464  value)
	{
		___forwardVector_11 = value;
	}

	inline static int32_t get_offset_of_backVector_12() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___backVector_12)); }
	inline Vector3_t3722313464  get_backVector_12() const { return ___backVector_12; }
	inline Vector3_t3722313464 * get_address_of_backVector_12() { return &___backVector_12; }
	inline void set_backVector_12(Vector3_t3722313464  value)
	{
		___backVector_12 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___positiveInfinityVector_13)); }
	inline Vector3_t3722313464  get_positiveInfinityVector_13() const { return ___positiveInfinityVector_13; }
	inline Vector3_t3722313464 * get_address_of_positiveInfinityVector_13() { return &___positiveInfinityVector_13; }
	inline void set_positiveInfinityVector_13(Vector3_t3722313464  value)
	{
		___positiveInfinityVector_13 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_14() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___negativeInfinityVector_14)); }
	inline Vector3_t3722313464  get_negativeInfinityVector_14() const { return ___negativeInfinityVector_14; }
	inline Vector3_t3722313464 * get_address_of_negativeInfinityVector_14() { return &___negativeInfinityVector_14; }
	inline void set_negativeInfinityVector_14(Vector3_t3722313464  value)
	{
		___negativeInfinityVector_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_T3722313464_H
#ifndef U3CDOBLENDABLECOLORU3EC__ANONSTOREY25_T2770270650_H
#define U3CDOBLENDABLECOLORU3EC__ANONSTOREY25_T2770270650_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.DOTweenModuleUI/<DOBlendableColor>c__AnonStorey25
struct  U3CDOBlendableColorU3Ec__AnonStorey25_t2770270650  : public RuntimeObject
{
public:
	// UnityEngine.Color DG.Tweening.DOTweenModuleUI/<DOBlendableColor>c__AnonStorey25::to
	Color_t2555686324  ___to_0;
	// UnityEngine.UI.Image DG.Tweening.DOTweenModuleUI/<DOBlendableColor>c__AnonStorey25::target
	Image_t2670269651 * ___target_1;

public:
	inline static int32_t get_offset_of_to_0() { return static_cast<int32_t>(offsetof(U3CDOBlendableColorU3Ec__AnonStorey25_t2770270650, ___to_0)); }
	inline Color_t2555686324  get_to_0() const { return ___to_0; }
	inline Color_t2555686324 * get_address_of_to_0() { return &___to_0; }
	inline void set_to_0(Color_t2555686324  value)
	{
		___to_0 = value;
	}

	inline static int32_t get_offset_of_target_1() { return static_cast<int32_t>(offsetof(U3CDOBlendableColorU3Ec__AnonStorey25_t2770270650, ___target_1)); }
	inline Image_t2670269651 * get_target_1() const { return ___target_1; }
	inline Image_t2670269651 ** get_address_of_target_1() { return &___target_1; }
	inline void set_target_1(Image_t2670269651 * value)
	{
		___target_1 = value;
		Il2CppCodeGenWriteBarrier((&___target_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CDOBLENDABLECOLORU3EC__ANONSTOREY25_T2770270650_H
#ifndef U3CDOBLENDABLECOLORU3EC__ANONSTOREY26_T2770270649_H
#define U3CDOBLENDABLECOLORU3EC__ANONSTOREY26_T2770270649_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.DOTweenModuleUI/<DOBlendableColor>c__AnonStorey26
struct  U3CDOBlendableColorU3Ec__AnonStorey26_t2770270649  : public RuntimeObject
{
public:
	// UnityEngine.Color DG.Tweening.DOTweenModuleUI/<DOBlendableColor>c__AnonStorey26::to
	Color_t2555686324  ___to_0;
	// UnityEngine.UI.Text DG.Tweening.DOTweenModuleUI/<DOBlendableColor>c__AnonStorey26::target
	Text_t1901882714 * ___target_1;

public:
	inline static int32_t get_offset_of_to_0() { return static_cast<int32_t>(offsetof(U3CDOBlendableColorU3Ec__AnonStorey26_t2770270649, ___to_0)); }
	inline Color_t2555686324  get_to_0() const { return ___to_0; }
	inline Color_t2555686324 * get_address_of_to_0() { return &___to_0; }
	inline void set_to_0(Color_t2555686324  value)
	{
		___to_0 = value;
	}

	inline static int32_t get_offset_of_target_1() { return static_cast<int32_t>(offsetof(U3CDOBlendableColorU3Ec__AnonStorey26_t2770270649, ___target_1)); }
	inline Text_t1901882714 * get_target_1() const { return ___target_1; }
	inline Text_t1901882714 ** get_address_of_target_1() { return &___target_1; }
	inline void set_target_1(Text_t1901882714 * value)
	{
		___target_1 = value;
		Il2CppCodeGenWriteBarrier((&___target_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CDOBLENDABLECOLORU3EC__ANONSTOREY26_T2770270649_H
#ifndef DEBUGLOGFILTER_T3310913660_H
#define DEBUGLOGFILTER_T3310913660_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IngameDebugConsole.DebugLogFilter
struct  DebugLogFilter_t3310913660 
{
public:
	// System.Int32 IngameDebugConsole.DebugLogFilter::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(DebugLogFilter_t3310913660, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEBUGLOGFILTER_T3310913660_H
#ifndef U3CMOVETOPOSANIMATIONU3EC__ITERATOR0_T2236991059_H
#define U3CMOVETOPOSANIMATIONU3EC__ITERATOR0_T2236991059_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IngameDebugConsole.DebugLogPopup/<MoveToPosAnimation>c__Iterator0
struct  U3CMoveToPosAnimationU3Ec__Iterator0_t2236991059  : public RuntimeObject
{
public:
	// System.Single IngameDebugConsole.DebugLogPopup/<MoveToPosAnimation>c__Iterator0::<modifier>__0
	float ___U3CmodifierU3E__0_0;
	// UnityEngine.Vector3 IngameDebugConsole.DebugLogPopup/<MoveToPosAnimation>c__Iterator0::<initialPos>__0
	Vector3_t3722313464  ___U3CinitialPosU3E__0_1;
	// UnityEngine.Vector3 IngameDebugConsole.DebugLogPopup/<MoveToPosAnimation>c__Iterator0::targetPos
	Vector3_t3722313464  ___targetPos_2;
	// IngameDebugConsole.DebugLogPopup IngameDebugConsole.DebugLogPopup/<MoveToPosAnimation>c__Iterator0::$this
	DebugLogPopup_t3403515216 * ___U24this_3;
	// System.Object IngameDebugConsole.DebugLogPopup/<MoveToPosAnimation>c__Iterator0::$current
	RuntimeObject * ___U24current_4;
	// System.Boolean IngameDebugConsole.DebugLogPopup/<MoveToPosAnimation>c__Iterator0::$disposing
	bool ___U24disposing_5;
	// System.Int32 IngameDebugConsole.DebugLogPopup/<MoveToPosAnimation>c__Iterator0::$PC
	int32_t ___U24PC_6;

public:
	inline static int32_t get_offset_of_U3CmodifierU3E__0_0() { return static_cast<int32_t>(offsetof(U3CMoveToPosAnimationU3Ec__Iterator0_t2236991059, ___U3CmodifierU3E__0_0)); }
	inline float get_U3CmodifierU3E__0_0() const { return ___U3CmodifierU3E__0_0; }
	inline float* get_address_of_U3CmodifierU3E__0_0() { return &___U3CmodifierU3E__0_0; }
	inline void set_U3CmodifierU3E__0_0(float value)
	{
		___U3CmodifierU3E__0_0 = value;
	}

	inline static int32_t get_offset_of_U3CinitialPosU3E__0_1() { return static_cast<int32_t>(offsetof(U3CMoveToPosAnimationU3Ec__Iterator0_t2236991059, ___U3CinitialPosU3E__0_1)); }
	inline Vector3_t3722313464  get_U3CinitialPosU3E__0_1() const { return ___U3CinitialPosU3E__0_1; }
	inline Vector3_t3722313464 * get_address_of_U3CinitialPosU3E__0_1() { return &___U3CinitialPosU3E__0_1; }
	inline void set_U3CinitialPosU3E__0_1(Vector3_t3722313464  value)
	{
		___U3CinitialPosU3E__0_1 = value;
	}

	inline static int32_t get_offset_of_targetPos_2() { return static_cast<int32_t>(offsetof(U3CMoveToPosAnimationU3Ec__Iterator0_t2236991059, ___targetPos_2)); }
	inline Vector3_t3722313464  get_targetPos_2() const { return ___targetPos_2; }
	inline Vector3_t3722313464 * get_address_of_targetPos_2() { return &___targetPos_2; }
	inline void set_targetPos_2(Vector3_t3722313464  value)
	{
		___targetPos_2 = value;
	}

	inline static int32_t get_offset_of_U24this_3() { return static_cast<int32_t>(offsetof(U3CMoveToPosAnimationU3Ec__Iterator0_t2236991059, ___U24this_3)); }
	inline DebugLogPopup_t3403515216 * get_U24this_3() const { return ___U24this_3; }
	inline DebugLogPopup_t3403515216 ** get_address_of_U24this_3() { return &___U24this_3; }
	inline void set_U24this_3(DebugLogPopup_t3403515216 * value)
	{
		___U24this_3 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_3), value);
	}

	inline static int32_t get_offset_of_U24current_4() { return static_cast<int32_t>(offsetof(U3CMoveToPosAnimationU3Ec__Iterator0_t2236991059, ___U24current_4)); }
	inline RuntimeObject * get_U24current_4() const { return ___U24current_4; }
	inline RuntimeObject ** get_address_of_U24current_4() { return &___U24current_4; }
	inline void set_U24current_4(RuntimeObject * value)
	{
		___U24current_4 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_4), value);
	}

	inline static int32_t get_offset_of_U24disposing_5() { return static_cast<int32_t>(offsetof(U3CMoveToPosAnimationU3Ec__Iterator0_t2236991059, ___U24disposing_5)); }
	inline bool get_U24disposing_5() const { return ___U24disposing_5; }
	inline bool* get_address_of_U24disposing_5() { return &___U24disposing_5; }
	inline void set_U24disposing_5(bool value)
	{
		___U24disposing_5 = value;
	}

	inline static int32_t get_offset_of_U24PC_6() { return static_cast<int32_t>(offsetof(U3CMoveToPosAnimationU3Ec__Iterator0_t2236991059, ___U24PC_6)); }
	inline int32_t get_U24PC_6() const { return ___U24PC_6; }
	inline int32_t* get_address_of_U24PC_6() { return &___U24PC_6; }
	inline void set_U24PC_6(int32_t value)
	{
		___U24PC_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMOVETOPOSANIMATIONU3EC__ITERATOR0_T2236991059_H
#ifndef DELEGATE_T1188392813_H
#define DELEGATE_T1188392813_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Delegate
struct  Delegate_t1188392813  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_5;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_6;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_7;
	// System.DelegateData System.Delegate::data
	DelegateData_t1677132599 * ___data_8;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_target_2), value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_method_code_5() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_code_5)); }
	inline intptr_t get_method_code_5() const { return ___method_code_5; }
	inline intptr_t* get_address_of_method_code_5() { return &___method_code_5; }
	inline void set_method_code_5(intptr_t value)
	{
		___method_code_5 = value;
	}

	inline static int32_t get_offset_of_method_info_6() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_info_6)); }
	inline MethodInfo_t * get_method_info_6() const { return ___method_info_6; }
	inline MethodInfo_t ** get_address_of_method_info_6() { return &___method_info_6; }
	inline void set_method_info_6(MethodInfo_t * value)
	{
		___method_info_6 = value;
		Il2CppCodeGenWriteBarrier((&___method_info_6), value);
	}

	inline static int32_t get_offset_of_original_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___original_method_info_7)); }
	inline MethodInfo_t * get_original_method_info_7() const { return ___original_method_info_7; }
	inline MethodInfo_t ** get_address_of_original_method_info_7() { return &___original_method_info_7; }
	inline void set_original_method_info_7(MethodInfo_t * value)
	{
		___original_method_info_7 = value;
		Il2CppCodeGenWriteBarrier((&___original_method_info_7), value);
	}

	inline static int32_t get_offset_of_data_8() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___data_8)); }
	inline DelegateData_t1677132599 * get_data_8() const { return ___data_8; }
	inline DelegateData_t1677132599 ** get_address_of_data_8() { return &___data_8; }
	inline void set_data_8(DelegateData_t1677132599 * value)
	{
		___data_8 = value;
		Il2CppCodeGenWriteBarrier((&___data_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DELEGATE_T1188392813_H
#ifndef OBJECT_T631007953_H
#define OBJECT_T631007953_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_t631007953  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_t631007953, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_t631007953_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_t631007953_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_T631007953_H
#ifndef MULTICASTDELEGATE_T_H
#define MULTICASTDELEGATE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MulticastDelegate
struct  MulticastDelegate_t  : public Delegate_t1188392813
{
public:
	// System.MulticastDelegate System.MulticastDelegate::prev
	MulticastDelegate_t * ___prev_9;
	// System.MulticastDelegate System.MulticastDelegate::kpm_next
	MulticastDelegate_t * ___kpm_next_10;

public:
	inline static int32_t get_offset_of_prev_9() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___prev_9)); }
	inline MulticastDelegate_t * get_prev_9() const { return ___prev_9; }
	inline MulticastDelegate_t ** get_address_of_prev_9() { return &___prev_9; }
	inline void set_prev_9(MulticastDelegate_t * value)
	{
		___prev_9 = value;
		Il2CppCodeGenWriteBarrier((&___prev_9), value);
	}

	inline static int32_t get_offset_of_kpm_next_10() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___kpm_next_10)); }
	inline MulticastDelegate_t * get_kpm_next_10() const { return ___kpm_next_10; }
	inline MulticastDelegate_t ** get_address_of_kpm_next_10() { return &___kpm_next_10; }
	inline void set_kpm_next_10(MulticastDelegate_t * value)
	{
		___kpm_next_10 = value;
		Il2CppCodeGenWriteBarrier((&___kpm_next_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MULTICASTDELEGATE_T_H
#ifndef COMPONENT_T1923634451_H
#define COMPONENT_T1923634451_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t1923634451  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T1923634451_H
#ifndef PARSEFUNCTION_T3373928245_H
#define PARSEFUNCTION_T3373928245_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IngameDebugConsole.DebugLogConsole/ParseFunction
struct  ParseFunction_t3373928245  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PARSEFUNCTION_T3373928245_H
#ifndef BEHAVIOUR_T1437897464_H
#define BEHAVIOUR_T1437897464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_t1437897464  : public Component_t1923634451
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_T1437897464_H
#ifndef MONOBEHAVIOUR_T3962482529_H
#define MONOBEHAVIOUR_T3962482529_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t3962482529  : public Behaviour_t1437897464
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T3962482529_H
#ifndef DEBUGLOGITEM_T1132107439_H
#define DEBUGLOGITEM_T1132107439_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IngameDebugConsole.DebugLogItem
struct  DebugLogItem_t1132107439  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.RectTransform IngameDebugConsole.DebugLogItem::transformComponent
	RectTransform_t3704657025 * ___transformComponent_4;
	// UnityEngine.UI.Image IngameDebugConsole.DebugLogItem::imageComponent
	Image_t2670269651 * ___imageComponent_5;
	// UnityEngine.UI.Text IngameDebugConsole.DebugLogItem::logText
	Text_t1901882714 * ___logText_6;
	// UnityEngine.UI.Image IngameDebugConsole.DebugLogItem::logTypeImage
	Image_t2670269651 * ___logTypeImage_7;
	// UnityEngine.GameObject IngameDebugConsole.DebugLogItem::logCountParent
	GameObject_t1113636619 * ___logCountParent_8;
	// UnityEngine.UI.Text IngameDebugConsole.DebugLogItem::logCountText
	Text_t1901882714 * ___logCountText_9;
	// IngameDebugConsole.DebugLogEntry IngameDebugConsole.DebugLogItem::logEntry
	DebugLogEntry_t3243893538 * ___logEntry_10;
	// System.Int32 IngameDebugConsole.DebugLogItem::entryIndex
	int32_t ___entryIndex_11;
	// IngameDebugConsole.DebugLogRecycledListView IngameDebugConsole.DebugLogItem::manager
	DebugLogRecycledListView_t3570785762 * ___manager_12;

public:
	inline static int32_t get_offset_of_transformComponent_4() { return static_cast<int32_t>(offsetof(DebugLogItem_t1132107439, ___transformComponent_4)); }
	inline RectTransform_t3704657025 * get_transformComponent_4() const { return ___transformComponent_4; }
	inline RectTransform_t3704657025 ** get_address_of_transformComponent_4() { return &___transformComponent_4; }
	inline void set_transformComponent_4(RectTransform_t3704657025 * value)
	{
		___transformComponent_4 = value;
		Il2CppCodeGenWriteBarrier((&___transformComponent_4), value);
	}

	inline static int32_t get_offset_of_imageComponent_5() { return static_cast<int32_t>(offsetof(DebugLogItem_t1132107439, ___imageComponent_5)); }
	inline Image_t2670269651 * get_imageComponent_5() const { return ___imageComponent_5; }
	inline Image_t2670269651 ** get_address_of_imageComponent_5() { return &___imageComponent_5; }
	inline void set_imageComponent_5(Image_t2670269651 * value)
	{
		___imageComponent_5 = value;
		Il2CppCodeGenWriteBarrier((&___imageComponent_5), value);
	}

	inline static int32_t get_offset_of_logText_6() { return static_cast<int32_t>(offsetof(DebugLogItem_t1132107439, ___logText_6)); }
	inline Text_t1901882714 * get_logText_6() const { return ___logText_6; }
	inline Text_t1901882714 ** get_address_of_logText_6() { return &___logText_6; }
	inline void set_logText_6(Text_t1901882714 * value)
	{
		___logText_6 = value;
		Il2CppCodeGenWriteBarrier((&___logText_6), value);
	}

	inline static int32_t get_offset_of_logTypeImage_7() { return static_cast<int32_t>(offsetof(DebugLogItem_t1132107439, ___logTypeImage_7)); }
	inline Image_t2670269651 * get_logTypeImage_7() const { return ___logTypeImage_7; }
	inline Image_t2670269651 ** get_address_of_logTypeImage_7() { return &___logTypeImage_7; }
	inline void set_logTypeImage_7(Image_t2670269651 * value)
	{
		___logTypeImage_7 = value;
		Il2CppCodeGenWriteBarrier((&___logTypeImage_7), value);
	}

	inline static int32_t get_offset_of_logCountParent_8() { return static_cast<int32_t>(offsetof(DebugLogItem_t1132107439, ___logCountParent_8)); }
	inline GameObject_t1113636619 * get_logCountParent_8() const { return ___logCountParent_8; }
	inline GameObject_t1113636619 ** get_address_of_logCountParent_8() { return &___logCountParent_8; }
	inline void set_logCountParent_8(GameObject_t1113636619 * value)
	{
		___logCountParent_8 = value;
		Il2CppCodeGenWriteBarrier((&___logCountParent_8), value);
	}

	inline static int32_t get_offset_of_logCountText_9() { return static_cast<int32_t>(offsetof(DebugLogItem_t1132107439, ___logCountText_9)); }
	inline Text_t1901882714 * get_logCountText_9() const { return ___logCountText_9; }
	inline Text_t1901882714 ** get_address_of_logCountText_9() { return &___logCountText_9; }
	inline void set_logCountText_9(Text_t1901882714 * value)
	{
		___logCountText_9 = value;
		Il2CppCodeGenWriteBarrier((&___logCountText_9), value);
	}

	inline static int32_t get_offset_of_logEntry_10() { return static_cast<int32_t>(offsetof(DebugLogItem_t1132107439, ___logEntry_10)); }
	inline DebugLogEntry_t3243893538 * get_logEntry_10() const { return ___logEntry_10; }
	inline DebugLogEntry_t3243893538 ** get_address_of_logEntry_10() { return &___logEntry_10; }
	inline void set_logEntry_10(DebugLogEntry_t3243893538 * value)
	{
		___logEntry_10 = value;
		Il2CppCodeGenWriteBarrier((&___logEntry_10), value);
	}

	inline static int32_t get_offset_of_entryIndex_11() { return static_cast<int32_t>(offsetof(DebugLogItem_t1132107439, ___entryIndex_11)); }
	inline int32_t get_entryIndex_11() const { return ___entryIndex_11; }
	inline int32_t* get_address_of_entryIndex_11() { return &___entryIndex_11; }
	inline void set_entryIndex_11(int32_t value)
	{
		___entryIndex_11 = value;
	}

	inline static int32_t get_offset_of_manager_12() { return static_cast<int32_t>(offsetof(DebugLogItem_t1132107439, ___manager_12)); }
	inline DebugLogRecycledListView_t3570785762 * get_manager_12() const { return ___manager_12; }
	inline DebugLogRecycledListView_t3570785762 ** get_address_of_manager_12() { return &___manager_12; }
	inline void set_manager_12(DebugLogRecycledListView_t3570785762 * value)
	{
		___manager_12 = value;
		Il2CppCodeGenWriteBarrier((&___manager_12), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEBUGLOGITEM_T1132107439_H
#ifndef DEBUGLOGMANAGER_T637049758_H
#define DEBUGLOGMANAGER_T637049758_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IngameDebugConsole.DebugLogManager
struct  DebugLogManager_t637049758  : public MonoBehaviour_t3962482529
{
public:
	// System.Boolean IngameDebugConsole.DebugLogManager::singleton
	bool ___singleton_5;
	// System.Single IngameDebugConsole.DebugLogManager::minimumHeight
	float ___minimumHeight_6;
	// System.Boolean IngameDebugConsole.DebugLogManager::startInPopupMode
	bool ___startInPopupMode_7;
	// System.Boolean IngameDebugConsole.DebugLogManager::clearCommandAfterExecution
	bool ___clearCommandAfterExecution_8;
	// System.Boolean IngameDebugConsole.DebugLogManager::receiveLogcatLogsInAndroid
	bool ___receiveLogcatLogsInAndroid_9;
	// System.String IngameDebugConsole.DebugLogManager::logcatArguments
	String_t* ___logcatArguments_10;
	// IngameDebugConsole.DebugLogItem IngameDebugConsole.DebugLogManager::logItemPrefab
	DebugLogItem_t1132107439 * ___logItemPrefab_11;
	// UnityEngine.Sprite IngameDebugConsole.DebugLogManager::infoLog
	Sprite_t280657092 * ___infoLog_12;
	// UnityEngine.Sprite IngameDebugConsole.DebugLogManager::warningLog
	Sprite_t280657092 * ___warningLog_13;
	// UnityEngine.Sprite IngameDebugConsole.DebugLogManager::errorLog
	Sprite_t280657092 * ___errorLog_14;
	// System.Collections.Generic.Dictionary`2<UnityEngine.LogType,UnityEngine.Sprite> IngameDebugConsole.DebugLogManager::logSpriteRepresentations
	Dictionary_2_t3851943986 * ___logSpriteRepresentations_15;
	// UnityEngine.Color IngameDebugConsole.DebugLogManager::collapseButtonNormalColor
	Color_t2555686324  ___collapseButtonNormalColor_16;
	// UnityEngine.Color IngameDebugConsole.DebugLogManager::collapseButtonSelectedColor
	Color_t2555686324  ___collapseButtonSelectedColor_17;
	// UnityEngine.Color IngameDebugConsole.DebugLogManager::filterButtonsNormalColor
	Color_t2555686324  ___filterButtonsNormalColor_18;
	// UnityEngine.Color IngameDebugConsole.DebugLogManager::filterButtonsSelectedColor
	Color_t2555686324  ___filterButtonsSelectedColor_19;
	// UnityEngine.RectTransform IngameDebugConsole.DebugLogManager::logWindowTR
	RectTransform_t3704657025 * ___logWindowTR_20;
	// UnityEngine.RectTransform IngameDebugConsole.DebugLogManager::canvasTR
	RectTransform_t3704657025 * ___canvasTR_21;
	// UnityEngine.RectTransform IngameDebugConsole.DebugLogManager::logItemsContainer
	RectTransform_t3704657025 * ___logItemsContainer_22;
	// UnityEngine.UI.InputField IngameDebugConsole.DebugLogManager::commandInputField
	InputField_t3762917431 * ___commandInputField_23;
	// UnityEngine.UI.Image IngameDebugConsole.DebugLogManager::collapseButton
	Image_t2670269651 * ___collapseButton_24;
	// UnityEngine.UI.Image IngameDebugConsole.DebugLogManager::filterInfoButton
	Image_t2670269651 * ___filterInfoButton_25;
	// UnityEngine.UI.Image IngameDebugConsole.DebugLogManager::filterWarningButton
	Image_t2670269651 * ___filterWarningButton_26;
	// UnityEngine.UI.Image IngameDebugConsole.DebugLogManager::filterErrorButton
	Image_t2670269651 * ___filterErrorButton_27;
	// UnityEngine.UI.Text IngameDebugConsole.DebugLogManager::infoEntryCountText
	Text_t1901882714 * ___infoEntryCountText_28;
	// UnityEngine.UI.Text IngameDebugConsole.DebugLogManager::warningEntryCountText
	Text_t1901882714 * ___warningEntryCountText_29;
	// UnityEngine.UI.Text IngameDebugConsole.DebugLogManager::errorEntryCountText
	Text_t1901882714 * ___errorEntryCountText_30;
	// UnityEngine.GameObject IngameDebugConsole.DebugLogManager::snapToBottomButton
	GameObject_t1113636619 * ___snapToBottomButton_31;
	// System.Int32 IngameDebugConsole.DebugLogManager::infoEntryCount
	int32_t ___infoEntryCount_32;
	// System.Int32 IngameDebugConsole.DebugLogManager::warningEntryCount
	int32_t ___warningEntryCount_33;
	// System.Int32 IngameDebugConsole.DebugLogManager::errorEntryCount
	int32_t ___errorEntryCount_34;
	// UnityEngine.CanvasGroup IngameDebugConsole.DebugLogManager::logWindowCanvasGroup
	CanvasGroup_t4083511760 * ___logWindowCanvasGroup_35;
	// System.Boolean IngameDebugConsole.DebugLogManager::isLogWindowVisible
	bool ___isLogWindowVisible_36;
	// System.Boolean IngameDebugConsole.DebugLogManager::screenDimensionsChanged
	bool ___screenDimensionsChanged_37;
	// IngameDebugConsole.DebugLogPopup IngameDebugConsole.DebugLogManager::popupManager
	DebugLogPopup_t3403515216 * ___popupManager_38;
	// UnityEngine.UI.ScrollRect IngameDebugConsole.DebugLogManager::logItemsScrollRect
	ScrollRect_t4137855814 * ___logItemsScrollRect_39;
	// IngameDebugConsole.DebugLogRecycledListView IngameDebugConsole.DebugLogManager::recycledListView
	DebugLogRecycledListView_t3570785762 * ___recycledListView_40;
	// System.Boolean IngameDebugConsole.DebugLogManager::isCollapseOn
	bool ___isCollapseOn_41;
	// IngameDebugConsole.DebugLogFilter IngameDebugConsole.DebugLogManager::logFilter
	int32_t ___logFilter_42;
	// System.Boolean IngameDebugConsole.DebugLogManager::snapToBottom
	bool ___snapToBottom_43;
	// System.Collections.Generic.List`1<IngameDebugConsole.DebugLogEntry> IngameDebugConsole.DebugLogManager::collapsedLogEntries
	List_1_t421000984 * ___collapsedLogEntries_44;
	// System.Collections.Generic.Dictionary`2<IngameDebugConsole.DebugLogEntry,System.Int32> IngameDebugConsole.DebugLogManager::collapsedLogEntriesMap
	Dictionary_2_t663053759 * ___collapsedLogEntriesMap_45;
	// IngameDebugConsole.DebugLogIndexList IngameDebugConsole.DebugLogManager::uncollapsedLogEntriesIndices
	DebugLogIndexList_t3544812386 * ___uncollapsedLogEntriesIndices_46;
	// IngameDebugConsole.DebugLogIndexList IngameDebugConsole.DebugLogManager::indicesOfListEntriesToShow
	DebugLogIndexList_t3544812386 * ___indicesOfListEntriesToShow_47;
	// System.Collections.Generic.List`1<IngameDebugConsole.DebugLogItem> IngameDebugConsole.DebugLogManager::pooledLogItems
	List_1_t2604182181 * ___pooledLogItems_48;
	// UnityEngine.EventSystems.PointerEventData IngameDebugConsole.DebugLogManager::nullPointerEventData
	PointerEventData_t3807901092 * ___nullPointerEventData_49;

public:
	inline static int32_t get_offset_of_singleton_5() { return static_cast<int32_t>(offsetof(DebugLogManager_t637049758, ___singleton_5)); }
	inline bool get_singleton_5() const { return ___singleton_5; }
	inline bool* get_address_of_singleton_5() { return &___singleton_5; }
	inline void set_singleton_5(bool value)
	{
		___singleton_5 = value;
	}

	inline static int32_t get_offset_of_minimumHeight_6() { return static_cast<int32_t>(offsetof(DebugLogManager_t637049758, ___minimumHeight_6)); }
	inline float get_minimumHeight_6() const { return ___minimumHeight_6; }
	inline float* get_address_of_minimumHeight_6() { return &___minimumHeight_6; }
	inline void set_minimumHeight_6(float value)
	{
		___minimumHeight_6 = value;
	}

	inline static int32_t get_offset_of_startInPopupMode_7() { return static_cast<int32_t>(offsetof(DebugLogManager_t637049758, ___startInPopupMode_7)); }
	inline bool get_startInPopupMode_7() const { return ___startInPopupMode_7; }
	inline bool* get_address_of_startInPopupMode_7() { return &___startInPopupMode_7; }
	inline void set_startInPopupMode_7(bool value)
	{
		___startInPopupMode_7 = value;
	}

	inline static int32_t get_offset_of_clearCommandAfterExecution_8() { return static_cast<int32_t>(offsetof(DebugLogManager_t637049758, ___clearCommandAfterExecution_8)); }
	inline bool get_clearCommandAfterExecution_8() const { return ___clearCommandAfterExecution_8; }
	inline bool* get_address_of_clearCommandAfterExecution_8() { return &___clearCommandAfterExecution_8; }
	inline void set_clearCommandAfterExecution_8(bool value)
	{
		___clearCommandAfterExecution_8 = value;
	}

	inline static int32_t get_offset_of_receiveLogcatLogsInAndroid_9() { return static_cast<int32_t>(offsetof(DebugLogManager_t637049758, ___receiveLogcatLogsInAndroid_9)); }
	inline bool get_receiveLogcatLogsInAndroid_9() const { return ___receiveLogcatLogsInAndroid_9; }
	inline bool* get_address_of_receiveLogcatLogsInAndroid_9() { return &___receiveLogcatLogsInAndroid_9; }
	inline void set_receiveLogcatLogsInAndroid_9(bool value)
	{
		___receiveLogcatLogsInAndroid_9 = value;
	}

	inline static int32_t get_offset_of_logcatArguments_10() { return static_cast<int32_t>(offsetof(DebugLogManager_t637049758, ___logcatArguments_10)); }
	inline String_t* get_logcatArguments_10() const { return ___logcatArguments_10; }
	inline String_t** get_address_of_logcatArguments_10() { return &___logcatArguments_10; }
	inline void set_logcatArguments_10(String_t* value)
	{
		___logcatArguments_10 = value;
		Il2CppCodeGenWriteBarrier((&___logcatArguments_10), value);
	}

	inline static int32_t get_offset_of_logItemPrefab_11() { return static_cast<int32_t>(offsetof(DebugLogManager_t637049758, ___logItemPrefab_11)); }
	inline DebugLogItem_t1132107439 * get_logItemPrefab_11() const { return ___logItemPrefab_11; }
	inline DebugLogItem_t1132107439 ** get_address_of_logItemPrefab_11() { return &___logItemPrefab_11; }
	inline void set_logItemPrefab_11(DebugLogItem_t1132107439 * value)
	{
		___logItemPrefab_11 = value;
		Il2CppCodeGenWriteBarrier((&___logItemPrefab_11), value);
	}

	inline static int32_t get_offset_of_infoLog_12() { return static_cast<int32_t>(offsetof(DebugLogManager_t637049758, ___infoLog_12)); }
	inline Sprite_t280657092 * get_infoLog_12() const { return ___infoLog_12; }
	inline Sprite_t280657092 ** get_address_of_infoLog_12() { return &___infoLog_12; }
	inline void set_infoLog_12(Sprite_t280657092 * value)
	{
		___infoLog_12 = value;
		Il2CppCodeGenWriteBarrier((&___infoLog_12), value);
	}

	inline static int32_t get_offset_of_warningLog_13() { return static_cast<int32_t>(offsetof(DebugLogManager_t637049758, ___warningLog_13)); }
	inline Sprite_t280657092 * get_warningLog_13() const { return ___warningLog_13; }
	inline Sprite_t280657092 ** get_address_of_warningLog_13() { return &___warningLog_13; }
	inline void set_warningLog_13(Sprite_t280657092 * value)
	{
		___warningLog_13 = value;
		Il2CppCodeGenWriteBarrier((&___warningLog_13), value);
	}

	inline static int32_t get_offset_of_errorLog_14() { return static_cast<int32_t>(offsetof(DebugLogManager_t637049758, ___errorLog_14)); }
	inline Sprite_t280657092 * get_errorLog_14() const { return ___errorLog_14; }
	inline Sprite_t280657092 ** get_address_of_errorLog_14() { return &___errorLog_14; }
	inline void set_errorLog_14(Sprite_t280657092 * value)
	{
		___errorLog_14 = value;
		Il2CppCodeGenWriteBarrier((&___errorLog_14), value);
	}

	inline static int32_t get_offset_of_logSpriteRepresentations_15() { return static_cast<int32_t>(offsetof(DebugLogManager_t637049758, ___logSpriteRepresentations_15)); }
	inline Dictionary_2_t3851943986 * get_logSpriteRepresentations_15() const { return ___logSpriteRepresentations_15; }
	inline Dictionary_2_t3851943986 ** get_address_of_logSpriteRepresentations_15() { return &___logSpriteRepresentations_15; }
	inline void set_logSpriteRepresentations_15(Dictionary_2_t3851943986 * value)
	{
		___logSpriteRepresentations_15 = value;
		Il2CppCodeGenWriteBarrier((&___logSpriteRepresentations_15), value);
	}

	inline static int32_t get_offset_of_collapseButtonNormalColor_16() { return static_cast<int32_t>(offsetof(DebugLogManager_t637049758, ___collapseButtonNormalColor_16)); }
	inline Color_t2555686324  get_collapseButtonNormalColor_16() const { return ___collapseButtonNormalColor_16; }
	inline Color_t2555686324 * get_address_of_collapseButtonNormalColor_16() { return &___collapseButtonNormalColor_16; }
	inline void set_collapseButtonNormalColor_16(Color_t2555686324  value)
	{
		___collapseButtonNormalColor_16 = value;
	}

	inline static int32_t get_offset_of_collapseButtonSelectedColor_17() { return static_cast<int32_t>(offsetof(DebugLogManager_t637049758, ___collapseButtonSelectedColor_17)); }
	inline Color_t2555686324  get_collapseButtonSelectedColor_17() const { return ___collapseButtonSelectedColor_17; }
	inline Color_t2555686324 * get_address_of_collapseButtonSelectedColor_17() { return &___collapseButtonSelectedColor_17; }
	inline void set_collapseButtonSelectedColor_17(Color_t2555686324  value)
	{
		___collapseButtonSelectedColor_17 = value;
	}

	inline static int32_t get_offset_of_filterButtonsNormalColor_18() { return static_cast<int32_t>(offsetof(DebugLogManager_t637049758, ___filterButtonsNormalColor_18)); }
	inline Color_t2555686324  get_filterButtonsNormalColor_18() const { return ___filterButtonsNormalColor_18; }
	inline Color_t2555686324 * get_address_of_filterButtonsNormalColor_18() { return &___filterButtonsNormalColor_18; }
	inline void set_filterButtonsNormalColor_18(Color_t2555686324  value)
	{
		___filterButtonsNormalColor_18 = value;
	}

	inline static int32_t get_offset_of_filterButtonsSelectedColor_19() { return static_cast<int32_t>(offsetof(DebugLogManager_t637049758, ___filterButtonsSelectedColor_19)); }
	inline Color_t2555686324  get_filterButtonsSelectedColor_19() const { return ___filterButtonsSelectedColor_19; }
	inline Color_t2555686324 * get_address_of_filterButtonsSelectedColor_19() { return &___filterButtonsSelectedColor_19; }
	inline void set_filterButtonsSelectedColor_19(Color_t2555686324  value)
	{
		___filterButtonsSelectedColor_19 = value;
	}

	inline static int32_t get_offset_of_logWindowTR_20() { return static_cast<int32_t>(offsetof(DebugLogManager_t637049758, ___logWindowTR_20)); }
	inline RectTransform_t3704657025 * get_logWindowTR_20() const { return ___logWindowTR_20; }
	inline RectTransform_t3704657025 ** get_address_of_logWindowTR_20() { return &___logWindowTR_20; }
	inline void set_logWindowTR_20(RectTransform_t3704657025 * value)
	{
		___logWindowTR_20 = value;
		Il2CppCodeGenWriteBarrier((&___logWindowTR_20), value);
	}

	inline static int32_t get_offset_of_canvasTR_21() { return static_cast<int32_t>(offsetof(DebugLogManager_t637049758, ___canvasTR_21)); }
	inline RectTransform_t3704657025 * get_canvasTR_21() const { return ___canvasTR_21; }
	inline RectTransform_t3704657025 ** get_address_of_canvasTR_21() { return &___canvasTR_21; }
	inline void set_canvasTR_21(RectTransform_t3704657025 * value)
	{
		___canvasTR_21 = value;
		Il2CppCodeGenWriteBarrier((&___canvasTR_21), value);
	}

	inline static int32_t get_offset_of_logItemsContainer_22() { return static_cast<int32_t>(offsetof(DebugLogManager_t637049758, ___logItemsContainer_22)); }
	inline RectTransform_t3704657025 * get_logItemsContainer_22() const { return ___logItemsContainer_22; }
	inline RectTransform_t3704657025 ** get_address_of_logItemsContainer_22() { return &___logItemsContainer_22; }
	inline void set_logItemsContainer_22(RectTransform_t3704657025 * value)
	{
		___logItemsContainer_22 = value;
		Il2CppCodeGenWriteBarrier((&___logItemsContainer_22), value);
	}

	inline static int32_t get_offset_of_commandInputField_23() { return static_cast<int32_t>(offsetof(DebugLogManager_t637049758, ___commandInputField_23)); }
	inline InputField_t3762917431 * get_commandInputField_23() const { return ___commandInputField_23; }
	inline InputField_t3762917431 ** get_address_of_commandInputField_23() { return &___commandInputField_23; }
	inline void set_commandInputField_23(InputField_t3762917431 * value)
	{
		___commandInputField_23 = value;
		Il2CppCodeGenWriteBarrier((&___commandInputField_23), value);
	}

	inline static int32_t get_offset_of_collapseButton_24() { return static_cast<int32_t>(offsetof(DebugLogManager_t637049758, ___collapseButton_24)); }
	inline Image_t2670269651 * get_collapseButton_24() const { return ___collapseButton_24; }
	inline Image_t2670269651 ** get_address_of_collapseButton_24() { return &___collapseButton_24; }
	inline void set_collapseButton_24(Image_t2670269651 * value)
	{
		___collapseButton_24 = value;
		Il2CppCodeGenWriteBarrier((&___collapseButton_24), value);
	}

	inline static int32_t get_offset_of_filterInfoButton_25() { return static_cast<int32_t>(offsetof(DebugLogManager_t637049758, ___filterInfoButton_25)); }
	inline Image_t2670269651 * get_filterInfoButton_25() const { return ___filterInfoButton_25; }
	inline Image_t2670269651 ** get_address_of_filterInfoButton_25() { return &___filterInfoButton_25; }
	inline void set_filterInfoButton_25(Image_t2670269651 * value)
	{
		___filterInfoButton_25 = value;
		Il2CppCodeGenWriteBarrier((&___filterInfoButton_25), value);
	}

	inline static int32_t get_offset_of_filterWarningButton_26() { return static_cast<int32_t>(offsetof(DebugLogManager_t637049758, ___filterWarningButton_26)); }
	inline Image_t2670269651 * get_filterWarningButton_26() const { return ___filterWarningButton_26; }
	inline Image_t2670269651 ** get_address_of_filterWarningButton_26() { return &___filterWarningButton_26; }
	inline void set_filterWarningButton_26(Image_t2670269651 * value)
	{
		___filterWarningButton_26 = value;
		Il2CppCodeGenWriteBarrier((&___filterWarningButton_26), value);
	}

	inline static int32_t get_offset_of_filterErrorButton_27() { return static_cast<int32_t>(offsetof(DebugLogManager_t637049758, ___filterErrorButton_27)); }
	inline Image_t2670269651 * get_filterErrorButton_27() const { return ___filterErrorButton_27; }
	inline Image_t2670269651 ** get_address_of_filterErrorButton_27() { return &___filterErrorButton_27; }
	inline void set_filterErrorButton_27(Image_t2670269651 * value)
	{
		___filterErrorButton_27 = value;
		Il2CppCodeGenWriteBarrier((&___filterErrorButton_27), value);
	}

	inline static int32_t get_offset_of_infoEntryCountText_28() { return static_cast<int32_t>(offsetof(DebugLogManager_t637049758, ___infoEntryCountText_28)); }
	inline Text_t1901882714 * get_infoEntryCountText_28() const { return ___infoEntryCountText_28; }
	inline Text_t1901882714 ** get_address_of_infoEntryCountText_28() { return &___infoEntryCountText_28; }
	inline void set_infoEntryCountText_28(Text_t1901882714 * value)
	{
		___infoEntryCountText_28 = value;
		Il2CppCodeGenWriteBarrier((&___infoEntryCountText_28), value);
	}

	inline static int32_t get_offset_of_warningEntryCountText_29() { return static_cast<int32_t>(offsetof(DebugLogManager_t637049758, ___warningEntryCountText_29)); }
	inline Text_t1901882714 * get_warningEntryCountText_29() const { return ___warningEntryCountText_29; }
	inline Text_t1901882714 ** get_address_of_warningEntryCountText_29() { return &___warningEntryCountText_29; }
	inline void set_warningEntryCountText_29(Text_t1901882714 * value)
	{
		___warningEntryCountText_29 = value;
		Il2CppCodeGenWriteBarrier((&___warningEntryCountText_29), value);
	}

	inline static int32_t get_offset_of_errorEntryCountText_30() { return static_cast<int32_t>(offsetof(DebugLogManager_t637049758, ___errorEntryCountText_30)); }
	inline Text_t1901882714 * get_errorEntryCountText_30() const { return ___errorEntryCountText_30; }
	inline Text_t1901882714 ** get_address_of_errorEntryCountText_30() { return &___errorEntryCountText_30; }
	inline void set_errorEntryCountText_30(Text_t1901882714 * value)
	{
		___errorEntryCountText_30 = value;
		Il2CppCodeGenWriteBarrier((&___errorEntryCountText_30), value);
	}

	inline static int32_t get_offset_of_snapToBottomButton_31() { return static_cast<int32_t>(offsetof(DebugLogManager_t637049758, ___snapToBottomButton_31)); }
	inline GameObject_t1113636619 * get_snapToBottomButton_31() const { return ___snapToBottomButton_31; }
	inline GameObject_t1113636619 ** get_address_of_snapToBottomButton_31() { return &___snapToBottomButton_31; }
	inline void set_snapToBottomButton_31(GameObject_t1113636619 * value)
	{
		___snapToBottomButton_31 = value;
		Il2CppCodeGenWriteBarrier((&___snapToBottomButton_31), value);
	}

	inline static int32_t get_offset_of_infoEntryCount_32() { return static_cast<int32_t>(offsetof(DebugLogManager_t637049758, ___infoEntryCount_32)); }
	inline int32_t get_infoEntryCount_32() const { return ___infoEntryCount_32; }
	inline int32_t* get_address_of_infoEntryCount_32() { return &___infoEntryCount_32; }
	inline void set_infoEntryCount_32(int32_t value)
	{
		___infoEntryCount_32 = value;
	}

	inline static int32_t get_offset_of_warningEntryCount_33() { return static_cast<int32_t>(offsetof(DebugLogManager_t637049758, ___warningEntryCount_33)); }
	inline int32_t get_warningEntryCount_33() const { return ___warningEntryCount_33; }
	inline int32_t* get_address_of_warningEntryCount_33() { return &___warningEntryCount_33; }
	inline void set_warningEntryCount_33(int32_t value)
	{
		___warningEntryCount_33 = value;
	}

	inline static int32_t get_offset_of_errorEntryCount_34() { return static_cast<int32_t>(offsetof(DebugLogManager_t637049758, ___errorEntryCount_34)); }
	inline int32_t get_errorEntryCount_34() const { return ___errorEntryCount_34; }
	inline int32_t* get_address_of_errorEntryCount_34() { return &___errorEntryCount_34; }
	inline void set_errorEntryCount_34(int32_t value)
	{
		___errorEntryCount_34 = value;
	}

	inline static int32_t get_offset_of_logWindowCanvasGroup_35() { return static_cast<int32_t>(offsetof(DebugLogManager_t637049758, ___logWindowCanvasGroup_35)); }
	inline CanvasGroup_t4083511760 * get_logWindowCanvasGroup_35() const { return ___logWindowCanvasGroup_35; }
	inline CanvasGroup_t4083511760 ** get_address_of_logWindowCanvasGroup_35() { return &___logWindowCanvasGroup_35; }
	inline void set_logWindowCanvasGroup_35(CanvasGroup_t4083511760 * value)
	{
		___logWindowCanvasGroup_35 = value;
		Il2CppCodeGenWriteBarrier((&___logWindowCanvasGroup_35), value);
	}

	inline static int32_t get_offset_of_isLogWindowVisible_36() { return static_cast<int32_t>(offsetof(DebugLogManager_t637049758, ___isLogWindowVisible_36)); }
	inline bool get_isLogWindowVisible_36() const { return ___isLogWindowVisible_36; }
	inline bool* get_address_of_isLogWindowVisible_36() { return &___isLogWindowVisible_36; }
	inline void set_isLogWindowVisible_36(bool value)
	{
		___isLogWindowVisible_36 = value;
	}

	inline static int32_t get_offset_of_screenDimensionsChanged_37() { return static_cast<int32_t>(offsetof(DebugLogManager_t637049758, ___screenDimensionsChanged_37)); }
	inline bool get_screenDimensionsChanged_37() const { return ___screenDimensionsChanged_37; }
	inline bool* get_address_of_screenDimensionsChanged_37() { return &___screenDimensionsChanged_37; }
	inline void set_screenDimensionsChanged_37(bool value)
	{
		___screenDimensionsChanged_37 = value;
	}

	inline static int32_t get_offset_of_popupManager_38() { return static_cast<int32_t>(offsetof(DebugLogManager_t637049758, ___popupManager_38)); }
	inline DebugLogPopup_t3403515216 * get_popupManager_38() const { return ___popupManager_38; }
	inline DebugLogPopup_t3403515216 ** get_address_of_popupManager_38() { return &___popupManager_38; }
	inline void set_popupManager_38(DebugLogPopup_t3403515216 * value)
	{
		___popupManager_38 = value;
		Il2CppCodeGenWriteBarrier((&___popupManager_38), value);
	}

	inline static int32_t get_offset_of_logItemsScrollRect_39() { return static_cast<int32_t>(offsetof(DebugLogManager_t637049758, ___logItemsScrollRect_39)); }
	inline ScrollRect_t4137855814 * get_logItemsScrollRect_39() const { return ___logItemsScrollRect_39; }
	inline ScrollRect_t4137855814 ** get_address_of_logItemsScrollRect_39() { return &___logItemsScrollRect_39; }
	inline void set_logItemsScrollRect_39(ScrollRect_t4137855814 * value)
	{
		___logItemsScrollRect_39 = value;
		Il2CppCodeGenWriteBarrier((&___logItemsScrollRect_39), value);
	}

	inline static int32_t get_offset_of_recycledListView_40() { return static_cast<int32_t>(offsetof(DebugLogManager_t637049758, ___recycledListView_40)); }
	inline DebugLogRecycledListView_t3570785762 * get_recycledListView_40() const { return ___recycledListView_40; }
	inline DebugLogRecycledListView_t3570785762 ** get_address_of_recycledListView_40() { return &___recycledListView_40; }
	inline void set_recycledListView_40(DebugLogRecycledListView_t3570785762 * value)
	{
		___recycledListView_40 = value;
		Il2CppCodeGenWriteBarrier((&___recycledListView_40), value);
	}

	inline static int32_t get_offset_of_isCollapseOn_41() { return static_cast<int32_t>(offsetof(DebugLogManager_t637049758, ___isCollapseOn_41)); }
	inline bool get_isCollapseOn_41() const { return ___isCollapseOn_41; }
	inline bool* get_address_of_isCollapseOn_41() { return &___isCollapseOn_41; }
	inline void set_isCollapseOn_41(bool value)
	{
		___isCollapseOn_41 = value;
	}

	inline static int32_t get_offset_of_logFilter_42() { return static_cast<int32_t>(offsetof(DebugLogManager_t637049758, ___logFilter_42)); }
	inline int32_t get_logFilter_42() const { return ___logFilter_42; }
	inline int32_t* get_address_of_logFilter_42() { return &___logFilter_42; }
	inline void set_logFilter_42(int32_t value)
	{
		___logFilter_42 = value;
	}

	inline static int32_t get_offset_of_snapToBottom_43() { return static_cast<int32_t>(offsetof(DebugLogManager_t637049758, ___snapToBottom_43)); }
	inline bool get_snapToBottom_43() const { return ___snapToBottom_43; }
	inline bool* get_address_of_snapToBottom_43() { return &___snapToBottom_43; }
	inline void set_snapToBottom_43(bool value)
	{
		___snapToBottom_43 = value;
	}

	inline static int32_t get_offset_of_collapsedLogEntries_44() { return static_cast<int32_t>(offsetof(DebugLogManager_t637049758, ___collapsedLogEntries_44)); }
	inline List_1_t421000984 * get_collapsedLogEntries_44() const { return ___collapsedLogEntries_44; }
	inline List_1_t421000984 ** get_address_of_collapsedLogEntries_44() { return &___collapsedLogEntries_44; }
	inline void set_collapsedLogEntries_44(List_1_t421000984 * value)
	{
		___collapsedLogEntries_44 = value;
		Il2CppCodeGenWriteBarrier((&___collapsedLogEntries_44), value);
	}

	inline static int32_t get_offset_of_collapsedLogEntriesMap_45() { return static_cast<int32_t>(offsetof(DebugLogManager_t637049758, ___collapsedLogEntriesMap_45)); }
	inline Dictionary_2_t663053759 * get_collapsedLogEntriesMap_45() const { return ___collapsedLogEntriesMap_45; }
	inline Dictionary_2_t663053759 ** get_address_of_collapsedLogEntriesMap_45() { return &___collapsedLogEntriesMap_45; }
	inline void set_collapsedLogEntriesMap_45(Dictionary_2_t663053759 * value)
	{
		___collapsedLogEntriesMap_45 = value;
		Il2CppCodeGenWriteBarrier((&___collapsedLogEntriesMap_45), value);
	}

	inline static int32_t get_offset_of_uncollapsedLogEntriesIndices_46() { return static_cast<int32_t>(offsetof(DebugLogManager_t637049758, ___uncollapsedLogEntriesIndices_46)); }
	inline DebugLogIndexList_t3544812386 * get_uncollapsedLogEntriesIndices_46() const { return ___uncollapsedLogEntriesIndices_46; }
	inline DebugLogIndexList_t3544812386 ** get_address_of_uncollapsedLogEntriesIndices_46() { return &___uncollapsedLogEntriesIndices_46; }
	inline void set_uncollapsedLogEntriesIndices_46(DebugLogIndexList_t3544812386 * value)
	{
		___uncollapsedLogEntriesIndices_46 = value;
		Il2CppCodeGenWriteBarrier((&___uncollapsedLogEntriesIndices_46), value);
	}

	inline static int32_t get_offset_of_indicesOfListEntriesToShow_47() { return static_cast<int32_t>(offsetof(DebugLogManager_t637049758, ___indicesOfListEntriesToShow_47)); }
	inline DebugLogIndexList_t3544812386 * get_indicesOfListEntriesToShow_47() const { return ___indicesOfListEntriesToShow_47; }
	inline DebugLogIndexList_t3544812386 ** get_address_of_indicesOfListEntriesToShow_47() { return &___indicesOfListEntriesToShow_47; }
	inline void set_indicesOfListEntriesToShow_47(DebugLogIndexList_t3544812386 * value)
	{
		___indicesOfListEntriesToShow_47 = value;
		Il2CppCodeGenWriteBarrier((&___indicesOfListEntriesToShow_47), value);
	}

	inline static int32_t get_offset_of_pooledLogItems_48() { return static_cast<int32_t>(offsetof(DebugLogManager_t637049758, ___pooledLogItems_48)); }
	inline List_1_t2604182181 * get_pooledLogItems_48() const { return ___pooledLogItems_48; }
	inline List_1_t2604182181 ** get_address_of_pooledLogItems_48() { return &___pooledLogItems_48; }
	inline void set_pooledLogItems_48(List_1_t2604182181 * value)
	{
		___pooledLogItems_48 = value;
		Il2CppCodeGenWriteBarrier((&___pooledLogItems_48), value);
	}

	inline static int32_t get_offset_of_nullPointerEventData_49() { return static_cast<int32_t>(offsetof(DebugLogManager_t637049758, ___nullPointerEventData_49)); }
	inline PointerEventData_t3807901092 * get_nullPointerEventData_49() const { return ___nullPointerEventData_49; }
	inline PointerEventData_t3807901092 ** get_address_of_nullPointerEventData_49() { return &___nullPointerEventData_49; }
	inline void set_nullPointerEventData_49(PointerEventData_t3807901092 * value)
	{
		___nullPointerEventData_49 = value;
		Il2CppCodeGenWriteBarrier((&___nullPointerEventData_49), value);
	}
};

struct DebugLogManager_t637049758_StaticFields
{
public:
	// IngameDebugConsole.DebugLogManager IngameDebugConsole.DebugLogManager::instance
	DebugLogManager_t637049758 * ___instance_4;

public:
	inline static int32_t get_offset_of_instance_4() { return static_cast<int32_t>(offsetof(DebugLogManager_t637049758_StaticFields, ___instance_4)); }
	inline DebugLogManager_t637049758 * get_instance_4() const { return ___instance_4; }
	inline DebugLogManager_t637049758 ** get_address_of_instance_4() { return &___instance_4; }
	inline void set_instance_4(DebugLogManager_t637049758 * value)
	{
		___instance_4 = value;
		Il2CppCodeGenWriteBarrier((&___instance_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEBUGLOGMANAGER_T637049758_H
#ifndef DEBUGLOGPOPUP_T3403515216_H
#define DEBUGLOGPOPUP_T3403515216_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IngameDebugConsole.DebugLogPopup
struct  DebugLogPopup_t3403515216  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.RectTransform IngameDebugConsole.DebugLogPopup::popupTransform
	RectTransform_t3704657025 * ___popupTransform_4;
	// UnityEngine.Vector2 IngameDebugConsole.DebugLogPopup::halfSize
	Vector2_t2156229523  ___halfSize_5;
	// UnityEngine.UI.Image IngameDebugConsole.DebugLogPopup::backgroundImage
	Image_t2670269651 * ___backgroundImage_6;
	// UnityEngine.CanvasGroup IngameDebugConsole.DebugLogPopup::canvasGroup
	CanvasGroup_t4083511760 * ___canvasGroup_7;
	// IngameDebugConsole.DebugLogManager IngameDebugConsole.DebugLogPopup::debugManager
	DebugLogManager_t637049758 * ___debugManager_8;
	// UnityEngine.UI.Text IngameDebugConsole.DebugLogPopup::newInfoCountText
	Text_t1901882714 * ___newInfoCountText_9;
	// UnityEngine.UI.Text IngameDebugConsole.DebugLogPopup::newWarningCountText
	Text_t1901882714 * ___newWarningCountText_10;
	// UnityEngine.UI.Text IngameDebugConsole.DebugLogPopup::newErrorCountText
	Text_t1901882714 * ___newErrorCountText_11;
	// System.Int32 IngameDebugConsole.DebugLogPopup::newInfoCount
	int32_t ___newInfoCount_12;
	// System.Int32 IngameDebugConsole.DebugLogPopup::newWarningCount
	int32_t ___newWarningCount_13;
	// System.Int32 IngameDebugConsole.DebugLogPopup::newErrorCount
	int32_t ___newErrorCount_14;
	// UnityEngine.Color IngameDebugConsole.DebugLogPopup::normalColor
	Color_t2555686324  ___normalColor_15;
	// UnityEngine.Color IngameDebugConsole.DebugLogPopup::alertColorInfo
	Color_t2555686324  ___alertColorInfo_16;
	// UnityEngine.Color IngameDebugConsole.DebugLogPopup::alertColorWarning
	Color_t2555686324  ___alertColorWarning_17;
	// UnityEngine.Color IngameDebugConsole.DebugLogPopup::alertColorError
	Color_t2555686324  ___alertColorError_18;
	// System.Boolean IngameDebugConsole.DebugLogPopup::isPopupBeingDragged
	bool ___isPopupBeingDragged_19;
	// System.Collections.IEnumerator IngameDebugConsole.DebugLogPopup::moveToPosCoroutine
	RuntimeObject* ___moveToPosCoroutine_20;

public:
	inline static int32_t get_offset_of_popupTransform_4() { return static_cast<int32_t>(offsetof(DebugLogPopup_t3403515216, ___popupTransform_4)); }
	inline RectTransform_t3704657025 * get_popupTransform_4() const { return ___popupTransform_4; }
	inline RectTransform_t3704657025 ** get_address_of_popupTransform_4() { return &___popupTransform_4; }
	inline void set_popupTransform_4(RectTransform_t3704657025 * value)
	{
		___popupTransform_4 = value;
		Il2CppCodeGenWriteBarrier((&___popupTransform_4), value);
	}

	inline static int32_t get_offset_of_halfSize_5() { return static_cast<int32_t>(offsetof(DebugLogPopup_t3403515216, ___halfSize_5)); }
	inline Vector2_t2156229523  get_halfSize_5() const { return ___halfSize_5; }
	inline Vector2_t2156229523 * get_address_of_halfSize_5() { return &___halfSize_5; }
	inline void set_halfSize_5(Vector2_t2156229523  value)
	{
		___halfSize_5 = value;
	}

	inline static int32_t get_offset_of_backgroundImage_6() { return static_cast<int32_t>(offsetof(DebugLogPopup_t3403515216, ___backgroundImage_6)); }
	inline Image_t2670269651 * get_backgroundImage_6() const { return ___backgroundImage_6; }
	inline Image_t2670269651 ** get_address_of_backgroundImage_6() { return &___backgroundImage_6; }
	inline void set_backgroundImage_6(Image_t2670269651 * value)
	{
		___backgroundImage_6 = value;
		Il2CppCodeGenWriteBarrier((&___backgroundImage_6), value);
	}

	inline static int32_t get_offset_of_canvasGroup_7() { return static_cast<int32_t>(offsetof(DebugLogPopup_t3403515216, ___canvasGroup_7)); }
	inline CanvasGroup_t4083511760 * get_canvasGroup_7() const { return ___canvasGroup_7; }
	inline CanvasGroup_t4083511760 ** get_address_of_canvasGroup_7() { return &___canvasGroup_7; }
	inline void set_canvasGroup_7(CanvasGroup_t4083511760 * value)
	{
		___canvasGroup_7 = value;
		Il2CppCodeGenWriteBarrier((&___canvasGroup_7), value);
	}

	inline static int32_t get_offset_of_debugManager_8() { return static_cast<int32_t>(offsetof(DebugLogPopup_t3403515216, ___debugManager_8)); }
	inline DebugLogManager_t637049758 * get_debugManager_8() const { return ___debugManager_8; }
	inline DebugLogManager_t637049758 ** get_address_of_debugManager_8() { return &___debugManager_8; }
	inline void set_debugManager_8(DebugLogManager_t637049758 * value)
	{
		___debugManager_8 = value;
		Il2CppCodeGenWriteBarrier((&___debugManager_8), value);
	}

	inline static int32_t get_offset_of_newInfoCountText_9() { return static_cast<int32_t>(offsetof(DebugLogPopup_t3403515216, ___newInfoCountText_9)); }
	inline Text_t1901882714 * get_newInfoCountText_9() const { return ___newInfoCountText_9; }
	inline Text_t1901882714 ** get_address_of_newInfoCountText_9() { return &___newInfoCountText_9; }
	inline void set_newInfoCountText_9(Text_t1901882714 * value)
	{
		___newInfoCountText_9 = value;
		Il2CppCodeGenWriteBarrier((&___newInfoCountText_9), value);
	}

	inline static int32_t get_offset_of_newWarningCountText_10() { return static_cast<int32_t>(offsetof(DebugLogPopup_t3403515216, ___newWarningCountText_10)); }
	inline Text_t1901882714 * get_newWarningCountText_10() const { return ___newWarningCountText_10; }
	inline Text_t1901882714 ** get_address_of_newWarningCountText_10() { return &___newWarningCountText_10; }
	inline void set_newWarningCountText_10(Text_t1901882714 * value)
	{
		___newWarningCountText_10 = value;
		Il2CppCodeGenWriteBarrier((&___newWarningCountText_10), value);
	}

	inline static int32_t get_offset_of_newErrorCountText_11() { return static_cast<int32_t>(offsetof(DebugLogPopup_t3403515216, ___newErrorCountText_11)); }
	inline Text_t1901882714 * get_newErrorCountText_11() const { return ___newErrorCountText_11; }
	inline Text_t1901882714 ** get_address_of_newErrorCountText_11() { return &___newErrorCountText_11; }
	inline void set_newErrorCountText_11(Text_t1901882714 * value)
	{
		___newErrorCountText_11 = value;
		Il2CppCodeGenWriteBarrier((&___newErrorCountText_11), value);
	}

	inline static int32_t get_offset_of_newInfoCount_12() { return static_cast<int32_t>(offsetof(DebugLogPopup_t3403515216, ___newInfoCount_12)); }
	inline int32_t get_newInfoCount_12() const { return ___newInfoCount_12; }
	inline int32_t* get_address_of_newInfoCount_12() { return &___newInfoCount_12; }
	inline void set_newInfoCount_12(int32_t value)
	{
		___newInfoCount_12 = value;
	}

	inline static int32_t get_offset_of_newWarningCount_13() { return static_cast<int32_t>(offsetof(DebugLogPopup_t3403515216, ___newWarningCount_13)); }
	inline int32_t get_newWarningCount_13() const { return ___newWarningCount_13; }
	inline int32_t* get_address_of_newWarningCount_13() { return &___newWarningCount_13; }
	inline void set_newWarningCount_13(int32_t value)
	{
		___newWarningCount_13 = value;
	}

	inline static int32_t get_offset_of_newErrorCount_14() { return static_cast<int32_t>(offsetof(DebugLogPopup_t3403515216, ___newErrorCount_14)); }
	inline int32_t get_newErrorCount_14() const { return ___newErrorCount_14; }
	inline int32_t* get_address_of_newErrorCount_14() { return &___newErrorCount_14; }
	inline void set_newErrorCount_14(int32_t value)
	{
		___newErrorCount_14 = value;
	}

	inline static int32_t get_offset_of_normalColor_15() { return static_cast<int32_t>(offsetof(DebugLogPopup_t3403515216, ___normalColor_15)); }
	inline Color_t2555686324  get_normalColor_15() const { return ___normalColor_15; }
	inline Color_t2555686324 * get_address_of_normalColor_15() { return &___normalColor_15; }
	inline void set_normalColor_15(Color_t2555686324  value)
	{
		___normalColor_15 = value;
	}

	inline static int32_t get_offset_of_alertColorInfo_16() { return static_cast<int32_t>(offsetof(DebugLogPopup_t3403515216, ___alertColorInfo_16)); }
	inline Color_t2555686324  get_alertColorInfo_16() const { return ___alertColorInfo_16; }
	inline Color_t2555686324 * get_address_of_alertColorInfo_16() { return &___alertColorInfo_16; }
	inline void set_alertColorInfo_16(Color_t2555686324  value)
	{
		___alertColorInfo_16 = value;
	}

	inline static int32_t get_offset_of_alertColorWarning_17() { return static_cast<int32_t>(offsetof(DebugLogPopup_t3403515216, ___alertColorWarning_17)); }
	inline Color_t2555686324  get_alertColorWarning_17() const { return ___alertColorWarning_17; }
	inline Color_t2555686324 * get_address_of_alertColorWarning_17() { return &___alertColorWarning_17; }
	inline void set_alertColorWarning_17(Color_t2555686324  value)
	{
		___alertColorWarning_17 = value;
	}

	inline static int32_t get_offset_of_alertColorError_18() { return static_cast<int32_t>(offsetof(DebugLogPopup_t3403515216, ___alertColorError_18)); }
	inline Color_t2555686324  get_alertColorError_18() const { return ___alertColorError_18; }
	inline Color_t2555686324 * get_address_of_alertColorError_18() { return &___alertColorError_18; }
	inline void set_alertColorError_18(Color_t2555686324  value)
	{
		___alertColorError_18 = value;
	}

	inline static int32_t get_offset_of_isPopupBeingDragged_19() { return static_cast<int32_t>(offsetof(DebugLogPopup_t3403515216, ___isPopupBeingDragged_19)); }
	inline bool get_isPopupBeingDragged_19() const { return ___isPopupBeingDragged_19; }
	inline bool* get_address_of_isPopupBeingDragged_19() { return &___isPopupBeingDragged_19; }
	inline void set_isPopupBeingDragged_19(bool value)
	{
		___isPopupBeingDragged_19 = value;
	}

	inline static int32_t get_offset_of_moveToPosCoroutine_20() { return static_cast<int32_t>(offsetof(DebugLogPopup_t3403515216, ___moveToPosCoroutine_20)); }
	inline RuntimeObject* get_moveToPosCoroutine_20() const { return ___moveToPosCoroutine_20; }
	inline RuntimeObject** get_address_of_moveToPosCoroutine_20() { return &___moveToPosCoroutine_20; }
	inline void set_moveToPosCoroutine_20(RuntimeObject* value)
	{
		___moveToPosCoroutine_20 = value;
		Il2CppCodeGenWriteBarrier((&___moveToPosCoroutine_20), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEBUGLOGPOPUP_T3403515216_H
#ifndef DEBUGLOGRECYCLEDLISTVIEW_T3570785762_H
#define DEBUGLOGRECYCLEDLISTVIEW_T3570785762_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IngameDebugConsole.DebugLogRecycledListView
struct  DebugLogRecycledListView_t3570785762  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.RectTransform IngameDebugConsole.DebugLogRecycledListView::transformComponent
	RectTransform_t3704657025 * ___transformComponent_4;
	// UnityEngine.RectTransform IngameDebugConsole.DebugLogRecycledListView::viewportTransform
	RectTransform_t3704657025 * ___viewportTransform_5;
	// IngameDebugConsole.DebugLogManager IngameDebugConsole.DebugLogRecycledListView::debugManager
	DebugLogManager_t637049758 * ___debugManager_6;
	// UnityEngine.Color IngameDebugConsole.DebugLogRecycledListView::logItemNormalColor1
	Color_t2555686324  ___logItemNormalColor1_7;
	// UnityEngine.Color IngameDebugConsole.DebugLogRecycledListView::logItemNormalColor2
	Color_t2555686324  ___logItemNormalColor2_8;
	// UnityEngine.Color IngameDebugConsole.DebugLogRecycledListView::logItemSelectedColor
	Color_t2555686324  ___logItemSelectedColor_9;
	// IngameDebugConsole.DebugLogManager IngameDebugConsole.DebugLogRecycledListView::manager
	DebugLogManager_t637049758 * ___manager_10;
	// System.Single IngameDebugConsole.DebugLogRecycledListView::logItemHeight
	float ___logItemHeight_11;
	// System.Single IngameDebugConsole.DebugLogRecycledListView::_1OverLogItemHeight
	float ____1OverLogItemHeight_12;
	// System.Single IngameDebugConsole.DebugLogRecycledListView::viewportHeight
	float ___viewportHeight_13;
	// System.Collections.Generic.List`1<IngameDebugConsole.DebugLogEntry> IngameDebugConsole.DebugLogRecycledListView::collapsedLogEntries
	List_1_t421000984 * ___collapsedLogEntries_14;
	// IngameDebugConsole.DebugLogIndexList IngameDebugConsole.DebugLogRecycledListView::indicesOfEntriesToShow
	DebugLogIndexList_t3544812386 * ___indicesOfEntriesToShow_15;
	// System.Int32 IngameDebugConsole.DebugLogRecycledListView::indexOfSelectedLogEntry
	int32_t ___indexOfSelectedLogEntry_16;
	// System.Single IngameDebugConsole.DebugLogRecycledListView::positionOfSelectedLogEntry
	float ___positionOfSelectedLogEntry_17;
	// System.Single IngameDebugConsole.DebugLogRecycledListView::heightOfSelectedLogEntry
	float ___heightOfSelectedLogEntry_18;
	// System.Single IngameDebugConsole.DebugLogRecycledListView::deltaHeightOfSelectedLogEntry
	float ___deltaHeightOfSelectedLogEntry_19;
	// System.Collections.Generic.Dictionary`2<System.Int32,IngameDebugConsole.DebugLogItem> IngameDebugConsole.DebugLogRecycledListView::logItemsAtIndices
	Dictionary_2_t20820770 * ___logItemsAtIndices_20;
	// System.Boolean IngameDebugConsole.DebugLogRecycledListView::isCollapseOn
	bool ___isCollapseOn_21;
	// System.Int32 IngameDebugConsole.DebugLogRecycledListView::currentTopIndex
	int32_t ___currentTopIndex_22;
	// System.Int32 IngameDebugConsole.DebugLogRecycledListView::currentBottomIndex
	int32_t ___currentBottomIndex_23;

public:
	inline static int32_t get_offset_of_transformComponent_4() { return static_cast<int32_t>(offsetof(DebugLogRecycledListView_t3570785762, ___transformComponent_4)); }
	inline RectTransform_t3704657025 * get_transformComponent_4() const { return ___transformComponent_4; }
	inline RectTransform_t3704657025 ** get_address_of_transformComponent_4() { return &___transformComponent_4; }
	inline void set_transformComponent_4(RectTransform_t3704657025 * value)
	{
		___transformComponent_4 = value;
		Il2CppCodeGenWriteBarrier((&___transformComponent_4), value);
	}

	inline static int32_t get_offset_of_viewportTransform_5() { return static_cast<int32_t>(offsetof(DebugLogRecycledListView_t3570785762, ___viewportTransform_5)); }
	inline RectTransform_t3704657025 * get_viewportTransform_5() const { return ___viewportTransform_5; }
	inline RectTransform_t3704657025 ** get_address_of_viewportTransform_5() { return &___viewportTransform_5; }
	inline void set_viewportTransform_5(RectTransform_t3704657025 * value)
	{
		___viewportTransform_5 = value;
		Il2CppCodeGenWriteBarrier((&___viewportTransform_5), value);
	}

	inline static int32_t get_offset_of_debugManager_6() { return static_cast<int32_t>(offsetof(DebugLogRecycledListView_t3570785762, ___debugManager_6)); }
	inline DebugLogManager_t637049758 * get_debugManager_6() const { return ___debugManager_6; }
	inline DebugLogManager_t637049758 ** get_address_of_debugManager_6() { return &___debugManager_6; }
	inline void set_debugManager_6(DebugLogManager_t637049758 * value)
	{
		___debugManager_6 = value;
		Il2CppCodeGenWriteBarrier((&___debugManager_6), value);
	}

	inline static int32_t get_offset_of_logItemNormalColor1_7() { return static_cast<int32_t>(offsetof(DebugLogRecycledListView_t3570785762, ___logItemNormalColor1_7)); }
	inline Color_t2555686324  get_logItemNormalColor1_7() const { return ___logItemNormalColor1_7; }
	inline Color_t2555686324 * get_address_of_logItemNormalColor1_7() { return &___logItemNormalColor1_7; }
	inline void set_logItemNormalColor1_7(Color_t2555686324  value)
	{
		___logItemNormalColor1_7 = value;
	}

	inline static int32_t get_offset_of_logItemNormalColor2_8() { return static_cast<int32_t>(offsetof(DebugLogRecycledListView_t3570785762, ___logItemNormalColor2_8)); }
	inline Color_t2555686324  get_logItemNormalColor2_8() const { return ___logItemNormalColor2_8; }
	inline Color_t2555686324 * get_address_of_logItemNormalColor2_8() { return &___logItemNormalColor2_8; }
	inline void set_logItemNormalColor2_8(Color_t2555686324  value)
	{
		___logItemNormalColor2_8 = value;
	}

	inline static int32_t get_offset_of_logItemSelectedColor_9() { return static_cast<int32_t>(offsetof(DebugLogRecycledListView_t3570785762, ___logItemSelectedColor_9)); }
	inline Color_t2555686324  get_logItemSelectedColor_9() const { return ___logItemSelectedColor_9; }
	inline Color_t2555686324 * get_address_of_logItemSelectedColor_9() { return &___logItemSelectedColor_9; }
	inline void set_logItemSelectedColor_9(Color_t2555686324  value)
	{
		___logItemSelectedColor_9 = value;
	}

	inline static int32_t get_offset_of_manager_10() { return static_cast<int32_t>(offsetof(DebugLogRecycledListView_t3570785762, ___manager_10)); }
	inline DebugLogManager_t637049758 * get_manager_10() const { return ___manager_10; }
	inline DebugLogManager_t637049758 ** get_address_of_manager_10() { return &___manager_10; }
	inline void set_manager_10(DebugLogManager_t637049758 * value)
	{
		___manager_10 = value;
		Il2CppCodeGenWriteBarrier((&___manager_10), value);
	}

	inline static int32_t get_offset_of_logItemHeight_11() { return static_cast<int32_t>(offsetof(DebugLogRecycledListView_t3570785762, ___logItemHeight_11)); }
	inline float get_logItemHeight_11() const { return ___logItemHeight_11; }
	inline float* get_address_of_logItemHeight_11() { return &___logItemHeight_11; }
	inline void set_logItemHeight_11(float value)
	{
		___logItemHeight_11 = value;
	}

	inline static int32_t get_offset_of__1OverLogItemHeight_12() { return static_cast<int32_t>(offsetof(DebugLogRecycledListView_t3570785762, ____1OverLogItemHeight_12)); }
	inline float get__1OverLogItemHeight_12() const { return ____1OverLogItemHeight_12; }
	inline float* get_address_of__1OverLogItemHeight_12() { return &____1OverLogItemHeight_12; }
	inline void set__1OverLogItemHeight_12(float value)
	{
		____1OverLogItemHeight_12 = value;
	}

	inline static int32_t get_offset_of_viewportHeight_13() { return static_cast<int32_t>(offsetof(DebugLogRecycledListView_t3570785762, ___viewportHeight_13)); }
	inline float get_viewportHeight_13() const { return ___viewportHeight_13; }
	inline float* get_address_of_viewportHeight_13() { return &___viewportHeight_13; }
	inline void set_viewportHeight_13(float value)
	{
		___viewportHeight_13 = value;
	}

	inline static int32_t get_offset_of_collapsedLogEntries_14() { return static_cast<int32_t>(offsetof(DebugLogRecycledListView_t3570785762, ___collapsedLogEntries_14)); }
	inline List_1_t421000984 * get_collapsedLogEntries_14() const { return ___collapsedLogEntries_14; }
	inline List_1_t421000984 ** get_address_of_collapsedLogEntries_14() { return &___collapsedLogEntries_14; }
	inline void set_collapsedLogEntries_14(List_1_t421000984 * value)
	{
		___collapsedLogEntries_14 = value;
		Il2CppCodeGenWriteBarrier((&___collapsedLogEntries_14), value);
	}

	inline static int32_t get_offset_of_indicesOfEntriesToShow_15() { return static_cast<int32_t>(offsetof(DebugLogRecycledListView_t3570785762, ___indicesOfEntriesToShow_15)); }
	inline DebugLogIndexList_t3544812386 * get_indicesOfEntriesToShow_15() const { return ___indicesOfEntriesToShow_15; }
	inline DebugLogIndexList_t3544812386 ** get_address_of_indicesOfEntriesToShow_15() { return &___indicesOfEntriesToShow_15; }
	inline void set_indicesOfEntriesToShow_15(DebugLogIndexList_t3544812386 * value)
	{
		___indicesOfEntriesToShow_15 = value;
		Il2CppCodeGenWriteBarrier((&___indicesOfEntriesToShow_15), value);
	}

	inline static int32_t get_offset_of_indexOfSelectedLogEntry_16() { return static_cast<int32_t>(offsetof(DebugLogRecycledListView_t3570785762, ___indexOfSelectedLogEntry_16)); }
	inline int32_t get_indexOfSelectedLogEntry_16() const { return ___indexOfSelectedLogEntry_16; }
	inline int32_t* get_address_of_indexOfSelectedLogEntry_16() { return &___indexOfSelectedLogEntry_16; }
	inline void set_indexOfSelectedLogEntry_16(int32_t value)
	{
		___indexOfSelectedLogEntry_16 = value;
	}

	inline static int32_t get_offset_of_positionOfSelectedLogEntry_17() { return static_cast<int32_t>(offsetof(DebugLogRecycledListView_t3570785762, ___positionOfSelectedLogEntry_17)); }
	inline float get_positionOfSelectedLogEntry_17() const { return ___positionOfSelectedLogEntry_17; }
	inline float* get_address_of_positionOfSelectedLogEntry_17() { return &___positionOfSelectedLogEntry_17; }
	inline void set_positionOfSelectedLogEntry_17(float value)
	{
		___positionOfSelectedLogEntry_17 = value;
	}

	inline static int32_t get_offset_of_heightOfSelectedLogEntry_18() { return static_cast<int32_t>(offsetof(DebugLogRecycledListView_t3570785762, ___heightOfSelectedLogEntry_18)); }
	inline float get_heightOfSelectedLogEntry_18() const { return ___heightOfSelectedLogEntry_18; }
	inline float* get_address_of_heightOfSelectedLogEntry_18() { return &___heightOfSelectedLogEntry_18; }
	inline void set_heightOfSelectedLogEntry_18(float value)
	{
		___heightOfSelectedLogEntry_18 = value;
	}

	inline static int32_t get_offset_of_deltaHeightOfSelectedLogEntry_19() { return static_cast<int32_t>(offsetof(DebugLogRecycledListView_t3570785762, ___deltaHeightOfSelectedLogEntry_19)); }
	inline float get_deltaHeightOfSelectedLogEntry_19() const { return ___deltaHeightOfSelectedLogEntry_19; }
	inline float* get_address_of_deltaHeightOfSelectedLogEntry_19() { return &___deltaHeightOfSelectedLogEntry_19; }
	inline void set_deltaHeightOfSelectedLogEntry_19(float value)
	{
		___deltaHeightOfSelectedLogEntry_19 = value;
	}

	inline static int32_t get_offset_of_logItemsAtIndices_20() { return static_cast<int32_t>(offsetof(DebugLogRecycledListView_t3570785762, ___logItemsAtIndices_20)); }
	inline Dictionary_2_t20820770 * get_logItemsAtIndices_20() const { return ___logItemsAtIndices_20; }
	inline Dictionary_2_t20820770 ** get_address_of_logItemsAtIndices_20() { return &___logItemsAtIndices_20; }
	inline void set_logItemsAtIndices_20(Dictionary_2_t20820770 * value)
	{
		___logItemsAtIndices_20 = value;
		Il2CppCodeGenWriteBarrier((&___logItemsAtIndices_20), value);
	}

	inline static int32_t get_offset_of_isCollapseOn_21() { return static_cast<int32_t>(offsetof(DebugLogRecycledListView_t3570785762, ___isCollapseOn_21)); }
	inline bool get_isCollapseOn_21() const { return ___isCollapseOn_21; }
	inline bool* get_address_of_isCollapseOn_21() { return &___isCollapseOn_21; }
	inline void set_isCollapseOn_21(bool value)
	{
		___isCollapseOn_21 = value;
	}

	inline static int32_t get_offset_of_currentTopIndex_22() { return static_cast<int32_t>(offsetof(DebugLogRecycledListView_t3570785762, ___currentTopIndex_22)); }
	inline int32_t get_currentTopIndex_22() const { return ___currentTopIndex_22; }
	inline int32_t* get_address_of_currentTopIndex_22() { return &___currentTopIndex_22; }
	inline void set_currentTopIndex_22(int32_t value)
	{
		___currentTopIndex_22 = value;
	}

	inline static int32_t get_offset_of_currentBottomIndex_23() { return static_cast<int32_t>(offsetof(DebugLogRecycledListView_t3570785762, ___currentBottomIndex_23)); }
	inline int32_t get_currentBottomIndex_23() const { return ___currentBottomIndex_23; }
	inline int32_t* get_address_of_currentBottomIndex_23() { return &___currentBottomIndex_23; }
	inline void set_currentBottomIndex_23(int32_t value)
	{
		___currentBottomIndex_23 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEBUGLOGRECYCLEDLISTVIEW_T3570785762_H
#ifndef DEBUGSONSCROLLLISTENER_T2173937987_H
#define DEBUGSONSCROLLLISTENER_T2173937987_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IngameDebugConsole.DebugsOnScrollListener
struct  DebugsOnScrollListener_t2173937987  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.UI.ScrollRect IngameDebugConsole.DebugsOnScrollListener::debugsScrollRect
	ScrollRect_t4137855814 * ___debugsScrollRect_4;
	// IngameDebugConsole.DebugLogManager IngameDebugConsole.DebugsOnScrollListener::debugLogManager
	DebugLogManager_t637049758 * ___debugLogManager_5;

public:
	inline static int32_t get_offset_of_debugsScrollRect_4() { return static_cast<int32_t>(offsetof(DebugsOnScrollListener_t2173937987, ___debugsScrollRect_4)); }
	inline ScrollRect_t4137855814 * get_debugsScrollRect_4() const { return ___debugsScrollRect_4; }
	inline ScrollRect_t4137855814 ** get_address_of_debugsScrollRect_4() { return &___debugsScrollRect_4; }
	inline void set_debugsScrollRect_4(ScrollRect_t4137855814 * value)
	{
		___debugsScrollRect_4 = value;
		Il2CppCodeGenWriteBarrier((&___debugsScrollRect_4), value);
	}

	inline static int32_t get_offset_of_debugLogManager_5() { return static_cast<int32_t>(offsetof(DebugsOnScrollListener_t2173937987, ___debugLogManager_5)); }
	inline DebugLogManager_t637049758 * get_debugLogManager_5() const { return ___debugLogManager_5; }
	inline DebugLogManager_t637049758 ** get_address_of_debugLogManager_5() { return &___debugLogManager_5; }
	inline void set_debugLogManager_5(DebugLogManager_t637049758 * value)
	{
		___debugLogManager_5 = value;
		Il2CppCodeGenWriteBarrier((&___debugLogManager_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEBUGSONSCROLLLISTENER_T2173937987_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3000 = { sizeof (U3CDOBlendableColorU3Ec__AnonStorey25_t2770270650), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3000[2] = 
{
	U3CDOBlendableColorU3Ec__AnonStorey25_t2770270650::get_offset_of_to_0(),
	U3CDOBlendableColorU3Ec__AnonStorey25_t2770270650::get_offset_of_target_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3001 = { sizeof (U3CDOBlendableColorU3Ec__AnonStorey26_t2770270649), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3001[2] = 
{
	U3CDOBlendableColorU3Ec__AnonStorey26_t2770270649::get_offset_of_to_0(),
	U3CDOBlendableColorU3Ec__AnonStorey26_t2770270649::get_offset_of_target_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3002 = { sizeof (DOTweenModuleUnityVersion_t1642064347), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3003 = { sizeof (DOTweenCYInstruction_t3873353372), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3004 = { sizeof (WaitForCompletion_t2334662098), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3004[1] = 
{
	WaitForCompletion_t2334662098::get_offset_of_t_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3005 = { sizeof (WaitForRewind_t1388328290), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3005[1] = 
{
	WaitForRewind_t1388328290::get_offset_of_t_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3006 = { sizeof (WaitForKill_t3778347930), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3006[1] = 
{
	WaitForKill_t3778347930::get_offset_of_t_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3007 = { sizeof (WaitForElapsedLoops_t268076146), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3007[2] = 
{
	WaitForElapsedLoops_t268076146::get_offset_of_t_0(),
	WaitForElapsedLoops_t268076146::get_offset_of_elapsedLoops_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3008 = { sizeof (WaitForPosition_t3077499975), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3008[2] = 
{
	WaitForPosition_t3077499975::get_offset_of_t_0(),
	WaitForPosition_t3077499975::get_offset_of_position_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3009 = { sizeof (WaitForStart_t1801061045), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3009[1] = 
{
	WaitForStart_t1801061045::get_offset_of_t_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3010 = { sizeof (DOTweenModuleUtils_t382849935), -1, sizeof(DOTweenModuleUtils_t382849935_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3010[2] = 
{
	DOTweenModuleUtils_t382849935_StaticFields::get_offset_of__initialized_0(),
	DOTweenModuleUtils_t382849935_StaticFields::get_offset_of_U3CU3Ef__mgU24cache0_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3011 = { sizeof (Physics_t3684376097), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3012 = { sizeof (ConsoleMethodAttribute_t1602398842), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3012[2] = 
{
	ConsoleMethodAttribute_t1602398842::get_offset_of_m_command_0(),
	ConsoleMethodAttribute_t1602398842::get_offset_of_m_description_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3013 = { sizeof (ConsoleMethodInfo_t3560758192), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3013[4] = 
{
	ConsoleMethodInfo_t3560758192::get_offset_of_method_0(),
	ConsoleMethodInfo_t3560758192::get_offset_of_parameterTypes_1(),
	ConsoleMethodInfo_t3560758192::get_offset_of_instance_2(),
	ConsoleMethodInfo_t3560758192::get_offset_of_signature_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3014 = { sizeof (DebugLogConsole_t1447296567), -1, sizeof(DebugLogConsole_t1447296567_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3014[23] = 
{
	DebugLogConsole_t1447296567_StaticFields::get_offset_of_methods_0(),
	DebugLogConsole_t1447296567_StaticFields::get_offset_of_parseFunctions_1(),
	DebugLogConsole_t1447296567_StaticFields::get_offset_of_typeReadableNames_2(),
	DebugLogConsole_t1447296567_StaticFields::get_offset_of_commandArguments_3(),
	DebugLogConsole_t1447296567_StaticFields::get_offset_of_inputDelimiters_4(),
	DebugLogConsole_t1447296567_StaticFields::get_offset_of_U3CU3Ef__mgU24cache0_5(),
	DebugLogConsole_t1447296567_StaticFields::get_offset_of_U3CU3Ef__mgU24cache1_6(),
	DebugLogConsole_t1447296567_StaticFields::get_offset_of_U3CU3Ef__mgU24cache2_7(),
	DebugLogConsole_t1447296567_StaticFields::get_offset_of_U3CU3Ef__mgU24cache3_8(),
	DebugLogConsole_t1447296567_StaticFields::get_offset_of_U3CU3Ef__mgU24cache4_9(),
	DebugLogConsole_t1447296567_StaticFields::get_offset_of_U3CU3Ef__mgU24cache5_10(),
	DebugLogConsole_t1447296567_StaticFields::get_offset_of_U3CU3Ef__mgU24cache6_11(),
	DebugLogConsole_t1447296567_StaticFields::get_offset_of_U3CU3Ef__mgU24cache7_12(),
	DebugLogConsole_t1447296567_StaticFields::get_offset_of_U3CU3Ef__mgU24cache8_13(),
	DebugLogConsole_t1447296567_StaticFields::get_offset_of_U3CU3Ef__mgU24cache9_14(),
	DebugLogConsole_t1447296567_StaticFields::get_offset_of_U3CU3Ef__mgU24cacheA_15(),
	DebugLogConsole_t1447296567_StaticFields::get_offset_of_U3CU3Ef__mgU24cacheB_16(),
	DebugLogConsole_t1447296567_StaticFields::get_offset_of_U3CU3Ef__mgU24cacheC_17(),
	DebugLogConsole_t1447296567_StaticFields::get_offset_of_U3CU3Ef__mgU24cacheD_18(),
	DebugLogConsole_t1447296567_StaticFields::get_offset_of_U3CU3Ef__mgU24cacheE_19(),
	DebugLogConsole_t1447296567_StaticFields::get_offset_of_U3CU3Ef__mgU24cacheF_20(),
	DebugLogConsole_t1447296567_StaticFields::get_offset_of_U3CU3Ef__mgU24cache10_21(),
	DebugLogConsole_t1447296567_StaticFields::get_offset_of_U3CU3Ef__mgU24cache11_22(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3015 = { sizeof (ParseFunction_t3373928245), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3016 = { sizeof (DebugLogEntry_t3243893538), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3016[7] = 
{
	0,
	DebugLogEntry_t3243893538::get_offset_of_logString_1(),
	DebugLogEntry_t3243893538::get_offset_of_stackTrace_2(),
	DebugLogEntry_t3243893538::get_offset_of_completeLog_3(),
	DebugLogEntry_t3243893538::get_offset_of_logTypeSpriteRepresentation_4(),
	DebugLogEntry_t3243893538::get_offset_of_count_5(),
	DebugLogEntry_t3243893538::get_offset_of_hashValue_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3017 = { sizeof (DebugLogIndexList_t3544812386), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3017[2] = 
{
	DebugLogIndexList_t3544812386::get_offset_of_indices_0(),
	DebugLogIndexList_t3544812386::get_offset_of_size_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3018 = { sizeof (DebugLogItem_t1132107439), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3018[9] = 
{
	DebugLogItem_t1132107439::get_offset_of_transformComponent_4(),
	DebugLogItem_t1132107439::get_offset_of_imageComponent_5(),
	DebugLogItem_t1132107439::get_offset_of_logText_6(),
	DebugLogItem_t1132107439::get_offset_of_logTypeImage_7(),
	DebugLogItem_t1132107439::get_offset_of_logCountParent_8(),
	DebugLogItem_t1132107439::get_offset_of_logCountText_9(),
	DebugLogItem_t1132107439::get_offset_of_logEntry_10(),
	DebugLogItem_t1132107439::get_offset_of_entryIndex_11(),
	DebugLogItem_t1132107439::get_offset_of_manager_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3019 = { sizeof (DebugLogFilter_t3310913660)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3019[6] = 
{
	DebugLogFilter_t3310913660::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3020 = { sizeof (DebugLogManager_t637049758), -1, sizeof(DebugLogManager_t637049758_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3020[46] = 
{
	DebugLogManager_t637049758_StaticFields::get_offset_of_instance_4(),
	DebugLogManager_t637049758::get_offset_of_singleton_5(),
	DebugLogManager_t637049758::get_offset_of_minimumHeight_6(),
	DebugLogManager_t637049758::get_offset_of_startInPopupMode_7(),
	DebugLogManager_t637049758::get_offset_of_clearCommandAfterExecution_8(),
	DebugLogManager_t637049758::get_offset_of_receiveLogcatLogsInAndroid_9(),
	DebugLogManager_t637049758::get_offset_of_logcatArguments_10(),
	DebugLogManager_t637049758::get_offset_of_logItemPrefab_11(),
	DebugLogManager_t637049758::get_offset_of_infoLog_12(),
	DebugLogManager_t637049758::get_offset_of_warningLog_13(),
	DebugLogManager_t637049758::get_offset_of_errorLog_14(),
	DebugLogManager_t637049758::get_offset_of_logSpriteRepresentations_15(),
	DebugLogManager_t637049758::get_offset_of_collapseButtonNormalColor_16(),
	DebugLogManager_t637049758::get_offset_of_collapseButtonSelectedColor_17(),
	DebugLogManager_t637049758::get_offset_of_filterButtonsNormalColor_18(),
	DebugLogManager_t637049758::get_offset_of_filterButtonsSelectedColor_19(),
	DebugLogManager_t637049758::get_offset_of_logWindowTR_20(),
	DebugLogManager_t637049758::get_offset_of_canvasTR_21(),
	DebugLogManager_t637049758::get_offset_of_logItemsContainer_22(),
	DebugLogManager_t637049758::get_offset_of_commandInputField_23(),
	DebugLogManager_t637049758::get_offset_of_collapseButton_24(),
	DebugLogManager_t637049758::get_offset_of_filterInfoButton_25(),
	DebugLogManager_t637049758::get_offset_of_filterWarningButton_26(),
	DebugLogManager_t637049758::get_offset_of_filterErrorButton_27(),
	DebugLogManager_t637049758::get_offset_of_infoEntryCountText_28(),
	DebugLogManager_t637049758::get_offset_of_warningEntryCountText_29(),
	DebugLogManager_t637049758::get_offset_of_errorEntryCountText_30(),
	DebugLogManager_t637049758::get_offset_of_snapToBottomButton_31(),
	DebugLogManager_t637049758::get_offset_of_infoEntryCount_32(),
	DebugLogManager_t637049758::get_offset_of_warningEntryCount_33(),
	DebugLogManager_t637049758::get_offset_of_errorEntryCount_34(),
	DebugLogManager_t637049758::get_offset_of_logWindowCanvasGroup_35(),
	DebugLogManager_t637049758::get_offset_of_isLogWindowVisible_36(),
	DebugLogManager_t637049758::get_offset_of_screenDimensionsChanged_37(),
	DebugLogManager_t637049758::get_offset_of_popupManager_38(),
	DebugLogManager_t637049758::get_offset_of_logItemsScrollRect_39(),
	DebugLogManager_t637049758::get_offset_of_recycledListView_40(),
	DebugLogManager_t637049758::get_offset_of_isCollapseOn_41(),
	DebugLogManager_t637049758::get_offset_of_logFilter_42(),
	DebugLogManager_t637049758::get_offset_of_snapToBottom_43(),
	DebugLogManager_t637049758::get_offset_of_collapsedLogEntries_44(),
	DebugLogManager_t637049758::get_offset_of_collapsedLogEntriesMap_45(),
	DebugLogManager_t637049758::get_offset_of_uncollapsedLogEntriesIndices_46(),
	DebugLogManager_t637049758::get_offset_of_indicesOfListEntriesToShow_47(),
	DebugLogManager_t637049758::get_offset_of_pooledLogItems_48(),
	DebugLogManager_t637049758::get_offset_of_nullPointerEventData_49(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3021 = { sizeof (DebugLogPopup_t3403515216), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3021[17] = 
{
	DebugLogPopup_t3403515216::get_offset_of_popupTransform_4(),
	DebugLogPopup_t3403515216::get_offset_of_halfSize_5(),
	DebugLogPopup_t3403515216::get_offset_of_backgroundImage_6(),
	DebugLogPopup_t3403515216::get_offset_of_canvasGroup_7(),
	DebugLogPopup_t3403515216::get_offset_of_debugManager_8(),
	DebugLogPopup_t3403515216::get_offset_of_newInfoCountText_9(),
	DebugLogPopup_t3403515216::get_offset_of_newWarningCountText_10(),
	DebugLogPopup_t3403515216::get_offset_of_newErrorCountText_11(),
	DebugLogPopup_t3403515216::get_offset_of_newInfoCount_12(),
	DebugLogPopup_t3403515216::get_offset_of_newWarningCount_13(),
	DebugLogPopup_t3403515216::get_offset_of_newErrorCount_14(),
	DebugLogPopup_t3403515216::get_offset_of_normalColor_15(),
	DebugLogPopup_t3403515216::get_offset_of_alertColorInfo_16(),
	DebugLogPopup_t3403515216::get_offset_of_alertColorWarning_17(),
	DebugLogPopup_t3403515216::get_offset_of_alertColorError_18(),
	DebugLogPopup_t3403515216::get_offset_of_isPopupBeingDragged_19(),
	DebugLogPopup_t3403515216::get_offset_of_moveToPosCoroutine_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3022 = { sizeof (U3CMoveToPosAnimationU3Ec__Iterator0_t2236991059), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3022[7] = 
{
	U3CMoveToPosAnimationU3Ec__Iterator0_t2236991059::get_offset_of_U3CmodifierU3E__0_0(),
	U3CMoveToPosAnimationU3Ec__Iterator0_t2236991059::get_offset_of_U3CinitialPosU3E__0_1(),
	U3CMoveToPosAnimationU3Ec__Iterator0_t2236991059::get_offset_of_targetPos_2(),
	U3CMoveToPosAnimationU3Ec__Iterator0_t2236991059::get_offset_of_U24this_3(),
	U3CMoveToPosAnimationU3Ec__Iterator0_t2236991059::get_offset_of_U24current_4(),
	U3CMoveToPosAnimationU3Ec__Iterator0_t2236991059::get_offset_of_U24disposing_5(),
	U3CMoveToPosAnimationU3Ec__Iterator0_t2236991059::get_offset_of_U24PC_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3023 = { sizeof (DebugLogRecycledListView_t3570785762), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3023[20] = 
{
	DebugLogRecycledListView_t3570785762::get_offset_of_transformComponent_4(),
	DebugLogRecycledListView_t3570785762::get_offset_of_viewportTransform_5(),
	DebugLogRecycledListView_t3570785762::get_offset_of_debugManager_6(),
	DebugLogRecycledListView_t3570785762::get_offset_of_logItemNormalColor1_7(),
	DebugLogRecycledListView_t3570785762::get_offset_of_logItemNormalColor2_8(),
	DebugLogRecycledListView_t3570785762::get_offset_of_logItemSelectedColor_9(),
	DebugLogRecycledListView_t3570785762::get_offset_of_manager_10(),
	DebugLogRecycledListView_t3570785762::get_offset_of_logItemHeight_11(),
	DebugLogRecycledListView_t3570785762::get_offset_of__1OverLogItemHeight_12(),
	DebugLogRecycledListView_t3570785762::get_offset_of_viewportHeight_13(),
	DebugLogRecycledListView_t3570785762::get_offset_of_collapsedLogEntries_14(),
	DebugLogRecycledListView_t3570785762::get_offset_of_indicesOfEntriesToShow_15(),
	DebugLogRecycledListView_t3570785762::get_offset_of_indexOfSelectedLogEntry_16(),
	DebugLogRecycledListView_t3570785762::get_offset_of_positionOfSelectedLogEntry_17(),
	DebugLogRecycledListView_t3570785762::get_offset_of_heightOfSelectedLogEntry_18(),
	DebugLogRecycledListView_t3570785762::get_offset_of_deltaHeightOfSelectedLogEntry_19(),
	DebugLogRecycledListView_t3570785762::get_offset_of_logItemsAtIndices_20(),
	DebugLogRecycledListView_t3570785762::get_offset_of_isCollapseOn_21(),
	DebugLogRecycledListView_t3570785762::get_offset_of_currentTopIndex_22(),
	DebugLogRecycledListView_t3570785762::get_offset_of_currentBottomIndex_23(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3024 = { sizeof (DebugsOnScrollListener_t2173937987), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3024[2] = 
{
	DebugsOnScrollListener_t2173937987::get_offset_of_debugsScrollRect_4(),
	DebugsOnScrollListener_t2173937987::get_offset_of_debugLogManager_5(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
