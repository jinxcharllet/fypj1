﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// BoolReference
struct BoolReference_t2741665770;
// BoolVariable
struct BoolVariable_t415785710;
// ByteVariable
struct ByteVariable_t2671702712;
// Dialogue
struct Dialogue_t118236717;
// DialogueManager
struct DialogueManager_t3506686710;
// DoubleVariable
struct DoubleVariable_t2292910480;
// FPSController
struct FPSController_t1027197541;
// FloatReference
struct FloatReference_t1374219879;
// FloatVariable
struct FloatVariable_t761998486;
// GameEvent
struct GameEvent_t4069578135;
// IGameLogic
struct IGameLogic_t3343192399;
// IntReference
struct IntReference_t2842414034;
// IntVariable
struct IntVariable_t1706759505;
// LongVariable
struct LongVariable_t812939517;
// MiniGameManager
struct MiniGameManager_t325152534;
// ObjectState
struct ObjectState_t1202103565;
// ShortVariable
struct ShortVariable_t886426368;
// Sound[]
struct SoundU5BU5D_t3647937991;
// System.AsyncCallback
struct AsyncCallback_t3962456242;
// System.Char[]
struct CharU5BU5D_t3528271667;
// System.Collections.Generic.Dictionary`2<System.String,System.Object>
struct Dictionary_2_t2865362463;
// System.Collections.Generic.Dictionary`2<System.Type,System.Func`2<System.Type,Vuforia.Tracker>>
struct Dictionary_2_t1322931057;
// System.Collections.Generic.Dictionary`2<System.Type,System.Func`2<Vuforia.Tracker,System.Boolean>>
struct Dictionary_2_t2058017892;
// System.Collections.Generic.Dictionary`2<System.Type,Vuforia.Tracker>
struct Dictionary_2_t858966067;
// System.Collections.Generic.HashSet`1<UnityEngine.MeshRenderer>
struct HashSet_1_t3446926030;
// System.Collections.Generic.List`1<EventListener>
struct List_1_t1161341311;
// System.Collections.Generic.List`1<GameEvent>
struct List_1_t1246685581;
// System.Collections.Generic.List`1<UnityEngine.Analytics.AnalyticsEventParam>
struct List_1_t3952196670;
// System.Collections.Generic.List`1<UnityEngine.Analytics.TrackableProperty/FieldWithTarget>
struct List_1_t235857739;
// System.Collections.Generic.List`1<UnityEngine.Analytics.TriggerRule>
struct List_1_t3418373063;
// System.Collections.Generic.List`1<UnityEngine.Events.UnityEvent>
struct List_1_t4053343389;
// System.Collections.Generic.List`1<UnityEngine.GameObject>
struct List_1_t2585711361;
// System.Collections.Generic.List`1<UnityEngine.Material>
struct List_1_t1812449865;
// System.Collections.Generic.List`1<Vuforia.IUserDefinedTargetEventHandler>
struct List_1_t2728888017;
// System.Collections.Generic.List`1<Vuforia.IVirtualButtonEventHandler>
struct List_1_t365750880;
// System.Collections.Generic.Queue`1<System.String>
struct Queue_1_t1693710183;
// System.DelegateData
struct DelegateData_t1677132599;
// System.Func`2<System.Type,Vuforia.Tracker>
struct Func_2_t3173551289;
// System.Func`2<Vuforia.Tracker,System.Boolean>
struct Func_2_t3908638124;
// System.Func`3<System.String,Vuforia.WebCamProfile/ProfileData,Vuforia.IWebCamTexAdaptor>
struct Func_3_t3440825513;
// System.IAsyncResult
struct IAsyncResult_t767004451;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.String
struct String_t;
// System.String[]
struct StringU5BU5D_t1281789340;
// System.Type
struct Type_t;
// System.Void
struct Void_t1185182177;
// TMPro.TextMeshProUGUI
struct TextMeshProUGUI_t529313277;
// UintVariable
struct UintVariable_t3613798951;
// UnityEngine.Analytics.AnalyticsEventParamListContainer
struct AnalyticsEventParamListContainer_t587083383;
// UnityEngine.Analytics.AnalyticsEventTracker
struct AnalyticsEventTracker_t2285229262;
// UnityEngine.Analytics.EventTrigger
struct EventTrigger_t2527451695;
// UnityEngine.Analytics.EventTrigger/OnTrigger
struct OnTrigger_t4184125570;
// UnityEngine.Analytics.StandardEventPayload
struct StandardEventPayload_t1629891255;
// UnityEngine.Analytics.TrackableField
struct TrackableField_t1772682203;
// UnityEngine.Analytics.TrackableProperty
struct TrackableProperty_t3943537984;
// UnityEngine.Analytics.TriggerListContainer
struct TriggerListContainer_t2032715483;
// UnityEngine.Analytics.TriggerMethod
struct TriggerMethod_t582536534;
// UnityEngine.Analytics.ValueProperty
struct ValueProperty_t1868393739;
// UnityEngine.Audio.AudioMixerGroup
struct AudioMixerGroup_t2743564464;
// UnityEngine.Camera
struct Camera_t4157153871;
// UnityEngine.Events.UnityEvent
struct UnityEvent_t2581268647;
// UnityEngine.GUIStyle
struct GUIStyle_t3956901511;
// UnityEngine.GameObject
struct GameObject_t1113636619;
// UnityEngine.Material
struct Material_t340375123;
// UnityEngine.Mesh
struct Mesh_t3648964284;
// UnityEngine.MeshRenderer
struct MeshRenderer_t587009260;
// UnityEngine.Object
struct Object_t631007953;
// UnityEngine.Shader
struct Shader_t4151988712;
// UnityEngine.Texture
struct Texture_t3661962703;
// UnityEngine.Texture2D
struct Texture2D_t3840446185;
// UnityEngine.Transform
struct Transform_t3600365921;
// UnityEngine.UI.Image
struct Image_t2670269651;
// UnityEngine.UI.Text
struct Text_t1901882714;
// Vuforia.BackgroundPlaneBehaviour
struct BackgroundPlaneBehaviour_t3333547397;
// Vuforia.DataSet
struct DataSet_t3286034874;
// Vuforia.ITrackerManager
struct ITrackerManager_t607206903;
// Vuforia.ImageTarget
struct ImageTarget_t3707016494;
// Vuforia.ObjectTracker
struct ObjectTracker_t4177997237;
// Vuforia.StateManager
struct StateManager_t1982749557;
// Vuforia.TrackableBehaviour
struct TrackableBehaviour_t1113559212;
// Vuforia.VirtualButton
struct VirtualButton_t386166510;
// Vuforia.VuforiaARController
struct VuforiaARController_t1876945237;
// Vuforia.VuforiaBehaviour
struct VuforiaBehaviour_t2151848540;
// Vuforia.WebCam
struct WebCam_t2427002488;




#ifndef U3CMODULEU3E_T692745552_H
#define U3CMODULEU3E_T692745552_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t692745552 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T692745552_H
#ifndef U3CMODULEU3E_T692745554_H
#define U3CMODULEU3E_T692745554_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t692745554 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T692745554_H
#ifndef U3CMODULEU3E_T692745553_H
#define U3CMODULEU3E_T692745553_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t692745553 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T692745553_H
#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef U3CPLAYU3EC__ANONSTOREY0_T2108231134_H
#define U3CPLAYU3EC__ANONSTOREY0_T2108231134_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AudioManager/<Play>c__AnonStorey0
struct  U3CPlayU3Ec__AnonStorey0_t2108231134  : public RuntimeObject
{
public:
	// System.String AudioManager/<Play>c__AnonStorey0::sound
	String_t* ___sound_0;

public:
	inline static int32_t get_offset_of_sound_0() { return static_cast<int32_t>(offsetof(U3CPlayU3Ec__AnonStorey0_t2108231134, ___sound_0)); }
	inline String_t* get_sound_0() const { return ___sound_0; }
	inline String_t** get_address_of_sound_0() { return &___sound_0; }
	inline void set_sound_0(String_t* value)
	{
		___sound_0 = value;
		Il2CppCodeGenWriteBarrier((&___sound_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CPLAYU3EC__ANONSTOREY0_T2108231134_H
#ifndef DIALOGUE_T118236717_H
#define DIALOGUE_T118236717_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Dialogue
struct  Dialogue_t118236717  : public RuntimeObject
{
public:
	// System.String[] Dialogue::sentences
	StringU5BU5D_t1281789340* ___sentences_0;

public:
	inline static int32_t get_offset_of_sentences_0() { return static_cast<int32_t>(offsetof(Dialogue_t118236717, ___sentences_0)); }
	inline StringU5BU5D_t1281789340* get_sentences_0() const { return ___sentences_0; }
	inline StringU5BU5D_t1281789340** get_address_of_sentences_0() { return &___sentences_0; }
	inline void set_sentences_0(StringU5BU5D_t1281789340* value)
	{
		___sentences_0 = value;
		Il2CppCodeGenWriteBarrier((&___sentences_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DIALOGUE_T118236717_H
#ifndef U3CTYPESENTENCEU3EC__ITERATOR0_T2432906426_H
#define U3CTYPESENTENCEU3EC__ITERATOR0_T2432906426_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DialogueManager/<TypeSentence>c__Iterator0
struct  U3CTypeSentenceU3Ec__Iterator0_t2432906426  : public RuntimeObject
{
public:
	// System.String DialogueManager/<TypeSentence>c__Iterator0::sentence
	String_t* ___sentence_0;
	// System.Char[] DialogueManager/<TypeSentence>c__Iterator0::$locvar0
	CharU5BU5D_t3528271667* ___U24locvar0_1;
	// System.Int32 DialogueManager/<TypeSentence>c__Iterator0::$locvar1
	int32_t ___U24locvar1_2;
	// System.Char DialogueManager/<TypeSentence>c__Iterator0::<letter>__1
	Il2CppChar ___U3CletterU3E__1_3;
	// DialogueManager DialogueManager/<TypeSentence>c__Iterator0::$this
	DialogueManager_t3506686710 * ___U24this_4;
	// System.Object DialogueManager/<TypeSentence>c__Iterator0::$current
	RuntimeObject * ___U24current_5;
	// System.Boolean DialogueManager/<TypeSentence>c__Iterator0::$disposing
	bool ___U24disposing_6;
	// System.Int32 DialogueManager/<TypeSentence>c__Iterator0::$PC
	int32_t ___U24PC_7;

public:
	inline static int32_t get_offset_of_sentence_0() { return static_cast<int32_t>(offsetof(U3CTypeSentenceU3Ec__Iterator0_t2432906426, ___sentence_0)); }
	inline String_t* get_sentence_0() const { return ___sentence_0; }
	inline String_t** get_address_of_sentence_0() { return &___sentence_0; }
	inline void set_sentence_0(String_t* value)
	{
		___sentence_0 = value;
		Il2CppCodeGenWriteBarrier((&___sentence_0), value);
	}

	inline static int32_t get_offset_of_U24locvar0_1() { return static_cast<int32_t>(offsetof(U3CTypeSentenceU3Ec__Iterator0_t2432906426, ___U24locvar0_1)); }
	inline CharU5BU5D_t3528271667* get_U24locvar0_1() const { return ___U24locvar0_1; }
	inline CharU5BU5D_t3528271667** get_address_of_U24locvar0_1() { return &___U24locvar0_1; }
	inline void set_U24locvar0_1(CharU5BU5D_t3528271667* value)
	{
		___U24locvar0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24locvar0_1), value);
	}

	inline static int32_t get_offset_of_U24locvar1_2() { return static_cast<int32_t>(offsetof(U3CTypeSentenceU3Ec__Iterator0_t2432906426, ___U24locvar1_2)); }
	inline int32_t get_U24locvar1_2() const { return ___U24locvar1_2; }
	inline int32_t* get_address_of_U24locvar1_2() { return &___U24locvar1_2; }
	inline void set_U24locvar1_2(int32_t value)
	{
		___U24locvar1_2 = value;
	}

	inline static int32_t get_offset_of_U3CletterU3E__1_3() { return static_cast<int32_t>(offsetof(U3CTypeSentenceU3Ec__Iterator0_t2432906426, ___U3CletterU3E__1_3)); }
	inline Il2CppChar get_U3CletterU3E__1_3() const { return ___U3CletterU3E__1_3; }
	inline Il2CppChar* get_address_of_U3CletterU3E__1_3() { return &___U3CletterU3E__1_3; }
	inline void set_U3CletterU3E__1_3(Il2CppChar value)
	{
		___U3CletterU3E__1_3 = value;
	}

	inline static int32_t get_offset_of_U24this_4() { return static_cast<int32_t>(offsetof(U3CTypeSentenceU3Ec__Iterator0_t2432906426, ___U24this_4)); }
	inline DialogueManager_t3506686710 * get_U24this_4() const { return ___U24this_4; }
	inline DialogueManager_t3506686710 ** get_address_of_U24this_4() { return &___U24this_4; }
	inline void set_U24this_4(DialogueManager_t3506686710 * value)
	{
		___U24this_4 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_4), value);
	}

	inline static int32_t get_offset_of_U24current_5() { return static_cast<int32_t>(offsetof(U3CTypeSentenceU3Ec__Iterator0_t2432906426, ___U24current_5)); }
	inline RuntimeObject * get_U24current_5() const { return ___U24current_5; }
	inline RuntimeObject ** get_address_of_U24current_5() { return &___U24current_5; }
	inline void set_U24current_5(RuntimeObject * value)
	{
		___U24current_5 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_5), value);
	}

	inline static int32_t get_offset_of_U24disposing_6() { return static_cast<int32_t>(offsetof(U3CTypeSentenceU3Ec__Iterator0_t2432906426, ___U24disposing_6)); }
	inline bool get_U24disposing_6() const { return ___U24disposing_6; }
	inline bool* get_address_of_U24disposing_6() { return &___U24disposing_6; }
	inline void set_U24disposing_6(bool value)
	{
		___U24disposing_6 = value;
	}

	inline static int32_t get_offset_of_U24PC_7() { return static_cast<int32_t>(offsetof(U3CTypeSentenceU3Ec__Iterator0_t2432906426, ___U24PC_7)); }
	inline int32_t get_U24PC_7() const { return ___U24PC_7; }
	inline int32_t* get_address_of_U24PC_7() { return &___U24PC_7; }
	inline void set_U24PC_7(int32_t value)
	{
		___U24PC_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CTYPESENTENCEU3EC__ITERATOR0_T2432906426_H
#ifndef U3CFPSU3EC__ITERATOR0_T1547198559_H
#define U3CFPSU3EC__ITERATOR0_T1547198559_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FPSController/<FPS>c__Iterator0
struct  U3CFPSU3Ec__Iterator0_t1547198559  : public RuntimeObject
{
public:
	// System.Int32 FPSController/<FPS>c__Iterator0::<lastFrameCount>__1
	int32_t ___U3ClastFrameCountU3E__1_0;
	// System.Single FPSController/<FPS>c__Iterator0::<lastTime>__1
	float ___U3ClastTimeU3E__1_1;
	// System.Single FPSController/<FPS>c__Iterator0::<timeSpan>__1
	float ___U3CtimeSpanU3E__1_2;
	// System.Int32 FPSController/<FPS>c__Iterator0::<frameCount>__1
	int32_t ___U3CframeCountU3E__1_3;
	// FPSController FPSController/<FPS>c__Iterator0::$this
	FPSController_t1027197541 * ___U24this_4;
	// System.Object FPSController/<FPS>c__Iterator0::$current
	RuntimeObject * ___U24current_5;
	// System.Boolean FPSController/<FPS>c__Iterator0::$disposing
	bool ___U24disposing_6;
	// System.Int32 FPSController/<FPS>c__Iterator0::$PC
	int32_t ___U24PC_7;

public:
	inline static int32_t get_offset_of_U3ClastFrameCountU3E__1_0() { return static_cast<int32_t>(offsetof(U3CFPSU3Ec__Iterator0_t1547198559, ___U3ClastFrameCountU3E__1_0)); }
	inline int32_t get_U3ClastFrameCountU3E__1_0() const { return ___U3ClastFrameCountU3E__1_0; }
	inline int32_t* get_address_of_U3ClastFrameCountU3E__1_0() { return &___U3ClastFrameCountU3E__1_0; }
	inline void set_U3ClastFrameCountU3E__1_0(int32_t value)
	{
		___U3ClastFrameCountU3E__1_0 = value;
	}

	inline static int32_t get_offset_of_U3ClastTimeU3E__1_1() { return static_cast<int32_t>(offsetof(U3CFPSU3Ec__Iterator0_t1547198559, ___U3ClastTimeU3E__1_1)); }
	inline float get_U3ClastTimeU3E__1_1() const { return ___U3ClastTimeU3E__1_1; }
	inline float* get_address_of_U3ClastTimeU3E__1_1() { return &___U3ClastTimeU3E__1_1; }
	inline void set_U3ClastTimeU3E__1_1(float value)
	{
		___U3ClastTimeU3E__1_1 = value;
	}

	inline static int32_t get_offset_of_U3CtimeSpanU3E__1_2() { return static_cast<int32_t>(offsetof(U3CFPSU3Ec__Iterator0_t1547198559, ___U3CtimeSpanU3E__1_2)); }
	inline float get_U3CtimeSpanU3E__1_2() const { return ___U3CtimeSpanU3E__1_2; }
	inline float* get_address_of_U3CtimeSpanU3E__1_2() { return &___U3CtimeSpanU3E__1_2; }
	inline void set_U3CtimeSpanU3E__1_2(float value)
	{
		___U3CtimeSpanU3E__1_2 = value;
	}

	inline static int32_t get_offset_of_U3CframeCountU3E__1_3() { return static_cast<int32_t>(offsetof(U3CFPSU3Ec__Iterator0_t1547198559, ___U3CframeCountU3E__1_3)); }
	inline int32_t get_U3CframeCountU3E__1_3() const { return ___U3CframeCountU3E__1_3; }
	inline int32_t* get_address_of_U3CframeCountU3E__1_3() { return &___U3CframeCountU3E__1_3; }
	inline void set_U3CframeCountU3E__1_3(int32_t value)
	{
		___U3CframeCountU3E__1_3 = value;
	}

	inline static int32_t get_offset_of_U24this_4() { return static_cast<int32_t>(offsetof(U3CFPSU3Ec__Iterator0_t1547198559, ___U24this_4)); }
	inline FPSController_t1027197541 * get_U24this_4() const { return ___U24this_4; }
	inline FPSController_t1027197541 ** get_address_of_U24this_4() { return &___U24this_4; }
	inline void set_U24this_4(FPSController_t1027197541 * value)
	{
		___U24this_4 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_4), value);
	}

	inline static int32_t get_offset_of_U24current_5() { return static_cast<int32_t>(offsetof(U3CFPSU3Ec__Iterator0_t1547198559, ___U24current_5)); }
	inline RuntimeObject * get_U24current_5() const { return ___U24current_5; }
	inline RuntimeObject ** get_address_of_U24current_5() { return &___U24current_5; }
	inline void set_U24current_5(RuntimeObject * value)
	{
		___U24current_5 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_5), value);
	}

	inline static int32_t get_offset_of_U24disposing_6() { return static_cast<int32_t>(offsetof(U3CFPSU3Ec__Iterator0_t1547198559, ___U24disposing_6)); }
	inline bool get_U24disposing_6() const { return ___U24disposing_6; }
	inline bool* get_address_of_U24disposing_6() { return &___U24disposing_6; }
	inline void set_U24disposing_6(bool value)
	{
		___U24disposing_6 = value;
	}

	inline static int32_t get_offset_of_U24PC_7() { return static_cast<int32_t>(offsetof(U3CFPSU3Ec__Iterator0_t1547198559, ___U24PC_7)); }
	inline int32_t get_U24PC_7() const { return ___U24PC_7; }
	inline int32_t* get_address_of_U24PC_7() { return &___U24PC_7; }
	inline void set_U24PC_7(int32_t value)
	{
		___U24PC_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CFPSU3EC__ITERATOR0_T1547198559_H
#ifndef GENERICREFERENCE_T1852046461_H
#define GENERICREFERENCE_T1852046461_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GenericReference
struct  GenericReference_t1852046461  : public RuntimeObject
{
public:
	// System.Boolean GenericReference::m_UseConstantVariable
	bool ___m_UseConstantVariable_0;

public:
	inline static int32_t get_offset_of_m_UseConstantVariable_0() { return static_cast<int32_t>(offsetof(GenericReference_t1852046461, ___m_UseConstantVariable_0)); }
	inline bool get_m_UseConstantVariable_0() const { return ___m_UseConstantVariable_0; }
	inline bool* get_address_of_m_UseConstantVariable_0() { return &___m_UseConstantVariable_0; }
	inline void set_m_UseConstantVariable_0(bool value)
	{
		___m_UseConstantVariable_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GENERICREFERENCE_T1852046461_H
#ifndef MESHHOLDER_T304490584_H
#define MESHHOLDER_T304490584_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MeshHolder
struct  MeshHolder_t304490584  : public RuntimeObject
{
public:
	// UnityEngine.Mesh MeshHolder::m_mesh
	Mesh_t3648964284 * ___m_mesh_0;
	// UnityEngine.Material MeshHolder::m_texture
	Material_t340375123 * ___m_texture_1;

public:
	inline static int32_t get_offset_of_m_mesh_0() { return static_cast<int32_t>(offsetof(MeshHolder_t304490584, ___m_mesh_0)); }
	inline Mesh_t3648964284 * get_m_mesh_0() const { return ___m_mesh_0; }
	inline Mesh_t3648964284 ** get_address_of_m_mesh_0() { return &___m_mesh_0; }
	inline void set_m_mesh_0(Mesh_t3648964284 * value)
	{
		___m_mesh_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_mesh_0), value);
	}

	inline static int32_t get_offset_of_m_texture_1() { return static_cast<int32_t>(offsetof(MeshHolder_t304490584, ___m_texture_1)); }
	inline Material_t340375123 * get_m_texture_1() const { return ___m_texture_1; }
	inline Material_t340375123 ** get_address_of_m_texture_1() { return &___m_texture_1; }
	inline void set_m_texture_1(Material_t340375123 * value)
	{
		___m_texture_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_texture_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MESHHOLDER_T304490584_H
#ifndef STATEWITHMATERIALS_T3107498960_H
#define STATEWITHMATERIALS_T3107498960_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// StateWithMaterials
struct  StateWithMaterials_t3107498960  : public RuntimeObject
{
public:
	// ObjectState StateWithMaterials::m_state
	ObjectState_t1202103565 * ___m_state_0;
	// System.Collections.Generic.List`1<UnityEngine.Material> StateWithMaterials::m_matList
	List_1_t1812449865 * ___m_matList_1;

public:
	inline static int32_t get_offset_of_m_state_0() { return static_cast<int32_t>(offsetof(StateWithMaterials_t3107498960, ___m_state_0)); }
	inline ObjectState_t1202103565 * get_m_state_0() const { return ___m_state_0; }
	inline ObjectState_t1202103565 ** get_address_of_m_state_0() { return &___m_state_0; }
	inline void set_m_state_0(ObjectState_t1202103565 * value)
	{
		___m_state_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_state_0), value);
	}

	inline static int32_t get_offset_of_m_matList_1() { return static_cast<int32_t>(offsetof(StateWithMaterials_t3107498960, ___m_matList_1)); }
	inline List_1_t1812449865 * get_m_matList_1() const { return ___m_matList_1; }
	inline List_1_t1812449865 ** get_address_of_m_matList_1() { return &___m_matList_1; }
	inline void set_m_matList_1(List_1_t1812449865 * value)
	{
		___m_matList_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_matList_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STATEWITHMATERIALS_T3107498960_H
#ifndef VALUETYPE_T3640485471_H
#define VALUETYPE_T3640485471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3640485471  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_com
{
};
#endif // VALUETYPE_T3640485471_H
#ifndef ANALYTICSEVENTPARAMLISTCONTAINER_T587083383_H
#define ANALYTICSEVENTPARAMLISTCONTAINER_T587083383_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.AnalyticsEventParamListContainer
struct  AnalyticsEventParamListContainer_t587083383  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<UnityEngine.Analytics.AnalyticsEventParam> UnityEngine.Analytics.AnalyticsEventParamListContainer::m_Parameters
	List_1_t3952196670 * ___m_Parameters_0;

public:
	inline static int32_t get_offset_of_m_Parameters_0() { return static_cast<int32_t>(offsetof(AnalyticsEventParamListContainer_t587083383, ___m_Parameters_0)); }
	inline List_1_t3952196670 * get_m_Parameters_0() const { return ___m_Parameters_0; }
	inline List_1_t3952196670 ** get_address_of_m_Parameters_0() { return &___m_Parameters_0; }
	inline void set_m_Parameters_0(List_1_t3952196670 * value)
	{
		___m_Parameters_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Parameters_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANALYTICSEVENTPARAMLISTCONTAINER_T587083383_H
#ifndef U3CTIMEDTRIGGERU3EC__ITERATOR0_T3813435494_H
#define U3CTIMEDTRIGGERU3EC__ITERATOR0_T3813435494_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.AnalyticsEventTracker/<TimedTrigger>c__Iterator0
struct  U3CTimedTriggerU3Ec__Iterator0_t3813435494  : public RuntimeObject
{
public:
	// UnityEngine.Analytics.AnalyticsEventTracker UnityEngine.Analytics.AnalyticsEventTracker/<TimedTrigger>c__Iterator0::$this
	AnalyticsEventTracker_t2285229262 * ___U24this_0;
	// System.Object UnityEngine.Analytics.AnalyticsEventTracker/<TimedTrigger>c__Iterator0::$current
	RuntimeObject * ___U24current_1;
	// System.Boolean UnityEngine.Analytics.AnalyticsEventTracker/<TimedTrigger>c__Iterator0::$disposing
	bool ___U24disposing_2;
	// System.Int32 UnityEngine.Analytics.AnalyticsEventTracker/<TimedTrigger>c__Iterator0::$PC
	int32_t ___U24PC_3;

public:
	inline static int32_t get_offset_of_U24this_0() { return static_cast<int32_t>(offsetof(U3CTimedTriggerU3Ec__Iterator0_t3813435494, ___U24this_0)); }
	inline AnalyticsEventTracker_t2285229262 * get_U24this_0() const { return ___U24this_0; }
	inline AnalyticsEventTracker_t2285229262 ** get_address_of_U24this_0() { return &___U24this_0; }
	inline void set_U24this_0(AnalyticsEventTracker_t2285229262 * value)
	{
		___U24this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_0), value);
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3CTimedTriggerU3Ec__Iterator0_t3813435494, ___U24current_1)); }
	inline RuntimeObject * get_U24current_1() const { return ___U24current_1; }
	inline RuntimeObject ** get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(RuntimeObject * value)
	{
		___U24current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_1), value);
	}

	inline static int32_t get_offset_of_U24disposing_2() { return static_cast<int32_t>(offsetof(U3CTimedTriggerU3Ec__Iterator0_t3813435494, ___U24disposing_2)); }
	inline bool get_U24disposing_2() const { return ___U24disposing_2; }
	inline bool* get_address_of_U24disposing_2() { return &___U24disposing_2; }
	inline void set_U24disposing_2(bool value)
	{
		___U24disposing_2 = value;
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CTimedTriggerU3Ec__Iterator0_t3813435494, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CTIMEDTRIGGERU3EC__ITERATOR0_T3813435494_H
#ifndef ANALYTICSEVENTTRACKERSETTINGS_T480422680_H
#define ANALYTICSEVENTTRACKERSETTINGS_T480422680_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.AnalyticsEventTrackerSettings
struct  AnalyticsEventTrackerSettings_t480422680  : public RuntimeObject
{
public:

public:
};

struct AnalyticsEventTrackerSettings_t480422680_StaticFields
{
public:
	// System.Int32 UnityEngine.Analytics.AnalyticsEventTrackerSettings::paramCountMax
	int32_t ___paramCountMax_0;
	// System.Int32 UnityEngine.Analytics.AnalyticsEventTrackerSettings::triggerRuleCountMax
	int32_t ___triggerRuleCountMax_1;

public:
	inline static int32_t get_offset_of_paramCountMax_0() { return static_cast<int32_t>(offsetof(AnalyticsEventTrackerSettings_t480422680_StaticFields, ___paramCountMax_0)); }
	inline int32_t get_paramCountMax_0() const { return ___paramCountMax_0; }
	inline int32_t* get_address_of_paramCountMax_0() { return &___paramCountMax_0; }
	inline void set_paramCountMax_0(int32_t value)
	{
		___paramCountMax_0 = value;
	}

	inline static int32_t get_offset_of_triggerRuleCountMax_1() { return static_cast<int32_t>(offsetof(AnalyticsEventTrackerSettings_t480422680_StaticFields, ___triggerRuleCountMax_1)); }
	inline int32_t get_triggerRuleCountMax_1() const { return ___triggerRuleCountMax_1; }
	inline int32_t* get_address_of_triggerRuleCountMax_1() { return &___triggerRuleCountMax_1; }
	inline void set_triggerRuleCountMax_1(int32_t value)
	{
		___triggerRuleCountMax_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANALYTICSEVENTTRACKERSETTINGS_T480422680_H
#ifndef STANDARDEVENTPAYLOAD_T1629891255_H
#define STANDARDEVENTPAYLOAD_T1629891255_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.StandardEventPayload
struct  StandardEventPayload_t1629891255  : public RuntimeObject
{
public:
	// System.Boolean UnityEngine.Analytics.StandardEventPayload::m_IsEventExpanded
	bool ___m_IsEventExpanded_0;
	// System.String UnityEngine.Analytics.StandardEventPayload::m_StandardEventType
	String_t* ___m_StandardEventType_1;
	// System.Type UnityEngine.Analytics.StandardEventPayload::standardEventType
	Type_t * ___standardEventType_2;
	// UnityEngine.Analytics.AnalyticsEventParamListContainer UnityEngine.Analytics.StandardEventPayload::m_Parameters
	AnalyticsEventParamListContainer_t587083383 * ___m_Parameters_3;
	// System.String UnityEngine.Analytics.StandardEventPayload::m_Name
	String_t* ___m_Name_5;

public:
	inline static int32_t get_offset_of_m_IsEventExpanded_0() { return static_cast<int32_t>(offsetof(StandardEventPayload_t1629891255, ___m_IsEventExpanded_0)); }
	inline bool get_m_IsEventExpanded_0() const { return ___m_IsEventExpanded_0; }
	inline bool* get_address_of_m_IsEventExpanded_0() { return &___m_IsEventExpanded_0; }
	inline void set_m_IsEventExpanded_0(bool value)
	{
		___m_IsEventExpanded_0 = value;
	}

	inline static int32_t get_offset_of_m_StandardEventType_1() { return static_cast<int32_t>(offsetof(StandardEventPayload_t1629891255, ___m_StandardEventType_1)); }
	inline String_t* get_m_StandardEventType_1() const { return ___m_StandardEventType_1; }
	inline String_t** get_address_of_m_StandardEventType_1() { return &___m_StandardEventType_1; }
	inline void set_m_StandardEventType_1(String_t* value)
	{
		___m_StandardEventType_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_StandardEventType_1), value);
	}

	inline static int32_t get_offset_of_standardEventType_2() { return static_cast<int32_t>(offsetof(StandardEventPayload_t1629891255, ___standardEventType_2)); }
	inline Type_t * get_standardEventType_2() const { return ___standardEventType_2; }
	inline Type_t ** get_address_of_standardEventType_2() { return &___standardEventType_2; }
	inline void set_standardEventType_2(Type_t * value)
	{
		___standardEventType_2 = value;
		Il2CppCodeGenWriteBarrier((&___standardEventType_2), value);
	}

	inline static int32_t get_offset_of_m_Parameters_3() { return static_cast<int32_t>(offsetof(StandardEventPayload_t1629891255, ___m_Parameters_3)); }
	inline AnalyticsEventParamListContainer_t587083383 * get_m_Parameters_3() const { return ___m_Parameters_3; }
	inline AnalyticsEventParamListContainer_t587083383 ** get_address_of_m_Parameters_3() { return &___m_Parameters_3; }
	inline void set_m_Parameters_3(AnalyticsEventParamListContainer_t587083383 * value)
	{
		___m_Parameters_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_Parameters_3), value);
	}

	inline static int32_t get_offset_of_m_Name_5() { return static_cast<int32_t>(offsetof(StandardEventPayload_t1629891255, ___m_Name_5)); }
	inline String_t* get_m_Name_5() const { return ___m_Name_5; }
	inline String_t** get_address_of_m_Name_5() { return &___m_Name_5; }
	inline void set_m_Name_5(String_t* value)
	{
		___m_Name_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_Name_5), value);
	}
};

struct StandardEventPayload_t1629891255_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,System.Object> UnityEngine.Analytics.StandardEventPayload::m_EventData
	Dictionary_2_t2865362463 * ___m_EventData_4;

public:
	inline static int32_t get_offset_of_m_EventData_4() { return static_cast<int32_t>(offsetof(StandardEventPayload_t1629891255_StaticFields, ___m_EventData_4)); }
	inline Dictionary_2_t2865362463 * get_m_EventData_4() const { return ___m_EventData_4; }
	inline Dictionary_2_t2865362463 ** get_address_of_m_EventData_4() { return &___m_EventData_4; }
	inline void set_m_EventData_4(Dictionary_2_t2865362463 * value)
	{
		___m_EventData_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_EventData_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STANDARDEVENTPAYLOAD_T1629891255_H
#ifndef TRACKABLEPROPERTY_T3943537984_H
#define TRACKABLEPROPERTY_T3943537984_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.TrackableProperty
struct  TrackableProperty_t3943537984  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<UnityEngine.Analytics.TrackableProperty/FieldWithTarget> UnityEngine.Analytics.TrackableProperty::m_Fields
	List_1_t235857739 * ___m_Fields_1;

public:
	inline static int32_t get_offset_of_m_Fields_1() { return static_cast<int32_t>(offsetof(TrackableProperty_t3943537984, ___m_Fields_1)); }
	inline List_1_t235857739 * get_m_Fields_1() const { return ___m_Fields_1; }
	inline List_1_t235857739 ** get_address_of_m_Fields_1() { return &___m_Fields_1; }
	inline void set_m_Fields_1(List_1_t235857739 * value)
	{
		___m_Fields_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_Fields_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRACKABLEPROPERTY_T3943537984_H
#ifndef FIELDWITHTARGET_T3058750293_H
#define FIELDWITHTARGET_T3058750293_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.TrackableProperty/FieldWithTarget
struct  FieldWithTarget_t3058750293  : public RuntimeObject
{
public:
	// System.String UnityEngine.Analytics.TrackableProperty/FieldWithTarget::m_ParamName
	String_t* ___m_ParamName_0;
	// UnityEngine.Object UnityEngine.Analytics.TrackableProperty/FieldWithTarget::m_Target
	Object_t631007953 * ___m_Target_1;
	// System.String UnityEngine.Analytics.TrackableProperty/FieldWithTarget::m_FieldPath
	String_t* ___m_FieldPath_2;
	// System.String UnityEngine.Analytics.TrackableProperty/FieldWithTarget::m_TypeString
	String_t* ___m_TypeString_3;
	// System.Boolean UnityEngine.Analytics.TrackableProperty/FieldWithTarget::m_DoStatic
	bool ___m_DoStatic_4;
	// System.String UnityEngine.Analytics.TrackableProperty/FieldWithTarget::m_StaticString
	String_t* ___m_StaticString_5;

public:
	inline static int32_t get_offset_of_m_ParamName_0() { return static_cast<int32_t>(offsetof(FieldWithTarget_t3058750293, ___m_ParamName_0)); }
	inline String_t* get_m_ParamName_0() const { return ___m_ParamName_0; }
	inline String_t** get_address_of_m_ParamName_0() { return &___m_ParamName_0; }
	inline void set_m_ParamName_0(String_t* value)
	{
		___m_ParamName_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_ParamName_0), value);
	}

	inline static int32_t get_offset_of_m_Target_1() { return static_cast<int32_t>(offsetof(FieldWithTarget_t3058750293, ___m_Target_1)); }
	inline Object_t631007953 * get_m_Target_1() const { return ___m_Target_1; }
	inline Object_t631007953 ** get_address_of_m_Target_1() { return &___m_Target_1; }
	inline void set_m_Target_1(Object_t631007953 * value)
	{
		___m_Target_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_Target_1), value);
	}

	inline static int32_t get_offset_of_m_FieldPath_2() { return static_cast<int32_t>(offsetof(FieldWithTarget_t3058750293, ___m_FieldPath_2)); }
	inline String_t* get_m_FieldPath_2() const { return ___m_FieldPath_2; }
	inline String_t** get_address_of_m_FieldPath_2() { return &___m_FieldPath_2; }
	inline void set_m_FieldPath_2(String_t* value)
	{
		___m_FieldPath_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_FieldPath_2), value);
	}

	inline static int32_t get_offset_of_m_TypeString_3() { return static_cast<int32_t>(offsetof(FieldWithTarget_t3058750293, ___m_TypeString_3)); }
	inline String_t* get_m_TypeString_3() const { return ___m_TypeString_3; }
	inline String_t** get_address_of_m_TypeString_3() { return &___m_TypeString_3; }
	inline void set_m_TypeString_3(String_t* value)
	{
		___m_TypeString_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_TypeString_3), value);
	}

	inline static int32_t get_offset_of_m_DoStatic_4() { return static_cast<int32_t>(offsetof(FieldWithTarget_t3058750293, ___m_DoStatic_4)); }
	inline bool get_m_DoStatic_4() const { return ___m_DoStatic_4; }
	inline bool* get_address_of_m_DoStatic_4() { return &___m_DoStatic_4; }
	inline void set_m_DoStatic_4(bool value)
	{
		___m_DoStatic_4 = value;
	}

	inline static int32_t get_offset_of_m_StaticString_5() { return static_cast<int32_t>(offsetof(FieldWithTarget_t3058750293, ___m_StaticString_5)); }
	inline String_t* get_m_StaticString_5() const { return ___m_StaticString_5; }
	inline String_t** get_address_of_m_StaticString_5() { return &___m_StaticString_5; }
	inline void set_m_StaticString_5(String_t* value)
	{
		___m_StaticString_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_StaticString_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FIELDWITHTARGET_T3058750293_H
#ifndef TRACKABLEPROPERTYBASE_T2121532948_H
#define TRACKABLEPROPERTYBASE_T2121532948_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.TrackablePropertyBase
struct  TrackablePropertyBase_t2121532948  : public RuntimeObject
{
public:
	// UnityEngine.Object UnityEngine.Analytics.TrackablePropertyBase::m_Target
	Object_t631007953 * ___m_Target_0;
	// System.String UnityEngine.Analytics.TrackablePropertyBase::m_Path
	String_t* ___m_Path_1;

public:
	inline static int32_t get_offset_of_m_Target_0() { return static_cast<int32_t>(offsetof(TrackablePropertyBase_t2121532948, ___m_Target_0)); }
	inline Object_t631007953 * get_m_Target_0() const { return ___m_Target_0; }
	inline Object_t631007953 ** get_address_of_m_Target_0() { return &___m_Target_0; }
	inline void set_m_Target_0(Object_t631007953 * value)
	{
		___m_Target_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Target_0), value);
	}

	inline static int32_t get_offset_of_m_Path_1() { return static_cast<int32_t>(offsetof(TrackablePropertyBase_t2121532948, ___m_Path_1)); }
	inline String_t* get_m_Path_1() const { return ___m_Path_1; }
	inline String_t** get_address_of_m_Path_1() { return &___m_Path_1; }
	inline void set_m_Path_1(String_t* value)
	{
		___m_Path_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_Path_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRACKABLEPROPERTYBASE_T2121532948_H
#ifndef TRACKABLETRIGGER_T621205209_H
#define TRACKABLETRIGGER_T621205209_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.TrackableTrigger
struct  TrackableTrigger_t621205209  : public RuntimeObject
{
public:
	// UnityEngine.GameObject UnityEngine.Analytics.TrackableTrigger::m_Target
	GameObject_t1113636619 * ___m_Target_0;
	// System.String UnityEngine.Analytics.TrackableTrigger::m_MethodPath
	String_t* ___m_MethodPath_1;

public:
	inline static int32_t get_offset_of_m_Target_0() { return static_cast<int32_t>(offsetof(TrackableTrigger_t621205209, ___m_Target_0)); }
	inline GameObject_t1113636619 * get_m_Target_0() const { return ___m_Target_0; }
	inline GameObject_t1113636619 ** get_address_of_m_Target_0() { return &___m_Target_0; }
	inline void set_m_Target_0(GameObject_t1113636619 * value)
	{
		___m_Target_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Target_0), value);
	}

	inline static int32_t get_offset_of_m_MethodPath_1() { return static_cast<int32_t>(offsetof(TrackableTrigger_t621205209, ___m_MethodPath_1)); }
	inline String_t* get_m_MethodPath_1() const { return ___m_MethodPath_1; }
	inline String_t** get_address_of_m_MethodPath_1() { return &___m_MethodPath_1; }
	inline void set_m_MethodPath_1(String_t* value)
	{
		___m_MethodPath_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_MethodPath_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRACKABLETRIGGER_T621205209_H
#ifndef TRIGGERLISTCONTAINER_T2032715483_H
#define TRIGGERLISTCONTAINER_T2032715483_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.TriggerListContainer
struct  TriggerListContainer_t2032715483  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<UnityEngine.Analytics.TriggerRule> UnityEngine.Analytics.TriggerListContainer::m_Rules
	List_1_t3418373063 * ___m_Rules_0;

public:
	inline static int32_t get_offset_of_m_Rules_0() { return static_cast<int32_t>(offsetof(TriggerListContainer_t2032715483, ___m_Rules_0)); }
	inline List_1_t3418373063 * get_m_Rules_0() const { return ___m_Rules_0; }
	inline List_1_t3418373063 ** get_address_of_m_Rules_0() { return &___m_Rules_0; }
	inline void set_m_Rules_0(List_1_t3418373063 * value)
	{
		___m_Rules_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Rules_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRIGGERLISTCONTAINER_T2032715483_H
#ifndef TRIGGERMETHOD_T582536534_H
#define TRIGGERMETHOD_T582536534_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.TriggerMethod
struct  TriggerMethod_t582536534  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRIGGERMETHOD_T582536534_H
#ifndef ARCONTROLLER_T116632334_H
#define ARCONTROLLER_T116632334_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.ARController
struct  ARController_t116632334  : public RuntimeObject
{
public:
	// Vuforia.VuforiaBehaviour Vuforia.ARController::mVuforiaBehaviour
	VuforiaBehaviour_t2151848540 * ___mVuforiaBehaviour_0;

public:
	inline static int32_t get_offset_of_mVuforiaBehaviour_0() { return static_cast<int32_t>(offsetof(ARController_t116632334, ___mVuforiaBehaviour_0)); }
	inline VuforiaBehaviour_t2151848540 * get_mVuforiaBehaviour_0() const { return ___mVuforiaBehaviour_0; }
	inline VuforiaBehaviour_t2151848540 ** get_address_of_mVuforiaBehaviour_0() { return &___mVuforiaBehaviour_0; }
	inline void set_mVuforiaBehaviour_0(VuforiaBehaviour_t2151848540 * value)
	{
		___mVuforiaBehaviour_0 = value;
		Il2CppCodeGenWriteBarrier((&___mVuforiaBehaviour_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARCONTROLLER_T116632334_H
#ifndef EYEWEARCALIBRATIONPROFILEMANAGER_T947793426_H
#define EYEWEARCALIBRATIONPROFILEMANAGER_T947793426_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.EyewearCalibrationProfileManager
struct  EyewearCalibrationProfileManager_t947793426  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EYEWEARCALIBRATIONPROFILEMANAGER_T947793426_H
#ifndef EYEWEARUSERCALIBRATOR_T2926839199_H
#define EYEWEARUSERCALIBRATOR_T2926839199_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.EyewearUserCalibrator
struct  EyewearUserCalibrator_t2926839199  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EYEWEARUSERCALIBRATOR_T2926839199_H
#ifndef TRACKER_T2709586299_H
#define TRACKER_T2709586299_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.Tracker
struct  Tracker_t2709586299  : public RuntimeObject
{
public:
	// System.Boolean Vuforia.Tracker::<IsActive>k__BackingField
	bool ___U3CIsActiveU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CIsActiveU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(Tracker_t2709586299, ___U3CIsActiveU3Ek__BackingField_0)); }
	inline bool get_U3CIsActiveU3Ek__BackingField_0() const { return ___U3CIsActiveU3Ek__BackingField_0; }
	inline bool* get_address_of_U3CIsActiveU3Ek__BackingField_0() { return &___U3CIsActiveU3Ek__BackingField_0; }
	inline void set_U3CIsActiveU3Ek__BackingField_0(bool value)
	{
		___U3CIsActiveU3Ek__BackingField_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRACKER_T2709586299_H
#ifndef TRACKERMANAGER_T1703337244_H
#define TRACKERMANAGER_T1703337244_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.TrackerManager
struct  TrackerManager_t1703337244  : public RuntimeObject
{
public:
	// Vuforia.StateManager Vuforia.TrackerManager::mStateManager
	StateManager_t1982749557 * ___mStateManager_1;
	// System.Collections.Generic.Dictionary`2<System.Type,Vuforia.Tracker> Vuforia.TrackerManager::mTrackers
	Dictionary_2_t858966067 * ___mTrackers_2;
	// System.Collections.Generic.Dictionary`2<System.Type,System.Func`2<System.Type,Vuforia.Tracker>> Vuforia.TrackerManager::mTrackerCreators
	Dictionary_2_t1322931057 * ___mTrackerCreators_3;
	// System.Collections.Generic.Dictionary`2<System.Type,System.Func`2<Vuforia.Tracker,System.Boolean>> Vuforia.TrackerManager::mTrackerNativeDeinitializers
	Dictionary_2_t2058017892 * ___mTrackerNativeDeinitializers_4;

public:
	inline static int32_t get_offset_of_mStateManager_1() { return static_cast<int32_t>(offsetof(TrackerManager_t1703337244, ___mStateManager_1)); }
	inline StateManager_t1982749557 * get_mStateManager_1() const { return ___mStateManager_1; }
	inline StateManager_t1982749557 ** get_address_of_mStateManager_1() { return &___mStateManager_1; }
	inline void set_mStateManager_1(StateManager_t1982749557 * value)
	{
		___mStateManager_1 = value;
		Il2CppCodeGenWriteBarrier((&___mStateManager_1), value);
	}

	inline static int32_t get_offset_of_mTrackers_2() { return static_cast<int32_t>(offsetof(TrackerManager_t1703337244, ___mTrackers_2)); }
	inline Dictionary_2_t858966067 * get_mTrackers_2() const { return ___mTrackers_2; }
	inline Dictionary_2_t858966067 ** get_address_of_mTrackers_2() { return &___mTrackers_2; }
	inline void set_mTrackers_2(Dictionary_2_t858966067 * value)
	{
		___mTrackers_2 = value;
		Il2CppCodeGenWriteBarrier((&___mTrackers_2), value);
	}

	inline static int32_t get_offset_of_mTrackerCreators_3() { return static_cast<int32_t>(offsetof(TrackerManager_t1703337244, ___mTrackerCreators_3)); }
	inline Dictionary_2_t1322931057 * get_mTrackerCreators_3() const { return ___mTrackerCreators_3; }
	inline Dictionary_2_t1322931057 ** get_address_of_mTrackerCreators_3() { return &___mTrackerCreators_3; }
	inline void set_mTrackerCreators_3(Dictionary_2_t1322931057 * value)
	{
		___mTrackerCreators_3 = value;
		Il2CppCodeGenWriteBarrier((&___mTrackerCreators_3), value);
	}

	inline static int32_t get_offset_of_mTrackerNativeDeinitializers_4() { return static_cast<int32_t>(offsetof(TrackerManager_t1703337244, ___mTrackerNativeDeinitializers_4)); }
	inline Dictionary_2_t2058017892 * get_mTrackerNativeDeinitializers_4() const { return ___mTrackerNativeDeinitializers_4; }
	inline Dictionary_2_t2058017892 ** get_address_of_mTrackerNativeDeinitializers_4() { return &___mTrackerNativeDeinitializers_4; }
	inline void set_mTrackerNativeDeinitializers_4(Dictionary_2_t2058017892 * value)
	{
		___mTrackerNativeDeinitializers_4 = value;
		Il2CppCodeGenWriteBarrier((&___mTrackerNativeDeinitializers_4), value);
	}
};

struct TrackerManager_t1703337244_StaticFields
{
public:
	// Vuforia.ITrackerManager Vuforia.TrackerManager::mInstance
	RuntimeObject* ___mInstance_0;

public:
	inline static int32_t get_offset_of_mInstance_0() { return static_cast<int32_t>(offsetof(TrackerManager_t1703337244_StaticFields, ___mInstance_0)); }
	inline RuntimeObject* get_mInstance_0() const { return ___mInstance_0; }
	inline RuntimeObject** get_address_of_mInstance_0() { return &___mInstance_0; }
	inline void set_mInstance_0(RuntimeObject* value)
	{
		___mInstance_0 = value;
		Il2CppCodeGenWriteBarrier((&___mInstance_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRACKERMANAGER_T1703337244_H
#ifndef U3CU3EC_T1451390621_H
#define U3CU3EC_T1451390621_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.TrackerManager/<>c
struct  U3CU3Ec_t1451390621  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_t1451390621_StaticFields
{
public:
	// Vuforia.TrackerManager/<>c Vuforia.TrackerManager/<>c::<>9
	U3CU3Ec_t1451390621 * ___U3CU3E9_0;
	// System.Func`2<System.Type,Vuforia.Tracker> Vuforia.TrackerManager/<>c::<>9__8_0
	Func_2_t3173551289 * ___U3CU3E9__8_0_1;
	// System.Func`2<System.Type,Vuforia.Tracker> Vuforia.TrackerManager/<>c::<>9__8_1
	Func_2_t3173551289 * ___U3CU3E9__8_1_2;
	// System.Func`2<Vuforia.Tracker,System.Boolean> Vuforia.TrackerManager/<>c::<>9__8_2
	Func_2_t3908638124 * ___U3CU3E9__8_2_3;
	// System.Func`2<Vuforia.Tracker,System.Boolean> Vuforia.TrackerManager/<>c::<>9__8_3
	Func_2_t3908638124 * ___U3CU3E9__8_3_4;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_t1451390621_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_t1451390621 * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_t1451390621 ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_t1451390621 * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__8_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_t1451390621_StaticFields, ___U3CU3E9__8_0_1)); }
	inline Func_2_t3173551289 * get_U3CU3E9__8_0_1() const { return ___U3CU3E9__8_0_1; }
	inline Func_2_t3173551289 ** get_address_of_U3CU3E9__8_0_1() { return &___U3CU3E9__8_0_1; }
	inline void set_U3CU3E9__8_0_1(Func_2_t3173551289 * value)
	{
		___U3CU3E9__8_0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__8_0_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__8_1_2() { return static_cast<int32_t>(offsetof(U3CU3Ec_t1451390621_StaticFields, ___U3CU3E9__8_1_2)); }
	inline Func_2_t3173551289 * get_U3CU3E9__8_1_2() const { return ___U3CU3E9__8_1_2; }
	inline Func_2_t3173551289 ** get_address_of_U3CU3E9__8_1_2() { return &___U3CU3E9__8_1_2; }
	inline void set_U3CU3E9__8_1_2(Func_2_t3173551289 * value)
	{
		___U3CU3E9__8_1_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__8_1_2), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__8_2_3() { return static_cast<int32_t>(offsetof(U3CU3Ec_t1451390621_StaticFields, ___U3CU3E9__8_2_3)); }
	inline Func_2_t3908638124 * get_U3CU3E9__8_2_3() const { return ___U3CU3E9__8_2_3; }
	inline Func_2_t3908638124 ** get_address_of_U3CU3E9__8_2_3() { return &___U3CU3E9__8_2_3; }
	inline void set_U3CU3E9__8_2_3(Func_2_t3908638124 * value)
	{
		___U3CU3E9__8_2_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__8_2_3), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__8_3_4() { return static_cast<int32_t>(offsetof(U3CU3Ec_t1451390621_StaticFields, ___U3CU3E9__8_3_4)); }
	inline Func_2_t3908638124 * get_U3CU3E9__8_3_4() const { return ___U3CU3E9__8_3_4; }
	inline Func_2_t3908638124 ** get_address_of_U3CU3E9__8_3_4() { return &___U3CU3E9__8_3_4; }
	inline void set_U3CU3E9__8_3_4(Func_2_t3908638124 * value)
	{
		___U3CU3E9__8_3_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__8_3_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC_T1451390621_H
#ifndef U3CU3EC_T3582055403_H
#define U3CU3EC_T3582055403_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.WebCamARController/<>c
struct  U3CU3Ec_t3582055403  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_t3582055403_StaticFields
{
public:
	// Vuforia.WebCamARController/<>c Vuforia.WebCamARController/<>c::<>9
	U3CU3Ec_t3582055403 * ___U3CU3E9_0;
	// System.Func`3<System.String,Vuforia.WebCamProfile/ProfileData,Vuforia.IWebCamTexAdaptor> Vuforia.WebCamARController/<>c::<>9__7_0
	Func_3_t3440825513 * ___U3CU3E9__7_0_1;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_t3582055403_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_t3582055403 * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_t3582055403 ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_t3582055403 * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__7_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_t3582055403_StaticFields, ___U3CU3E9__7_0_1)); }
	inline Func_3_t3440825513 * get_U3CU3E9__7_0_1() const { return ___U3CU3E9__7_0_1; }
	inline Func_3_t3440825513 ** get_address_of_U3CU3E9__7_0_1() { return &___U3CU3E9__7_0_1; }
	inline void set_U3CU3E9__7_0_1(Func_3_t3440825513 * value)
	{
		___U3CU3E9__7_0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__7_0_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC_T3582055403_H
#ifndef __STATICARRAYINITTYPESIZEU3D24_T3517759979_H
#define __STATICARRAYINITTYPESIZEU3D24_T3517759979_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=24
struct  __StaticArrayInitTypeSizeU3D24_t3517759979 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D24_t3517759979__padding[24];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // __STATICARRAYINITTYPESIZEU3D24_T3517759979_H
#ifndef BOOLREFERENCE_T2741665770_H
#define BOOLREFERENCE_T2741665770_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BoolReference
struct  BoolReference_t2741665770  : public GenericReference_t1852046461
{
public:
	// BoolVariable BoolReference::m_Variable
	BoolVariable_t415785710 * ___m_Variable_1;
	// System.Boolean BoolReference::m_ConstantVariable
	bool ___m_ConstantVariable_2;

public:
	inline static int32_t get_offset_of_m_Variable_1() { return static_cast<int32_t>(offsetof(BoolReference_t2741665770, ___m_Variable_1)); }
	inline BoolVariable_t415785710 * get_m_Variable_1() const { return ___m_Variable_1; }
	inline BoolVariable_t415785710 ** get_address_of_m_Variable_1() { return &___m_Variable_1; }
	inline void set_m_Variable_1(BoolVariable_t415785710 * value)
	{
		___m_Variable_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_Variable_1), value);
	}

	inline static int32_t get_offset_of_m_ConstantVariable_2() { return static_cast<int32_t>(offsetof(BoolReference_t2741665770, ___m_ConstantVariable_2)); }
	inline bool get_m_ConstantVariable_2() const { return ___m_ConstantVariable_2; }
	inline bool* get_address_of_m_ConstantVariable_2() { return &___m_ConstantVariable_2; }
	inline void set_m_ConstantVariable_2(bool value)
	{
		___m_ConstantVariable_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOOLREFERENCE_T2741665770_H
#ifndef BYTEREFERENCE_T441371523_H
#define BYTEREFERENCE_T441371523_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ByteReference
struct  ByteReference_t441371523  : public GenericReference_t1852046461
{
public:
	// ByteVariable ByteReference::m_Variable
	ByteVariable_t2671702712 * ___m_Variable_1;
	// System.Byte ByteReference::m_ConstantVariable
	uint8_t ___m_ConstantVariable_2;

public:
	inline static int32_t get_offset_of_m_Variable_1() { return static_cast<int32_t>(offsetof(ByteReference_t441371523, ___m_Variable_1)); }
	inline ByteVariable_t2671702712 * get_m_Variable_1() const { return ___m_Variable_1; }
	inline ByteVariable_t2671702712 ** get_address_of_m_Variable_1() { return &___m_Variable_1; }
	inline void set_m_Variable_1(ByteVariable_t2671702712 * value)
	{
		___m_Variable_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_Variable_1), value);
	}

	inline static int32_t get_offset_of_m_ConstantVariable_2() { return static_cast<int32_t>(offsetof(ByteReference_t441371523, ___m_ConstantVariable_2)); }
	inline uint8_t get_m_ConstantVariable_2() const { return ___m_ConstantVariable_2; }
	inline uint8_t* get_address_of_m_ConstantVariable_2() { return &___m_ConstantVariable_2; }
	inline void set_m_ConstantVariable_2(uint8_t value)
	{
		___m_ConstantVariable_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BYTEREFERENCE_T441371523_H
#ifndef DOUBLEREFERENCE_T1729311906_H
#define DOUBLEREFERENCE_T1729311906_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DoubleReference
struct  DoubleReference_t1729311906  : public GenericReference_t1852046461
{
public:
	// DoubleVariable DoubleReference::m_Variable
	DoubleVariable_t2292910480 * ___m_Variable_1;
	// System.Double DoubleReference::m_ConstantVariable
	double ___m_ConstantVariable_2;

public:
	inline static int32_t get_offset_of_m_Variable_1() { return static_cast<int32_t>(offsetof(DoubleReference_t1729311906, ___m_Variable_1)); }
	inline DoubleVariable_t2292910480 * get_m_Variable_1() const { return ___m_Variable_1; }
	inline DoubleVariable_t2292910480 ** get_address_of_m_Variable_1() { return &___m_Variable_1; }
	inline void set_m_Variable_1(DoubleVariable_t2292910480 * value)
	{
		___m_Variable_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_Variable_1), value);
	}

	inline static int32_t get_offset_of_m_ConstantVariable_2() { return static_cast<int32_t>(offsetof(DoubleReference_t1729311906, ___m_ConstantVariable_2)); }
	inline double get_m_ConstantVariable_2() const { return ___m_ConstantVariable_2; }
	inline double* get_address_of_m_ConstantVariable_2() { return &___m_ConstantVariable_2; }
	inline void set_m_ConstantVariable_2(double value)
	{
		___m_ConstantVariable_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DOUBLEREFERENCE_T1729311906_H
#ifndef FLOATREFERENCE_T1374219879_H
#define FLOATREFERENCE_T1374219879_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FloatReference
struct  FloatReference_t1374219879  : public GenericReference_t1852046461
{
public:
	// FloatVariable FloatReference::m_Variable
	FloatVariable_t761998486 * ___m_Variable_1;
	// System.Single FloatReference::m_ConstantVariable
	float ___m_ConstantVariable_2;

public:
	inline static int32_t get_offset_of_m_Variable_1() { return static_cast<int32_t>(offsetof(FloatReference_t1374219879, ___m_Variable_1)); }
	inline FloatVariable_t761998486 * get_m_Variable_1() const { return ___m_Variable_1; }
	inline FloatVariable_t761998486 ** get_address_of_m_Variable_1() { return &___m_Variable_1; }
	inline void set_m_Variable_1(FloatVariable_t761998486 * value)
	{
		___m_Variable_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_Variable_1), value);
	}

	inline static int32_t get_offset_of_m_ConstantVariable_2() { return static_cast<int32_t>(offsetof(FloatReference_t1374219879, ___m_ConstantVariable_2)); }
	inline float get_m_ConstantVariable_2() const { return ___m_ConstantVariable_2; }
	inline float* get_address_of_m_ConstantVariable_2() { return &___m_ConstantVariable_2; }
	inline void set_m_ConstantVariable_2(float value)
	{
		___m_ConstantVariable_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FLOATREFERENCE_T1374219879_H
#ifndef INTREFERENCE_T2842414034_H
#define INTREFERENCE_T2842414034_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IntReference
struct  IntReference_t2842414034  : public GenericReference_t1852046461
{
public:
	// IntVariable IntReference::m_Variable
	IntVariable_t1706759505 * ___m_Variable_1;
	// System.Int32 IntReference::m_ConstantVariable
	int32_t ___m_ConstantVariable_2;

public:
	inline static int32_t get_offset_of_m_Variable_1() { return static_cast<int32_t>(offsetof(IntReference_t2842414034, ___m_Variable_1)); }
	inline IntVariable_t1706759505 * get_m_Variable_1() const { return ___m_Variable_1; }
	inline IntVariable_t1706759505 ** get_address_of_m_Variable_1() { return &___m_Variable_1; }
	inline void set_m_Variable_1(IntVariable_t1706759505 * value)
	{
		___m_Variable_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_Variable_1), value);
	}

	inline static int32_t get_offset_of_m_ConstantVariable_2() { return static_cast<int32_t>(offsetof(IntReference_t2842414034, ___m_ConstantVariable_2)); }
	inline int32_t get_m_ConstantVariable_2() const { return ___m_ConstantVariable_2; }
	inline int32_t* get_address_of_m_ConstantVariable_2() { return &___m_ConstantVariable_2; }
	inline void set_m_ConstantVariable_2(int32_t value)
	{
		___m_ConstantVariable_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTREFERENCE_T2842414034_H
#ifndef LONGREFERENCE_T3770171985_H
#define LONGREFERENCE_T3770171985_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LongReference
struct  LongReference_t3770171985  : public GenericReference_t1852046461
{
public:
	// LongVariable LongReference::m_Variable
	LongVariable_t812939517 * ___m_Variable_1;
	// System.Int64 LongReference::m_ConstantVariable
	int64_t ___m_ConstantVariable_2;

public:
	inline static int32_t get_offset_of_m_Variable_1() { return static_cast<int32_t>(offsetof(LongReference_t3770171985, ___m_Variable_1)); }
	inline LongVariable_t812939517 * get_m_Variable_1() const { return ___m_Variable_1; }
	inline LongVariable_t812939517 ** get_address_of_m_Variable_1() { return &___m_Variable_1; }
	inline void set_m_Variable_1(LongVariable_t812939517 * value)
	{
		___m_Variable_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_Variable_1), value);
	}

	inline static int32_t get_offset_of_m_ConstantVariable_2() { return static_cast<int32_t>(offsetof(LongReference_t3770171985, ___m_ConstantVariable_2)); }
	inline int64_t get_m_ConstantVariable_2() const { return ___m_ConstantVariable_2; }
	inline int64_t* get_address_of_m_ConstantVariable_2() { return &___m_ConstantVariable_2; }
	inline void set_m_ConstantVariable_2(int64_t value)
	{
		___m_ConstantVariable_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LONGREFERENCE_T3770171985_H
#ifndef SHORTREFERENCE_T1700820959_H
#define SHORTREFERENCE_T1700820959_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ShortReference
struct  ShortReference_t1700820959  : public GenericReference_t1852046461
{
public:
	// ShortVariable ShortReference::m_Variable
	ShortVariable_t886426368 * ___m_Variable_1;
	// System.Int16 ShortReference::m_ConstantVariable
	int16_t ___m_ConstantVariable_2;

public:
	inline static int32_t get_offset_of_m_Variable_1() { return static_cast<int32_t>(offsetof(ShortReference_t1700820959, ___m_Variable_1)); }
	inline ShortVariable_t886426368 * get_m_Variable_1() const { return ___m_Variable_1; }
	inline ShortVariable_t886426368 ** get_address_of_m_Variable_1() { return &___m_Variable_1; }
	inline void set_m_Variable_1(ShortVariable_t886426368 * value)
	{
		___m_Variable_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_Variable_1), value);
	}

	inline static int32_t get_offset_of_m_ConstantVariable_2() { return static_cast<int32_t>(offsetof(ShortReference_t1700820959, ___m_ConstantVariable_2)); }
	inline int16_t get_m_ConstantVariable_2() const { return ___m_ConstantVariable_2; }
	inline int16_t* get_address_of_m_ConstantVariable_2() { return &___m_ConstantVariable_2; }
	inline void set_m_ConstantVariable_2(int16_t value)
	{
		___m_ConstantVariable_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SHORTREFERENCE_T1700820959_H
#ifndef ENUM_T4135868527_H
#define ENUM_T4135868527_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t4135868527  : public ValueType_t3640485471
{
public:

public:
};

struct Enum_t4135868527_StaticFields
{
public:
	// System.Char[] System.Enum::split_char
	CharU5BU5D_t3528271667* ___split_char_0;

public:
	inline static int32_t get_offset_of_split_char_0() { return static_cast<int32_t>(offsetof(Enum_t4135868527_StaticFields, ___split_char_0)); }
	inline CharU5BU5D_t3528271667* get_split_char_0() const { return ___split_char_0; }
	inline CharU5BU5D_t3528271667** get_address_of_split_char_0() { return &___split_char_0; }
	inline void set_split_char_0(CharU5BU5D_t3528271667* value)
	{
		___split_char_0 = value;
		Il2CppCodeGenWriteBarrier((&___split_char_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t4135868527_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t4135868527_marshaled_com
{
};
#endif // ENUM_T4135868527_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef VOID_T1185182177_H
#define VOID_T1185182177_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t1185182177 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T1185182177_H
#ifndef UINTREFERENCE_T2491003739_H
#define UINTREFERENCE_T2491003739_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UintReference
struct  UintReference_t2491003739  : public GenericReference_t1852046461
{
public:
	// UintVariable UintReference::m_Variable
	UintVariable_t3613798951 * ___m_Variable_1;
	// System.UInt32 UintReference::m_ConstantVariable
	uint32_t ___m_ConstantVariable_2;

public:
	inline static int32_t get_offset_of_m_Variable_1() { return static_cast<int32_t>(offsetof(UintReference_t2491003739, ___m_Variable_1)); }
	inline UintVariable_t3613798951 * get_m_Variable_1() const { return ___m_Variable_1; }
	inline UintVariable_t3613798951 ** get_address_of_m_Variable_1() { return &___m_Variable_1; }
	inline void set_m_Variable_1(UintVariable_t3613798951 * value)
	{
		___m_Variable_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_Variable_1), value);
	}

	inline static int32_t get_offset_of_m_ConstantVariable_2() { return static_cast<int32_t>(offsetof(UintReference_t2491003739, ___m_ConstantVariable_2)); }
	inline uint32_t get_m_ConstantVariable_2() const { return ___m_ConstantVariable_2; }
	inline uint32_t* get_address_of_m_ConstantVariable_2() { return &___m_ConstantVariable_2; }
	inline void set_m_ConstantVariable_2(uint32_t value)
	{
		___m_ConstantVariable_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UINTREFERENCE_T2491003739_H
#ifndef TRACKABLEFIELD_T1772682203_H
#define TRACKABLEFIELD_T1772682203_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.TrackableField
struct  TrackableField_t1772682203  : public TrackablePropertyBase_t2121532948
{
public:
	// System.String[] UnityEngine.Analytics.TrackableField::m_ValidTypeNames
	StringU5BU5D_t1281789340* ___m_ValidTypeNames_2;
	// System.String UnityEngine.Analytics.TrackableField::m_Type
	String_t* ___m_Type_3;
	// System.String UnityEngine.Analytics.TrackableField::m_EnumType
	String_t* ___m_EnumType_4;

public:
	inline static int32_t get_offset_of_m_ValidTypeNames_2() { return static_cast<int32_t>(offsetof(TrackableField_t1772682203, ___m_ValidTypeNames_2)); }
	inline StringU5BU5D_t1281789340* get_m_ValidTypeNames_2() const { return ___m_ValidTypeNames_2; }
	inline StringU5BU5D_t1281789340** get_address_of_m_ValidTypeNames_2() { return &___m_ValidTypeNames_2; }
	inline void set_m_ValidTypeNames_2(StringU5BU5D_t1281789340* value)
	{
		___m_ValidTypeNames_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_ValidTypeNames_2), value);
	}

	inline static int32_t get_offset_of_m_Type_3() { return static_cast<int32_t>(offsetof(TrackableField_t1772682203, ___m_Type_3)); }
	inline String_t* get_m_Type_3() const { return ___m_Type_3; }
	inline String_t** get_address_of_m_Type_3() { return &___m_Type_3; }
	inline void set_m_Type_3(String_t* value)
	{
		___m_Type_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_Type_3), value);
	}

	inline static int32_t get_offset_of_m_EnumType_4() { return static_cast<int32_t>(offsetof(TrackableField_t1772682203, ___m_EnumType_4)); }
	inline String_t* get_m_EnumType_4() const { return ___m_EnumType_4; }
	inline String_t** get_address_of_m_EnumType_4() { return &___m_EnumType_4; }
	inline void set_m_EnumType_4(String_t* value)
	{
		___m_EnumType_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_EnumType_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRACKABLEFIELD_T1772682203_H
#ifndef COLOR_T2555686324_H
#define COLOR_T2555686324_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color
struct  Color_t2555686324 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR_T2555686324_H
#ifndef MATRIX4X4_T1817901843_H
#define MATRIX4X4_T1817901843_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Matrix4x4
struct  Matrix4x4_t1817901843 
{
public:
	// System.Single UnityEngine.Matrix4x4::m00
	float ___m00_0;
	// System.Single UnityEngine.Matrix4x4::m10
	float ___m10_1;
	// System.Single UnityEngine.Matrix4x4::m20
	float ___m20_2;
	// System.Single UnityEngine.Matrix4x4::m30
	float ___m30_3;
	// System.Single UnityEngine.Matrix4x4::m01
	float ___m01_4;
	// System.Single UnityEngine.Matrix4x4::m11
	float ___m11_5;
	// System.Single UnityEngine.Matrix4x4::m21
	float ___m21_6;
	// System.Single UnityEngine.Matrix4x4::m31
	float ___m31_7;
	// System.Single UnityEngine.Matrix4x4::m02
	float ___m02_8;
	// System.Single UnityEngine.Matrix4x4::m12
	float ___m12_9;
	// System.Single UnityEngine.Matrix4x4::m22
	float ___m22_10;
	// System.Single UnityEngine.Matrix4x4::m32
	float ___m32_11;
	// System.Single UnityEngine.Matrix4x4::m03
	float ___m03_12;
	// System.Single UnityEngine.Matrix4x4::m13
	float ___m13_13;
	// System.Single UnityEngine.Matrix4x4::m23
	float ___m23_14;
	// System.Single UnityEngine.Matrix4x4::m33
	float ___m33_15;

public:
	inline static int32_t get_offset_of_m00_0() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m00_0)); }
	inline float get_m00_0() const { return ___m00_0; }
	inline float* get_address_of_m00_0() { return &___m00_0; }
	inline void set_m00_0(float value)
	{
		___m00_0 = value;
	}

	inline static int32_t get_offset_of_m10_1() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m10_1)); }
	inline float get_m10_1() const { return ___m10_1; }
	inline float* get_address_of_m10_1() { return &___m10_1; }
	inline void set_m10_1(float value)
	{
		___m10_1 = value;
	}

	inline static int32_t get_offset_of_m20_2() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m20_2)); }
	inline float get_m20_2() const { return ___m20_2; }
	inline float* get_address_of_m20_2() { return &___m20_2; }
	inline void set_m20_2(float value)
	{
		___m20_2 = value;
	}

	inline static int32_t get_offset_of_m30_3() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m30_3)); }
	inline float get_m30_3() const { return ___m30_3; }
	inline float* get_address_of_m30_3() { return &___m30_3; }
	inline void set_m30_3(float value)
	{
		___m30_3 = value;
	}

	inline static int32_t get_offset_of_m01_4() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m01_4)); }
	inline float get_m01_4() const { return ___m01_4; }
	inline float* get_address_of_m01_4() { return &___m01_4; }
	inline void set_m01_4(float value)
	{
		___m01_4 = value;
	}

	inline static int32_t get_offset_of_m11_5() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m11_5)); }
	inline float get_m11_5() const { return ___m11_5; }
	inline float* get_address_of_m11_5() { return &___m11_5; }
	inline void set_m11_5(float value)
	{
		___m11_5 = value;
	}

	inline static int32_t get_offset_of_m21_6() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m21_6)); }
	inline float get_m21_6() const { return ___m21_6; }
	inline float* get_address_of_m21_6() { return &___m21_6; }
	inline void set_m21_6(float value)
	{
		___m21_6 = value;
	}

	inline static int32_t get_offset_of_m31_7() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m31_7)); }
	inline float get_m31_7() const { return ___m31_7; }
	inline float* get_address_of_m31_7() { return &___m31_7; }
	inline void set_m31_7(float value)
	{
		___m31_7 = value;
	}

	inline static int32_t get_offset_of_m02_8() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m02_8)); }
	inline float get_m02_8() const { return ___m02_8; }
	inline float* get_address_of_m02_8() { return &___m02_8; }
	inline void set_m02_8(float value)
	{
		___m02_8 = value;
	}

	inline static int32_t get_offset_of_m12_9() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m12_9)); }
	inline float get_m12_9() const { return ___m12_9; }
	inline float* get_address_of_m12_9() { return &___m12_9; }
	inline void set_m12_9(float value)
	{
		___m12_9 = value;
	}

	inline static int32_t get_offset_of_m22_10() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m22_10)); }
	inline float get_m22_10() const { return ___m22_10; }
	inline float* get_address_of_m22_10() { return &___m22_10; }
	inline void set_m22_10(float value)
	{
		___m22_10 = value;
	}

	inline static int32_t get_offset_of_m32_11() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m32_11)); }
	inline float get_m32_11() const { return ___m32_11; }
	inline float* get_address_of_m32_11() { return &___m32_11; }
	inline void set_m32_11(float value)
	{
		___m32_11 = value;
	}

	inline static int32_t get_offset_of_m03_12() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m03_12)); }
	inline float get_m03_12() const { return ___m03_12; }
	inline float* get_address_of_m03_12() { return &___m03_12; }
	inline void set_m03_12(float value)
	{
		___m03_12 = value;
	}

	inline static int32_t get_offset_of_m13_13() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m13_13)); }
	inline float get_m13_13() const { return ___m13_13; }
	inline float* get_address_of_m13_13() { return &___m13_13; }
	inline void set_m13_13(float value)
	{
		___m13_13 = value;
	}

	inline static int32_t get_offset_of_m23_14() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m23_14)); }
	inline float get_m23_14() const { return ___m23_14; }
	inline float* get_address_of_m23_14() { return &___m23_14; }
	inline void set_m23_14(float value)
	{
		___m23_14 = value;
	}

	inline static int32_t get_offset_of_m33_15() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m33_15)); }
	inline float get_m33_15() const { return ___m33_15; }
	inline float* get_address_of_m33_15() { return &___m33_15; }
	inline void set_m33_15(float value)
	{
		___m33_15 = value;
	}
};

struct Matrix4x4_t1817901843_StaticFields
{
public:
	// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::zeroMatrix
	Matrix4x4_t1817901843  ___zeroMatrix_16;
	// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::identityMatrix
	Matrix4x4_t1817901843  ___identityMatrix_17;

public:
	inline static int32_t get_offset_of_zeroMatrix_16() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843_StaticFields, ___zeroMatrix_16)); }
	inline Matrix4x4_t1817901843  get_zeroMatrix_16() const { return ___zeroMatrix_16; }
	inline Matrix4x4_t1817901843 * get_address_of_zeroMatrix_16() { return &___zeroMatrix_16; }
	inline void set_zeroMatrix_16(Matrix4x4_t1817901843  value)
	{
		___zeroMatrix_16 = value;
	}

	inline static int32_t get_offset_of_identityMatrix_17() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843_StaticFields, ___identityMatrix_17)); }
	inline Matrix4x4_t1817901843  get_identityMatrix_17() const { return ___identityMatrix_17; }
	inline Matrix4x4_t1817901843 * get_address_of_identityMatrix_17() { return &___identityMatrix_17; }
	inline void set_identityMatrix_17(Matrix4x4_t1817901843  value)
	{
		___identityMatrix_17 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MATRIX4X4_T1817901843_H
#ifndef VECTOR2_T2156229523_H
#define VECTOR2_T2156229523_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector2
struct  Vector2_t2156229523 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_t2156229523, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_t2156229523, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_t2156229523_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_t2156229523  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_t2156229523  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_t2156229523  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_t2156229523  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_t2156229523  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_t2156229523  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_t2156229523  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_t2156229523  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___zeroVector_2)); }
	inline Vector2_t2156229523  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_t2156229523 * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_t2156229523  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___oneVector_3)); }
	inline Vector2_t2156229523  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_t2156229523 * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_t2156229523  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___upVector_4)); }
	inline Vector2_t2156229523  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_t2156229523 * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_t2156229523  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___downVector_5)); }
	inline Vector2_t2156229523  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_t2156229523 * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_t2156229523  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___leftVector_6)); }
	inline Vector2_t2156229523  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_t2156229523 * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_t2156229523  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___rightVector_7)); }
	inline Vector2_t2156229523  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_t2156229523 * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_t2156229523  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_t2156229523  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_t2156229523 * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_t2156229523  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_t2156229523  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_t2156229523 * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_t2156229523  value)
	{
		___negativeInfinityVector_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2_T2156229523_H
#ifndef VECTOR3_T3722313464_H
#define VECTOR3_T3722313464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector3
struct  Vector3_t3722313464 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_2;
	// System.Single UnityEngine.Vector3::y
	float ___y_3;
	// System.Single UnityEngine.Vector3::z
	float ___z_4;

public:
	inline static int32_t get_offset_of_x_2() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___x_2)); }
	inline float get_x_2() const { return ___x_2; }
	inline float* get_address_of_x_2() { return &___x_2; }
	inline void set_x_2(float value)
	{
		___x_2 = value;
	}

	inline static int32_t get_offset_of_y_3() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___y_3)); }
	inline float get_y_3() const { return ___y_3; }
	inline float* get_address_of_y_3() { return &___y_3; }
	inline void set_y_3(float value)
	{
		___y_3 = value;
	}

	inline static int32_t get_offset_of_z_4() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___z_4)); }
	inline float get_z_4() const { return ___z_4; }
	inline float* get_address_of_z_4() { return &___z_4; }
	inline void set_z_4(float value)
	{
		___z_4 = value;
	}
};

struct Vector3_t3722313464_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_t3722313464  ___zeroVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_t3722313464  ___oneVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_t3722313464  ___upVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_t3722313464  ___downVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_t3722313464  ___leftVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_t3722313464  ___rightVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_t3722313464  ___forwardVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_t3722313464  ___backVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_t3722313464  ___positiveInfinityVector_13;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_t3722313464  ___negativeInfinityVector_14;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___zeroVector_5)); }
	inline Vector3_t3722313464  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector3_t3722313464 * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector3_t3722313464  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___oneVector_6)); }
	inline Vector3_t3722313464  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector3_t3722313464 * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector3_t3722313464  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_upVector_7() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___upVector_7)); }
	inline Vector3_t3722313464  get_upVector_7() const { return ___upVector_7; }
	inline Vector3_t3722313464 * get_address_of_upVector_7() { return &___upVector_7; }
	inline void set_upVector_7(Vector3_t3722313464  value)
	{
		___upVector_7 = value;
	}

	inline static int32_t get_offset_of_downVector_8() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___downVector_8)); }
	inline Vector3_t3722313464  get_downVector_8() const { return ___downVector_8; }
	inline Vector3_t3722313464 * get_address_of_downVector_8() { return &___downVector_8; }
	inline void set_downVector_8(Vector3_t3722313464  value)
	{
		___downVector_8 = value;
	}

	inline static int32_t get_offset_of_leftVector_9() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___leftVector_9)); }
	inline Vector3_t3722313464  get_leftVector_9() const { return ___leftVector_9; }
	inline Vector3_t3722313464 * get_address_of_leftVector_9() { return &___leftVector_9; }
	inline void set_leftVector_9(Vector3_t3722313464  value)
	{
		___leftVector_9 = value;
	}

	inline static int32_t get_offset_of_rightVector_10() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___rightVector_10)); }
	inline Vector3_t3722313464  get_rightVector_10() const { return ___rightVector_10; }
	inline Vector3_t3722313464 * get_address_of_rightVector_10() { return &___rightVector_10; }
	inline void set_rightVector_10(Vector3_t3722313464  value)
	{
		___rightVector_10 = value;
	}

	inline static int32_t get_offset_of_forwardVector_11() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___forwardVector_11)); }
	inline Vector3_t3722313464  get_forwardVector_11() const { return ___forwardVector_11; }
	inline Vector3_t3722313464 * get_address_of_forwardVector_11() { return &___forwardVector_11; }
	inline void set_forwardVector_11(Vector3_t3722313464  value)
	{
		___forwardVector_11 = value;
	}

	inline static int32_t get_offset_of_backVector_12() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___backVector_12)); }
	inline Vector3_t3722313464  get_backVector_12() const { return ___backVector_12; }
	inline Vector3_t3722313464 * get_address_of_backVector_12() { return &___backVector_12; }
	inline void set_backVector_12(Vector3_t3722313464  value)
	{
		___backVector_12 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___positiveInfinityVector_13)); }
	inline Vector3_t3722313464  get_positiveInfinityVector_13() const { return ___positiveInfinityVector_13; }
	inline Vector3_t3722313464 * get_address_of_positiveInfinityVector_13() { return &___positiveInfinityVector_13; }
	inline void set_positiveInfinityVector_13(Vector3_t3722313464  value)
	{
		___positiveInfinityVector_13 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_14() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___negativeInfinityVector_14)); }
	inline Vector3_t3722313464  get_negativeInfinityVector_14() const { return ___negativeInfinityVector_14; }
	inline Vector3_t3722313464 * get_address_of_negativeInfinityVector_14() { return &___negativeInfinityVector_14; }
	inline void set_negativeInfinityVector_14(Vector3_t3722313464  value)
	{
		___negativeInfinityVector_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_T3722313464_H
#ifndef RECTANGLEDATA_T1039179782_H
#define RECTANGLEDATA_T1039179782_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.RectangleData
#pragma pack(push, tp, 1)
struct  RectangleData_t1039179782 
{
public:
	// System.Single Vuforia.RectangleData::leftTopX
	float ___leftTopX_0;
	// System.Single Vuforia.RectangleData::leftTopY
	float ___leftTopY_1;
	// System.Single Vuforia.RectangleData::rightBottomX
	float ___rightBottomX_2;
	// System.Single Vuforia.RectangleData::rightBottomY
	float ___rightBottomY_3;

public:
	inline static int32_t get_offset_of_leftTopX_0() { return static_cast<int32_t>(offsetof(RectangleData_t1039179782, ___leftTopX_0)); }
	inline float get_leftTopX_0() const { return ___leftTopX_0; }
	inline float* get_address_of_leftTopX_0() { return &___leftTopX_0; }
	inline void set_leftTopX_0(float value)
	{
		___leftTopX_0 = value;
	}

	inline static int32_t get_offset_of_leftTopY_1() { return static_cast<int32_t>(offsetof(RectangleData_t1039179782, ___leftTopY_1)); }
	inline float get_leftTopY_1() const { return ___leftTopY_1; }
	inline float* get_address_of_leftTopY_1() { return &___leftTopY_1; }
	inline void set_leftTopY_1(float value)
	{
		___leftTopY_1 = value;
	}

	inline static int32_t get_offset_of_rightBottomX_2() { return static_cast<int32_t>(offsetof(RectangleData_t1039179782, ___rightBottomX_2)); }
	inline float get_rightBottomX_2() const { return ___rightBottomX_2; }
	inline float* get_address_of_rightBottomX_2() { return &___rightBottomX_2; }
	inline void set_rightBottomX_2(float value)
	{
		___rightBottomX_2 = value;
	}

	inline static int32_t get_offset_of_rightBottomY_3() { return static_cast<int32_t>(offsetof(RectangleData_t1039179782, ___rightBottomY_3)); }
	inline float get_rightBottomY_3() const { return ___rightBottomY_3; }
	inline float* get_address_of_rightBottomY_3() { return &___rightBottomY_3; }
	inline void set_rightBottomY_3(float value)
	{
		___rightBottomY_3 = value;
	}
};
#pragma pack(pop, tp)

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RECTANGLEDATA_T1039179782_H
#ifndef WEBCAMARCONTROLLER_T3718642882_H
#define WEBCAMARCONTROLLER_T3718642882_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.WebCamARController
struct  WebCamARController_t3718642882  : public ARController_t116632334
{
public:
	// System.Int32 Vuforia.WebCamARController::RenderTextureLayer
	int32_t ___RenderTextureLayer_1;
	// System.String Vuforia.WebCamARController::mDeviceNameSetInEditor
	String_t* ___mDeviceNameSetInEditor_2;
	// System.Boolean Vuforia.WebCamARController::mFlipHorizontally
	bool ___mFlipHorizontally_3;
	// Vuforia.WebCam Vuforia.WebCamARController::mWebCamImpl
	WebCam_t2427002488 * ___mWebCamImpl_4;
	// System.Func`3<System.String,Vuforia.WebCamProfile/ProfileData,Vuforia.IWebCamTexAdaptor> Vuforia.WebCamARController::mWebCamTexAdaptorProvider
	Func_3_t3440825513 * ___mWebCamTexAdaptorProvider_5;

public:
	inline static int32_t get_offset_of_RenderTextureLayer_1() { return static_cast<int32_t>(offsetof(WebCamARController_t3718642882, ___RenderTextureLayer_1)); }
	inline int32_t get_RenderTextureLayer_1() const { return ___RenderTextureLayer_1; }
	inline int32_t* get_address_of_RenderTextureLayer_1() { return &___RenderTextureLayer_1; }
	inline void set_RenderTextureLayer_1(int32_t value)
	{
		___RenderTextureLayer_1 = value;
	}

	inline static int32_t get_offset_of_mDeviceNameSetInEditor_2() { return static_cast<int32_t>(offsetof(WebCamARController_t3718642882, ___mDeviceNameSetInEditor_2)); }
	inline String_t* get_mDeviceNameSetInEditor_2() const { return ___mDeviceNameSetInEditor_2; }
	inline String_t** get_address_of_mDeviceNameSetInEditor_2() { return &___mDeviceNameSetInEditor_2; }
	inline void set_mDeviceNameSetInEditor_2(String_t* value)
	{
		___mDeviceNameSetInEditor_2 = value;
		Il2CppCodeGenWriteBarrier((&___mDeviceNameSetInEditor_2), value);
	}

	inline static int32_t get_offset_of_mFlipHorizontally_3() { return static_cast<int32_t>(offsetof(WebCamARController_t3718642882, ___mFlipHorizontally_3)); }
	inline bool get_mFlipHorizontally_3() const { return ___mFlipHorizontally_3; }
	inline bool* get_address_of_mFlipHorizontally_3() { return &___mFlipHorizontally_3; }
	inline void set_mFlipHorizontally_3(bool value)
	{
		___mFlipHorizontally_3 = value;
	}

	inline static int32_t get_offset_of_mWebCamImpl_4() { return static_cast<int32_t>(offsetof(WebCamARController_t3718642882, ___mWebCamImpl_4)); }
	inline WebCam_t2427002488 * get_mWebCamImpl_4() const { return ___mWebCamImpl_4; }
	inline WebCam_t2427002488 ** get_address_of_mWebCamImpl_4() { return &___mWebCamImpl_4; }
	inline void set_mWebCamImpl_4(WebCam_t2427002488 * value)
	{
		___mWebCamImpl_4 = value;
		Il2CppCodeGenWriteBarrier((&___mWebCamImpl_4), value);
	}

	inline static int32_t get_offset_of_mWebCamTexAdaptorProvider_5() { return static_cast<int32_t>(offsetof(WebCamARController_t3718642882, ___mWebCamTexAdaptorProvider_5)); }
	inline Func_3_t3440825513 * get_mWebCamTexAdaptorProvider_5() const { return ___mWebCamTexAdaptorProvider_5; }
	inline Func_3_t3440825513 ** get_address_of_mWebCamTexAdaptorProvider_5() { return &___mWebCamTexAdaptorProvider_5; }
	inline void set_mWebCamTexAdaptorProvider_5(Func_3_t3440825513 * value)
	{
		___mWebCamTexAdaptorProvider_5 = value;
		Il2CppCodeGenWriteBarrier((&___mWebCamTexAdaptorProvider_5), value);
	}
};

struct WebCamARController_t3718642882_StaticFields
{
public:
	// Vuforia.WebCamARController Vuforia.WebCamARController::mInstance
	WebCamARController_t3718642882 * ___mInstance_6;
	// System.Object Vuforia.WebCamARController::mPadlock
	RuntimeObject * ___mPadlock_7;

public:
	inline static int32_t get_offset_of_mInstance_6() { return static_cast<int32_t>(offsetof(WebCamARController_t3718642882_StaticFields, ___mInstance_6)); }
	inline WebCamARController_t3718642882 * get_mInstance_6() const { return ___mInstance_6; }
	inline WebCamARController_t3718642882 ** get_address_of_mInstance_6() { return &___mInstance_6; }
	inline void set_mInstance_6(WebCamARController_t3718642882 * value)
	{
		___mInstance_6 = value;
		Il2CppCodeGenWriteBarrier((&___mInstance_6), value);
	}

	inline static int32_t get_offset_of_mPadlock_7() { return static_cast<int32_t>(offsetof(WebCamARController_t3718642882_StaticFields, ___mPadlock_7)); }
	inline RuntimeObject * get_mPadlock_7() const { return ___mPadlock_7; }
	inline RuntimeObject ** get_address_of_mPadlock_7() { return &___mPadlock_7; }
	inline void set_mPadlock_7(RuntimeObject * value)
	{
		___mPadlock_7 = value;
		Il2CppCodeGenWriteBarrier((&___mPadlock_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WEBCAMARCONTROLLER_T3718642882_H
#ifndef U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T3057255368_H
#define U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T3057255368_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>
struct  U3CPrivateImplementationDetailsU3E_t3057255368  : public RuntimeObject
{
public:

public:
};

struct U3CPrivateImplementationDetailsU3E_t3057255368_StaticFields
{
public:
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=24 <PrivateImplementationDetails>::898C2022A0C02FCE602BF05E1C09BD48301606E5
	__StaticArrayInitTypeSizeU3D24_t3517759979  ___898C2022A0C02FCE602BF05E1C09BD48301606E5_0;

public:
	inline static int32_t get_offset_of_U3898C2022A0C02FCE602BF05E1C09BD48301606E5_0() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255368_StaticFields, ___898C2022A0C02FCE602BF05E1C09BD48301606E5_0)); }
	inline __StaticArrayInitTypeSizeU3D24_t3517759979  get_U3898C2022A0C02FCE602BF05E1C09BD48301606E5_0() const { return ___898C2022A0C02FCE602BF05E1C09BD48301606E5_0; }
	inline __StaticArrayInitTypeSizeU3D24_t3517759979 * get_address_of_U3898C2022A0C02FCE602BF05E1C09BD48301606E5_0() { return &___898C2022A0C02FCE602BF05E1C09BD48301606E5_0; }
	inline void set_U3898C2022A0C02FCE602BF05E1C09BD48301606E5_0(__StaticArrayInitTypeSizeU3D24_t3517759979  value)
	{
		___898C2022A0C02FCE602BF05E1C09BD48301606E5_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T3057255368_H
#ifndef EASE_T4010715394_H
#define EASE_T4010715394_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Ease
struct  Ease_t4010715394 
{
public:
	// System.Int32 DG.Tweening.Ease::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Ease_t4010715394, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EASE_T4010715394_H
#ifndef GAMELOGICSTATE_T909530467_H
#define GAMELOGICSTATE_T909530467_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameLogicState
struct  GameLogicState_t909530467 
{
public:
	// System.Int32 GameLogicState::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(GameLogicState_t909530467, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GAMELOGICSTATE_T909530467_H
#ifndef MATERIALWCOLORHOLDER_T2420114684_H
#define MATERIALWCOLORHOLDER_T2420114684_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MaterialwColorHolder
struct  MaterialwColorHolder_t2420114684  : public RuntimeObject
{
public:
	// UnityEngine.Material MaterialwColorHolder::m_mat
	Material_t340375123 * ___m_mat_0;
	// UnityEngine.Color MaterialwColorHolder::m_color
	Color_t2555686324  ___m_color_1;

public:
	inline static int32_t get_offset_of_m_mat_0() { return static_cast<int32_t>(offsetof(MaterialwColorHolder_t2420114684, ___m_mat_0)); }
	inline Material_t340375123 * get_m_mat_0() const { return ___m_mat_0; }
	inline Material_t340375123 ** get_address_of_m_mat_0() { return &___m_mat_0; }
	inline void set_m_mat_0(Material_t340375123 * value)
	{
		___m_mat_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_mat_0), value);
	}

	inline static int32_t get_offset_of_m_color_1() { return static_cast<int32_t>(offsetof(MaterialwColorHolder_t2420114684, ___m_color_1)); }
	inline Color_t2555686324  get_m_color_1() const { return ___m_color_1; }
	inline Color_t2555686324 * get_address_of_m_color_1() { return &___m_color_1; }
	inline void set_m_color_1(Color_t2555686324  value)
	{
		___m_color_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MATERIALWCOLORHOLDER_T2420114684_H
#ifndef TYPE_T2057018713_H
#define TYPE_T2057018713_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ObjectState/TYPE
struct  TYPE_t2057018713 
{
public:
	// System.Int32 ObjectState/TYPE::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TYPE_t2057018713, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TYPE_T2057018713_H
#ifndef DELEGATE_T1188392813_H
#define DELEGATE_T1188392813_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Delegate
struct  Delegate_t1188392813  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_5;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_6;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_7;
	// System.DelegateData System.Delegate::data
	DelegateData_t1677132599 * ___data_8;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_target_2), value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_method_code_5() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_code_5)); }
	inline intptr_t get_method_code_5() const { return ___method_code_5; }
	inline intptr_t* get_address_of_method_code_5() { return &___method_code_5; }
	inline void set_method_code_5(intptr_t value)
	{
		___method_code_5 = value;
	}

	inline static int32_t get_offset_of_method_info_6() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_info_6)); }
	inline MethodInfo_t * get_method_info_6() const { return ___method_info_6; }
	inline MethodInfo_t ** get_address_of_method_info_6() { return &___method_info_6; }
	inline void set_method_info_6(MethodInfo_t * value)
	{
		___method_info_6 = value;
		Il2CppCodeGenWriteBarrier((&___method_info_6), value);
	}

	inline static int32_t get_offset_of_original_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___original_method_info_7)); }
	inline MethodInfo_t * get_original_method_info_7() const { return ___original_method_info_7; }
	inline MethodInfo_t ** get_address_of_original_method_info_7() { return &___original_method_info_7; }
	inline void set_original_method_info_7(MethodInfo_t * value)
	{
		___original_method_info_7 = value;
		Il2CppCodeGenWriteBarrier((&___original_method_info_7), value);
	}

	inline static int32_t get_offset_of_data_8() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___data_8)); }
	inline DelegateData_t1677132599 * get_data_8() const { return ___data_8; }
	inline DelegateData_t1677132599 ** get_address_of_data_8() { return &___data_8; }
	inline void set_data_8(DelegateData_t1677132599 * value)
	{
		___data_8 = value;
		Il2CppCodeGenWriteBarrier((&___data_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DELEGATE_T1188392813_H
#ifndef REQUIREMENTTYPE_T3584265503_H
#define REQUIREMENTTYPE_T3584265503_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.AnalyticsEventParam/RequirementType
struct  RequirementType_t3584265503 
{
public:
	// System.Int32 UnityEngine.Analytics.AnalyticsEventParam/RequirementType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(RequirementType_t3584265503, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REQUIREMENTTYPE_T3584265503_H
#ifndef TRIGGER_T4199345191_H
#define TRIGGER_T4199345191_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.AnalyticsTracker/Trigger
struct  Trigger_t4199345191 
{
public:
	// System.Int32 UnityEngine.Analytics.AnalyticsTracker/Trigger::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Trigger_t4199345191, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRIGGER_T4199345191_H
#ifndef TRIGGERBOOL_T501031542_H
#define TRIGGERBOOL_T501031542_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.TriggerBool
struct  TriggerBool_t501031542 
{
public:
	// System.Int32 UnityEngine.Analytics.TriggerBool::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TriggerBool_t501031542, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRIGGERBOOL_T501031542_H
#ifndef TRIGGERLIFECYCLEEVENT_T3193146760_H
#define TRIGGERLIFECYCLEEVENT_T3193146760_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.TriggerLifecycleEvent
struct  TriggerLifecycleEvent_t3193146760 
{
public:
	// System.Int32 UnityEngine.Analytics.TriggerLifecycleEvent::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TriggerLifecycleEvent_t3193146760, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRIGGERLIFECYCLEEVENT_T3193146760_H
#ifndef TRIGGEROPERATOR_T3611898925_H
#define TRIGGEROPERATOR_T3611898925_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.TriggerOperator
struct  TriggerOperator_t3611898925 
{
public:
	// System.Int32 UnityEngine.Analytics.TriggerOperator::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TriggerOperator_t3611898925, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRIGGEROPERATOR_T3611898925_H
#ifndef TRIGGERTYPE_T105272677_H
#define TRIGGERTYPE_T105272677_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.TriggerType
struct  TriggerType_t105272677 
{
public:
	// System.Int32 UnityEngine.Analytics.TriggerType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TriggerType_t105272677, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRIGGERTYPE_T105272677_H
#ifndef PROPERTYTYPE_T4040930247_H
#define PROPERTYTYPE_T4040930247_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.ValueProperty/PropertyType
struct  PropertyType_t4040930247 
{
public:
	// System.Int32 UnityEngine.Analytics.ValueProperty/PropertyType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(PropertyType_t4040930247, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROPERTYTYPE_T4040930247_H
#ifndef OBJECT_T631007953_H
#define OBJECT_T631007953_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_t631007953  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_t631007953, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_t631007953_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_t631007953_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_T631007953_H
#ifndef RAY_T3785851493_H
#define RAY_T3785851493_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Ray
struct  Ray_t3785851493 
{
public:
	// UnityEngine.Vector3 UnityEngine.Ray::m_Origin
	Vector3_t3722313464  ___m_Origin_0;
	// UnityEngine.Vector3 UnityEngine.Ray::m_Direction
	Vector3_t3722313464  ___m_Direction_1;

public:
	inline static int32_t get_offset_of_m_Origin_0() { return static_cast<int32_t>(offsetof(Ray_t3785851493, ___m_Origin_0)); }
	inline Vector3_t3722313464  get_m_Origin_0() const { return ___m_Origin_0; }
	inline Vector3_t3722313464 * get_address_of_m_Origin_0() { return &___m_Origin_0; }
	inline void set_m_Origin_0(Vector3_t3722313464  value)
	{
		___m_Origin_0 = value;
	}

	inline static int32_t get_offset_of_m_Direction_1() { return static_cast<int32_t>(offsetof(Ray_t3785851493, ___m_Direction_1)); }
	inline Vector3_t3722313464  get_m_Direction_1() const { return ___m_Direction_1; }
	inline Vector3_t3722313464 * get_address_of_m_Direction_1() { return &___m_Direction_1; }
	inline void set_m_Direction_1(Vector3_t3722313464  value)
	{
		___m_Direction_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RAY_T3785851493_H
#ifndef RAYCASTHIT_T1056001966_H
#define RAYCASTHIT_T1056001966_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.RaycastHit
struct  RaycastHit_t1056001966 
{
public:
	// UnityEngine.Vector3 UnityEngine.RaycastHit::m_Point
	Vector3_t3722313464  ___m_Point_0;
	// UnityEngine.Vector3 UnityEngine.RaycastHit::m_Normal
	Vector3_t3722313464  ___m_Normal_1;
	// System.UInt32 UnityEngine.RaycastHit::m_FaceID
	uint32_t ___m_FaceID_2;
	// System.Single UnityEngine.RaycastHit::m_Distance
	float ___m_Distance_3;
	// UnityEngine.Vector2 UnityEngine.RaycastHit::m_UV
	Vector2_t2156229523  ___m_UV_4;
	// System.Int32 UnityEngine.RaycastHit::m_Collider
	int32_t ___m_Collider_5;

public:
	inline static int32_t get_offset_of_m_Point_0() { return static_cast<int32_t>(offsetof(RaycastHit_t1056001966, ___m_Point_0)); }
	inline Vector3_t3722313464  get_m_Point_0() const { return ___m_Point_0; }
	inline Vector3_t3722313464 * get_address_of_m_Point_0() { return &___m_Point_0; }
	inline void set_m_Point_0(Vector3_t3722313464  value)
	{
		___m_Point_0 = value;
	}

	inline static int32_t get_offset_of_m_Normal_1() { return static_cast<int32_t>(offsetof(RaycastHit_t1056001966, ___m_Normal_1)); }
	inline Vector3_t3722313464  get_m_Normal_1() const { return ___m_Normal_1; }
	inline Vector3_t3722313464 * get_address_of_m_Normal_1() { return &___m_Normal_1; }
	inline void set_m_Normal_1(Vector3_t3722313464  value)
	{
		___m_Normal_1 = value;
	}

	inline static int32_t get_offset_of_m_FaceID_2() { return static_cast<int32_t>(offsetof(RaycastHit_t1056001966, ___m_FaceID_2)); }
	inline uint32_t get_m_FaceID_2() const { return ___m_FaceID_2; }
	inline uint32_t* get_address_of_m_FaceID_2() { return &___m_FaceID_2; }
	inline void set_m_FaceID_2(uint32_t value)
	{
		___m_FaceID_2 = value;
	}

	inline static int32_t get_offset_of_m_Distance_3() { return static_cast<int32_t>(offsetof(RaycastHit_t1056001966, ___m_Distance_3)); }
	inline float get_m_Distance_3() const { return ___m_Distance_3; }
	inline float* get_address_of_m_Distance_3() { return &___m_Distance_3; }
	inline void set_m_Distance_3(float value)
	{
		___m_Distance_3 = value;
	}

	inline static int32_t get_offset_of_m_UV_4() { return static_cast<int32_t>(offsetof(RaycastHit_t1056001966, ___m_UV_4)); }
	inline Vector2_t2156229523  get_m_UV_4() const { return ___m_UV_4; }
	inline Vector2_t2156229523 * get_address_of_m_UV_4() { return &___m_UV_4; }
	inline void set_m_UV_4(Vector2_t2156229523  value)
	{
		___m_UV_4 = value;
	}

	inline static int32_t get_offset_of_m_Collider_5() { return static_cast<int32_t>(offsetof(RaycastHit_t1056001966, ___m_Collider_5)); }
	inline int32_t get_m_Collider_5() const { return ___m_Collider_5; }
	inline int32_t* get_address_of_m_Collider_5() { return &___m_Collider_5; }
	inline void set_m_Collider_5(int32_t value)
	{
		___m_Collider_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RAYCASTHIT_T1056001966_H
#ifndef CLIPPING_MODE_T2655398006_H
#define CLIPPING_MODE_T2655398006_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.HideExcessAreaUtility/CLIPPING_MODE
struct  CLIPPING_MODE_t2655398006 
{
public:
	// System.Int32 Vuforia.HideExcessAreaUtility/CLIPPING_MODE::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(CLIPPING_MODE_t2655398006, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CLIPPING_MODE_T2655398006_H
#ifndef FRAMEQUALITY_T46289180_H
#define FRAMEQUALITY_T46289180_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.ImageTargetBuilder/FrameQuality
struct  FrameQuality_t46289180 
{
public:
	// System.Int32 Vuforia.ImageTargetBuilder/FrameQuality::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(FrameQuality_t46289180, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FRAMEQUALITY_T46289180_H
#ifndef STATUS_T1100905814_H
#define STATUS_T1100905814_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.TrackableBehaviour/Status
struct  Status_t1100905814 
{
public:
	// System.Int32 Vuforia.TrackableBehaviour/Status::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Status_t1100905814, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STATUS_T1100905814_H
#ifndef TRACKABLESOURCE_T2567074243_H
#define TRACKABLESOURCE_T2567074243_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.TrackableSource
struct  TrackableSource_t2567074243  : public RuntimeObject
{
public:
	// System.IntPtr Vuforia.TrackableSource::<TrackableSourcePtr>k__BackingField
	intptr_t ___U3CTrackableSourcePtrU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CTrackableSourcePtrU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(TrackableSource_t2567074243, ___U3CTrackableSourcePtrU3Ek__BackingField_0)); }
	inline intptr_t get_U3CTrackableSourcePtrU3Ek__BackingField_0() const { return ___U3CTrackableSourcePtrU3Ek__BackingField_0; }
	inline intptr_t* get_address_of_U3CTrackableSourcePtrU3Ek__BackingField_0() { return &___U3CTrackableSourcePtrU3Ek__BackingField_0; }
	inline void set_U3CTrackableSourcePtrU3Ek__BackingField_0(intptr_t value)
	{
		___U3CTrackableSourcePtrU3Ek__BackingField_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRACKABLESOURCE_T2567074243_H
#ifndef SENSITIVITY_T3045829715_H
#define SENSITIVITY_T3045829715_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.VirtualButton/Sensitivity
struct  Sensitivity_t3045829715 
{
public:
	// System.Int32 Vuforia.VirtualButton/Sensitivity::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Sensitivity_t3045829715, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SENSITIVITY_T3045829715_H
#ifndef MULTICASTDELEGATE_T_H
#define MULTICASTDELEGATE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MulticastDelegate
struct  MulticastDelegate_t  : public Delegate_t1188392813
{
public:
	// System.MulticastDelegate System.MulticastDelegate::prev
	MulticastDelegate_t * ___prev_9;
	// System.MulticastDelegate System.MulticastDelegate::kpm_next
	MulticastDelegate_t * ___kpm_next_10;

public:
	inline static int32_t get_offset_of_prev_9() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___prev_9)); }
	inline MulticastDelegate_t * get_prev_9() const { return ___prev_9; }
	inline MulticastDelegate_t ** get_address_of_prev_9() { return &___prev_9; }
	inline void set_prev_9(MulticastDelegate_t * value)
	{
		___prev_9 = value;
		Il2CppCodeGenWriteBarrier((&___prev_9), value);
	}

	inline static int32_t get_offset_of_kpm_next_10() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___kpm_next_10)); }
	inline MulticastDelegate_t * get_kpm_next_10() const { return ___kpm_next_10; }
	inline MulticastDelegate_t ** get_address_of_kpm_next_10() { return &___kpm_next_10; }
	inline void set_kpm_next_10(MulticastDelegate_t * value)
	{
		___kpm_next_10 = value;
		Il2CppCodeGenWriteBarrier((&___kpm_next_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MULTICASTDELEGATE_T_H
#ifndef ANALYTICSEVENTPARAM_T2480121928_H
#define ANALYTICSEVENTPARAM_T2480121928_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.AnalyticsEventParam
struct  AnalyticsEventParam_t2480121928  : public RuntimeObject
{
public:
	// UnityEngine.Analytics.AnalyticsEventParam/RequirementType UnityEngine.Analytics.AnalyticsEventParam::m_RequirementType
	int32_t ___m_RequirementType_0;
	// System.String UnityEngine.Analytics.AnalyticsEventParam::m_GroupID
	String_t* ___m_GroupID_1;
	// System.String UnityEngine.Analytics.AnalyticsEventParam::m_Tooltip
	String_t* ___m_Tooltip_2;
	// System.String UnityEngine.Analytics.AnalyticsEventParam::m_Name
	String_t* ___m_Name_3;
	// UnityEngine.Analytics.ValueProperty UnityEngine.Analytics.AnalyticsEventParam::m_Value
	ValueProperty_t1868393739 * ___m_Value_4;

public:
	inline static int32_t get_offset_of_m_RequirementType_0() { return static_cast<int32_t>(offsetof(AnalyticsEventParam_t2480121928, ___m_RequirementType_0)); }
	inline int32_t get_m_RequirementType_0() const { return ___m_RequirementType_0; }
	inline int32_t* get_address_of_m_RequirementType_0() { return &___m_RequirementType_0; }
	inline void set_m_RequirementType_0(int32_t value)
	{
		___m_RequirementType_0 = value;
	}

	inline static int32_t get_offset_of_m_GroupID_1() { return static_cast<int32_t>(offsetof(AnalyticsEventParam_t2480121928, ___m_GroupID_1)); }
	inline String_t* get_m_GroupID_1() const { return ___m_GroupID_1; }
	inline String_t** get_address_of_m_GroupID_1() { return &___m_GroupID_1; }
	inline void set_m_GroupID_1(String_t* value)
	{
		___m_GroupID_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_GroupID_1), value);
	}

	inline static int32_t get_offset_of_m_Tooltip_2() { return static_cast<int32_t>(offsetof(AnalyticsEventParam_t2480121928, ___m_Tooltip_2)); }
	inline String_t* get_m_Tooltip_2() const { return ___m_Tooltip_2; }
	inline String_t** get_address_of_m_Tooltip_2() { return &___m_Tooltip_2; }
	inline void set_m_Tooltip_2(String_t* value)
	{
		___m_Tooltip_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_Tooltip_2), value);
	}

	inline static int32_t get_offset_of_m_Name_3() { return static_cast<int32_t>(offsetof(AnalyticsEventParam_t2480121928, ___m_Name_3)); }
	inline String_t* get_m_Name_3() const { return ___m_Name_3; }
	inline String_t** get_address_of_m_Name_3() { return &___m_Name_3; }
	inline void set_m_Name_3(String_t* value)
	{
		___m_Name_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_Name_3), value);
	}

	inline static int32_t get_offset_of_m_Value_4() { return static_cast<int32_t>(offsetof(AnalyticsEventParam_t2480121928, ___m_Value_4)); }
	inline ValueProperty_t1868393739 * get_m_Value_4() const { return ___m_Value_4; }
	inline ValueProperty_t1868393739 ** get_address_of_m_Value_4() { return &___m_Value_4; }
	inline void set_m_Value_4(ValueProperty_t1868393739 * value)
	{
		___m_Value_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_Value_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANALYTICSEVENTPARAM_T2480121928_H
#ifndef EVENTTRIGGER_T2527451695_H
#define EVENTTRIGGER_T2527451695_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.EventTrigger
struct  EventTrigger_t2527451695  : public RuntimeObject
{
public:
	// System.Boolean UnityEngine.Analytics.EventTrigger::m_IsTriggerExpanded
	bool ___m_IsTriggerExpanded_0;
	// UnityEngine.Analytics.TriggerType UnityEngine.Analytics.EventTrigger::m_Type
	int32_t ___m_Type_1;
	// UnityEngine.Analytics.TriggerLifecycleEvent UnityEngine.Analytics.EventTrigger::m_LifecycleEvent
	int32_t ___m_LifecycleEvent_2;
	// System.Boolean UnityEngine.Analytics.EventTrigger::m_ApplyRules
	bool ___m_ApplyRules_3;
	// UnityEngine.Analytics.TriggerListContainer UnityEngine.Analytics.EventTrigger::m_Rules
	TriggerListContainer_t2032715483 * ___m_Rules_4;
	// UnityEngine.Analytics.TriggerBool UnityEngine.Analytics.EventTrigger::m_TriggerBool
	int32_t ___m_TriggerBool_5;
	// System.Single UnityEngine.Analytics.EventTrigger::m_InitTime
	float ___m_InitTime_6;
	// System.Single UnityEngine.Analytics.EventTrigger::m_RepeatTime
	float ___m_RepeatTime_7;
	// System.Int32 UnityEngine.Analytics.EventTrigger::m_Repetitions
	int32_t ___m_Repetitions_8;
	// System.Int32 UnityEngine.Analytics.EventTrigger::repetitionCount
	int32_t ___repetitionCount_9;
	// UnityEngine.Analytics.EventTrigger/OnTrigger UnityEngine.Analytics.EventTrigger::m_TriggerFunction
	OnTrigger_t4184125570 * ___m_TriggerFunction_10;
	// UnityEngine.Analytics.TriggerMethod UnityEngine.Analytics.EventTrigger::m_Method
	TriggerMethod_t582536534 * ___m_Method_11;

public:
	inline static int32_t get_offset_of_m_IsTriggerExpanded_0() { return static_cast<int32_t>(offsetof(EventTrigger_t2527451695, ___m_IsTriggerExpanded_0)); }
	inline bool get_m_IsTriggerExpanded_0() const { return ___m_IsTriggerExpanded_0; }
	inline bool* get_address_of_m_IsTriggerExpanded_0() { return &___m_IsTriggerExpanded_0; }
	inline void set_m_IsTriggerExpanded_0(bool value)
	{
		___m_IsTriggerExpanded_0 = value;
	}

	inline static int32_t get_offset_of_m_Type_1() { return static_cast<int32_t>(offsetof(EventTrigger_t2527451695, ___m_Type_1)); }
	inline int32_t get_m_Type_1() const { return ___m_Type_1; }
	inline int32_t* get_address_of_m_Type_1() { return &___m_Type_1; }
	inline void set_m_Type_1(int32_t value)
	{
		___m_Type_1 = value;
	}

	inline static int32_t get_offset_of_m_LifecycleEvent_2() { return static_cast<int32_t>(offsetof(EventTrigger_t2527451695, ___m_LifecycleEvent_2)); }
	inline int32_t get_m_LifecycleEvent_2() const { return ___m_LifecycleEvent_2; }
	inline int32_t* get_address_of_m_LifecycleEvent_2() { return &___m_LifecycleEvent_2; }
	inline void set_m_LifecycleEvent_2(int32_t value)
	{
		___m_LifecycleEvent_2 = value;
	}

	inline static int32_t get_offset_of_m_ApplyRules_3() { return static_cast<int32_t>(offsetof(EventTrigger_t2527451695, ___m_ApplyRules_3)); }
	inline bool get_m_ApplyRules_3() const { return ___m_ApplyRules_3; }
	inline bool* get_address_of_m_ApplyRules_3() { return &___m_ApplyRules_3; }
	inline void set_m_ApplyRules_3(bool value)
	{
		___m_ApplyRules_3 = value;
	}

	inline static int32_t get_offset_of_m_Rules_4() { return static_cast<int32_t>(offsetof(EventTrigger_t2527451695, ___m_Rules_4)); }
	inline TriggerListContainer_t2032715483 * get_m_Rules_4() const { return ___m_Rules_4; }
	inline TriggerListContainer_t2032715483 ** get_address_of_m_Rules_4() { return &___m_Rules_4; }
	inline void set_m_Rules_4(TriggerListContainer_t2032715483 * value)
	{
		___m_Rules_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_Rules_4), value);
	}

	inline static int32_t get_offset_of_m_TriggerBool_5() { return static_cast<int32_t>(offsetof(EventTrigger_t2527451695, ___m_TriggerBool_5)); }
	inline int32_t get_m_TriggerBool_5() const { return ___m_TriggerBool_5; }
	inline int32_t* get_address_of_m_TriggerBool_5() { return &___m_TriggerBool_5; }
	inline void set_m_TriggerBool_5(int32_t value)
	{
		___m_TriggerBool_5 = value;
	}

	inline static int32_t get_offset_of_m_InitTime_6() { return static_cast<int32_t>(offsetof(EventTrigger_t2527451695, ___m_InitTime_6)); }
	inline float get_m_InitTime_6() const { return ___m_InitTime_6; }
	inline float* get_address_of_m_InitTime_6() { return &___m_InitTime_6; }
	inline void set_m_InitTime_6(float value)
	{
		___m_InitTime_6 = value;
	}

	inline static int32_t get_offset_of_m_RepeatTime_7() { return static_cast<int32_t>(offsetof(EventTrigger_t2527451695, ___m_RepeatTime_7)); }
	inline float get_m_RepeatTime_7() const { return ___m_RepeatTime_7; }
	inline float* get_address_of_m_RepeatTime_7() { return &___m_RepeatTime_7; }
	inline void set_m_RepeatTime_7(float value)
	{
		___m_RepeatTime_7 = value;
	}

	inline static int32_t get_offset_of_m_Repetitions_8() { return static_cast<int32_t>(offsetof(EventTrigger_t2527451695, ___m_Repetitions_8)); }
	inline int32_t get_m_Repetitions_8() const { return ___m_Repetitions_8; }
	inline int32_t* get_address_of_m_Repetitions_8() { return &___m_Repetitions_8; }
	inline void set_m_Repetitions_8(int32_t value)
	{
		___m_Repetitions_8 = value;
	}

	inline static int32_t get_offset_of_repetitionCount_9() { return static_cast<int32_t>(offsetof(EventTrigger_t2527451695, ___repetitionCount_9)); }
	inline int32_t get_repetitionCount_9() const { return ___repetitionCount_9; }
	inline int32_t* get_address_of_repetitionCount_9() { return &___repetitionCount_9; }
	inline void set_repetitionCount_9(int32_t value)
	{
		___repetitionCount_9 = value;
	}

	inline static int32_t get_offset_of_m_TriggerFunction_10() { return static_cast<int32_t>(offsetof(EventTrigger_t2527451695, ___m_TriggerFunction_10)); }
	inline OnTrigger_t4184125570 * get_m_TriggerFunction_10() const { return ___m_TriggerFunction_10; }
	inline OnTrigger_t4184125570 ** get_address_of_m_TriggerFunction_10() { return &___m_TriggerFunction_10; }
	inline void set_m_TriggerFunction_10(OnTrigger_t4184125570 * value)
	{
		___m_TriggerFunction_10 = value;
		Il2CppCodeGenWriteBarrier((&___m_TriggerFunction_10), value);
	}

	inline static int32_t get_offset_of_m_Method_11() { return static_cast<int32_t>(offsetof(EventTrigger_t2527451695, ___m_Method_11)); }
	inline TriggerMethod_t582536534 * get_m_Method_11() const { return ___m_Method_11; }
	inline TriggerMethod_t582536534 ** get_address_of_m_Method_11() { return &___m_Method_11; }
	inline void set_m_Method_11(TriggerMethod_t582536534 * value)
	{
		___m_Method_11 = value;
		Il2CppCodeGenWriteBarrier((&___m_Method_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EVENTTRIGGER_T2527451695_H
#ifndef TRIGGERRULE_T1946298321_H
#define TRIGGERRULE_T1946298321_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.TriggerRule
struct  TriggerRule_t1946298321  : public RuntimeObject
{
public:
	// UnityEngine.Analytics.TrackableField UnityEngine.Analytics.TriggerRule::m_Target
	TrackableField_t1772682203 * ___m_Target_0;
	// UnityEngine.Analytics.TriggerOperator UnityEngine.Analytics.TriggerRule::m_Operator
	int32_t ___m_Operator_1;
	// UnityEngine.Analytics.ValueProperty UnityEngine.Analytics.TriggerRule::m_Value
	ValueProperty_t1868393739 * ___m_Value_2;
	// UnityEngine.Analytics.ValueProperty UnityEngine.Analytics.TriggerRule::m_Value2
	ValueProperty_t1868393739 * ___m_Value2_3;

public:
	inline static int32_t get_offset_of_m_Target_0() { return static_cast<int32_t>(offsetof(TriggerRule_t1946298321, ___m_Target_0)); }
	inline TrackableField_t1772682203 * get_m_Target_0() const { return ___m_Target_0; }
	inline TrackableField_t1772682203 ** get_address_of_m_Target_0() { return &___m_Target_0; }
	inline void set_m_Target_0(TrackableField_t1772682203 * value)
	{
		___m_Target_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Target_0), value);
	}

	inline static int32_t get_offset_of_m_Operator_1() { return static_cast<int32_t>(offsetof(TriggerRule_t1946298321, ___m_Operator_1)); }
	inline int32_t get_m_Operator_1() const { return ___m_Operator_1; }
	inline int32_t* get_address_of_m_Operator_1() { return &___m_Operator_1; }
	inline void set_m_Operator_1(int32_t value)
	{
		___m_Operator_1 = value;
	}

	inline static int32_t get_offset_of_m_Value_2() { return static_cast<int32_t>(offsetof(TriggerRule_t1946298321, ___m_Value_2)); }
	inline ValueProperty_t1868393739 * get_m_Value_2() const { return ___m_Value_2; }
	inline ValueProperty_t1868393739 ** get_address_of_m_Value_2() { return &___m_Value_2; }
	inline void set_m_Value_2(ValueProperty_t1868393739 * value)
	{
		___m_Value_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_Value_2), value);
	}

	inline static int32_t get_offset_of_m_Value2_3() { return static_cast<int32_t>(offsetof(TriggerRule_t1946298321, ___m_Value2_3)); }
	inline ValueProperty_t1868393739 * get_m_Value2_3() const { return ___m_Value2_3; }
	inline ValueProperty_t1868393739 ** get_address_of_m_Value2_3() { return &___m_Value2_3; }
	inline void set_m_Value2_3(ValueProperty_t1868393739 * value)
	{
		___m_Value2_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_Value2_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRIGGERRULE_T1946298321_H
#ifndef VALUEPROPERTY_T1868393739_H
#define VALUEPROPERTY_T1868393739_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.ValueProperty
struct  ValueProperty_t1868393739  : public RuntimeObject
{
public:
	// System.Boolean UnityEngine.Analytics.ValueProperty::m_EditingCustomValue
	bool ___m_EditingCustomValue_0;
	// System.Int32 UnityEngine.Analytics.ValueProperty::m_PopupIndex
	int32_t ___m_PopupIndex_1;
	// System.String UnityEngine.Analytics.ValueProperty::m_CustomValue
	String_t* ___m_CustomValue_2;
	// System.Boolean UnityEngine.Analytics.ValueProperty::m_FixedType
	bool ___m_FixedType_3;
	// System.String UnityEngine.Analytics.ValueProperty::m_EnumType
	String_t* ___m_EnumType_4;
	// System.Boolean UnityEngine.Analytics.ValueProperty::m_EnumTypeIsCustomizable
	bool ___m_EnumTypeIsCustomizable_5;
	// System.Boolean UnityEngine.Analytics.ValueProperty::m_CanDisable
	bool ___m_CanDisable_6;
	// UnityEngine.Analytics.ValueProperty/PropertyType UnityEngine.Analytics.ValueProperty::m_PropertyType
	int32_t ___m_PropertyType_7;
	// System.String UnityEngine.Analytics.ValueProperty::m_ValueType
	String_t* ___m_ValueType_8;
	// System.String UnityEngine.Analytics.ValueProperty::m_Value
	String_t* ___m_Value_9;
	// UnityEngine.Analytics.TrackableField UnityEngine.Analytics.ValueProperty::m_Target
	TrackableField_t1772682203 * ___m_Target_10;

public:
	inline static int32_t get_offset_of_m_EditingCustomValue_0() { return static_cast<int32_t>(offsetof(ValueProperty_t1868393739, ___m_EditingCustomValue_0)); }
	inline bool get_m_EditingCustomValue_0() const { return ___m_EditingCustomValue_0; }
	inline bool* get_address_of_m_EditingCustomValue_0() { return &___m_EditingCustomValue_0; }
	inline void set_m_EditingCustomValue_0(bool value)
	{
		___m_EditingCustomValue_0 = value;
	}

	inline static int32_t get_offset_of_m_PopupIndex_1() { return static_cast<int32_t>(offsetof(ValueProperty_t1868393739, ___m_PopupIndex_1)); }
	inline int32_t get_m_PopupIndex_1() const { return ___m_PopupIndex_1; }
	inline int32_t* get_address_of_m_PopupIndex_1() { return &___m_PopupIndex_1; }
	inline void set_m_PopupIndex_1(int32_t value)
	{
		___m_PopupIndex_1 = value;
	}

	inline static int32_t get_offset_of_m_CustomValue_2() { return static_cast<int32_t>(offsetof(ValueProperty_t1868393739, ___m_CustomValue_2)); }
	inline String_t* get_m_CustomValue_2() const { return ___m_CustomValue_2; }
	inline String_t** get_address_of_m_CustomValue_2() { return &___m_CustomValue_2; }
	inline void set_m_CustomValue_2(String_t* value)
	{
		___m_CustomValue_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_CustomValue_2), value);
	}

	inline static int32_t get_offset_of_m_FixedType_3() { return static_cast<int32_t>(offsetof(ValueProperty_t1868393739, ___m_FixedType_3)); }
	inline bool get_m_FixedType_3() const { return ___m_FixedType_3; }
	inline bool* get_address_of_m_FixedType_3() { return &___m_FixedType_3; }
	inline void set_m_FixedType_3(bool value)
	{
		___m_FixedType_3 = value;
	}

	inline static int32_t get_offset_of_m_EnumType_4() { return static_cast<int32_t>(offsetof(ValueProperty_t1868393739, ___m_EnumType_4)); }
	inline String_t* get_m_EnumType_4() const { return ___m_EnumType_4; }
	inline String_t** get_address_of_m_EnumType_4() { return &___m_EnumType_4; }
	inline void set_m_EnumType_4(String_t* value)
	{
		___m_EnumType_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_EnumType_4), value);
	}

	inline static int32_t get_offset_of_m_EnumTypeIsCustomizable_5() { return static_cast<int32_t>(offsetof(ValueProperty_t1868393739, ___m_EnumTypeIsCustomizable_5)); }
	inline bool get_m_EnumTypeIsCustomizable_5() const { return ___m_EnumTypeIsCustomizable_5; }
	inline bool* get_address_of_m_EnumTypeIsCustomizable_5() { return &___m_EnumTypeIsCustomizable_5; }
	inline void set_m_EnumTypeIsCustomizable_5(bool value)
	{
		___m_EnumTypeIsCustomizable_5 = value;
	}

	inline static int32_t get_offset_of_m_CanDisable_6() { return static_cast<int32_t>(offsetof(ValueProperty_t1868393739, ___m_CanDisable_6)); }
	inline bool get_m_CanDisable_6() const { return ___m_CanDisable_6; }
	inline bool* get_address_of_m_CanDisable_6() { return &___m_CanDisable_6; }
	inline void set_m_CanDisable_6(bool value)
	{
		___m_CanDisable_6 = value;
	}

	inline static int32_t get_offset_of_m_PropertyType_7() { return static_cast<int32_t>(offsetof(ValueProperty_t1868393739, ___m_PropertyType_7)); }
	inline int32_t get_m_PropertyType_7() const { return ___m_PropertyType_7; }
	inline int32_t* get_address_of_m_PropertyType_7() { return &___m_PropertyType_7; }
	inline void set_m_PropertyType_7(int32_t value)
	{
		___m_PropertyType_7 = value;
	}

	inline static int32_t get_offset_of_m_ValueType_8() { return static_cast<int32_t>(offsetof(ValueProperty_t1868393739, ___m_ValueType_8)); }
	inline String_t* get_m_ValueType_8() const { return ___m_ValueType_8; }
	inline String_t** get_address_of_m_ValueType_8() { return &___m_ValueType_8; }
	inline void set_m_ValueType_8(String_t* value)
	{
		___m_ValueType_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_ValueType_8), value);
	}

	inline static int32_t get_offset_of_m_Value_9() { return static_cast<int32_t>(offsetof(ValueProperty_t1868393739, ___m_Value_9)); }
	inline String_t* get_m_Value_9() const { return ___m_Value_9; }
	inline String_t** get_address_of_m_Value_9() { return &___m_Value_9; }
	inline void set_m_Value_9(String_t* value)
	{
		___m_Value_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_Value_9), value);
	}

	inline static int32_t get_offset_of_m_Target_10() { return static_cast<int32_t>(offsetof(ValueProperty_t1868393739, ___m_Target_10)); }
	inline TrackableField_t1772682203 * get_m_Target_10() const { return ___m_Target_10; }
	inline TrackableField_t1772682203 ** get_address_of_m_Target_10() { return &___m_Target_10; }
	inline void set_m_Target_10(TrackableField_t1772682203 * value)
	{
		___m_Target_10 = value;
		Il2CppCodeGenWriteBarrier((&___m_Target_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VALUEPROPERTY_T1868393739_H
#ifndef COMPONENT_T1923634451_H
#define COMPONENT_T1923634451_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t1923634451  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T1923634451_H
#ifndef SCRIPTABLEOBJECT_T2528358522_H
#define SCRIPTABLEOBJECT_T2528358522_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.ScriptableObject
struct  ScriptableObject_t2528358522  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_t2528358522_marshaled_pinvoke : public Object_t631007953_marshaled_pinvoke
{
};
// Native definition for COM marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_t2528358522_marshaled_com : public Object_t631007953_marshaled_com
{
};
#endif // SCRIPTABLEOBJECT_T2528358522_H
#ifndef VIDEOBACKGROUNDMANAGER_T2198727358_H
#define VIDEOBACKGROUNDMANAGER_T2198727358_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.VideoBackgroundManager
struct  VideoBackgroundManager_t2198727358  : public ARController_t116632334
{
public:
	// Vuforia.HideExcessAreaUtility/CLIPPING_MODE Vuforia.VideoBackgroundManager::mClippingMode
	int32_t ___mClippingMode_1;
	// UnityEngine.Shader Vuforia.VideoBackgroundManager::mMatteShader
	Shader_t4151988712 * ___mMatteShader_2;
	// System.Boolean Vuforia.VideoBackgroundManager::mVideoBackgroundEnabled
	bool ___mVideoBackgroundEnabled_3;
	// UnityEngine.Texture Vuforia.VideoBackgroundManager::mTexture
	Texture_t3661962703 * ___mTexture_4;
	// System.Boolean Vuforia.VideoBackgroundManager::mVideoBgConfigChanged
	bool ___mVideoBgConfigChanged_5;
	// System.IntPtr Vuforia.VideoBackgroundManager::mNativeTexturePtr
	intptr_t ___mNativeTexturePtr_6;

public:
	inline static int32_t get_offset_of_mClippingMode_1() { return static_cast<int32_t>(offsetof(VideoBackgroundManager_t2198727358, ___mClippingMode_1)); }
	inline int32_t get_mClippingMode_1() const { return ___mClippingMode_1; }
	inline int32_t* get_address_of_mClippingMode_1() { return &___mClippingMode_1; }
	inline void set_mClippingMode_1(int32_t value)
	{
		___mClippingMode_1 = value;
	}

	inline static int32_t get_offset_of_mMatteShader_2() { return static_cast<int32_t>(offsetof(VideoBackgroundManager_t2198727358, ___mMatteShader_2)); }
	inline Shader_t4151988712 * get_mMatteShader_2() const { return ___mMatteShader_2; }
	inline Shader_t4151988712 ** get_address_of_mMatteShader_2() { return &___mMatteShader_2; }
	inline void set_mMatteShader_2(Shader_t4151988712 * value)
	{
		___mMatteShader_2 = value;
		Il2CppCodeGenWriteBarrier((&___mMatteShader_2), value);
	}

	inline static int32_t get_offset_of_mVideoBackgroundEnabled_3() { return static_cast<int32_t>(offsetof(VideoBackgroundManager_t2198727358, ___mVideoBackgroundEnabled_3)); }
	inline bool get_mVideoBackgroundEnabled_3() const { return ___mVideoBackgroundEnabled_3; }
	inline bool* get_address_of_mVideoBackgroundEnabled_3() { return &___mVideoBackgroundEnabled_3; }
	inline void set_mVideoBackgroundEnabled_3(bool value)
	{
		___mVideoBackgroundEnabled_3 = value;
	}

	inline static int32_t get_offset_of_mTexture_4() { return static_cast<int32_t>(offsetof(VideoBackgroundManager_t2198727358, ___mTexture_4)); }
	inline Texture_t3661962703 * get_mTexture_4() const { return ___mTexture_4; }
	inline Texture_t3661962703 ** get_address_of_mTexture_4() { return &___mTexture_4; }
	inline void set_mTexture_4(Texture_t3661962703 * value)
	{
		___mTexture_4 = value;
		Il2CppCodeGenWriteBarrier((&___mTexture_4), value);
	}

	inline static int32_t get_offset_of_mVideoBgConfigChanged_5() { return static_cast<int32_t>(offsetof(VideoBackgroundManager_t2198727358, ___mVideoBgConfigChanged_5)); }
	inline bool get_mVideoBgConfigChanged_5() const { return ___mVideoBgConfigChanged_5; }
	inline bool* get_address_of_mVideoBgConfigChanged_5() { return &___mVideoBgConfigChanged_5; }
	inline void set_mVideoBgConfigChanged_5(bool value)
	{
		___mVideoBgConfigChanged_5 = value;
	}

	inline static int32_t get_offset_of_mNativeTexturePtr_6() { return static_cast<int32_t>(offsetof(VideoBackgroundManager_t2198727358, ___mNativeTexturePtr_6)); }
	inline intptr_t get_mNativeTexturePtr_6() const { return ___mNativeTexturePtr_6; }
	inline intptr_t* get_address_of_mNativeTexturePtr_6() { return &___mNativeTexturePtr_6; }
	inline void set_mNativeTexturePtr_6(intptr_t value)
	{
		___mNativeTexturePtr_6 = value;
	}
};

struct VideoBackgroundManager_t2198727358_StaticFields
{
public:
	// Vuforia.VideoBackgroundManager Vuforia.VideoBackgroundManager::mInstance
	VideoBackgroundManager_t2198727358 * ___mInstance_7;
	// System.Object Vuforia.VideoBackgroundManager::mPadlock
	RuntimeObject * ___mPadlock_8;

public:
	inline static int32_t get_offset_of_mInstance_7() { return static_cast<int32_t>(offsetof(VideoBackgroundManager_t2198727358_StaticFields, ___mInstance_7)); }
	inline VideoBackgroundManager_t2198727358 * get_mInstance_7() const { return ___mInstance_7; }
	inline VideoBackgroundManager_t2198727358 ** get_address_of_mInstance_7() { return &___mInstance_7; }
	inline void set_mInstance_7(VideoBackgroundManager_t2198727358 * value)
	{
		___mInstance_7 = value;
		Il2CppCodeGenWriteBarrier((&___mInstance_7), value);
	}

	inline static int32_t get_offset_of_mPadlock_8() { return static_cast<int32_t>(offsetof(VideoBackgroundManager_t2198727358_StaticFields, ___mPadlock_8)); }
	inline RuntimeObject * get_mPadlock_8() const { return ___mPadlock_8; }
	inline RuntimeObject ** get_address_of_mPadlock_8() { return &___mPadlock_8; }
	inline void set_mPadlock_8(RuntimeObject * value)
	{
		___mPadlock_8 = value;
		Il2CppCodeGenWriteBarrier((&___mPadlock_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VIDEOBACKGROUNDMANAGER_T2198727358_H
#ifndef VIRTUALBUTTON_T386166510_H
#define VIRTUALBUTTON_T386166510_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.VirtualButton
struct  VirtualButton_t386166510  : public RuntimeObject
{
public:
	// System.String Vuforia.VirtualButton::mName
	String_t* ___mName_0;
	// System.Int32 Vuforia.VirtualButton::mID
	int32_t ___mID_1;
	// Vuforia.RectangleData Vuforia.VirtualButton::mArea
	RectangleData_t1039179782  ___mArea_2;
	// System.Boolean Vuforia.VirtualButton::mIsEnabled
	bool ___mIsEnabled_3;
	// Vuforia.ImageTarget Vuforia.VirtualButton::mParentImageTarget
	RuntimeObject* ___mParentImageTarget_4;
	// Vuforia.DataSet Vuforia.VirtualButton::mParentDataSet
	DataSet_t3286034874 * ___mParentDataSet_5;

public:
	inline static int32_t get_offset_of_mName_0() { return static_cast<int32_t>(offsetof(VirtualButton_t386166510, ___mName_0)); }
	inline String_t* get_mName_0() const { return ___mName_0; }
	inline String_t** get_address_of_mName_0() { return &___mName_0; }
	inline void set_mName_0(String_t* value)
	{
		___mName_0 = value;
		Il2CppCodeGenWriteBarrier((&___mName_0), value);
	}

	inline static int32_t get_offset_of_mID_1() { return static_cast<int32_t>(offsetof(VirtualButton_t386166510, ___mID_1)); }
	inline int32_t get_mID_1() const { return ___mID_1; }
	inline int32_t* get_address_of_mID_1() { return &___mID_1; }
	inline void set_mID_1(int32_t value)
	{
		___mID_1 = value;
	}

	inline static int32_t get_offset_of_mArea_2() { return static_cast<int32_t>(offsetof(VirtualButton_t386166510, ___mArea_2)); }
	inline RectangleData_t1039179782  get_mArea_2() const { return ___mArea_2; }
	inline RectangleData_t1039179782 * get_address_of_mArea_2() { return &___mArea_2; }
	inline void set_mArea_2(RectangleData_t1039179782  value)
	{
		___mArea_2 = value;
	}

	inline static int32_t get_offset_of_mIsEnabled_3() { return static_cast<int32_t>(offsetof(VirtualButton_t386166510, ___mIsEnabled_3)); }
	inline bool get_mIsEnabled_3() const { return ___mIsEnabled_3; }
	inline bool* get_address_of_mIsEnabled_3() { return &___mIsEnabled_3; }
	inline void set_mIsEnabled_3(bool value)
	{
		___mIsEnabled_3 = value;
	}

	inline static int32_t get_offset_of_mParentImageTarget_4() { return static_cast<int32_t>(offsetof(VirtualButton_t386166510, ___mParentImageTarget_4)); }
	inline RuntimeObject* get_mParentImageTarget_4() const { return ___mParentImageTarget_4; }
	inline RuntimeObject** get_address_of_mParentImageTarget_4() { return &___mParentImageTarget_4; }
	inline void set_mParentImageTarget_4(RuntimeObject* value)
	{
		___mParentImageTarget_4 = value;
		Il2CppCodeGenWriteBarrier((&___mParentImageTarget_4), value);
	}

	inline static int32_t get_offset_of_mParentDataSet_5() { return static_cast<int32_t>(offsetof(VirtualButton_t386166510, ___mParentDataSet_5)); }
	inline DataSet_t3286034874 * get_mParentDataSet_5() const { return ___mParentDataSet_5; }
	inline DataSet_t3286034874 ** get_address_of_mParentDataSet_5() { return &___mParentDataSet_5; }
	inline void set_mParentDataSet_5(DataSet_t3286034874 * value)
	{
		___mParentDataSet_5 = value;
		Il2CppCodeGenWriteBarrier((&___mParentDataSet_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VIRTUALBUTTON_T386166510_H
#ifndef GAMEEVENT_T4069578135_H
#define GAMEEVENT_T4069578135_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameEvent
struct  GameEvent_t4069578135  : public ScriptableObject_t2528358522
{
public:
	// System.Collections.Generic.List`1<EventListener> GameEvent::Event_Listeners
	List_1_t1161341311 * ___Event_Listeners_4;

public:
	inline static int32_t get_offset_of_Event_Listeners_4() { return static_cast<int32_t>(offsetof(GameEvent_t4069578135, ___Event_Listeners_4)); }
	inline List_1_t1161341311 * get_Event_Listeners_4() const { return ___Event_Listeners_4; }
	inline List_1_t1161341311 ** get_address_of_Event_Listeners_4() { return &___Event_Listeners_4; }
	inline void set_Event_Listeners_4(List_1_t1161341311 * value)
	{
		___Event_Listeners_4 = value;
		Il2CppCodeGenWriteBarrier((&___Event_Listeners_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GAMEEVENT_T4069578135_H
#ifndef GENERICLIST_1_T412107121_H
#define GENERICLIST_1_T412107121_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GenericList`1<UnityEngine.GameObject>
struct  GenericList_1_t412107121  : public ScriptableObject_t2528358522
{
public:
	// System.Collections.Generic.List`1<T> GenericList`1::m_value
	List_1_t2585711361 * ___m_value_4;

public:
	inline static int32_t get_offset_of_m_value_4() { return static_cast<int32_t>(offsetof(GenericList_1_t412107121, ___m_value_4)); }
	inline List_1_t2585711361 * get_m_value_4() const { return ___m_value_4; }
	inline List_1_t2585711361 ** get_address_of_m_value_4() { return &___m_value_4; }
	inline void set_m_value_4(List_1_t2585711361 * value)
	{
		___m_value_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_value_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GENERICLIST_1_T412107121_H
#ifndef GENERICLIST_1_T3933812921_H
#define GENERICLIST_1_T3933812921_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GenericList`1<UnityEngine.Material>
struct  GenericList_1_t3933812921  : public ScriptableObject_t2528358522
{
public:
	// System.Collections.Generic.List`1<T> GenericList`1::m_value
	List_1_t1812449865 * ___m_value_4;

public:
	inline static int32_t get_offset_of_m_value_4() { return static_cast<int32_t>(offsetof(GenericList_1_t3933812921, ___m_value_4)); }
	inline List_1_t1812449865 * get_m_value_4() const { return ___m_value_4; }
	inline List_1_t1812449865 ** get_address_of_m_value_4() { return &___m_value_4; }
	inline void set_m_value_4(List_1_t1812449865 * value)
	{
		___m_value_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_value_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GENERICLIST_1_T3933812921_H
#ifndef GENERICVARIABLE_1_T1950689995_H
#define GENERICVARIABLE_1_T1950689995_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GenericVariable`1<System.Boolean>
struct  GenericVariable_1_t1950689995  : public ScriptableObject_t2528358522
{
public:
	// T GenericVariable`1::m_value
	bool ___m_value_4;

public:
	inline static int32_t get_offset_of_m_value_4() { return static_cast<int32_t>(offsetof(GenericVariable_1_t1950689995, ___m_value_4)); }
	inline bool get_m_value_4() const { return ___m_value_4; }
	inline bool* get_address_of_m_value_4() { return &___m_value_4; }
	inline void set_m_value_4(bool value)
	{
		___m_value_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GENERICVARIABLE_1_T1950689995_H
#ifndef GENERICVARIABLE_1_T2987698406_H
#define GENERICVARIABLE_1_T2987698406_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GenericVariable`1<System.Byte>
struct  GenericVariable_1_t2987698406  : public ScriptableObject_t2528358522
{
public:
	// T GenericVariable`1::m_value
	uint8_t ___m_value_4;

public:
	inline static int32_t get_offset_of_m_value_4() { return static_cast<int32_t>(offsetof(GenericVariable_1_t2987698406, ___m_value_4)); }
	inline uint8_t get_m_value_4() const { return ___m_value_4; }
	inline uint8_t* get_address_of_m_value_4() { return &___m_value_4; }
	inline void set_m_value_4(uint8_t value)
	{
		___m_value_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GENERICVARIABLE_1_T2987698406_H
#ifndef GENERICVARIABLE_1_T2448067393_H
#define GENERICVARIABLE_1_T2448067393_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GenericVariable`1<System.Double>
struct  GenericVariable_1_t2448067393  : public ScriptableObject_t2528358522
{
public:
	// T GenericVariable`1::m_value
	double ___m_value_4;

public:
	inline static int32_t get_offset_of_m_value_4() { return static_cast<int32_t>(offsetof(GenericVariable_1_t2448067393, ___m_value_4)); }
	inline double get_m_value_4() const { return ___m_value_4; }
	inline double* get_address_of_m_value_4() { return &___m_value_4; }
	inline void set_m_value_4(double value)
	{
		___m_value_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GENERICVARIABLE_1_T2448067393_H
#ifndef GENERICVARIABLE_1_T509380487_H
#define GENERICVARIABLE_1_T509380487_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GenericVariable`1<System.Int32>
struct  GenericVariable_1_t509380487  : public ScriptableObject_t2528358522
{
public:
	// T GenericVariable`1::m_value
	int32_t ___m_value_4;

public:
	inline static int32_t get_offset_of_m_value_4() { return static_cast<int32_t>(offsetof(GenericVariable_1_t509380487, ___m_value_4)); }
	inline int32_t get_m_value_4() const { return ___m_value_4; }
	inline int32_t* get_address_of_m_value_4() { return &___m_value_4; }
	inline void set_m_value_4(int32_t value)
	{
		___m_value_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GENERICVARIABLE_1_T509380487_H
#ifndef GENERICVARIABLE_1_T1295002038_H
#define GENERICVARIABLE_1_T1295002038_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GenericVariable`1<System.Int64>
struct  GenericVariable_1_t1295002038  : public ScriptableObject_t2528358522
{
public:
	// T GenericVariable`1::m_value
	int64_t ___m_value_4;

public:
	inline static int32_t get_offset_of_m_value_4() { return static_cast<int32_t>(offsetof(GenericVariable_1_t1295002038, ___m_value_4)); }
	inline int64_t get_m_value_4() const { return ___m_value_4; }
	inline int64_t* get_address_of_m_value_4() { return &___m_value_4; }
	inline void set_m_value_4(int64_t value)
	{
		___m_value_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GENERICVARIABLE_1_T1295002038_H
#ifndef GENERICVARIABLE_1_T3250668804_H
#define GENERICVARIABLE_1_T3250668804_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GenericVariable`1<System.Single>
struct  GenericVariable_1_t3250668804  : public ScriptableObject_t2528358522
{
public:
	// T GenericVariable`1::m_value
	float ___m_value_4;

public:
	inline static int32_t get_offset_of_m_value_4() { return static_cast<int32_t>(offsetof(GenericVariable_1_t3250668804, ___m_value_4)); }
	inline float get_m_value_4() const { return ___m_value_4; }
	inline float* get_address_of_m_value_4() { return &___m_value_4; }
	inline void set_m_value_4(float value)
	{
		___m_value_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GENERICVARIABLE_1_T3250668804_H
#ifndef IGAMELOGIC_T3343192399_H
#define IGAMELOGIC_T3343192399_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IGameLogic
struct  IGameLogic_t3343192399  : public ScriptableObject_t2528358522
{
public:
	// GameLogicState IGameLogic::gameLogicState
	int32_t ___gameLogicState_4;
	// MiniGameManager IGameLogic::miniGameManager
	MiniGameManager_t325152534 * ___miniGameManager_5;

public:
	inline static int32_t get_offset_of_gameLogicState_4() { return static_cast<int32_t>(offsetof(IGameLogic_t3343192399, ___gameLogicState_4)); }
	inline int32_t get_gameLogicState_4() const { return ___gameLogicState_4; }
	inline int32_t* get_address_of_gameLogicState_4() { return &___gameLogicState_4; }
	inline void set_gameLogicState_4(int32_t value)
	{
		___gameLogicState_4 = value;
	}

	inline static int32_t get_offset_of_miniGameManager_5() { return static_cast<int32_t>(offsetof(IGameLogic_t3343192399, ___miniGameManager_5)); }
	inline MiniGameManager_t325152534 * get_miniGameManager_5() const { return ___miniGameManager_5; }
	inline MiniGameManager_t325152534 ** get_address_of_miniGameManager_5() { return &___miniGameManager_5; }
	inline void set_miniGameManager_5(MiniGameManager_t325152534 * value)
	{
		___miniGameManager_5 = value;
		Il2CppCodeGenWriteBarrier((&___miniGameManager_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IGAMELOGIC_T3343192399_H
#ifndef OBJECTSTATE_T1202103565_H
#define OBJECTSTATE_T1202103565_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ObjectState
struct  ObjectState_t1202103565  : public ScriptableObject_t2528358522
{
public:
	// BoolReference ObjectState::m_affectFever
	BoolReference_t2741665770 * ___m_affectFever_4;
	// BoolReference ObjectState::m_affectCombo
	BoolReference_t2741665770 * ___m_affectCombo_5;
	// FloatReference ObjectState::m_FeverAmount
	FloatReference_t1374219879 * ___m_FeverAmount_6;
	// IntReference ObjectState::m_typeAmount
	IntReference_t2842414034 * ___m_typeAmount_7;
	// IntReference ObjectState::m_comboAmount
	IntReference_t2842414034 * ___m_comboAmount_8;
	// ObjectState/TYPE ObjectState::m_type
	int32_t ___m_type_9;

public:
	inline static int32_t get_offset_of_m_affectFever_4() { return static_cast<int32_t>(offsetof(ObjectState_t1202103565, ___m_affectFever_4)); }
	inline BoolReference_t2741665770 * get_m_affectFever_4() const { return ___m_affectFever_4; }
	inline BoolReference_t2741665770 ** get_address_of_m_affectFever_4() { return &___m_affectFever_4; }
	inline void set_m_affectFever_4(BoolReference_t2741665770 * value)
	{
		___m_affectFever_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_affectFever_4), value);
	}

	inline static int32_t get_offset_of_m_affectCombo_5() { return static_cast<int32_t>(offsetof(ObjectState_t1202103565, ___m_affectCombo_5)); }
	inline BoolReference_t2741665770 * get_m_affectCombo_5() const { return ___m_affectCombo_5; }
	inline BoolReference_t2741665770 ** get_address_of_m_affectCombo_5() { return &___m_affectCombo_5; }
	inline void set_m_affectCombo_5(BoolReference_t2741665770 * value)
	{
		___m_affectCombo_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_affectCombo_5), value);
	}

	inline static int32_t get_offset_of_m_FeverAmount_6() { return static_cast<int32_t>(offsetof(ObjectState_t1202103565, ___m_FeverAmount_6)); }
	inline FloatReference_t1374219879 * get_m_FeverAmount_6() const { return ___m_FeverAmount_6; }
	inline FloatReference_t1374219879 ** get_address_of_m_FeverAmount_6() { return &___m_FeverAmount_6; }
	inline void set_m_FeverAmount_6(FloatReference_t1374219879 * value)
	{
		___m_FeverAmount_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_FeverAmount_6), value);
	}

	inline static int32_t get_offset_of_m_typeAmount_7() { return static_cast<int32_t>(offsetof(ObjectState_t1202103565, ___m_typeAmount_7)); }
	inline IntReference_t2842414034 * get_m_typeAmount_7() const { return ___m_typeAmount_7; }
	inline IntReference_t2842414034 ** get_address_of_m_typeAmount_7() { return &___m_typeAmount_7; }
	inline void set_m_typeAmount_7(IntReference_t2842414034 * value)
	{
		___m_typeAmount_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_typeAmount_7), value);
	}

	inline static int32_t get_offset_of_m_comboAmount_8() { return static_cast<int32_t>(offsetof(ObjectState_t1202103565, ___m_comboAmount_8)); }
	inline IntReference_t2842414034 * get_m_comboAmount_8() const { return ___m_comboAmount_8; }
	inline IntReference_t2842414034 ** get_address_of_m_comboAmount_8() { return &___m_comboAmount_8; }
	inline void set_m_comboAmount_8(IntReference_t2842414034 * value)
	{
		___m_comboAmount_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_comboAmount_8), value);
	}

	inline static int32_t get_offset_of_m_type_9() { return static_cast<int32_t>(offsetof(ObjectState_t1202103565, ___m_type_9)); }
	inline int32_t get_m_type_9() const { return ___m_type_9; }
	inline int32_t* get_address_of_m_type_9() { return &___m_type_9; }
	inline void set_m_type_9(int32_t value)
	{
		___m_type_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OBJECTSTATE_T1202103565_H
#ifndef ONTRIGGER_T4184125570_H
#define ONTRIGGER_T4184125570_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.EventTrigger/OnTrigger
struct  OnTrigger_t4184125570  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONTRIGGER_T4184125570_H
#ifndef BEHAVIOUR_T1437897464_H
#define BEHAVIOUR_T1437897464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_t1437897464  : public Component_t1923634451
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_T1437897464_H
#ifndef BOOLVARIABLE_T415785710_H
#define BOOLVARIABLE_T415785710_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BoolVariable
struct  BoolVariable_t415785710  : public GenericVariable_1_t1950689995
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOOLVARIABLE_T415785710_H
#ifndef BYTEVARIABLE_T2671702712_H
#define BYTEVARIABLE_T2671702712_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ByteVariable
struct  ByteVariable_t2671702712  : public GenericVariable_1_t2987698406
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BYTEVARIABLE_T2671702712_H
#ifndef DGADGAMELOGIC_T3582670137_H
#define DGADGAMELOGIC_T3582670137_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DGADGameLogic
struct  DGADGameLogic_t3582670137  : public IGameLogic_t3343192399
{
public:
	// FloatVariable DGADGameLogic::m_energyIncrement
	FloatVariable_t761998486 * ___m_energyIncrement_6;
	// FloatVariable DGADGameLogic::m_currEnergy
	FloatVariable_t761998486 * ___m_currEnergy_7;
	// FloatVariable DGADGameLogic::m_maxEnergy
	FloatVariable_t761998486 * ___m_maxEnergy_8;
	// UintVariable DGADGameLogic::m_currHealth
	UintVariable_t3613798951 * ___m_currHealth_9;
	// UintVariable DGADGameLogic::m_maxHealth
	UintVariable_t3613798951 * ___m_maxHealth_10;
	// IntReference DGADGameLogic::m_assignLevel
	IntReference_t2842414034 * ___m_assignLevel_11;
	// GameEvent DGADGameLogic::m_destroyAllEnemies
	GameEvent_t4069578135 * ___m_destroyAllEnemies_12;
	// GameEvent DGADGameLogic::m_spawnEnemies
	GameEvent_t4069578135 * ___m_spawnEnemies_13;
	// GameEvent DGADGameLogic::m_resetAssignmentStage
	GameEvent_t4069578135 * ___m_resetAssignmentStage_14;
	// GameEvent DGADGameLogic::m_resetPenType
	GameEvent_t4069578135 * ___m_resetPenType_15;
	// System.Single DGADGameLogic::spawnTimer
	float ___spawnTimer_16;
	// System.Single DGADGameLogic::baseSpawnTime
	float ___baseSpawnTime_17;
	// System.Single DGADGameLogic::waveTimer
	float ___waveTimer_18;
	// System.Single DGADGameLogic::baseWaveTime
	float ___baseWaveTime_19;
	// System.Int32 DGADGameLogic::wave
	int32_t ___wave_20;
	// System.Int32 DGADGameLogic::mons
	int32_t ___mons_21;
	// System.Boolean DGADGameLogic::canSpawn
	bool ___canSpawn_22;

public:
	inline static int32_t get_offset_of_m_energyIncrement_6() { return static_cast<int32_t>(offsetof(DGADGameLogic_t3582670137, ___m_energyIncrement_6)); }
	inline FloatVariable_t761998486 * get_m_energyIncrement_6() const { return ___m_energyIncrement_6; }
	inline FloatVariable_t761998486 ** get_address_of_m_energyIncrement_6() { return &___m_energyIncrement_6; }
	inline void set_m_energyIncrement_6(FloatVariable_t761998486 * value)
	{
		___m_energyIncrement_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_energyIncrement_6), value);
	}

	inline static int32_t get_offset_of_m_currEnergy_7() { return static_cast<int32_t>(offsetof(DGADGameLogic_t3582670137, ___m_currEnergy_7)); }
	inline FloatVariable_t761998486 * get_m_currEnergy_7() const { return ___m_currEnergy_7; }
	inline FloatVariable_t761998486 ** get_address_of_m_currEnergy_7() { return &___m_currEnergy_7; }
	inline void set_m_currEnergy_7(FloatVariable_t761998486 * value)
	{
		___m_currEnergy_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_currEnergy_7), value);
	}

	inline static int32_t get_offset_of_m_maxEnergy_8() { return static_cast<int32_t>(offsetof(DGADGameLogic_t3582670137, ___m_maxEnergy_8)); }
	inline FloatVariable_t761998486 * get_m_maxEnergy_8() const { return ___m_maxEnergy_8; }
	inline FloatVariable_t761998486 ** get_address_of_m_maxEnergy_8() { return &___m_maxEnergy_8; }
	inline void set_m_maxEnergy_8(FloatVariable_t761998486 * value)
	{
		___m_maxEnergy_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_maxEnergy_8), value);
	}

	inline static int32_t get_offset_of_m_currHealth_9() { return static_cast<int32_t>(offsetof(DGADGameLogic_t3582670137, ___m_currHealth_9)); }
	inline UintVariable_t3613798951 * get_m_currHealth_9() const { return ___m_currHealth_9; }
	inline UintVariable_t3613798951 ** get_address_of_m_currHealth_9() { return &___m_currHealth_9; }
	inline void set_m_currHealth_9(UintVariable_t3613798951 * value)
	{
		___m_currHealth_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_currHealth_9), value);
	}

	inline static int32_t get_offset_of_m_maxHealth_10() { return static_cast<int32_t>(offsetof(DGADGameLogic_t3582670137, ___m_maxHealth_10)); }
	inline UintVariable_t3613798951 * get_m_maxHealth_10() const { return ___m_maxHealth_10; }
	inline UintVariable_t3613798951 ** get_address_of_m_maxHealth_10() { return &___m_maxHealth_10; }
	inline void set_m_maxHealth_10(UintVariable_t3613798951 * value)
	{
		___m_maxHealth_10 = value;
		Il2CppCodeGenWriteBarrier((&___m_maxHealth_10), value);
	}

	inline static int32_t get_offset_of_m_assignLevel_11() { return static_cast<int32_t>(offsetof(DGADGameLogic_t3582670137, ___m_assignLevel_11)); }
	inline IntReference_t2842414034 * get_m_assignLevel_11() const { return ___m_assignLevel_11; }
	inline IntReference_t2842414034 ** get_address_of_m_assignLevel_11() { return &___m_assignLevel_11; }
	inline void set_m_assignLevel_11(IntReference_t2842414034 * value)
	{
		___m_assignLevel_11 = value;
		Il2CppCodeGenWriteBarrier((&___m_assignLevel_11), value);
	}

	inline static int32_t get_offset_of_m_destroyAllEnemies_12() { return static_cast<int32_t>(offsetof(DGADGameLogic_t3582670137, ___m_destroyAllEnemies_12)); }
	inline GameEvent_t4069578135 * get_m_destroyAllEnemies_12() const { return ___m_destroyAllEnemies_12; }
	inline GameEvent_t4069578135 ** get_address_of_m_destroyAllEnemies_12() { return &___m_destroyAllEnemies_12; }
	inline void set_m_destroyAllEnemies_12(GameEvent_t4069578135 * value)
	{
		___m_destroyAllEnemies_12 = value;
		Il2CppCodeGenWriteBarrier((&___m_destroyAllEnemies_12), value);
	}

	inline static int32_t get_offset_of_m_spawnEnemies_13() { return static_cast<int32_t>(offsetof(DGADGameLogic_t3582670137, ___m_spawnEnemies_13)); }
	inline GameEvent_t4069578135 * get_m_spawnEnemies_13() const { return ___m_spawnEnemies_13; }
	inline GameEvent_t4069578135 ** get_address_of_m_spawnEnemies_13() { return &___m_spawnEnemies_13; }
	inline void set_m_spawnEnemies_13(GameEvent_t4069578135 * value)
	{
		___m_spawnEnemies_13 = value;
		Il2CppCodeGenWriteBarrier((&___m_spawnEnemies_13), value);
	}

	inline static int32_t get_offset_of_m_resetAssignmentStage_14() { return static_cast<int32_t>(offsetof(DGADGameLogic_t3582670137, ___m_resetAssignmentStage_14)); }
	inline GameEvent_t4069578135 * get_m_resetAssignmentStage_14() const { return ___m_resetAssignmentStage_14; }
	inline GameEvent_t4069578135 ** get_address_of_m_resetAssignmentStage_14() { return &___m_resetAssignmentStage_14; }
	inline void set_m_resetAssignmentStage_14(GameEvent_t4069578135 * value)
	{
		___m_resetAssignmentStage_14 = value;
		Il2CppCodeGenWriteBarrier((&___m_resetAssignmentStage_14), value);
	}

	inline static int32_t get_offset_of_m_resetPenType_15() { return static_cast<int32_t>(offsetof(DGADGameLogic_t3582670137, ___m_resetPenType_15)); }
	inline GameEvent_t4069578135 * get_m_resetPenType_15() const { return ___m_resetPenType_15; }
	inline GameEvent_t4069578135 ** get_address_of_m_resetPenType_15() { return &___m_resetPenType_15; }
	inline void set_m_resetPenType_15(GameEvent_t4069578135 * value)
	{
		___m_resetPenType_15 = value;
		Il2CppCodeGenWriteBarrier((&___m_resetPenType_15), value);
	}

	inline static int32_t get_offset_of_spawnTimer_16() { return static_cast<int32_t>(offsetof(DGADGameLogic_t3582670137, ___spawnTimer_16)); }
	inline float get_spawnTimer_16() const { return ___spawnTimer_16; }
	inline float* get_address_of_spawnTimer_16() { return &___spawnTimer_16; }
	inline void set_spawnTimer_16(float value)
	{
		___spawnTimer_16 = value;
	}

	inline static int32_t get_offset_of_baseSpawnTime_17() { return static_cast<int32_t>(offsetof(DGADGameLogic_t3582670137, ___baseSpawnTime_17)); }
	inline float get_baseSpawnTime_17() const { return ___baseSpawnTime_17; }
	inline float* get_address_of_baseSpawnTime_17() { return &___baseSpawnTime_17; }
	inline void set_baseSpawnTime_17(float value)
	{
		___baseSpawnTime_17 = value;
	}

	inline static int32_t get_offset_of_waveTimer_18() { return static_cast<int32_t>(offsetof(DGADGameLogic_t3582670137, ___waveTimer_18)); }
	inline float get_waveTimer_18() const { return ___waveTimer_18; }
	inline float* get_address_of_waveTimer_18() { return &___waveTimer_18; }
	inline void set_waveTimer_18(float value)
	{
		___waveTimer_18 = value;
	}

	inline static int32_t get_offset_of_baseWaveTime_19() { return static_cast<int32_t>(offsetof(DGADGameLogic_t3582670137, ___baseWaveTime_19)); }
	inline float get_baseWaveTime_19() const { return ___baseWaveTime_19; }
	inline float* get_address_of_baseWaveTime_19() { return &___baseWaveTime_19; }
	inline void set_baseWaveTime_19(float value)
	{
		___baseWaveTime_19 = value;
	}

	inline static int32_t get_offset_of_wave_20() { return static_cast<int32_t>(offsetof(DGADGameLogic_t3582670137, ___wave_20)); }
	inline int32_t get_wave_20() const { return ___wave_20; }
	inline int32_t* get_address_of_wave_20() { return &___wave_20; }
	inline void set_wave_20(int32_t value)
	{
		___wave_20 = value;
	}

	inline static int32_t get_offset_of_mons_21() { return static_cast<int32_t>(offsetof(DGADGameLogic_t3582670137, ___mons_21)); }
	inline int32_t get_mons_21() const { return ___mons_21; }
	inline int32_t* get_address_of_mons_21() { return &___mons_21; }
	inline void set_mons_21(int32_t value)
	{
		___mons_21 = value;
	}

	inline static int32_t get_offset_of_canSpawn_22() { return static_cast<int32_t>(offsetof(DGADGameLogic_t3582670137, ___canSpawn_22)); }
	inline bool get_canSpawn_22() const { return ___canSpawn_22; }
	inline bool* get_address_of_canSpawn_22() { return &___canSpawn_22; }
	inline void set_canSpawn_22(bool value)
	{
		___canSpawn_22 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DGADGAMELOGIC_T3582670137_H
#ifndef DOUBLEVARIABLE_T2292910480_H
#define DOUBLEVARIABLE_T2292910480_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DoubleVariable
struct  DoubleVariable_t2292910480  : public GenericVariable_1_t2448067393
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DOUBLEVARIABLE_T2292910480_H
#ifndef FLOATVARIABLE_T761998486_H
#define FLOATVARIABLE_T761998486_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FloatVariable
struct  FloatVariable_t761998486  : public GenericVariable_1_t3250668804
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FLOATVARIABLE_T761998486_H
#ifndef GDTGAMELOGIC_T4243140947_H
#define GDTGAMELOGIC_T4243140947_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GDTGameLogic
struct  GDTGameLogic_t4243140947  : public IGameLogic_t3343192399
{
public:
	// FloatVariable GDTGameLogic::currTimer
	FloatVariable_t761998486 * ___currTimer_6;
	// FloatVariable GDTGameLogic::currFever
	FloatVariable_t761998486 * ___currFever_7;
	// FloatVariable GDTGameLogic::maxTimer
	FloatVariable_t761998486 * ___maxTimer_8;
	// FloatVariable GDTGameLogic::maxFever
	FloatVariable_t761998486 * ___maxFever_9;
	// UintVariable GDTGameLogic::currHealth
	UintVariable_t3613798951 * ___currHealth_10;
	// UintVariable GDTGameLogic::currCombo
	UintVariable_t3613798951 * ___currCombo_11;
	// UintVariable GDTGameLogic::currScore
	UintVariable_t3613798951 * ___currScore_12;
	// UintVariable GDTGameLogic::maxScore
	UintVariable_t3613798951 * ___maxScore_13;
	// UintVariable GDTGameLogic::maxHealth
	UintVariable_t3613798951 * ___maxHealth_14;
	// System.Single GDTGameLogic::feverBubbleSpawnTime
	float ___feverBubbleSpawnTime_16;
	// System.Boolean GDTGameLogic::isFeverMode
	bool ___isFeverMode_17;
	// System.Boolean GDTGameLogic::isPowerUpSpawned
	bool ___isPowerUpSpawned_18;
	// System.Single GDTGameLogic::spawnTimer
	float ___spawnTimer_19;
	// System.Single GDTGameLogic::spawnTime
	float ___spawnTime_20;
	// System.Single GDTGameLogic::baseSpawnTime
	float ___baseSpawnTime_21;
	// System.Single GDTGameLogic::minSpawnTime
	float ___minSpawnTime_22;
	// System.Single GDTGameLogic::freezeDuration
	float ___freezeDuration_23;
	// System.Single GDTGameLogic::freezeTimer
	float ___freezeTimer_24;
	// System.Single GDTGameLogic::hurtTime
	float ___hurtTime_25;
	// System.Single GDTGameLogic::hurtTimer
	float ___hurtTimer_26;
	// GameEvent GDTGameLogic::spawnBubble
	GameEvent_t4069578135 * ___spawnBubble_27;
	// GameEvent GDTGameLogic::spawnFeverBubble
	GameEvent_t4069578135 * ___spawnFeverBubble_28;
	// GameEvent GDTGameLogic::spawnPowerUp
	GameEvent_t4069578135 * ___spawnPowerUp_29;
	// GameEvent GDTGameLogic::destroyRedBubble
	GameEvent_t4069578135 * ___destroyRedBubble_30;
	// GameEvent GDTGameLogic::destroyAllBubble
	GameEvent_t4069578135 * ___destroyAllBubble_31;
	// GameEvent GDTGameLogic::hurtStart
	GameEvent_t4069578135 * ___hurtStart_32;
	// GameEvent GDTGameLogic::hurtEnd
	GameEvent_t4069578135 * ___hurtEnd_33;
	// GameEvent GDTGameLogic::feverStart
	GameEvent_t4069578135 * ___feverStart_34;
	// GameEvent GDTGameLogic::feverEnd
	GameEvent_t4069578135 * ___feverEnd_35;
	// GameEvent GDTGameLogic::freezeStart
	GameEvent_t4069578135 * ___freezeStart_36;
	// GameEvent GDTGameLogic::freezeEnd
	GameEvent_t4069578135 * ___freezeEnd_37;

public:
	inline static int32_t get_offset_of_currTimer_6() { return static_cast<int32_t>(offsetof(GDTGameLogic_t4243140947, ___currTimer_6)); }
	inline FloatVariable_t761998486 * get_currTimer_6() const { return ___currTimer_6; }
	inline FloatVariable_t761998486 ** get_address_of_currTimer_6() { return &___currTimer_6; }
	inline void set_currTimer_6(FloatVariable_t761998486 * value)
	{
		___currTimer_6 = value;
		Il2CppCodeGenWriteBarrier((&___currTimer_6), value);
	}

	inline static int32_t get_offset_of_currFever_7() { return static_cast<int32_t>(offsetof(GDTGameLogic_t4243140947, ___currFever_7)); }
	inline FloatVariable_t761998486 * get_currFever_7() const { return ___currFever_7; }
	inline FloatVariable_t761998486 ** get_address_of_currFever_7() { return &___currFever_7; }
	inline void set_currFever_7(FloatVariable_t761998486 * value)
	{
		___currFever_7 = value;
		Il2CppCodeGenWriteBarrier((&___currFever_7), value);
	}

	inline static int32_t get_offset_of_maxTimer_8() { return static_cast<int32_t>(offsetof(GDTGameLogic_t4243140947, ___maxTimer_8)); }
	inline FloatVariable_t761998486 * get_maxTimer_8() const { return ___maxTimer_8; }
	inline FloatVariable_t761998486 ** get_address_of_maxTimer_8() { return &___maxTimer_8; }
	inline void set_maxTimer_8(FloatVariable_t761998486 * value)
	{
		___maxTimer_8 = value;
		Il2CppCodeGenWriteBarrier((&___maxTimer_8), value);
	}

	inline static int32_t get_offset_of_maxFever_9() { return static_cast<int32_t>(offsetof(GDTGameLogic_t4243140947, ___maxFever_9)); }
	inline FloatVariable_t761998486 * get_maxFever_9() const { return ___maxFever_9; }
	inline FloatVariable_t761998486 ** get_address_of_maxFever_9() { return &___maxFever_9; }
	inline void set_maxFever_9(FloatVariable_t761998486 * value)
	{
		___maxFever_9 = value;
		Il2CppCodeGenWriteBarrier((&___maxFever_9), value);
	}

	inline static int32_t get_offset_of_currHealth_10() { return static_cast<int32_t>(offsetof(GDTGameLogic_t4243140947, ___currHealth_10)); }
	inline UintVariable_t3613798951 * get_currHealth_10() const { return ___currHealth_10; }
	inline UintVariable_t3613798951 ** get_address_of_currHealth_10() { return &___currHealth_10; }
	inline void set_currHealth_10(UintVariable_t3613798951 * value)
	{
		___currHealth_10 = value;
		Il2CppCodeGenWriteBarrier((&___currHealth_10), value);
	}

	inline static int32_t get_offset_of_currCombo_11() { return static_cast<int32_t>(offsetof(GDTGameLogic_t4243140947, ___currCombo_11)); }
	inline UintVariable_t3613798951 * get_currCombo_11() const { return ___currCombo_11; }
	inline UintVariable_t3613798951 ** get_address_of_currCombo_11() { return &___currCombo_11; }
	inline void set_currCombo_11(UintVariable_t3613798951 * value)
	{
		___currCombo_11 = value;
		Il2CppCodeGenWriteBarrier((&___currCombo_11), value);
	}

	inline static int32_t get_offset_of_currScore_12() { return static_cast<int32_t>(offsetof(GDTGameLogic_t4243140947, ___currScore_12)); }
	inline UintVariable_t3613798951 * get_currScore_12() const { return ___currScore_12; }
	inline UintVariable_t3613798951 ** get_address_of_currScore_12() { return &___currScore_12; }
	inline void set_currScore_12(UintVariable_t3613798951 * value)
	{
		___currScore_12 = value;
		Il2CppCodeGenWriteBarrier((&___currScore_12), value);
	}

	inline static int32_t get_offset_of_maxScore_13() { return static_cast<int32_t>(offsetof(GDTGameLogic_t4243140947, ___maxScore_13)); }
	inline UintVariable_t3613798951 * get_maxScore_13() const { return ___maxScore_13; }
	inline UintVariable_t3613798951 ** get_address_of_maxScore_13() { return &___maxScore_13; }
	inline void set_maxScore_13(UintVariable_t3613798951 * value)
	{
		___maxScore_13 = value;
		Il2CppCodeGenWriteBarrier((&___maxScore_13), value);
	}

	inline static int32_t get_offset_of_maxHealth_14() { return static_cast<int32_t>(offsetof(GDTGameLogic_t4243140947, ___maxHealth_14)); }
	inline UintVariable_t3613798951 * get_maxHealth_14() const { return ___maxHealth_14; }
	inline UintVariable_t3613798951 ** get_address_of_maxHealth_14() { return &___maxHealth_14; }
	inline void set_maxHealth_14(UintVariable_t3613798951 * value)
	{
		___maxHealth_14 = value;
		Il2CppCodeGenWriteBarrier((&___maxHealth_14), value);
	}

	inline static int32_t get_offset_of_feverBubbleSpawnTime_16() { return static_cast<int32_t>(offsetof(GDTGameLogic_t4243140947, ___feverBubbleSpawnTime_16)); }
	inline float get_feverBubbleSpawnTime_16() const { return ___feverBubbleSpawnTime_16; }
	inline float* get_address_of_feverBubbleSpawnTime_16() { return &___feverBubbleSpawnTime_16; }
	inline void set_feverBubbleSpawnTime_16(float value)
	{
		___feverBubbleSpawnTime_16 = value;
	}

	inline static int32_t get_offset_of_isFeverMode_17() { return static_cast<int32_t>(offsetof(GDTGameLogic_t4243140947, ___isFeverMode_17)); }
	inline bool get_isFeverMode_17() const { return ___isFeverMode_17; }
	inline bool* get_address_of_isFeverMode_17() { return &___isFeverMode_17; }
	inline void set_isFeverMode_17(bool value)
	{
		___isFeverMode_17 = value;
	}

	inline static int32_t get_offset_of_isPowerUpSpawned_18() { return static_cast<int32_t>(offsetof(GDTGameLogic_t4243140947, ___isPowerUpSpawned_18)); }
	inline bool get_isPowerUpSpawned_18() const { return ___isPowerUpSpawned_18; }
	inline bool* get_address_of_isPowerUpSpawned_18() { return &___isPowerUpSpawned_18; }
	inline void set_isPowerUpSpawned_18(bool value)
	{
		___isPowerUpSpawned_18 = value;
	}

	inline static int32_t get_offset_of_spawnTimer_19() { return static_cast<int32_t>(offsetof(GDTGameLogic_t4243140947, ___spawnTimer_19)); }
	inline float get_spawnTimer_19() const { return ___spawnTimer_19; }
	inline float* get_address_of_spawnTimer_19() { return &___spawnTimer_19; }
	inline void set_spawnTimer_19(float value)
	{
		___spawnTimer_19 = value;
	}

	inline static int32_t get_offset_of_spawnTime_20() { return static_cast<int32_t>(offsetof(GDTGameLogic_t4243140947, ___spawnTime_20)); }
	inline float get_spawnTime_20() const { return ___spawnTime_20; }
	inline float* get_address_of_spawnTime_20() { return &___spawnTime_20; }
	inline void set_spawnTime_20(float value)
	{
		___spawnTime_20 = value;
	}

	inline static int32_t get_offset_of_baseSpawnTime_21() { return static_cast<int32_t>(offsetof(GDTGameLogic_t4243140947, ___baseSpawnTime_21)); }
	inline float get_baseSpawnTime_21() const { return ___baseSpawnTime_21; }
	inline float* get_address_of_baseSpawnTime_21() { return &___baseSpawnTime_21; }
	inline void set_baseSpawnTime_21(float value)
	{
		___baseSpawnTime_21 = value;
	}

	inline static int32_t get_offset_of_minSpawnTime_22() { return static_cast<int32_t>(offsetof(GDTGameLogic_t4243140947, ___minSpawnTime_22)); }
	inline float get_minSpawnTime_22() const { return ___minSpawnTime_22; }
	inline float* get_address_of_minSpawnTime_22() { return &___minSpawnTime_22; }
	inline void set_minSpawnTime_22(float value)
	{
		___minSpawnTime_22 = value;
	}

	inline static int32_t get_offset_of_freezeDuration_23() { return static_cast<int32_t>(offsetof(GDTGameLogic_t4243140947, ___freezeDuration_23)); }
	inline float get_freezeDuration_23() const { return ___freezeDuration_23; }
	inline float* get_address_of_freezeDuration_23() { return &___freezeDuration_23; }
	inline void set_freezeDuration_23(float value)
	{
		___freezeDuration_23 = value;
	}

	inline static int32_t get_offset_of_freezeTimer_24() { return static_cast<int32_t>(offsetof(GDTGameLogic_t4243140947, ___freezeTimer_24)); }
	inline float get_freezeTimer_24() const { return ___freezeTimer_24; }
	inline float* get_address_of_freezeTimer_24() { return &___freezeTimer_24; }
	inline void set_freezeTimer_24(float value)
	{
		___freezeTimer_24 = value;
	}

	inline static int32_t get_offset_of_hurtTime_25() { return static_cast<int32_t>(offsetof(GDTGameLogic_t4243140947, ___hurtTime_25)); }
	inline float get_hurtTime_25() const { return ___hurtTime_25; }
	inline float* get_address_of_hurtTime_25() { return &___hurtTime_25; }
	inline void set_hurtTime_25(float value)
	{
		___hurtTime_25 = value;
	}

	inline static int32_t get_offset_of_hurtTimer_26() { return static_cast<int32_t>(offsetof(GDTGameLogic_t4243140947, ___hurtTimer_26)); }
	inline float get_hurtTimer_26() const { return ___hurtTimer_26; }
	inline float* get_address_of_hurtTimer_26() { return &___hurtTimer_26; }
	inline void set_hurtTimer_26(float value)
	{
		___hurtTimer_26 = value;
	}

	inline static int32_t get_offset_of_spawnBubble_27() { return static_cast<int32_t>(offsetof(GDTGameLogic_t4243140947, ___spawnBubble_27)); }
	inline GameEvent_t4069578135 * get_spawnBubble_27() const { return ___spawnBubble_27; }
	inline GameEvent_t4069578135 ** get_address_of_spawnBubble_27() { return &___spawnBubble_27; }
	inline void set_spawnBubble_27(GameEvent_t4069578135 * value)
	{
		___spawnBubble_27 = value;
		Il2CppCodeGenWriteBarrier((&___spawnBubble_27), value);
	}

	inline static int32_t get_offset_of_spawnFeverBubble_28() { return static_cast<int32_t>(offsetof(GDTGameLogic_t4243140947, ___spawnFeverBubble_28)); }
	inline GameEvent_t4069578135 * get_spawnFeverBubble_28() const { return ___spawnFeverBubble_28; }
	inline GameEvent_t4069578135 ** get_address_of_spawnFeverBubble_28() { return &___spawnFeverBubble_28; }
	inline void set_spawnFeverBubble_28(GameEvent_t4069578135 * value)
	{
		___spawnFeverBubble_28 = value;
		Il2CppCodeGenWriteBarrier((&___spawnFeverBubble_28), value);
	}

	inline static int32_t get_offset_of_spawnPowerUp_29() { return static_cast<int32_t>(offsetof(GDTGameLogic_t4243140947, ___spawnPowerUp_29)); }
	inline GameEvent_t4069578135 * get_spawnPowerUp_29() const { return ___spawnPowerUp_29; }
	inline GameEvent_t4069578135 ** get_address_of_spawnPowerUp_29() { return &___spawnPowerUp_29; }
	inline void set_spawnPowerUp_29(GameEvent_t4069578135 * value)
	{
		___spawnPowerUp_29 = value;
		Il2CppCodeGenWriteBarrier((&___spawnPowerUp_29), value);
	}

	inline static int32_t get_offset_of_destroyRedBubble_30() { return static_cast<int32_t>(offsetof(GDTGameLogic_t4243140947, ___destroyRedBubble_30)); }
	inline GameEvent_t4069578135 * get_destroyRedBubble_30() const { return ___destroyRedBubble_30; }
	inline GameEvent_t4069578135 ** get_address_of_destroyRedBubble_30() { return &___destroyRedBubble_30; }
	inline void set_destroyRedBubble_30(GameEvent_t4069578135 * value)
	{
		___destroyRedBubble_30 = value;
		Il2CppCodeGenWriteBarrier((&___destroyRedBubble_30), value);
	}

	inline static int32_t get_offset_of_destroyAllBubble_31() { return static_cast<int32_t>(offsetof(GDTGameLogic_t4243140947, ___destroyAllBubble_31)); }
	inline GameEvent_t4069578135 * get_destroyAllBubble_31() const { return ___destroyAllBubble_31; }
	inline GameEvent_t4069578135 ** get_address_of_destroyAllBubble_31() { return &___destroyAllBubble_31; }
	inline void set_destroyAllBubble_31(GameEvent_t4069578135 * value)
	{
		___destroyAllBubble_31 = value;
		Il2CppCodeGenWriteBarrier((&___destroyAllBubble_31), value);
	}

	inline static int32_t get_offset_of_hurtStart_32() { return static_cast<int32_t>(offsetof(GDTGameLogic_t4243140947, ___hurtStart_32)); }
	inline GameEvent_t4069578135 * get_hurtStart_32() const { return ___hurtStart_32; }
	inline GameEvent_t4069578135 ** get_address_of_hurtStart_32() { return &___hurtStart_32; }
	inline void set_hurtStart_32(GameEvent_t4069578135 * value)
	{
		___hurtStart_32 = value;
		Il2CppCodeGenWriteBarrier((&___hurtStart_32), value);
	}

	inline static int32_t get_offset_of_hurtEnd_33() { return static_cast<int32_t>(offsetof(GDTGameLogic_t4243140947, ___hurtEnd_33)); }
	inline GameEvent_t4069578135 * get_hurtEnd_33() const { return ___hurtEnd_33; }
	inline GameEvent_t4069578135 ** get_address_of_hurtEnd_33() { return &___hurtEnd_33; }
	inline void set_hurtEnd_33(GameEvent_t4069578135 * value)
	{
		___hurtEnd_33 = value;
		Il2CppCodeGenWriteBarrier((&___hurtEnd_33), value);
	}

	inline static int32_t get_offset_of_feverStart_34() { return static_cast<int32_t>(offsetof(GDTGameLogic_t4243140947, ___feverStart_34)); }
	inline GameEvent_t4069578135 * get_feverStart_34() const { return ___feverStart_34; }
	inline GameEvent_t4069578135 ** get_address_of_feverStart_34() { return &___feverStart_34; }
	inline void set_feverStart_34(GameEvent_t4069578135 * value)
	{
		___feverStart_34 = value;
		Il2CppCodeGenWriteBarrier((&___feverStart_34), value);
	}

	inline static int32_t get_offset_of_feverEnd_35() { return static_cast<int32_t>(offsetof(GDTGameLogic_t4243140947, ___feverEnd_35)); }
	inline GameEvent_t4069578135 * get_feverEnd_35() const { return ___feverEnd_35; }
	inline GameEvent_t4069578135 ** get_address_of_feverEnd_35() { return &___feverEnd_35; }
	inline void set_feverEnd_35(GameEvent_t4069578135 * value)
	{
		___feverEnd_35 = value;
		Il2CppCodeGenWriteBarrier((&___feverEnd_35), value);
	}

	inline static int32_t get_offset_of_freezeStart_36() { return static_cast<int32_t>(offsetof(GDTGameLogic_t4243140947, ___freezeStart_36)); }
	inline GameEvent_t4069578135 * get_freezeStart_36() const { return ___freezeStart_36; }
	inline GameEvent_t4069578135 ** get_address_of_freezeStart_36() { return &___freezeStart_36; }
	inline void set_freezeStart_36(GameEvent_t4069578135 * value)
	{
		___freezeStart_36 = value;
		Il2CppCodeGenWriteBarrier((&___freezeStart_36), value);
	}

	inline static int32_t get_offset_of_freezeEnd_37() { return static_cast<int32_t>(offsetof(GDTGameLogic_t4243140947, ___freezeEnd_37)); }
	inline GameEvent_t4069578135 * get_freezeEnd_37() const { return ___freezeEnd_37; }
	inline GameEvent_t4069578135 ** get_address_of_freezeEnd_37() { return &___freezeEnd_37; }
	inline void set_freezeEnd_37(GameEvent_t4069578135 * value)
	{
		___freezeEnd_37 = value;
		Il2CppCodeGenWriteBarrier((&___freezeEnd_37), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GDTGAMELOGIC_T4243140947_H
#ifndef GAMEOBJECTLIST_T328636558_H
#define GAMEOBJECTLIST_T328636558_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameObjectList
struct  GameObjectList_t328636558  : public GenericList_1_t412107121
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GAMEOBJECTLIST_T328636558_H
#ifndef INTVARIABLE_T1706759505_H
#define INTVARIABLE_T1706759505_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IntVariable
struct  IntVariable_t1706759505  : public GenericVariable_1_t509380487
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTVARIABLE_T1706759505_H
#ifndef LONGVARIABLE_T812939517_H
#define LONGVARIABLE_T812939517_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LongVariable
struct  LongVariable_t812939517  : public GenericVariable_1_t1295002038
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LONGVARIABLE_T812939517_H
#ifndef MATERIALLIST_T2938811163_H
#define MATERIALLIST_T2938811163_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MaterialList
struct  MaterialList_t2938811163  : public GenericList_1_t3933812921
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MATERIALLIST_T2938811163_H
#ifndef MONOBEHAVIOUR_T3962482529_H
#define MONOBEHAVIOUR_T3962482529_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t3962482529  : public Behaviour_t1437897464
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T3962482529_H
#ifndef ANIMATIONEVENTGAMEOBJECTACTIVE_T1825876594_H
#define ANIMATIONEVENTGAMEOBJECTACTIVE_T1825876594_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AnimationEventGameObjectActive
struct  AnimationEventGameObjectActive_t1825876594  : public MonoBehaviour_t3962482529
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANIMATIONEVENTGAMEOBJECTACTIVE_T1825876594_H
#ifndef AUDIOMANAGER_T3267510698_H
#define AUDIOMANAGER_T3267510698_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AudioManager
struct  AudioManager_t3267510698  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Audio.AudioMixerGroup AudioManager::mixerGroup
	AudioMixerGroup_t2743564464 * ___mixerGroup_5;
	// Sound[] AudioManager::sounds
	SoundU5BU5D_t3647937991* ___sounds_6;

public:
	inline static int32_t get_offset_of_mixerGroup_5() { return static_cast<int32_t>(offsetof(AudioManager_t3267510698, ___mixerGroup_5)); }
	inline AudioMixerGroup_t2743564464 * get_mixerGroup_5() const { return ___mixerGroup_5; }
	inline AudioMixerGroup_t2743564464 ** get_address_of_mixerGroup_5() { return &___mixerGroup_5; }
	inline void set_mixerGroup_5(AudioMixerGroup_t2743564464 * value)
	{
		___mixerGroup_5 = value;
		Il2CppCodeGenWriteBarrier((&___mixerGroup_5), value);
	}

	inline static int32_t get_offset_of_sounds_6() { return static_cast<int32_t>(offsetof(AudioManager_t3267510698, ___sounds_6)); }
	inline SoundU5BU5D_t3647937991* get_sounds_6() const { return ___sounds_6; }
	inline SoundU5BU5D_t3647937991** get_address_of_sounds_6() { return &___sounds_6; }
	inline void set_sounds_6(SoundU5BU5D_t3647937991* value)
	{
		___sounds_6 = value;
		Il2CppCodeGenWriteBarrier((&___sounds_6), value);
	}
};

struct AudioManager_t3267510698_StaticFields
{
public:
	// AudioManager AudioManager::instance
	AudioManager_t3267510698 * ___instance_4;

public:
	inline static int32_t get_offset_of_instance_4() { return static_cast<int32_t>(offsetof(AudioManager_t3267510698_StaticFields, ___instance_4)); }
	inline AudioManager_t3267510698 * get_instance_4() const { return ___instance_4; }
	inline AudioManager_t3267510698 ** get_address_of_instance_4() { return &___instance_4; }
	inline void set_instance_4(AudioManager_t3267510698 * value)
	{
		___instance_4 = value;
		Il2CppCodeGenWriteBarrier((&___instance_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AUDIOMANAGER_T3267510698_H
#ifndef DEFAULTTRACKABLEEVENTHANDLER_T1588957063_H
#define DEFAULTTRACKABLEEVENTHANDLER_T1588957063_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DefaultTrackableEventHandler
struct  DefaultTrackableEventHandler_t1588957063  : public MonoBehaviour_t3962482529
{
public:
	// Vuforia.TrackableBehaviour DefaultTrackableEventHandler::mTrackableBehaviour
	TrackableBehaviour_t1113559212 * ___mTrackableBehaviour_4;
	// Vuforia.TrackableBehaviour/Status DefaultTrackableEventHandler::m_PreviousStatus
	int32_t ___m_PreviousStatus_5;
	// Vuforia.TrackableBehaviour/Status DefaultTrackableEventHandler::m_NewStatus
	int32_t ___m_NewStatus_6;

public:
	inline static int32_t get_offset_of_mTrackableBehaviour_4() { return static_cast<int32_t>(offsetof(DefaultTrackableEventHandler_t1588957063, ___mTrackableBehaviour_4)); }
	inline TrackableBehaviour_t1113559212 * get_mTrackableBehaviour_4() const { return ___mTrackableBehaviour_4; }
	inline TrackableBehaviour_t1113559212 ** get_address_of_mTrackableBehaviour_4() { return &___mTrackableBehaviour_4; }
	inline void set_mTrackableBehaviour_4(TrackableBehaviour_t1113559212 * value)
	{
		___mTrackableBehaviour_4 = value;
		Il2CppCodeGenWriteBarrier((&___mTrackableBehaviour_4), value);
	}

	inline static int32_t get_offset_of_m_PreviousStatus_5() { return static_cast<int32_t>(offsetof(DefaultTrackableEventHandler_t1588957063, ___m_PreviousStatus_5)); }
	inline int32_t get_m_PreviousStatus_5() const { return ___m_PreviousStatus_5; }
	inline int32_t* get_address_of_m_PreviousStatus_5() { return &___m_PreviousStatus_5; }
	inline void set_m_PreviousStatus_5(int32_t value)
	{
		___m_PreviousStatus_5 = value;
	}

	inline static int32_t get_offset_of_m_NewStatus_6() { return static_cast<int32_t>(offsetof(DefaultTrackableEventHandler_t1588957063, ___m_NewStatus_6)); }
	inline int32_t get_m_NewStatus_6() const { return ___m_NewStatus_6; }
	inline int32_t* get_address_of_m_NewStatus_6() { return &___m_NewStatus_6; }
	inline void set_m_NewStatus_6(int32_t value)
	{
		___m_NewStatus_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEFAULTTRACKABLEEVENTHANDLER_T1588957063_H
#ifndef DIALOGUEMANAGER_T3506686710_H
#define DIALOGUEMANAGER_T3506686710_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DialogueManager
struct  DialogueManager_t3506686710  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.UI.Text DialogueManager::dialogueText
	Text_t1901882714 * ___dialogueText_4;
	// System.Collections.Generic.Queue`1<System.String> DialogueManager::sentences
	Queue_1_t1693710183 * ___sentences_5;

public:
	inline static int32_t get_offset_of_dialogueText_4() { return static_cast<int32_t>(offsetof(DialogueManager_t3506686710, ___dialogueText_4)); }
	inline Text_t1901882714 * get_dialogueText_4() const { return ___dialogueText_4; }
	inline Text_t1901882714 ** get_address_of_dialogueText_4() { return &___dialogueText_4; }
	inline void set_dialogueText_4(Text_t1901882714 * value)
	{
		___dialogueText_4 = value;
		Il2CppCodeGenWriteBarrier((&___dialogueText_4), value);
	}

	inline static int32_t get_offset_of_sentences_5() { return static_cast<int32_t>(offsetof(DialogueManager_t3506686710, ___sentences_5)); }
	inline Queue_1_t1693710183 * get_sentences_5() const { return ___sentences_5; }
	inline Queue_1_t1693710183 ** get_address_of_sentences_5() { return &___sentences_5; }
	inline void set_sentences_5(Queue_1_t1693710183 * value)
	{
		___sentences_5 = value;
		Il2CppCodeGenWriteBarrier((&___sentences_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DIALOGUEMANAGER_T3506686710_H
#ifndef DIALOGUETRIGGER_T3849820820_H
#define DIALOGUETRIGGER_T3849820820_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DialogueTrigger
struct  DialogueTrigger_t3849820820  : public MonoBehaviour_t3962482529
{
public:
	// Dialogue DialogueTrigger::dialogue
	Dialogue_t118236717 * ___dialogue_4;

public:
	inline static int32_t get_offset_of_dialogue_4() { return static_cast<int32_t>(offsetof(DialogueTrigger_t3849820820, ___dialogue_4)); }
	inline Dialogue_t118236717 * get_dialogue_4() const { return ___dialogue_4; }
	inline Dialogue_t118236717 ** get_address_of_dialogue_4() { return &___dialogue_4; }
	inline void set_dialogue_4(Dialogue_t118236717 * value)
	{
		___dialogue_4 = value;
		Il2CppCodeGenWriteBarrier((&___dialogue_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DIALOGUETRIGGER_T3849820820_H
#ifndef EVENTLISTENER_T3984233865_H
#define EVENTLISTENER_T3984233865_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EventListener
struct  EventListener_t3984233865  : public MonoBehaviour_t3962482529
{
public:
	// System.Collections.Generic.List`1<GameEvent> EventListener::GameEvent
	List_1_t1246685581 * ___GameEvent_4;
	// System.Collections.Generic.List`1<UnityEngine.Events.UnityEvent> EventListener::Response
	List_1_t4053343389 * ___Response_5;

public:
	inline static int32_t get_offset_of_GameEvent_4() { return static_cast<int32_t>(offsetof(EventListener_t3984233865, ___GameEvent_4)); }
	inline List_1_t1246685581 * get_GameEvent_4() const { return ___GameEvent_4; }
	inline List_1_t1246685581 ** get_address_of_GameEvent_4() { return &___GameEvent_4; }
	inline void set_GameEvent_4(List_1_t1246685581 * value)
	{
		___GameEvent_4 = value;
		Il2CppCodeGenWriteBarrier((&___GameEvent_4), value);
	}

	inline static int32_t get_offset_of_Response_5() { return static_cast<int32_t>(offsetof(EventListener_t3984233865, ___Response_5)); }
	inline List_1_t4053343389 * get_Response_5() const { return ___Response_5; }
	inline List_1_t4053343389 ** get_address_of_Response_5() { return &___Response_5; }
	inline void set_Response_5(List_1_t4053343389 * value)
	{
		___Response_5 = value;
		Il2CppCodeGenWriteBarrier((&___Response_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EVENTLISTENER_T3984233865_H
#ifndef FPSCONTROLLER_T1027197541_H
#define FPSCONTROLLER_T1027197541_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FPSController
struct  FPSController_t1027197541  : public MonoBehaviour_t3962482529
{
public:
	// System.Int32 FPSController::targetFPS
	int32_t ___targetFPS_4;
	// System.Single FPSController::frequency
	float ___frequency_5;
	// System.String FPSController::fps
	String_t* ___fps_6;

public:
	inline static int32_t get_offset_of_targetFPS_4() { return static_cast<int32_t>(offsetof(FPSController_t1027197541, ___targetFPS_4)); }
	inline int32_t get_targetFPS_4() const { return ___targetFPS_4; }
	inline int32_t* get_address_of_targetFPS_4() { return &___targetFPS_4; }
	inline void set_targetFPS_4(int32_t value)
	{
		___targetFPS_4 = value;
	}

	inline static int32_t get_offset_of_frequency_5() { return static_cast<int32_t>(offsetof(FPSController_t1027197541, ___frequency_5)); }
	inline float get_frequency_5() const { return ___frequency_5; }
	inline float* get_address_of_frequency_5() { return &___frequency_5; }
	inline void set_frequency_5(float value)
	{
		___frequency_5 = value;
	}

	inline static int32_t get_offset_of_fps_6() { return static_cast<int32_t>(offsetof(FPSController_t1027197541, ___fps_6)); }
	inline String_t* get_fps_6() const { return ___fps_6; }
	inline String_t** get_address_of_fps_6() { return &___fps_6; }
	inline void set_fps_6(String_t* value)
	{
		___fps_6 = value;
		Il2CppCodeGenWriteBarrier((&___fps_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FPSCONTROLLER_T1027197541_H
#ifndef FLICKER_T3044644596_H
#define FLICKER_T3044644596_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Flicker
struct  Flicker_t3044644596  : public MonoBehaviour_t3962482529
{
public:
	// FloatReference Flicker::m_flickerDelay
	FloatReference_t1374219879 * ___m_flickerDelay_4;
	// FloatReference Flicker::m_flickerAmount
	FloatReference_t1374219879 * ___m_flickerAmount_5;
	// FloatReference Flicker::m_startFlickerDelay
	FloatReference_t1374219879 * ___m_startFlickerDelay_6;
	// UnityEngine.MeshRenderer Flicker::m_meshRenderer
	MeshRenderer_t587009260 * ___m_meshRenderer_7;
	// System.Boolean Flicker::m_useShader
	bool ___m_useShader_8;
	// System.Single Flicker::m_amount
	float ___m_amount_9;
	// System.Single Flicker::m_timer
	float ___m_timer_10;
	// System.Single Flicker::m_startTimer
	float ___m_startTimer_11;
	// System.Boolean Flicker::m_startFlicker
	bool ___m_startFlicker_12;

public:
	inline static int32_t get_offset_of_m_flickerDelay_4() { return static_cast<int32_t>(offsetof(Flicker_t3044644596, ___m_flickerDelay_4)); }
	inline FloatReference_t1374219879 * get_m_flickerDelay_4() const { return ___m_flickerDelay_4; }
	inline FloatReference_t1374219879 ** get_address_of_m_flickerDelay_4() { return &___m_flickerDelay_4; }
	inline void set_m_flickerDelay_4(FloatReference_t1374219879 * value)
	{
		___m_flickerDelay_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_flickerDelay_4), value);
	}

	inline static int32_t get_offset_of_m_flickerAmount_5() { return static_cast<int32_t>(offsetof(Flicker_t3044644596, ___m_flickerAmount_5)); }
	inline FloatReference_t1374219879 * get_m_flickerAmount_5() const { return ___m_flickerAmount_5; }
	inline FloatReference_t1374219879 ** get_address_of_m_flickerAmount_5() { return &___m_flickerAmount_5; }
	inline void set_m_flickerAmount_5(FloatReference_t1374219879 * value)
	{
		___m_flickerAmount_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_flickerAmount_5), value);
	}

	inline static int32_t get_offset_of_m_startFlickerDelay_6() { return static_cast<int32_t>(offsetof(Flicker_t3044644596, ___m_startFlickerDelay_6)); }
	inline FloatReference_t1374219879 * get_m_startFlickerDelay_6() const { return ___m_startFlickerDelay_6; }
	inline FloatReference_t1374219879 ** get_address_of_m_startFlickerDelay_6() { return &___m_startFlickerDelay_6; }
	inline void set_m_startFlickerDelay_6(FloatReference_t1374219879 * value)
	{
		___m_startFlickerDelay_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_startFlickerDelay_6), value);
	}

	inline static int32_t get_offset_of_m_meshRenderer_7() { return static_cast<int32_t>(offsetof(Flicker_t3044644596, ___m_meshRenderer_7)); }
	inline MeshRenderer_t587009260 * get_m_meshRenderer_7() const { return ___m_meshRenderer_7; }
	inline MeshRenderer_t587009260 ** get_address_of_m_meshRenderer_7() { return &___m_meshRenderer_7; }
	inline void set_m_meshRenderer_7(MeshRenderer_t587009260 * value)
	{
		___m_meshRenderer_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_meshRenderer_7), value);
	}

	inline static int32_t get_offset_of_m_useShader_8() { return static_cast<int32_t>(offsetof(Flicker_t3044644596, ___m_useShader_8)); }
	inline bool get_m_useShader_8() const { return ___m_useShader_8; }
	inline bool* get_address_of_m_useShader_8() { return &___m_useShader_8; }
	inline void set_m_useShader_8(bool value)
	{
		___m_useShader_8 = value;
	}

	inline static int32_t get_offset_of_m_amount_9() { return static_cast<int32_t>(offsetof(Flicker_t3044644596, ___m_amount_9)); }
	inline float get_m_amount_9() const { return ___m_amount_9; }
	inline float* get_address_of_m_amount_9() { return &___m_amount_9; }
	inline void set_m_amount_9(float value)
	{
		___m_amount_9 = value;
	}

	inline static int32_t get_offset_of_m_timer_10() { return static_cast<int32_t>(offsetof(Flicker_t3044644596, ___m_timer_10)); }
	inline float get_m_timer_10() const { return ___m_timer_10; }
	inline float* get_address_of_m_timer_10() { return &___m_timer_10; }
	inline void set_m_timer_10(float value)
	{
		___m_timer_10 = value;
	}

	inline static int32_t get_offset_of_m_startTimer_11() { return static_cast<int32_t>(offsetof(Flicker_t3044644596, ___m_startTimer_11)); }
	inline float get_m_startTimer_11() const { return ___m_startTimer_11; }
	inline float* get_address_of_m_startTimer_11() { return &___m_startTimer_11; }
	inline void set_m_startTimer_11(float value)
	{
		___m_startTimer_11 = value;
	}

	inline static int32_t get_offset_of_m_startFlicker_12() { return static_cast<int32_t>(offsetof(Flicker_t3044644596, ___m_startFlicker_12)); }
	inline bool get_m_startFlicker_12() const { return ___m_startFlicker_12; }
	inline bool* get_address_of_m_startFlicker_12() { return &___m_startFlicker_12; }
	inline void set_m_startFlicker_12(bool value)
	{
		___m_startFlicker_12 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FLICKER_T3044644596_H
#ifndef FOLLOWCAMERA_T4234496323_H
#define FOLLOWCAMERA_T4234496323_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FollowCamera
struct  FollowCamera_t4234496323  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Transform FollowCamera::m_cameraTransform
	Transform_t3600365921 * ___m_cameraTransform_4;
	// UnityEngine.Vector3 FollowCamera::m_offset
	Vector3_t3722313464  ___m_offset_5;

public:
	inline static int32_t get_offset_of_m_cameraTransform_4() { return static_cast<int32_t>(offsetof(FollowCamera_t4234496323, ___m_cameraTransform_4)); }
	inline Transform_t3600365921 * get_m_cameraTransform_4() const { return ___m_cameraTransform_4; }
	inline Transform_t3600365921 ** get_address_of_m_cameraTransform_4() { return &___m_cameraTransform_4; }
	inline void set_m_cameraTransform_4(Transform_t3600365921 * value)
	{
		___m_cameraTransform_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_cameraTransform_4), value);
	}

	inline static int32_t get_offset_of_m_offset_5() { return static_cast<int32_t>(offsetof(FollowCamera_t4234496323, ___m_offset_5)); }
	inline Vector3_t3722313464  get_m_offset_5() const { return ___m_offset_5; }
	inline Vector3_t3722313464 * get_address_of_m_offset_5() { return &___m_offset_5; }
	inline void set_m_offset_5(Vector3_t3722313464  value)
	{
		___m_offset_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FOLLOWCAMERA_T4234496323_H
#ifndef HITDETECTION_T1652923020_H
#define HITDETECTION_T1652923020_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HitDetection
struct  HitDetection_t1652923020  : public MonoBehaviour_t3962482529
{
public:
	// GameEvent HitDetection::m_bubblePopEvent
	GameEvent_t4069578135 * ___m_bubblePopEvent_4;
	// GameEvent HitDetection::m_freezePopEvent
	GameEvent_t4069578135 * ___m_freezePopEvent_5;
	// GameEvent HitDetection::m_upAssignmentEvent
	GameEvent_t4069578135 * ___m_upAssignmentEvent_6;
	// GameEvent HitDetection::m_killEnemy
	GameEvent_t4069578135 * ___m_killEnemy_7;
	// FloatReference HitDetection::m_energy
	FloatReference_t1374219879 * ___m_energy_8;
	// FloatReference HitDetection::m_maxEnergy
	FloatReference_t1374219879 * ___m_maxEnergy_9;
	// UnityEngine.RaycastHit HitDetection::m_hit
	RaycastHit_t1056001966  ___m_hit_10;
	// UnityEngine.Ray HitDetection::m_ray
	Ray_t3785851493  ___m_ray_11;
	// System.Boolean HitDetection::m_DGADpressAssignment
	bool ___m_DGADpressAssignment_12;
	// GameEvent HitDetection::m_resetPenType
	GameEvent_t4069578135 * ___m_resetPenType_13;
	// GameEvent HitDetection::m_resetPenTypeUI
	GameEvent_t4069578135 * ___m_resetPenTypeUI_14;

public:
	inline static int32_t get_offset_of_m_bubblePopEvent_4() { return static_cast<int32_t>(offsetof(HitDetection_t1652923020, ___m_bubblePopEvent_4)); }
	inline GameEvent_t4069578135 * get_m_bubblePopEvent_4() const { return ___m_bubblePopEvent_4; }
	inline GameEvent_t4069578135 ** get_address_of_m_bubblePopEvent_4() { return &___m_bubblePopEvent_4; }
	inline void set_m_bubblePopEvent_4(GameEvent_t4069578135 * value)
	{
		___m_bubblePopEvent_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_bubblePopEvent_4), value);
	}

	inline static int32_t get_offset_of_m_freezePopEvent_5() { return static_cast<int32_t>(offsetof(HitDetection_t1652923020, ___m_freezePopEvent_5)); }
	inline GameEvent_t4069578135 * get_m_freezePopEvent_5() const { return ___m_freezePopEvent_5; }
	inline GameEvent_t4069578135 ** get_address_of_m_freezePopEvent_5() { return &___m_freezePopEvent_5; }
	inline void set_m_freezePopEvent_5(GameEvent_t4069578135 * value)
	{
		___m_freezePopEvent_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_freezePopEvent_5), value);
	}

	inline static int32_t get_offset_of_m_upAssignmentEvent_6() { return static_cast<int32_t>(offsetof(HitDetection_t1652923020, ___m_upAssignmentEvent_6)); }
	inline GameEvent_t4069578135 * get_m_upAssignmentEvent_6() const { return ___m_upAssignmentEvent_6; }
	inline GameEvent_t4069578135 ** get_address_of_m_upAssignmentEvent_6() { return &___m_upAssignmentEvent_6; }
	inline void set_m_upAssignmentEvent_6(GameEvent_t4069578135 * value)
	{
		___m_upAssignmentEvent_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_upAssignmentEvent_6), value);
	}

	inline static int32_t get_offset_of_m_killEnemy_7() { return static_cast<int32_t>(offsetof(HitDetection_t1652923020, ___m_killEnemy_7)); }
	inline GameEvent_t4069578135 * get_m_killEnemy_7() const { return ___m_killEnemy_7; }
	inline GameEvent_t4069578135 ** get_address_of_m_killEnemy_7() { return &___m_killEnemy_7; }
	inline void set_m_killEnemy_7(GameEvent_t4069578135 * value)
	{
		___m_killEnemy_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_killEnemy_7), value);
	}

	inline static int32_t get_offset_of_m_energy_8() { return static_cast<int32_t>(offsetof(HitDetection_t1652923020, ___m_energy_8)); }
	inline FloatReference_t1374219879 * get_m_energy_8() const { return ___m_energy_8; }
	inline FloatReference_t1374219879 ** get_address_of_m_energy_8() { return &___m_energy_8; }
	inline void set_m_energy_8(FloatReference_t1374219879 * value)
	{
		___m_energy_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_energy_8), value);
	}

	inline static int32_t get_offset_of_m_maxEnergy_9() { return static_cast<int32_t>(offsetof(HitDetection_t1652923020, ___m_maxEnergy_9)); }
	inline FloatReference_t1374219879 * get_m_maxEnergy_9() const { return ___m_maxEnergy_9; }
	inline FloatReference_t1374219879 ** get_address_of_m_maxEnergy_9() { return &___m_maxEnergy_9; }
	inline void set_m_maxEnergy_9(FloatReference_t1374219879 * value)
	{
		___m_maxEnergy_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_maxEnergy_9), value);
	}

	inline static int32_t get_offset_of_m_hit_10() { return static_cast<int32_t>(offsetof(HitDetection_t1652923020, ___m_hit_10)); }
	inline RaycastHit_t1056001966  get_m_hit_10() const { return ___m_hit_10; }
	inline RaycastHit_t1056001966 * get_address_of_m_hit_10() { return &___m_hit_10; }
	inline void set_m_hit_10(RaycastHit_t1056001966  value)
	{
		___m_hit_10 = value;
	}

	inline static int32_t get_offset_of_m_ray_11() { return static_cast<int32_t>(offsetof(HitDetection_t1652923020, ___m_ray_11)); }
	inline Ray_t3785851493  get_m_ray_11() const { return ___m_ray_11; }
	inline Ray_t3785851493 * get_address_of_m_ray_11() { return &___m_ray_11; }
	inline void set_m_ray_11(Ray_t3785851493  value)
	{
		___m_ray_11 = value;
	}

	inline static int32_t get_offset_of_m_DGADpressAssignment_12() { return static_cast<int32_t>(offsetof(HitDetection_t1652923020, ___m_DGADpressAssignment_12)); }
	inline bool get_m_DGADpressAssignment_12() const { return ___m_DGADpressAssignment_12; }
	inline bool* get_address_of_m_DGADpressAssignment_12() { return &___m_DGADpressAssignment_12; }
	inline void set_m_DGADpressAssignment_12(bool value)
	{
		___m_DGADpressAssignment_12 = value;
	}

	inline static int32_t get_offset_of_m_resetPenType_13() { return static_cast<int32_t>(offsetof(HitDetection_t1652923020, ___m_resetPenType_13)); }
	inline GameEvent_t4069578135 * get_m_resetPenType_13() const { return ___m_resetPenType_13; }
	inline GameEvent_t4069578135 ** get_address_of_m_resetPenType_13() { return &___m_resetPenType_13; }
	inline void set_m_resetPenType_13(GameEvent_t4069578135 * value)
	{
		___m_resetPenType_13 = value;
		Il2CppCodeGenWriteBarrier((&___m_resetPenType_13), value);
	}

	inline static int32_t get_offset_of_m_resetPenTypeUI_14() { return static_cast<int32_t>(offsetof(HitDetection_t1652923020, ___m_resetPenTypeUI_14)); }
	inline GameEvent_t4069578135 * get_m_resetPenTypeUI_14() const { return ___m_resetPenTypeUI_14; }
	inline GameEvent_t4069578135 ** get_address_of_m_resetPenTypeUI_14() { return &___m_resetPenTypeUI_14; }
	inline void set_m_resetPenTypeUI_14(GameEvent_t4069578135 * value)
	{
		___m_resetPenTypeUI_14 = value;
		Il2CppCodeGenWriteBarrier((&___m_resetPenTypeUI_14), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HITDETECTION_T1652923020_H
#ifndef IMAGEDETECTED_T3895480982_H
#define IMAGEDETECTED_T3895480982_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ImageDetected
struct  ImageDetected_t3895480982  : public MonoBehaviour_t3962482529
{
public:
	// System.Collections.Generic.List`1<UnityEngine.GameObject> ImageDetected::targetScenes
	List_1_t2585711361 * ___targetScenes_4;
	// IGameLogic ImageDetected::targetGameLogic
	IGameLogic_t3343192399 * ___targetGameLogic_5;
	// GameEvent ImageDetected::m_ImageDetected
	GameEvent_t4069578135 * ___m_ImageDetected_6;
	// GameEvent ImageDetected::m_GlobalImageDetected
	GameEvent_t4069578135 * ___m_GlobalImageDetected_7;
	// GameEvent ImageDetected::m_ImageLost
	GameEvent_t4069578135 * ___m_ImageLost_8;
	// GameEvent ImageDetected::m_GlobalImageLost
	GameEvent_t4069578135 * ___m_GlobalImageLost_9;
	// Vuforia.TrackableBehaviour ImageDetected::mTrackableBehaviour
	TrackableBehaviour_t1113559212 * ___mTrackableBehaviour_10;

public:
	inline static int32_t get_offset_of_targetScenes_4() { return static_cast<int32_t>(offsetof(ImageDetected_t3895480982, ___targetScenes_4)); }
	inline List_1_t2585711361 * get_targetScenes_4() const { return ___targetScenes_4; }
	inline List_1_t2585711361 ** get_address_of_targetScenes_4() { return &___targetScenes_4; }
	inline void set_targetScenes_4(List_1_t2585711361 * value)
	{
		___targetScenes_4 = value;
		Il2CppCodeGenWriteBarrier((&___targetScenes_4), value);
	}

	inline static int32_t get_offset_of_targetGameLogic_5() { return static_cast<int32_t>(offsetof(ImageDetected_t3895480982, ___targetGameLogic_5)); }
	inline IGameLogic_t3343192399 * get_targetGameLogic_5() const { return ___targetGameLogic_5; }
	inline IGameLogic_t3343192399 ** get_address_of_targetGameLogic_5() { return &___targetGameLogic_5; }
	inline void set_targetGameLogic_5(IGameLogic_t3343192399 * value)
	{
		___targetGameLogic_5 = value;
		Il2CppCodeGenWriteBarrier((&___targetGameLogic_5), value);
	}

	inline static int32_t get_offset_of_m_ImageDetected_6() { return static_cast<int32_t>(offsetof(ImageDetected_t3895480982, ___m_ImageDetected_6)); }
	inline GameEvent_t4069578135 * get_m_ImageDetected_6() const { return ___m_ImageDetected_6; }
	inline GameEvent_t4069578135 ** get_address_of_m_ImageDetected_6() { return &___m_ImageDetected_6; }
	inline void set_m_ImageDetected_6(GameEvent_t4069578135 * value)
	{
		___m_ImageDetected_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_ImageDetected_6), value);
	}

	inline static int32_t get_offset_of_m_GlobalImageDetected_7() { return static_cast<int32_t>(offsetof(ImageDetected_t3895480982, ___m_GlobalImageDetected_7)); }
	inline GameEvent_t4069578135 * get_m_GlobalImageDetected_7() const { return ___m_GlobalImageDetected_7; }
	inline GameEvent_t4069578135 ** get_address_of_m_GlobalImageDetected_7() { return &___m_GlobalImageDetected_7; }
	inline void set_m_GlobalImageDetected_7(GameEvent_t4069578135 * value)
	{
		___m_GlobalImageDetected_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_GlobalImageDetected_7), value);
	}

	inline static int32_t get_offset_of_m_ImageLost_8() { return static_cast<int32_t>(offsetof(ImageDetected_t3895480982, ___m_ImageLost_8)); }
	inline GameEvent_t4069578135 * get_m_ImageLost_8() const { return ___m_ImageLost_8; }
	inline GameEvent_t4069578135 ** get_address_of_m_ImageLost_8() { return &___m_ImageLost_8; }
	inline void set_m_ImageLost_8(GameEvent_t4069578135 * value)
	{
		___m_ImageLost_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_ImageLost_8), value);
	}

	inline static int32_t get_offset_of_m_GlobalImageLost_9() { return static_cast<int32_t>(offsetof(ImageDetected_t3895480982, ___m_GlobalImageLost_9)); }
	inline GameEvent_t4069578135 * get_m_GlobalImageLost_9() const { return ___m_GlobalImageLost_9; }
	inline GameEvent_t4069578135 ** get_address_of_m_GlobalImageLost_9() { return &___m_GlobalImageLost_9; }
	inline void set_m_GlobalImageLost_9(GameEvent_t4069578135 * value)
	{
		___m_GlobalImageLost_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_GlobalImageLost_9), value);
	}

	inline static int32_t get_offset_of_mTrackableBehaviour_10() { return static_cast<int32_t>(offsetof(ImageDetected_t3895480982, ___mTrackableBehaviour_10)); }
	inline TrackableBehaviour_t1113559212 * get_mTrackableBehaviour_10() const { return ___mTrackableBehaviour_10; }
	inline TrackableBehaviour_t1113559212 ** get_address_of_mTrackableBehaviour_10() { return &___mTrackableBehaviour_10; }
	inline void set_mTrackableBehaviour_10(TrackableBehaviour_t1113559212 * value)
	{
		___mTrackableBehaviour_10 = value;
		Il2CppCodeGenWriteBarrier((&___mTrackableBehaviour_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IMAGEDETECTED_T3895480982_H
#ifndef MOVER_T2250641681_H
#define MOVER_T2250641681_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mover
struct  Mover_t2250641681  : public MonoBehaviour_t3962482529
{
public:
	// FloatReference Mover::m_maxDistance
	FloatReference_t1374219879 * ___m_maxDistance_4;
	// UnityEngine.Vector3 Mover::m_startingPos
	Vector3_t3722313464  ___m_startingPos_5;
	// UnityEngine.Vector3 Mover::m_endPos
	Vector3_t3722313464  ___m_endPos_6;
	// System.Single Mover::m_movingSpeed
	float ___m_movingSpeed_7;
	// System.Single Mover::m_finalSpeed
	float ___m_finalSpeed_8;
	// System.Boolean Mover::m_isReachedEnd
	bool ___m_isReachedEnd_9;

public:
	inline static int32_t get_offset_of_m_maxDistance_4() { return static_cast<int32_t>(offsetof(Mover_t2250641681, ___m_maxDistance_4)); }
	inline FloatReference_t1374219879 * get_m_maxDistance_4() const { return ___m_maxDistance_4; }
	inline FloatReference_t1374219879 ** get_address_of_m_maxDistance_4() { return &___m_maxDistance_4; }
	inline void set_m_maxDistance_4(FloatReference_t1374219879 * value)
	{
		___m_maxDistance_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_maxDistance_4), value);
	}

	inline static int32_t get_offset_of_m_startingPos_5() { return static_cast<int32_t>(offsetof(Mover_t2250641681, ___m_startingPos_5)); }
	inline Vector3_t3722313464  get_m_startingPos_5() const { return ___m_startingPos_5; }
	inline Vector3_t3722313464 * get_address_of_m_startingPos_5() { return &___m_startingPos_5; }
	inline void set_m_startingPos_5(Vector3_t3722313464  value)
	{
		___m_startingPos_5 = value;
	}

	inline static int32_t get_offset_of_m_endPos_6() { return static_cast<int32_t>(offsetof(Mover_t2250641681, ___m_endPos_6)); }
	inline Vector3_t3722313464  get_m_endPos_6() const { return ___m_endPos_6; }
	inline Vector3_t3722313464 * get_address_of_m_endPos_6() { return &___m_endPos_6; }
	inline void set_m_endPos_6(Vector3_t3722313464  value)
	{
		___m_endPos_6 = value;
	}

	inline static int32_t get_offset_of_m_movingSpeed_7() { return static_cast<int32_t>(offsetof(Mover_t2250641681, ___m_movingSpeed_7)); }
	inline float get_m_movingSpeed_7() const { return ___m_movingSpeed_7; }
	inline float* get_address_of_m_movingSpeed_7() { return &___m_movingSpeed_7; }
	inline void set_m_movingSpeed_7(float value)
	{
		___m_movingSpeed_7 = value;
	}

	inline static int32_t get_offset_of_m_finalSpeed_8() { return static_cast<int32_t>(offsetof(Mover_t2250641681, ___m_finalSpeed_8)); }
	inline float get_m_finalSpeed_8() const { return ___m_finalSpeed_8; }
	inline float* get_address_of_m_finalSpeed_8() { return &___m_finalSpeed_8; }
	inline void set_m_finalSpeed_8(float value)
	{
		___m_finalSpeed_8 = value;
	}

	inline static int32_t get_offset_of_m_isReachedEnd_9() { return static_cast<int32_t>(offsetof(Mover_t2250641681, ___m_isReachedEnd_9)); }
	inline bool get_m_isReachedEnd_9() const { return ___m_isReachedEnd_9; }
	inline bool* get_address_of_m_isReachedEnd_9() { return &___m_isReachedEnd_9; }
	inline void set_m_isReachedEnd_9(bool value)
	{
		___m_isReachedEnd_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MOVER_T2250641681_H
#ifndef ONTOUCHSPAWN_T2434404655_H
#define ONTOUCHSPAWN_T2434404655_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// OnTouchSpawn
struct  OnTouchSpawn_t2434404655  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.GameObject OnTouchSpawn::objectToSpawn
	GameObject_t1113636619 * ___objectToSpawn_4;
	// System.Single OnTouchSpawn::objectLifetime
	float ___objectLifetime_5;
	// UnityEngine.Events.UnityEvent OnTouchSpawn::eventsToFire
	UnityEvent_t2581268647 * ___eventsToFire_6;
	// System.Int32 OnTouchSpawn::clicksToActivate
	int32_t ___clicksToActivate_7;
	// System.Int32 OnTouchSpawn::accumulatedClicks
	int32_t ___accumulatedClicks_8;

public:
	inline static int32_t get_offset_of_objectToSpawn_4() { return static_cast<int32_t>(offsetof(OnTouchSpawn_t2434404655, ___objectToSpawn_4)); }
	inline GameObject_t1113636619 * get_objectToSpawn_4() const { return ___objectToSpawn_4; }
	inline GameObject_t1113636619 ** get_address_of_objectToSpawn_4() { return &___objectToSpawn_4; }
	inline void set_objectToSpawn_4(GameObject_t1113636619 * value)
	{
		___objectToSpawn_4 = value;
		Il2CppCodeGenWriteBarrier((&___objectToSpawn_4), value);
	}

	inline static int32_t get_offset_of_objectLifetime_5() { return static_cast<int32_t>(offsetof(OnTouchSpawn_t2434404655, ___objectLifetime_5)); }
	inline float get_objectLifetime_5() const { return ___objectLifetime_5; }
	inline float* get_address_of_objectLifetime_5() { return &___objectLifetime_5; }
	inline void set_objectLifetime_5(float value)
	{
		___objectLifetime_5 = value;
	}

	inline static int32_t get_offset_of_eventsToFire_6() { return static_cast<int32_t>(offsetof(OnTouchSpawn_t2434404655, ___eventsToFire_6)); }
	inline UnityEvent_t2581268647 * get_eventsToFire_6() const { return ___eventsToFire_6; }
	inline UnityEvent_t2581268647 ** get_address_of_eventsToFire_6() { return &___eventsToFire_6; }
	inline void set_eventsToFire_6(UnityEvent_t2581268647 * value)
	{
		___eventsToFire_6 = value;
		Il2CppCodeGenWriteBarrier((&___eventsToFire_6), value);
	}

	inline static int32_t get_offset_of_clicksToActivate_7() { return static_cast<int32_t>(offsetof(OnTouchSpawn_t2434404655, ___clicksToActivate_7)); }
	inline int32_t get_clicksToActivate_7() const { return ___clicksToActivate_7; }
	inline int32_t* get_address_of_clicksToActivate_7() { return &___clicksToActivate_7; }
	inline void set_clicksToActivate_7(int32_t value)
	{
		___clicksToActivate_7 = value;
	}

	inline static int32_t get_offset_of_accumulatedClicks_8() { return static_cast<int32_t>(offsetof(OnTouchSpawn_t2434404655, ___accumulatedClicks_8)); }
	inline int32_t get_accumulatedClicks_8() const { return ___accumulatedClicks_8; }
	inline int32_t* get_address_of_accumulatedClicks_8() { return &___accumulatedClicks_8; }
	inline void set_accumulatedClicks_8(int32_t value)
	{
		___accumulatedClicks_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONTOUCHSPAWN_T2434404655_H
#ifndef PARTICLESPAWNER_T2092424095_H
#define PARTICLESPAWNER_T2092424095_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ParticleSpawner
struct  ParticleSpawner_t2092424095  : public MonoBehaviour_t3962482529
{
public:
	// System.Collections.Generic.List`1<UnityEngine.GameObject> ParticleSpawner::m_particles
	List_1_t2585711361 * ___m_particles_4;
	// FloatReference ParticleSpawner::m_delayTimer
	FloatReference_t1374219879 * ___m_delayTimer_5;
	// System.Single ParticleSpawner::m_timer
	float ___m_timer_6;
	// System.Single ParticleSpawner::m_totalParticleTime
	float ___m_totalParticleTime_7;

public:
	inline static int32_t get_offset_of_m_particles_4() { return static_cast<int32_t>(offsetof(ParticleSpawner_t2092424095, ___m_particles_4)); }
	inline List_1_t2585711361 * get_m_particles_4() const { return ___m_particles_4; }
	inline List_1_t2585711361 ** get_address_of_m_particles_4() { return &___m_particles_4; }
	inline void set_m_particles_4(List_1_t2585711361 * value)
	{
		___m_particles_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_particles_4), value);
	}

	inline static int32_t get_offset_of_m_delayTimer_5() { return static_cast<int32_t>(offsetof(ParticleSpawner_t2092424095, ___m_delayTimer_5)); }
	inline FloatReference_t1374219879 * get_m_delayTimer_5() const { return ___m_delayTimer_5; }
	inline FloatReference_t1374219879 ** get_address_of_m_delayTimer_5() { return &___m_delayTimer_5; }
	inline void set_m_delayTimer_5(FloatReference_t1374219879 * value)
	{
		___m_delayTimer_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_delayTimer_5), value);
	}

	inline static int32_t get_offset_of_m_timer_6() { return static_cast<int32_t>(offsetof(ParticleSpawner_t2092424095, ___m_timer_6)); }
	inline float get_m_timer_6() const { return ___m_timer_6; }
	inline float* get_address_of_m_timer_6() { return &___m_timer_6; }
	inline void set_m_timer_6(float value)
	{
		___m_timer_6 = value;
	}

	inline static int32_t get_offset_of_m_totalParticleTime_7() { return static_cast<int32_t>(offsetof(ParticleSpawner_t2092424095, ___m_totalParticleTime_7)); }
	inline float get_m_totalParticleTime_7() const { return ___m_totalParticleTime_7; }
	inline float* get_address_of_m_totalParticleTime_7() { return &___m_totalParticleTime_7; }
	inline void set_m_totalParticleTime_7(float value)
	{
		___m_totalParticleTime_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PARTICLESPAWNER_T2092424095_H
#ifndef SCALER_T1764693111_H
#define SCALER_T1764693111_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Scaler
struct  Scaler_t1764693111  : public MonoBehaviour_t3962482529
{
public:
	// FloatReference Scaler::m_maxSize
	FloatReference_t1374219879 * ___m_maxSize_4;
	// System.Single Scaler::m_scaleSpeed
	float ___m_scaleSpeed_5;

public:
	inline static int32_t get_offset_of_m_maxSize_4() { return static_cast<int32_t>(offsetof(Scaler_t1764693111, ___m_maxSize_4)); }
	inline FloatReference_t1374219879 * get_m_maxSize_4() const { return ___m_maxSize_4; }
	inline FloatReference_t1374219879 ** get_address_of_m_maxSize_4() { return &___m_maxSize_4; }
	inline void set_m_maxSize_4(FloatReference_t1374219879 * value)
	{
		___m_maxSize_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_maxSize_4), value);
	}

	inline static int32_t get_offset_of_m_scaleSpeed_5() { return static_cast<int32_t>(offsetof(Scaler_t1764693111, ___m_scaleSpeed_5)); }
	inline float get_m_scaleSpeed_5() const { return ___m_scaleSpeed_5; }
	inline float* get_address_of_m_scaleSpeed_5() { return &___m_scaleSpeed_5; }
	inline void set_m_scaleSpeed_5(float value)
	{
		___m_scaleSpeed_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCALER_T1764693111_H
#ifndef SCENETRANSITION_T1138091307_H
#define SCENETRANSITION_T1138091307_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SceneTransition
struct  SceneTransition_t1138091307  : public MonoBehaviour_t3962482529
{
public:
	// System.String SceneTransition::nextSceneName
	String_t* ___nextSceneName_4;
	// UnityEngine.Color SceneTransition::fadeColor
	Color_t2555686324  ___fadeColor_5;
	// System.Single SceneTransition::fadeTime
	float ___fadeTime_6;
	// System.Boolean SceneTransition::autoChange
	bool ___autoChange_7;
	// UnityEngine.UI.Image SceneTransition::fader
	Image_t2670269651 * ___fader_8;
	// System.Single SceneTransition::currAlpha
	float ___currAlpha_9;
	// System.Single SceneTransition::endAlpha
	float ___endAlpha_10;

public:
	inline static int32_t get_offset_of_nextSceneName_4() { return static_cast<int32_t>(offsetof(SceneTransition_t1138091307, ___nextSceneName_4)); }
	inline String_t* get_nextSceneName_4() const { return ___nextSceneName_4; }
	inline String_t** get_address_of_nextSceneName_4() { return &___nextSceneName_4; }
	inline void set_nextSceneName_4(String_t* value)
	{
		___nextSceneName_4 = value;
		Il2CppCodeGenWriteBarrier((&___nextSceneName_4), value);
	}

	inline static int32_t get_offset_of_fadeColor_5() { return static_cast<int32_t>(offsetof(SceneTransition_t1138091307, ___fadeColor_5)); }
	inline Color_t2555686324  get_fadeColor_5() const { return ___fadeColor_5; }
	inline Color_t2555686324 * get_address_of_fadeColor_5() { return &___fadeColor_5; }
	inline void set_fadeColor_5(Color_t2555686324  value)
	{
		___fadeColor_5 = value;
	}

	inline static int32_t get_offset_of_fadeTime_6() { return static_cast<int32_t>(offsetof(SceneTransition_t1138091307, ___fadeTime_6)); }
	inline float get_fadeTime_6() const { return ___fadeTime_6; }
	inline float* get_address_of_fadeTime_6() { return &___fadeTime_6; }
	inline void set_fadeTime_6(float value)
	{
		___fadeTime_6 = value;
	}

	inline static int32_t get_offset_of_autoChange_7() { return static_cast<int32_t>(offsetof(SceneTransition_t1138091307, ___autoChange_7)); }
	inline bool get_autoChange_7() const { return ___autoChange_7; }
	inline bool* get_address_of_autoChange_7() { return &___autoChange_7; }
	inline void set_autoChange_7(bool value)
	{
		___autoChange_7 = value;
	}

	inline static int32_t get_offset_of_fader_8() { return static_cast<int32_t>(offsetof(SceneTransition_t1138091307, ___fader_8)); }
	inline Image_t2670269651 * get_fader_8() const { return ___fader_8; }
	inline Image_t2670269651 ** get_address_of_fader_8() { return &___fader_8; }
	inline void set_fader_8(Image_t2670269651 * value)
	{
		___fader_8 = value;
		Il2CppCodeGenWriteBarrier((&___fader_8), value);
	}

	inline static int32_t get_offset_of_currAlpha_9() { return static_cast<int32_t>(offsetof(SceneTransition_t1138091307, ___currAlpha_9)); }
	inline float get_currAlpha_9() const { return ___currAlpha_9; }
	inline float* get_address_of_currAlpha_9() { return &___currAlpha_9; }
	inline void set_currAlpha_9(float value)
	{
		___currAlpha_9 = value;
	}

	inline static int32_t get_offset_of_endAlpha_10() { return static_cast<int32_t>(offsetof(SceneTransition_t1138091307, ___endAlpha_10)); }
	inline float get_endAlpha_10() const { return ___endAlpha_10; }
	inline float* get_address_of_endAlpha_10() { return &___endAlpha_10; }
	inline void set_endAlpha_10(float value)
	{
		___endAlpha_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCENETRANSITION_T1138091307_H
#ifndef TEXTMESHEDITOR_T1187695439_H
#define TEXTMESHEDITOR_T1187695439_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TextMeshEditor
struct  TextMeshEditor_t1187695439  : public MonoBehaviour_t3962482529
{
public:
	// TMPro.TextMeshProUGUI TextMeshEditor::m_textMesh
	TextMeshProUGUI_t529313277 * ___m_textMesh_4;
	// IntReference TextMeshEditor::m_text
	IntReference_t2842414034 * ___m_text_5;

public:
	inline static int32_t get_offset_of_m_textMesh_4() { return static_cast<int32_t>(offsetof(TextMeshEditor_t1187695439, ___m_textMesh_4)); }
	inline TextMeshProUGUI_t529313277 * get_m_textMesh_4() const { return ___m_textMesh_4; }
	inline TextMeshProUGUI_t529313277 ** get_address_of_m_textMesh_4() { return &___m_textMesh_4; }
	inline void set_m_textMesh_4(TextMeshProUGUI_t529313277 * value)
	{
		___m_textMesh_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_textMesh_4), value);
	}

	inline static int32_t get_offset_of_m_text_5() { return static_cast<int32_t>(offsetof(TextMeshEditor_t1187695439, ___m_text_5)); }
	inline IntReference_t2842414034 * get_m_text_5() const { return ___m_text_5; }
	inline IntReference_t2842414034 ** get_address_of_m_text_5() { return &___m_text_5; }
	inline void set_m_text_5(IntReference_t2842414034 * value)
	{
		___m_text_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_text_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTMESHEDITOR_T1187695439_H
#ifndef TEXTURESWITCH_T147972937_H
#define TEXTURESWITCH_T147972937_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TextureSwitch
struct  TextureSwitch_t147972937  : public MonoBehaviour_t3962482529
{
public:
	// FloatReference TextureSwitch::m_switchTimer
	FloatReference_t1374219879 * ___m_switchTimer_4;
	// System.Single TextureSwitch::m_timer
	float ___m_timer_5;
	// UnityEngine.MeshRenderer TextureSwitch::m_meshRenderer
	MeshRenderer_t587009260 * ___m_meshRenderer_6;
	// System.Boolean TextureSwitch::m_switch
	bool ___m_switch_7;

public:
	inline static int32_t get_offset_of_m_switchTimer_4() { return static_cast<int32_t>(offsetof(TextureSwitch_t147972937, ___m_switchTimer_4)); }
	inline FloatReference_t1374219879 * get_m_switchTimer_4() const { return ___m_switchTimer_4; }
	inline FloatReference_t1374219879 ** get_address_of_m_switchTimer_4() { return &___m_switchTimer_4; }
	inline void set_m_switchTimer_4(FloatReference_t1374219879 * value)
	{
		___m_switchTimer_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_switchTimer_4), value);
	}

	inline static int32_t get_offset_of_m_timer_5() { return static_cast<int32_t>(offsetof(TextureSwitch_t147972937, ___m_timer_5)); }
	inline float get_m_timer_5() const { return ___m_timer_5; }
	inline float* get_address_of_m_timer_5() { return &___m_timer_5; }
	inline void set_m_timer_5(float value)
	{
		___m_timer_5 = value;
	}

	inline static int32_t get_offset_of_m_meshRenderer_6() { return static_cast<int32_t>(offsetof(TextureSwitch_t147972937, ___m_meshRenderer_6)); }
	inline MeshRenderer_t587009260 * get_m_meshRenderer_6() const { return ___m_meshRenderer_6; }
	inline MeshRenderer_t587009260 ** get_address_of_m_meshRenderer_6() { return &___m_meshRenderer_6; }
	inline void set_m_meshRenderer_6(MeshRenderer_t587009260 * value)
	{
		___m_meshRenderer_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_meshRenderer_6), value);
	}

	inline static int32_t get_offset_of_m_switch_7() { return static_cast<int32_t>(offsetof(TextureSwitch_t147972937, ___m_switch_7)); }
	inline bool get_m_switch_7() const { return ___m_switch_7; }
	inline bool* get_address_of_m_switch_7() { return &___m_switch_7; }
	inline void set_m_switch_7(bool value)
	{
		___m_switch_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTURESWITCH_T147972937_H
#ifndef TWEENER_T3008954595_H
#define TWEENER_T3008954595_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Tweener
struct  Tweener_t3008954595  : public MonoBehaviour_t3962482529
{
public:
	// System.Boolean Tweener::startOnEnable
	bool ___startOnEnable_4;
	// System.Boolean Tweener::startRepeatOnEnable
	bool ___startRepeatOnEnable_5;
	// System.Boolean Tweener::disableOnEndTween
	bool ___disableOnEndTween_6;
	// System.Boolean Tweener::autoEndTween
	bool ___autoEndTween_7;
	// System.Boolean Tweener::setToStart
	bool ___setToStart_8;
	// System.Boolean Tweener::tweenPosition
	bool ___tweenPosition_9;
	// System.Boolean Tweener::tweenPositionRepeat
	bool ___tweenPositionRepeat_10;
	// UnityEngine.Vector3 Tweener::startPosition
	Vector3_t3722313464  ___startPosition_11;
	// UnityEngine.Vector3 Tweener::endPosition
	Vector3_t3722313464  ___endPosition_12;
	// UnityEngine.Vector3 Tweener::currPosition
	Vector3_t3722313464  ___currPosition_13;
	// DG.Tweening.Ease Tweener::positionEaseType
	int32_t ___positionEaseType_14;
	// System.Single Tweener::positionTweenDuration
	float ___positionTweenDuration_15;
	// UnityEngine.Transform Tweener::positionTweenTarget
	Transform_t3600365921 * ___positionTweenTarget_16;
	// System.Boolean Tweener::tweenRotation
	bool ___tweenRotation_17;
	// System.Boolean Tweener::tweenRotationRepeat
	bool ___tweenRotationRepeat_18;
	// UnityEngine.Vector3 Tweener::startRotation
	Vector3_t3722313464  ___startRotation_19;
	// UnityEngine.Vector3 Tweener::endRotation
	Vector3_t3722313464  ___endRotation_20;
	// UnityEngine.Vector3 Tweener::currRotation
	Vector3_t3722313464  ___currRotation_21;
	// DG.Tweening.Ease Tweener::rotationEaseType
	int32_t ___rotationEaseType_22;
	// System.Single Tweener::rotationTweenDuration
	float ___rotationTweenDuration_23;
	// UnityEngine.Transform Tweener::rotationTweenTarget
	Transform_t3600365921 * ___rotationTweenTarget_24;
	// System.Boolean Tweener::tweenScale
	bool ___tweenScale_25;
	// System.Boolean Tweener::tweenScaleRepeat
	bool ___tweenScaleRepeat_26;
	// UnityEngine.Vector3 Tweener::startScale
	Vector3_t3722313464  ___startScale_27;
	// UnityEngine.Vector3 Tweener::endScale
	Vector3_t3722313464  ___endScale_28;
	// UnityEngine.Vector3 Tweener::currScale
	Vector3_t3722313464  ___currScale_29;
	// DG.Tweening.Ease Tweener::scaleEaseType
	int32_t ___scaleEaseType_30;
	// System.Single Tweener::scaleTweenDuration
	float ___scaleTweenDuration_31;
	// UnityEngine.Transform Tweener::scaleTweenTarget
	Transform_t3600365921 * ___scaleTweenTarget_32;
	// System.Boolean Tweener::tweenColor
	bool ___tweenColor_33;
	// System.Boolean Tweener::tweenColorRepeat
	bool ___tweenColorRepeat_34;
	// UnityEngine.Color Tweener::startColor
	Color_t2555686324  ___startColor_35;
	// UnityEngine.Color Tweener::endColor
	Color_t2555686324  ___endColor_36;
	// UnityEngine.Color Tweener::currColor
	Color_t2555686324  ___currColor_37;
	// DG.Tweening.Ease Tweener::colorEaseType
	int32_t ___colorEaseType_38;
	// System.Single Tweener::colorTweenDuration
	float ___colorTweenDuration_39;
	// UnityEngine.UI.Image Tweener::colorTweenTarget
	Image_t2670269651 * ___colorTweenTarget_40;
	// System.Boolean Tweener::tweenAlpha
	bool ___tweenAlpha_41;
	// System.Boolean Tweener::tweenAlphaRepeat
	bool ___tweenAlphaRepeat_42;
	// System.Single Tweener::startAlpha
	float ___startAlpha_43;
	// System.Single Tweener::endAlpha
	float ___endAlpha_44;
	// System.Single Tweener::currAlpha
	float ___currAlpha_45;
	// DG.Tweening.Ease Tweener::alphaEaseType
	int32_t ___alphaEaseType_46;
	// System.Single Tweener::alphaTweenDuration
	float ___alphaTweenDuration_47;
	// UnityEngine.UI.Image Tweener::alphaTweenTarget
	Image_t2670269651 * ___alphaTweenTarget_48;

public:
	inline static int32_t get_offset_of_startOnEnable_4() { return static_cast<int32_t>(offsetof(Tweener_t3008954595, ___startOnEnable_4)); }
	inline bool get_startOnEnable_4() const { return ___startOnEnable_4; }
	inline bool* get_address_of_startOnEnable_4() { return &___startOnEnable_4; }
	inline void set_startOnEnable_4(bool value)
	{
		___startOnEnable_4 = value;
	}

	inline static int32_t get_offset_of_startRepeatOnEnable_5() { return static_cast<int32_t>(offsetof(Tweener_t3008954595, ___startRepeatOnEnable_5)); }
	inline bool get_startRepeatOnEnable_5() const { return ___startRepeatOnEnable_5; }
	inline bool* get_address_of_startRepeatOnEnable_5() { return &___startRepeatOnEnable_5; }
	inline void set_startRepeatOnEnable_5(bool value)
	{
		___startRepeatOnEnable_5 = value;
	}

	inline static int32_t get_offset_of_disableOnEndTween_6() { return static_cast<int32_t>(offsetof(Tweener_t3008954595, ___disableOnEndTween_6)); }
	inline bool get_disableOnEndTween_6() const { return ___disableOnEndTween_6; }
	inline bool* get_address_of_disableOnEndTween_6() { return &___disableOnEndTween_6; }
	inline void set_disableOnEndTween_6(bool value)
	{
		___disableOnEndTween_6 = value;
	}

	inline static int32_t get_offset_of_autoEndTween_7() { return static_cast<int32_t>(offsetof(Tweener_t3008954595, ___autoEndTween_7)); }
	inline bool get_autoEndTween_7() const { return ___autoEndTween_7; }
	inline bool* get_address_of_autoEndTween_7() { return &___autoEndTween_7; }
	inline void set_autoEndTween_7(bool value)
	{
		___autoEndTween_7 = value;
	}

	inline static int32_t get_offset_of_setToStart_8() { return static_cast<int32_t>(offsetof(Tweener_t3008954595, ___setToStart_8)); }
	inline bool get_setToStart_8() const { return ___setToStart_8; }
	inline bool* get_address_of_setToStart_8() { return &___setToStart_8; }
	inline void set_setToStart_8(bool value)
	{
		___setToStart_8 = value;
	}

	inline static int32_t get_offset_of_tweenPosition_9() { return static_cast<int32_t>(offsetof(Tweener_t3008954595, ___tweenPosition_9)); }
	inline bool get_tweenPosition_9() const { return ___tweenPosition_9; }
	inline bool* get_address_of_tweenPosition_9() { return &___tweenPosition_9; }
	inline void set_tweenPosition_9(bool value)
	{
		___tweenPosition_9 = value;
	}

	inline static int32_t get_offset_of_tweenPositionRepeat_10() { return static_cast<int32_t>(offsetof(Tweener_t3008954595, ___tweenPositionRepeat_10)); }
	inline bool get_tweenPositionRepeat_10() const { return ___tweenPositionRepeat_10; }
	inline bool* get_address_of_tweenPositionRepeat_10() { return &___tweenPositionRepeat_10; }
	inline void set_tweenPositionRepeat_10(bool value)
	{
		___tweenPositionRepeat_10 = value;
	}

	inline static int32_t get_offset_of_startPosition_11() { return static_cast<int32_t>(offsetof(Tweener_t3008954595, ___startPosition_11)); }
	inline Vector3_t3722313464  get_startPosition_11() const { return ___startPosition_11; }
	inline Vector3_t3722313464 * get_address_of_startPosition_11() { return &___startPosition_11; }
	inline void set_startPosition_11(Vector3_t3722313464  value)
	{
		___startPosition_11 = value;
	}

	inline static int32_t get_offset_of_endPosition_12() { return static_cast<int32_t>(offsetof(Tweener_t3008954595, ___endPosition_12)); }
	inline Vector3_t3722313464  get_endPosition_12() const { return ___endPosition_12; }
	inline Vector3_t3722313464 * get_address_of_endPosition_12() { return &___endPosition_12; }
	inline void set_endPosition_12(Vector3_t3722313464  value)
	{
		___endPosition_12 = value;
	}

	inline static int32_t get_offset_of_currPosition_13() { return static_cast<int32_t>(offsetof(Tweener_t3008954595, ___currPosition_13)); }
	inline Vector3_t3722313464  get_currPosition_13() const { return ___currPosition_13; }
	inline Vector3_t3722313464 * get_address_of_currPosition_13() { return &___currPosition_13; }
	inline void set_currPosition_13(Vector3_t3722313464  value)
	{
		___currPosition_13 = value;
	}

	inline static int32_t get_offset_of_positionEaseType_14() { return static_cast<int32_t>(offsetof(Tweener_t3008954595, ___positionEaseType_14)); }
	inline int32_t get_positionEaseType_14() const { return ___positionEaseType_14; }
	inline int32_t* get_address_of_positionEaseType_14() { return &___positionEaseType_14; }
	inline void set_positionEaseType_14(int32_t value)
	{
		___positionEaseType_14 = value;
	}

	inline static int32_t get_offset_of_positionTweenDuration_15() { return static_cast<int32_t>(offsetof(Tweener_t3008954595, ___positionTweenDuration_15)); }
	inline float get_positionTweenDuration_15() const { return ___positionTweenDuration_15; }
	inline float* get_address_of_positionTweenDuration_15() { return &___positionTweenDuration_15; }
	inline void set_positionTweenDuration_15(float value)
	{
		___positionTweenDuration_15 = value;
	}

	inline static int32_t get_offset_of_positionTweenTarget_16() { return static_cast<int32_t>(offsetof(Tweener_t3008954595, ___positionTweenTarget_16)); }
	inline Transform_t3600365921 * get_positionTweenTarget_16() const { return ___positionTweenTarget_16; }
	inline Transform_t3600365921 ** get_address_of_positionTweenTarget_16() { return &___positionTweenTarget_16; }
	inline void set_positionTweenTarget_16(Transform_t3600365921 * value)
	{
		___positionTweenTarget_16 = value;
		Il2CppCodeGenWriteBarrier((&___positionTweenTarget_16), value);
	}

	inline static int32_t get_offset_of_tweenRotation_17() { return static_cast<int32_t>(offsetof(Tweener_t3008954595, ___tweenRotation_17)); }
	inline bool get_tweenRotation_17() const { return ___tweenRotation_17; }
	inline bool* get_address_of_tweenRotation_17() { return &___tweenRotation_17; }
	inline void set_tweenRotation_17(bool value)
	{
		___tweenRotation_17 = value;
	}

	inline static int32_t get_offset_of_tweenRotationRepeat_18() { return static_cast<int32_t>(offsetof(Tweener_t3008954595, ___tweenRotationRepeat_18)); }
	inline bool get_tweenRotationRepeat_18() const { return ___tweenRotationRepeat_18; }
	inline bool* get_address_of_tweenRotationRepeat_18() { return &___tweenRotationRepeat_18; }
	inline void set_tweenRotationRepeat_18(bool value)
	{
		___tweenRotationRepeat_18 = value;
	}

	inline static int32_t get_offset_of_startRotation_19() { return static_cast<int32_t>(offsetof(Tweener_t3008954595, ___startRotation_19)); }
	inline Vector3_t3722313464  get_startRotation_19() const { return ___startRotation_19; }
	inline Vector3_t3722313464 * get_address_of_startRotation_19() { return &___startRotation_19; }
	inline void set_startRotation_19(Vector3_t3722313464  value)
	{
		___startRotation_19 = value;
	}

	inline static int32_t get_offset_of_endRotation_20() { return static_cast<int32_t>(offsetof(Tweener_t3008954595, ___endRotation_20)); }
	inline Vector3_t3722313464  get_endRotation_20() const { return ___endRotation_20; }
	inline Vector3_t3722313464 * get_address_of_endRotation_20() { return &___endRotation_20; }
	inline void set_endRotation_20(Vector3_t3722313464  value)
	{
		___endRotation_20 = value;
	}

	inline static int32_t get_offset_of_currRotation_21() { return static_cast<int32_t>(offsetof(Tweener_t3008954595, ___currRotation_21)); }
	inline Vector3_t3722313464  get_currRotation_21() const { return ___currRotation_21; }
	inline Vector3_t3722313464 * get_address_of_currRotation_21() { return &___currRotation_21; }
	inline void set_currRotation_21(Vector3_t3722313464  value)
	{
		___currRotation_21 = value;
	}

	inline static int32_t get_offset_of_rotationEaseType_22() { return static_cast<int32_t>(offsetof(Tweener_t3008954595, ___rotationEaseType_22)); }
	inline int32_t get_rotationEaseType_22() const { return ___rotationEaseType_22; }
	inline int32_t* get_address_of_rotationEaseType_22() { return &___rotationEaseType_22; }
	inline void set_rotationEaseType_22(int32_t value)
	{
		___rotationEaseType_22 = value;
	}

	inline static int32_t get_offset_of_rotationTweenDuration_23() { return static_cast<int32_t>(offsetof(Tweener_t3008954595, ___rotationTweenDuration_23)); }
	inline float get_rotationTweenDuration_23() const { return ___rotationTweenDuration_23; }
	inline float* get_address_of_rotationTweenDuration_23() { return &___rotationTweenDuration_23; }
	inline void set_rotationTweenDuration_23(float value)
	{
		___rotationTweenDuration_23 = value;
	}

	inline static int32_t get_offset_of_rotationTweenTarget_24() { return static_cast<int32_t>(offsetof(Tweener_t3008954595, ___rotationTweenTarget_24)); }
	inline Transform_t3600365921 * get_rotationTweenTarget_24() const { return ___rotationTweenTarget_24; }
	inline Transform_t3600365921 ** get_address_of_rotationTweenTarget_24() { return &___rotationTweenTarget_24; }
	inline void set_rotationTweenTarget_24(Transform_t3600365921 * value)
	{
		___rotationTweenTarget_24 = value;
		Il2CppCodeGenWriteBarrier((&___rotationTweenTarget_24), value);
	}

	inline static int32_t get_offset_of_tweenScale_25() { return static_cast<int32_t>(offsetof(Tweener_t3008954595, ___tweenScale_25)); }
	inline bool get_tweenScale_25() const { return ___tweenScale_25; }
	inline bool* get_address_of_tweenScale_25() { return &___tweenScale_25; }
	inline void set_tweenScale_25(bool value)
	{
		___tweenScale_25 = value;
	}

	inline static int32_t get_offset_of_tweenScaleRepeat_26() { return static_cast<int32_t>(offsetof(Tweener_t3008954595, ___tweenScaleRepeat_26)); }
	inline bool get_tweenScaleRepeat_26() const { return ___tweenScaleRepeat_26; }
	inline bool* get_address_of_tweenScaleRepeat_26() { return &___tweenScaleRepeat_26; }
	inline void set_tweenScaleRepeat_26(bool value)
	{
		___tweenScaleRepeat_26 = value;
	}

	inline static int32_t get_offset_of_startScale_27() { return static_cast<int32_t>(offsetof(Tweener_t3008954595, ___startScale_27)); }
	inline Vector3_t3722313464  get_startScale_27() const { return ___startScale_27; }
	inline Vector3_t3722313464 * get_address_of_startScale_27() { return &___startScale_27; }
	inline void set_startScale_27(Vector3_t3722313464  value)
	{
		___startScale_27 = value;
	}

	inline static int32_t get_offset_of_endScale_28() { return static_cast<int32_t>(offsetof(Tweener_t3008954595, ___endScale_28)); }
	inline Vector3_t3722313464  get_endScale_28() const { return ___endScale_28; }
	inline Vector3_t3722313464 * get_address_of_endScale_28() { return &___endScale_28; }
	inline void set_endScale_28(Vector3_t3722313464  value)
	{
		___endScale_28 = value;
	}

	inline static int32_t get_offset_of_currScale_29() { return static_cast<int32_t>(offsetof(Tweener_t3008954595, ___currScale_29)); }
	inline Vector3_t3722313464  get_currScale_29() const { return ___currScale_29; }
	inline Vector3_t3722313464 * get_address_of_currScale_29() { return &___currScale_29; }
	inline void set_currScale_29(Vector3_t3722313464  value)
	{
		___currScale_29 = value;
	}

	inline static int32_t get_offset_of_scaleEaseType_30() { return static_cast<int32_t>(offsetof(Tweener_t3008954595, ___scaleEaseType_30)); }
	inline int32_t get_scaleEaseType_30() const { return ___scaleEaseType_30; }
	inline int32_t* get_address_of_scaleEaseType_30() { return &___scaleEaseType_30; }
	inline void set_scaleEaseType_30(int32_t value)
	{
		___scaleEaseType_30 = value;
	}

	inline static int32_t get_offset_of_scaleTweenDuration_31() { return static_cast<int32_t>(offsetof(Tweener_t3008954595, ___scaleTweenDuration_31)); }
	inline float get_scaleTweenDuration_31() const { return ___scaleTweenDuration_31; }
	inline float* get_address_of_scaleTweenDuration_31() { return &___scaleTweenDuration_31; }
	inline void set_scaleTweenDuration_31(float value)
	{
		___scaleTweenDuration_31 = value;
	}

	inline static int32_t get_offset_of_scaleTweenTarget_32() { return static_cast<int32_t>(offsetof(Tweener_t3008954595, ___scaleTweenTarget_32)); }
	inline Transform_t3600365921 * get_scaleTweenTarget_32() const { return ___scaleTweenTarget_32; }
	inline Transform_t3600365921 ** get_address_of_scaleTweenTarget_32() { return &___scaleTweenTarget_32; }
	inline void set_scaleTweenTarget_32(Transform_t3600365921 * value)
	{
		___scaleTweenTarget_32 = value;
		Il2CppCodeGenWriteBarrier((&___scaleTweenTarget_32), value);
	}

	inline static int32_t get_offset_of_tweenColor_33() { return static_cast<int32_t>(offsetof(Tweener_t3008954595, ___tweenColor_33)); }
	inline bool get_tweenColor_33() const { return ___tweenColor_33; }
	inline bool* get_address_of_tweenColor_33() { return &___tweenColor_33; }
	inline void set_tweenColor_33(bool value)
	{
		___tweenColor_33 = value;
	}

	inline static int32_t get_offset_of_tweenColorRepeat_34() { return static_cast<int32_t>(offsetof(Tweener_t3008954595, ___tweenColorRepeat_34)); }
	inline bool get_tweenColorRepeat_34() const { return ___tweenColorRepeat_34; }
	inline bool* get_address_of_tweenColorRepeat_34() { return &___tweenColorRepeat_34; }
	inline void set_tweenColorRepeat_34(bool value)
	{
		___tweenColorRepeat_34 = value;
	}

	inline static int32_t get_offset_of_startColor_35() { return static_cast<int32_t>(offsetof(Tweener_t3008954595, ___startColor_35)); }
	inline Color_t2555686324  get_startColor_35() const { return ___startColor_35; }
	inline Color_t2555686324 * get_address_of_startColor_35() { return &___startColor_35; }
	inline void set_startColor_35(Color_t2555686324  value)
	{
		___startColor_35 = value;
	}

	inline static int32_t get_offset_of_endColor_36() { return static_cast<int32_t>(offsetof(Tweener_t3008954595, ___endColor_36)); }
	inline Color_t2555686324  get_endColor_36() const { return ___endColor_36; }
	inline Color_t2555686324 * get_address_of_endColor_36() { return &___endColor_36; }
	inline void set_endColor_36(Color_t2555686324  value)
	{
		___endColor_36 = value;
	}

	inline static int32_t get_offset_of_currColor_37() { return static_cast<int32_t>(offsetof(Tweener_t3008954595, ___currColor_37)); }
	inline Color_t2555686324  get_currColor_37() const { return ___currColor_37; }
	inline Color_t2555686324 * get_address_of_currColor_37() { return &___currColor_37; }
	inline void set_currColor_37(Color_t2555686324  value)
	{
		___currColor_37 = value;
	}

	inline static int32_t get_offset_of_colorEaseType_38() { return static_cast<int32_t>(offsetof(Tweener_t3008954595, ___colorEaseType_38)); }
	inline int32_t get_colorEaseType_38() const { return ___colorEaseType_38; }
	inline int32_t* get_address_of_colorEaseType_38() { return &___colorEaseType_38; }
	inline void set_colorEaseType_38(int32_t value)
	{
		___colorEaseType_38 = value;
	}

	inline static int32_t get_offset_of_colorTweenDuration_39() { return static_cast<int32_t>(offsetof(Tweener_t3008954595, ___colorTweenDuration_39)); }
	inline float get_colorTweenDuration_39() const { return ___colorTweenDuration_39; }
	inline float* get_address_of_colorTweenDuration_39() { return &___colorTweenDuration_39; }
	inline void set_colorTweenDuration_39(float value)
	{
		___colorTweenDuration_39 = value;
	}

	inline static int32_t get_offset_of_colorTweenTarget_40() { return static_cast<int32_t>(offsetof(Tweener_t3008954595, ___colorTweenTarget_40)); }
	inline Image_t2670269651 * get_colorTweenTarget_40() const { return ___colorTweenTarget_40; }
	inline Image_t2670269651 ** get_address_of_colorTweenTarget_40() { return &___colorTweenTarget_40; }
	inline void set_colorTweenTarget_40(Image_t2670269651 * value)
	{
		___colorTweenTarget_40 = value;
		Il2CppCodeGenWriteBarrier((&___colorTweenTarget_40), value);
	}

	inline static int32_t get_offset_of_tweenAlpha_41() { return static_cast<int32_t>(offsetof(Tweener_t3008954595, ___tweenAlpha_41)); }
	inline bool get_tweenAlpha_41() const { return ___tweenAlpha_41; }
	inline bool* get_address_of_tweenAlpha_41() { return &___tweenAlpha_41; }
	inline void set_tweenAlpha_41(bool value)
	{
		___tweenAlpha_41 = value;
	}

	inline static int32_t get_offset_of_tweenAlphaRepeat_42() { return static_cast<int32_t>(offsetof(Tweener_t3008954595, ___tweenAlphaRepeat_42)); }
	inline bool get_tweenAlphaRepeat_42() const { return ___tweenAlphaRepeat_42; }
	inline bool* get_address_of_tweenAlphaRepeat_42() { return &___tweenAlphaRepeat_42; }
	inline void set_tweenAlphaRepeat_42(bool value)
	{
		___tweenAlphaRepeat_42 = value;
	}

	inline static int32_t get_offset_of_startAlpha_43() { return static_cast<int32_t>(offsetof(Tweener_t3008954595, ___startAlpha_43)); }
	inline float get_startAlpha_43() const { return ___startAlpha_43; }
	inline float* get_address_of_startAlpha_43() { return &___startAlpha_43; }
	inline void set_startAlpha_43(float value)
	{
		___startAlpha_43 = value;
	}

	inline static int32_t get_offset_of_endAlpha_44() { return static_cast<int32_t>(offsetof(Tweener_t3008954595, ___endAlpha_44)); }
	inline float get_endAlpha_44() const { return ___endAlpha_44; }
	inline float* get_address_of_endAlpha_44() { return &___endAlpha_44; }
	inline void set_endAlpha_44(float value)
	{
		___endAlpha_44 = value;
	}

	inline static int32_t get_offset_of_currAlpha_45() { return static_cast<int32_t>(offsetof(Tweener_t3008954595, ___currAlpha_45)); }
	inline float get_currAlpha_45() const { return ___currAlpha_45; }
	inline float* get_address_of_currAlpha_45() { return &___currAlpha_45; }
	inline void set_currAlpha_45(float value)
	{
		___currAlpha_45 = value;
	}

	inline static int32_t get_offset_of_alphaEaseType_46() { return static_cast<int32_t>(offsetof(Tweener_t3008954595, ___alphaEaseType_46)); }
	inline int32_t get_alphaEaseType_46() const { return ___alphaEaseType_46; }
	inline int32_t* get_address_of_alphaEaseType_46() { return &___alphaEaseType_46; }
	inline void set_alphaEaseType_46(int32_t value)
	{
		___alphaEaseType_46 = value;
	}

	inline static int32_t get_offset_of_alphaTweenDuration_47() { return static_cast<int32_t>(offsetof(Tweener_t3008954595, ___alphaTweenDuration_47)); }
	inline float get_alphaTweenDuration_47() const { return ___alphaTweenDuration_47; }
	inline float* get_address_of_alphaTweenDuration_47() { return &___alphaTweenDuration_47; }
	inline void set_alphaTweenDuration_47(float value)
	{
		___alphaTweenDuration_47 = value;
	}

	inline static int32_t get_offset_of_alphaTweenTarget_48() { return static_cast<int32_t>(offsetof(Tweener_t3008954595, ___alphaTweenTarget_48)); }
	inline Image_t2670269651 * get_alphaTweenTarget_48() const { return ___alphaTweenTarget_48; }
	inline Image_t2670269651 ** get_address_of_alphaTweenTarget_48() { return &___alphaTweenTarget_48; }
	inline void set_alphaTweenTarget_48(Image_t2670269651 * value)
	{
		___alphaTweenTarget_48 = value;
		Il2CppCodeGenWriteBarrier((&___alphaTweenTarget_48), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TWEENER_T3008954595_H
#ifndef ANALYTICSEVENTTRACKER_T2285229262_H
#define ANALYTICSEVENTTRACKER_T2285229262_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.AnalyticsEventTracker
struct  AnalyticsEventTracker_t2285229262  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Analytics.EventTrigger UnityEngine.Analytics.AnalyticsEventTracker::m_Trigger
	EventTrigger_t2527451695 * ___m_Trigger_4;
	// UnityEngine.Analytics.StandardEventPayload UnityEngine.Analytics.AnalyticsEventTracker::m_EventPayload
	StandardEventPayload_t1629891255 * ___m_EventPayload_5;

public:
	inline static int32_t get_offset_of_m_Trigger_4() { return static_cast<int32_t>(offsetof(AnalyticsEventTracker_t2285229262, ___m_Trigger_4)); }
	inline EventTrigger_t2527451695 * get_m_Trigger_4() const { return ___m_Trigger_4; }
	inline EventTrigger_t2527451695 ** get_address_of_m_Trigger_4() { return &___m_Trigger_4; }
	inline void set_m_Trigger_4(EventTrigger_t2527451695 * value)
	{
		___m_Trigger_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_Trigger_4), value);
	}

	inline static int32_t get_offset_of_m_EventPayload_5() { return static_cast<int32_t>(offsetof(AnalyticsEventTracker_t2285229262, ___m_EventPayload_5)); }
	inline StandardEventPayload_t1629891255 * get_m_EventPayload_5() const { return ___m_EventPayload_5; }
	inline StandardEventPayload_t1629891255 ** get_address_of_m_EventPayload_5() { return &___m_EventPayload_5; }
	inline void set_m_EventPayload_5(StandardEventPayload_t1629891255 * value)
	{
		___m_EventPayload_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_EventPayload_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANALYTICSEVENTTRACKER_T2285229262_H
#ifndef ANALYTICSTRACKER_T731021378_H
#define ANALYTICSTRACKER_T731021378_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.AnalyticsTracker
struct  AnalyticsTracker_t731021378  : public MonoBehaviour_t3962482529
{
public:
	// System.String UnityEngine.Analytics.AnalyticsTracker::m_EventName
	String_t* ___m_EventName_4;
	// System.Collections.Generic.Dictionary`2<System.String,System.Object> UnityEngine.Analytics.AnalyticsTracker::m_Dict
	Dictionary_2_t2865362463 * ___m_Dict_5;
	// System.Int32 UnityEngine.Analytics.AnalyticsTracker::m_PrevDictHash
	int32_t ___m_PrevDictHash_6;
	// UnityEngine.Analytics.TrackableProperty UnityEngine.Analytics.AnalyticsTracker::m_TrackableProperty
	TrackableProperty_t3943537984 * ___m_TrackableProperty_7;
	// UnityEngine.Analytics.AnalyticsTracker/Trigger UnityEngine.Analytics.AnalyticsTracker::m_Trigger
	int32_t ___m_Trigger_8;

public:
	inline static int32_t get_offset_of_m_EventName_4() { return static_cast<int32_t>(offsetof(AnalyticsTracker_t731021378, ___m_EventName_4)); }
	inline String_t* get_m_EventName_4() const { return ___m_EventName_4; }
	inline String_t** get_address_of_m_EventName_4() { return &___m_EventName_4; }
	inline void set_m_EventName_4(String_t* value)
	{
		___m_EventName_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_EventName_4), value);
	}

	inline static int32_t get_offset_of_m_Dict_5() { return static_cast<int32_t>(offsetof(AnalyticsTracker_t731021378, ___m_Dict_5)); }
	inline Dictionary_2_t2865362463 * get_m_Dict_5() const { return ___m_Dict_5; }
	inline Dictionary_2_t2865362463 ** get_address_of_m_Dict_5() { return &___m_Dict_5; }
	inline void set_m_Dict_5(Dictionary_2_t2865362463 * value)
	{
		___m_Dict_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_Dict_5), value);
	}

	inline static int32_t get_offset_of_m_PrevDictHash_6() { return static_cast<int32_t>(offsetof(AnalyticsTracker_t731021378, ___m_PrevDictHash_6)); }
	inline int32_t get_m_PrevDictHash_6() const { return ___m_PrevDictHash_6; }
	inline int32_t* get_address_of_m_PrevDictHash_6() { return &___m_PrevDictHash_6; }
	inline void set_m_PrevDictHash_6(int32_t value)
	{
		___m_PrevDictHash_6 = value;
	}

	inline static int32_t get_offset_of_m_TrackableProperty_7() { return static_cast<int32_t>(offsetof(AnalyticsTracker_t731021378, ___m_TrackableProperty_7)); }
	inline TrackableProperty_t3943537984 * get_m_TrackableProperty_7() const { return ___m_TrackableProperty_7; }
	inline TrackableProperty_t3943537984 ** get_address_of_m_TrackableProperty_7() { return &___m_TrackableProperty_7; }
	inline void set_m_TrackableProperty_7(TrackableProperty_t3943537984 * value)
	{
		___m_TrackableProperty_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_TrackableProperty_7), value);
	}

	inline static int32_t get_offset_of_m_Trigger_8() { return static_cast<int32_t>(offsetof(AnalyticsTracker_t731021378, ___m_Trigger_8)); }
	inline int32_t get_m_Trigger_8() const { return ___m_Trigger_8; }
	inline int32_t* get_address_of_m_Trigger_8() { return &___m_Trigger_8; }
	inline void set_m_Trigger_8(int32_t value)
	{
		___m_Trigger_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANALYTICSTRACKER_T731021378_H
#ifndef VUFORIAMONOBEHAVIOUR_T1150221792_H
#define VUFORIAMONOBEHAVIOUR_T1150221792_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VuforiaMonoBehaviour
struct  VuforiaMonoBehaviour_t1150221792  : public MonoBehaviour_t3962482529
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VUFORIAMONOBEHAVIOUR_T1150221792_H
#ifndef DEFAULTINITIALIZATIONERRORHANDLER_T3109936861_H
#define DEFAULTINITIALIZATIONERRORHANDLER_T3109936861_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DefaultInitializationErrorHandler
struct  DefaultInitializationErrorHandler_t3109936861  : public VuforiaMonoBehaviour_t1150221792
{
public:
	// System.String DefaultInitializationErrorHandler::mErrorText
	String_t* ___mErrorText_4;
	// System.Boolean DefaultInitializationErrorHandler::mErrorOccurred
	bool ___mErrorOccurred_5;
	// UnityEngine.GUIStyle DefaultInitializationErrorHandler::bodyStyle
	GUIStyle_t3956901511 * ___bodyStyle_7;
	// UnityEngine.GUIStyle DefaultInitializationErrorHandler::headerStyle
	GUIStyle_t3956901511 * ___headerStyle_8;
	// UnityEngine.GUIStyle DefaultInitializationErrorHandler::footerStyle
	GUIStyle_t3956901511 * ___footerStyle_9;
	// UnityEngine.Texture2D DefaultInitializationErrorHandler::bodyTexture
	Texture2D_t3840446185 * ___bodyTexture_10;
	// UnityEngine.Texture2D DefaultInitializationErrorHandler::headerTexture
	Texture2D_t3840446185 * ___headerTexture_11;
	// UnityEngine.Texture2D DefaultInitializationErrorHandler::footerTexture
	Texture2D_t3840446185 * ___footerTexture_12;

public:
	inline static int32_t get_offset_of_mErrorText_4() { return static_cast<int32_t>(offsetof(DefaultInitializationErrorHandler_t3109936861, ___mErrorText_4)); }
	inline String_t* get_mErrorText_4() const { return ___mErrorText_4; }
	inline String_t** get_address_of_mErrorText_4() { return &___mErrorText_4; }
	inline void set_mErrorText_4(String_t* value)
	{
		___mErrorText_4 = value;
		Il2CppCodeGenWriteBarrier((&___mErrorText_4), value);
	}

	inline static int32_t get_offset_of_mErrorOccurred_5() { return static_cast<int32_t>(offsetof(DefaultInitializationErrorHandler_t3109936861, ___mErrorOccurred_5)); }
	inline bool get_mErrorOccurred_5() const { return ___mErrorOccurred_5; }
	inline bool* get_address_of_mErrorOccurred_5() { return &___mErrorOccurred_5; }
	inline void set_mErrorOccurred_5(bool value)
	{
		___mErrorOccurred_5 = value;
	}

	inline static int32_t get_offset_of_bodyStyle_7() { return static_cast<int32_t>(offsetof(DefaultInitializationErrorHandler_t3109936861, ___bodyStyle_7)); }
	inline GUIStyle_t3956901511 * get_bodyStyle_7() const { return ___bodyStyle_7; }
	inline GUIStyle_t3956901511 ** get_address_of_bodyStyle_7() { return &___bodyStyle_7; }
	inline void set_bodyStyle_7(GUIStyle_t3956901511 * value)
	{
		___bodyStyle_7 = value;
		Il2CppCodeGenWriteBarrier((&___bodyStyle_7), value);
	}

	inline static int32_t get_offset_of_headerStyle_8() { return static_cast<int32_t>(offsetof(DefaultInitializationErrorHandler_t3109936861, ___headerStyle_8)); }
	inline GUIStyle_t3956901511 * get_headerStyle_8() const { return ___headerStyle_8; }
	inline GUIStyle_t3956901511 ** get_address_of_headerStyle_8() { return &___headerStyle_8; }
	inline void set_headerStyle_8(GUIStyle_t3956901511 * value)
	{
		___headerStyle_8 = value;
		Il2CppCodeGenWriteBarrier((&___headerStyle_8), value);
	}

	inline static int32_t get_offset_of_footerStyle_9() { return static_cast<int32_t>(offsetof(DefaultInitializationErrorHandler_t3109936861, ___footerStyle_9)); }
	inline GUIStyle_t3956901511 * get_footerStyle_9() const { return ___footerStyle_9; }
	inline GUIStyle_t3956901511 ** get_address_of_footerStyle_9() { return &___footerStyle_9; }
	inline void set_footerStyle_9(GUIStyle_t3956901511 * value)
	{
		___footerStyle_9 = value;
		Il2CppCodeGenWriteBarrier((&___footerStyle_9), value);
	}

	inline static int32_t get_offset_of_bodyTexture_10() { return static_cast<int32_t>(offsetof(DefaultInitializationErrorHandler_t3109936861, ___bodyTexture_10)); }
	inline Texture2D_t3840446185 * get_bodyTexture_10() const { return ___bodyTexture_10; }
	inline Texture2D_t3840446185 ** get_address_of_bodyTexture_10() { return &___bodyTexture_10; }
	inline void set_bodyTexture_10(Texture2D_t3840446185 * value)
	{
		___bodyTexture_10 = value;
		Il2CppCodeGenWriteBarrier((&___bodyTexture_10), value);
	}

	inline static int32_t get_offset_of_headerTexture_11() { return static_cast<int32_t>(offsetof(DefaultInitializationErrorHandler_t3109936861, ___headerTexture_11)); }
	inline Texture2D_t3840446185 * get_headerTexture_11() const { return ___headerTexture_11; }
	inline Texture2D_t3840446185 ** get_address_of_headerTexture_11() { return &___headerTexture_11; }
	inline void set_headerTexture_11(Texture2D_t3840446185 * value)
	{
		___headerTexture_11 = value;
		Il2CppCodeGenWriteBarrier((&___headerTexture_11), value);
	}

	inline static int32_t get_offset_of_footerTexture_12() { return static_cast<int32_t>(offsetof(DefaultInitializationErrorHandler_t3109936861, ___footerTexture_12)); }
	inline Texture2D_t3840446185 * get_footerTexture_12() const { return ___footerTexture_12; }
	inline Texture2D_t3840446185 ** get_address_of_footerTexture_12() { return &___footerTexture_12; }
	inline void set_footerTexture_12(Texture2D_t3840446185 * value)
	{
		___footerTexture_12 = value;
		Il2CppCodeGenWriteBarrier((&___footerTexture_12), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEFAULTINITIALIZATIONERRORHANDLER_T3109936861_H
#ifndef USERDEFINEDTARGETBUILDINGBEHAVIOUR_T4262637471_H
#define USERDEFINEDTARGETBUILDINGBEHAVIOUR_T4262637471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.UserDefinedTargetBuildingBehaviour
struct  UserDefinedTargetBuildingBehaviour_t4262637471  : public VuforiaMonoBehaviour_t1150221792
{
public:
	// Vuforia.ObjectTracker Vuforia.UserDefinedTargetBuildingBehaviour::mObjectTracker
	ObjectTracker_t4177997237 * ___mObjectTracker_4;
	// Vuforia.ImageTargetBuilder/FrameQuality Vuforia.UserDefinedTargetBuildingBehaviour::mLastFrameQuality
	int32_t ___mLastFrameQuality_5;
	// System.Boolean Vuforia.UserDefinedTargetBuildingBehaviour::mCurrentlyScanning
	bool ___mCurrentlyScanning_6;
	// System.Boolean Vuforia.UserDefinedTargetBuildingBehaviour::mWasScanningBeforeDisable
	bool ___mWasScanningBeforeDisable_7;
	// System.Boolean Vuforia.UserDefinedTargetBuildingBehaviour::mCurrentlyBuilding
	bool ___mCurrentlyBuilding_8;
	// System.Boolean Vuforia.UserDefinedTargetBuildingBehaviour::mWasBuildingBeforeDisable
	bool ___mWasBuildingBeforeDisable_9;
	// System.Boolean Vuforia.UserDefinedTargetBuildingBehaviour::mOnInitializedCalled
	bool ___mOnInitializedCalled_10;
	// System.Collections.Generic.List`1<Vuforia.IUserDefinedTargetEventHandler> Vuforia.UserDefinedTargetBuildingBehaviour::mHandlers
	List_1_t2728888017 * ___mHandlers_11;
	// System.Boolean Vuforia.UserDefinedTargetBuildingBehaviour::StopTrackerWhileScanning
	bool ___StopTrackerWhileScanning_12;
	// System.Boolean Vuforia.UserDefinedTargetBuildingBehaviour::StartScanningAutomatically
	bool ___StartScanningAutomatically_13;
	// System.Boolean Vuforia.UserDefinedTargetBuildingBehaviour::StopScanningWhenFinshedBuilding
	bool ___StopScanningWhenFinshedBuilding_14;

public:
	inline static int32_t get_offset_of_mObjectTracker_4() { return static_cast<int32_t>(offsetof(UserDefinedTargetBuildingBehaviour_t4262637471, ___mObjectTracker_4)); }
	inline ObjectTracker_t4177997237 * get_mObjectTracker_4() const { return ___mObjectTracker_4; }
	inline ObjectTracker_t4177997237 ** get_address_of_mObjectTracker_4() { return &___mObjectTracker_4; }
	inline void set_mObjectTracker_4(ObjectTracker_t4177997237 * value)
	{
		___mObjectTracker_4 = value;
		Il2CppCodeGenWriteBarrier((&___mObjectTracker_4), value);
	}

	inline static int32_t get_offset_of_mLastFrameQuality_5() { return static_cast<int32_t>(offsetof(UserDefinedTargetBuildingBehaviour_t4262637471, ___mLastFrameQuality_5)); }
	inline int32_t get_mLastFrameQuality_5() const { return ___mLastFrameQuality_5; }
	inline int32_t* get_address_of_mLastFrameQuality_5() { return &___mLastFrameQuality_5; }
	inline void set_mLastFrameQuality_5(int32_t value)
	{
		___mLastFrameQuality_5 = value;
	}

	inline static int32_t get_offset_of_mCurrentlyScanning_6() { return static_cast<int32_t>(offsetof(UserDefinedTargetBuildingBehaviour_t4262637471, ___mCurrentlyScanning_6)); }
	inline bool get_mCurrentlyScanning_6() const { return ___mCurrentlyScanning_6; }
	inline bool* get_address_of_mCurrentlyScanning_6() { return &___mCurrentlyScanning_6; }
	inline void set_mCurrentlyScanning_6(bool value)
	{
		___mCurrentlyScanning_6 = value;
	}

	inline static int32_t get_offset_of_mWasScanningBeforeDisable_7() { return static_cast<int32_t>(offsetof(UserDefinedTargetBuildingBehaviour_t4262637471, ___mWasScanningBeforeDisable_7)); }
	inline bool get_mWasScanningBeforeDisable_7() const { return ___mWasScanningBeforeDisable_7; }
	inline bool* get_address_of_mWasScanningBeforeDisable_7() { return &___mWasScanningBeforeDisable_7; }
	inline void set_mWasScanningBeforeDisable_7(bool value)
	{
		___mWasScanningBeforeDisable_7 = value;
	}

	inline static int32_t get_offset_of_mCurrentlyBuilding_8() { return static_cast<int32_t>(offsetof(UserDefinedTargetBuildingBehaviour_t4262637471, ___mCurrentlyBuilding_8)); }
	inline bool get_mCurrentlyBuilding_8() const { return ___mCurrentlyBuilding_8; }
	inline bool* get_address_of_mCurrentlyBuilding_8() { return &___mCurrentlyBuilding_8; }
	inline void set_mCurrentlyBuilding_8(bool value)
	{
		___mCurrentlyBuilding_8 = value;
	}

	inline static int32_t get_offset_of_mWasBuildingBeforeDisable_9() { return static_cast<int32_t>(offsetof(UserDefinedTargetBuildingBehaviour_t4262637471, ___mWasBuildingBeforeDisable_9)); }
	inline bool get_mWasBuildingBeforeDisable_9() const { return ___mWasBuildingBeforeDisable_9; }
	inline bool* get_address_of_mWasBuildingBeforeDisable_9() { return &___mWasBuildingBeforeDisable_9; }
	inline void set_mWasBuildingBeforeDisable_9(bool value)
	{
		___mWasBuildingBeforeDisable_9 = value;
	}

	inline static int32_t get_offset_of_mOnInitializedCalled_10() { return static_cast<int32_t>(offsetof(UserDefinedTargetBuildingBehaviour_t4262637471, ___mOnInitializedCalled_10)); }
	inline bool get_mOnInitializedCalled_10() const { return ___mOnInitializedCalled_10; }
	inline bool* get_address_of_mOnInitializedCalled_10() { return &___mOnInitializedCalled_10; }
	inline void set_mOnInitializedCalled_10(bool value)
	{
		___mOnInitializedCalled_10 = value;
	}

	inline static int32_t get_offset_of_mHandlers_11() { return static_cast<int32_t>(offsetof(UserDefinedTargetBuildingBehaviour_t4262637471, ___mHandlers_11)); }
	inline List_1_t2728888017 * get_mHandlers_11() const { return ___mHandlers_11; }
	inline List_1_t2728888017 ** get_address_of_mHandlers_11() { return &___mHandlers_11; }
	inline void set_mHandlers_11(List_1_t2728888017 * value)
	{
		___mHandlers_11 = value;
		Il2CppCodeGenWriteBarrier((&___mHandlers_11), value);
	}

	inline static int32_t get_offset_of_StopTrackerWhileScanning_12() { return static_cast<int32_t>(offsetof(UserDefinedTargetBuildingBehaviour_t4262637471, ___StopTrackerWhileScanning_12)); }
	inline bool get_StopTrackerWhileScanning_12() const { return ___StopTrackerWhileScanning_12; }
	inline bool* get_address_of_StopTrackerWhileScanning_12() { return &___StopTrackerWhileScanning_12; }
	inline void set_StopTrackerWhileScanning_12(bool value)
	{
		___StopTrackerWhileScanning_12 = value;
	}

	inline static int32_t get_offset_of_StartScanningAutomatically_13() { return static_cast<int32_t>(offsetof(UserDefinedTargetBuildingBehaviour_t4262637471, ___StartScanningAutomatically_13)); }
	inline bool get_StartScanningAutomatically_13() const { return ___StartScanningAutomatically_13; }
	inline bool* get_address_of_StartScanningAutomatically_13() { return &___StartScanningAutomatically_13; }
	inline void set_StartScanningAutomatically_13(bool value)
	{
		___StartScanningAutomatically_13 = value;
	}

	inline static int32_t get_offset_of_StopScanningWhenFinshedBuilding_14() { return static_cast<int32_t>(offsetof(UserDefinedTargetBuildingBehaviour_t4262637471, ___StopScanningWhenFinshedBuilding_14)); }
	inline bool get_StopScanningWhenFinshedBuilding_14() const { return ___StopScanningWhenFinshedBuilding_14; }
	inline bool* get_address_of_StopScanningWhenFinshedBuilding_14() { return &___StopScanningWhenFinshedBuilding_14; }
	inline void set_StopScanningWhenFinshedBuilding_14(bool value)
	{
		___StopScanningWhenFinshedBuilding_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // USERDEFINEDTARGETBUILDINGBEHAVIOUR_T4262637471_H
#ifndef VIDEOBACKGROUNDBEHAVIOUR_T1552899074_H
#define VIDEOBACKGROUNDBEHAVIOUR_T1552899074_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.VideoBackgroundBehaviour
struct  VideoBackgroundBehaviour_t1552899074  : public VuforiaMonoBehaviour_t1150221792
{
public:
	// System.Int32 Vuforia.VideoBackgroundBehaviour::mClearBuffers
	int32_t ___mClearBuffers_4;
	// System.Int32 Vuforia.VideoBackgroundBehaviour::mSkipStateUpdates
	int32_t ___mSkipStateUpdates_5;
	// Vuforia.VuforiaARController Vuforia.VideoBackgroundBehaviour::mVuforiaARController
	VuforiaARController_t1876945237 * ___mVuforiaARController_6;
	// UnityEngine.Camera Vuforia.VideoBackgroundBehaviour::mCamera
	Camera_t4157153871 * ___mCamera_7;
	// Vuforia.BackgroundPlaneBehaviour Vuforia.VideoBackgroundBehaviour::mBackgroundBehaviour
	BackgroundPlaneBehaviour_t3333547397 * ___mBackgroundBehaviour_8;
	// System.Collections.Generic.HashSet`1<UnityEngine.MeshRenderer> Vuforia.VideoBackgroundBehaviour::mDisabledMeshRenderers
	HashSet_1_t3446926030 * ___mDisabledMeshRenderers_11;

public:
	inline static int32_t get_offset_of_mClearBuffers_4() { return static_cast<int32_t>(offsetof(VideoBackgroundBehaviour_t1552899074, ___mClearBuffers_4)); }
	inline int32_t get_mClearBuffers_4() const { return ___mClearBuffers_4; }
	inline int32_t* get_address_of_mClearBuffers_4() { return &___mClearBuffers_4; }
	inline void set_mClearBuffers_4(int32_t value)
	{
		___mClearBuffers_4 = value;
	}

	inline static int32_t get_offset_of_mSkipStateUpdates_5() { return static_cast<int32_t>(offsetof(VideoBackgroundBehaviour_t1552899074, ___mSkipStateUpdates_5)); }
	inline int32_t get_mSkipStateUpdates_5() const { return ___mSkipStateUpdates_5; }
	inline int32_t* get_address_of_mSkipStateUpdates_5() { return &___mSkipStateUpdates_5; }
	inline void set_mSkipStateUpdates_5(int32_t value)
	{
		___mSkipStateUpdates_5 = value;
	}

	inline static int32_t get_offset_of_mVuforiaARController_6() { return static_cast<int32_t>(offsetof(VideoBackgroundBehaviour_t1552899074, ___mVuforiaARController_6)); }
	inline VuforiaARController_t1876945237 * get_mVuforiaARController_6() const { return ___mVuforiaARController_6; }
	inline VuforiaARController_t1876945237 ** get_address_of_mVuforiaARController_6() { return &___mVuforiaARController_6; }
	inline void set_mVuforiaARController_6(VuforiaARController_t1876945237 * value)
	{
		___mVuforiaARController_6 = value;
		Il2CppCodeGenWriteBarrier((&___mVuforiaARController_6), value);
	}

	inline static int32_t get_offset_of_mCamera_7() { return static_cast<int32_t>(offsetof(VideoBackgroundBehaviour_t1552899074, ___mCamera_7)); }
	inline Camera_t4157153871 * get_mCamera_7() const { return ___mCamera_7; }
	inline Camera_t4157153871 ** get_address_of_mCamera_7() { return &___mCamera_7; }
	inline void set_mCamera_7(Camera_t4157153871 * value)
	{
		___mCamera_7 = value;
		Il2CppCodeGenWriteBarrier((&___mCamera_7), value);
	}

	inline static int32_t get_offset_of_mBackgroundBehaviour_8() { return static_cast<int32_t>(offsetof(VideoBackgroundBehaviour_t1552899074, ___mBackgroundBehaviour_8)); }
	inline BackgroundPlaneBehaviour_t3333547397 * get_mBackgroundBehaviour_8() const { return ___mBackgroundBehaviour_8; }
	inline BackgroundPlaneBehaviour_t3333547397 ** get_address_of_mBackgroundBehaviour_8() { return &___mBackgroundBehaviour_8; }
	inline void set_mBackgroundBehaviour_8(BackgroundPlaneBehaviour_t3333547397 * value)
	{
		___mBackgroundBehaviour_8 = value;
		Il2CppCodeGenWriteBarrier((&___mBackgroundBehaviour_8), value);
	}

	inline static int32_t get_offset_of_mDisabledMeshRenderers_11() { return static_cast<int32_t>(offsetof(VideoBackgroundBehaviour_t1552899074, ___mDisabledMeshRenderers_11)); }
	inline HashSet_1_t3446926030 * get_mDisabledMeshRenderers_11() const { return ___mDisabledMeshRenderers_11; }
	inline HashSet_1_t3446926030 ** get_address_of_mDisabledMeshRenderers_11() { return &___mDisabledMeshRenderers_11; }
	inline void set_mDisabledMeshRenderers_11(HashSet_1_t3446926030 * value)
	{
		___mDisabledMeshRenderers_11 = value;
		Il2CppCodeGenWriteBarrier((&___mDisabledMeshRenderers_11), value);
	}
};

struct VideoBackgroundBehaviour_t1552899074_StaticFields
{
public:
	// System.Int32 Vuforia.VideoBackgroundBehaviour::mFrameCounter
	int32_t ___mFrameCounter_9;
	// System.Int32 Vuforia.VideoBackgroundBehaviour::mRenderCounter
	int32_t ___mRenderCounter_10;

public:
	inline static int32_t get_offset_of_mFrameCounter_9() { return static_cast<int32_t>(offsetof(VideoBackgroundBehaviour_t1552899074_StaticFields, ___mFrameCounter_9)); }
	inline int32_t get_mFrameCounter_9() const { return ___mFrameCounter_9; }
	inline int32_t* get_address_of_mFrameCounter_9() { return &___mFrameCounter_9; }
	inline void set_mFrameCounter_9(int32_t value)
	{
		___mFrameCounter_9 = value;
	}

	inline static int32_t get_offset_of_mRenderCounter_10() { return static_cast<int32_t>(offsetof(VideoBackgroundBehaviour_t1552899074_StaticFields, ___mRenderCounter_10)); }
	inline int32_t get_mRenderCounter_10() const { return ___mRenderCounter_10; }
	inline int32_t* get_address_of_mRenderCounter_10() { return &___mRenderCounter_10; }
	inline void set_mRenderCounter_10(int32_t value)
	{
		___mRenderCounter_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VIDEOBACKGROUNDBEHAVIOUR_T1552899074_H
#ifndef VIRTUALBUTTONBEHAVIOUR_T1436326451_H
#define VIRTUALBUTTONBEHAVIOUR_T1436326451_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.VirtualButtonBehaviour
struct  VirtualButtonBehaviour_t1436326451  : public VuforiaMonoBehaviour_t1150221792
{
public:
	// System.String Vuforia.VirtualButtonBehaviour::mName
	String_t* ___mName_5;
	// Vuforia.VirtualButton/Sensitivity Vuforia.VirtualButtonBehaviour::mSensitivity
	int32_t ___mSensitivity_6;
	// System.Boolean Vuforia.VirtualButtonBehaviour::mHasUpdatedPose
	bool ___mHasUpdatedPose_7;
	// UnityEngine.Matrix4x4 Vuforia.VirtualButtonBehaviour::mPrevTransform
	Matrix4x4_t1817901843  ___mPrevTransform_8;
	// UnityEngine.GameObject Vuforia.VirtualButtonBehaviour::mPrevParent
	GameObject_t1113636619 * ___mPrevParent_9;
	// System.Boolean Vuforia.VirtualButtonBehaviour::mSensitivityDirty
	bool ___mSensitivityDirty_10;
	// Vuforia.VirtualButton/Sensitivity Vuforia.VirtualButtonBehaviour::mPreviousSensitivity
	int32_t ___mPreviousSensitivity_11;
	// System.Boolean Vuforia.VirtualButtonBehaviour::mPreviouslyEnabled
	bool ___mPreviouslyEnabled_12;
	// System.Boolean Vuforia.VirtualButtonBehaviour::mPressed
	bool ___mPressed_13;
	// System.Collections.Generic.List`1<Vuforia.IVirtualButtonEventHandler> Vuforia.VirtualButtonBehaviour::mHandlers
	List_1_t365750880 * ___mHandlers_14;
	// UnityEngine.Vector2 Vuforia.VirtualButtonBehaviour::mLeftTop
	Vector2_t2156229523  ___mLeftTop_15;
	// UnityEngine.Vector2 Vuforia.VirtualButtonBehaviour::mRightBottom
	Vector2_t2156229523  ___mRightBottom_16;
	// System.Boolean Vuforia.VirtualButtonBehaviour::mUnregisterOnDestroy
	bool ___mUnregisterOnDestroy_17;
	// Vuforia.VirtualButton Vuforia.VirtualButtonBehaviour::mVirtualButton
	VirtualButton_t386166510 * ___mVirtualButton_18;

public:
	inline static int32_t get_offset_of_mName_5() { return static_cast<int32_t>(offsetof(VirtualButtonBehaviour_t1436326451, ___mName_5)); }
	inline String_t* get_mName_5() const { return ___mName_5; }
	inline String_t** get_address_of_mName_5() { return &___mName_5; }
	inline void set_mName_5(String_t* value)
	{
		___mName_5 = value;
		Il2CppCodeGenWriteBarrier((&___mName_5), value);
	}

	inline static int32_t get_offset_of_mSensitivity_6() { return static_cast<int32_t>(offsetof(VirtualButtonBehaviour_t1436326451, ___mSensitivity_6)); }
	inline int32_t get_mSensitivity_6() const { return ___mSensitivity_6; }
	inline int32_t* get_address_of_mSensitivity_6() { return &___mSensitivity_6; }
	inline void set_mSensitivity_6(int32_t value)
	{
		___mSensitivity_6 = value;
	}

	inline static int32_t get_offset_of_mHasUpdatedPose_7() { return static_cast<int32_t>(offsetof(VirtualButtonBehaviour_t1436326451, ___mHasUpdatedPose_7)); }
	inline bool get_mHasUpdatedPose_7() const { return ___mHasUpdatedPose_7; }
	inline bool* get_address_of_mHasUpdatedPose_7() { return &___mHasUpdatedPose_7; }
	inline void set_mHasUpdatedPose_7(bool value)
	{
		___mHasUpdatedPose_7 = value;
	}

	inline static int32_t get_offset_of_mPrevTransform_8() { return static_cast<int32_t>(offsetof(VirtualButtonBehaviour_t1436326451, ___mPrevTransform_8)); }
	inline Matrix4x4_t1817901843  get_mPrevTransform_8() const { return ___mPrevTransform_8; }
	inline Matrix4x4_t1817901843 * get_address_of_mPrevTransform_8() { return &___mPrevTransform_8; }
	inline void set_mPrevTransform_8(Matrix4x4_t1817901843  value)
	{
		___mPrevTransform_8 = value;
	}

	inline static int32_t get_offset_of_mPrevParent_9() { return static_cast<int32_t>(offsetof(VirtualButtonBehaviour_t1436326451, ___mPrevParent_9)); }
	inline GameObject_t1113636619 * get_mPrevParent_9() const { return ___mPrevParent_9; }
	inline GameObject_t1113636619 ** get_address_of_mPrevParent_9() { return &___mPrevParent_9; }
	inline void set_mPrevParent_9(GameObject_t1113636619 * value)
	{
		___mPrevParent_9 = value;
		Il2CppCodeGenWriteBarrier((&___mPrevParent_9), value);
	}

	inline static int32_t get_offset_of_mSensitivityDirty_10() { return static_cast<int32_t>(offsetof(VirtualButtonBehaviour_t1436326451, ___mSensitivityDirty_10)); }
	inline bool get_mSensitivityDirty_10() const { return ___mSensitivityDirty_10; }
	inline bool* get_address_of_mSensitivityDirty_10() { return &___mSensitivityDirty_10; }
	inline void set_mSensitivityDirty_10(bool value)
	{
		___mSensitivityDirty_10 = value;
	}

	inline static int32_t get_offset_of_mPreviousSensitivity_11() { return static_cast<int32_t>(offsetof(VirtualButtonBehaviour_t1436326451, ___mPreviousSensitivity_11)); }
	inline int32_t get_mPreviousSensitivity_11() const { return ___mPreviousSensitivity_11; }
	inline int32_t* get_address_of_mPreviousSensitivity_11() { return &___mPreviousSensitivity_11; }
	inline void set_mPreviousSensitivity_11(int32_t value)
	{
		___mPreviousSensitivity_11 = value;
	}

	inline static int32_t get_offset_of_mPreviouslyEnabled_12() { return static_cast<int32_t>(offsetof(VirtualButtonBehaviour_t1436326451, ___mPreviouslyEnabled_12)); }
	inline bool get_mPreviouslyEnabled_12() const { return ___mPreviouslyEnabled_12; }
	inline bool* get_address_of_mPreviouslyEnabled_12() { return &___mPreviouslyEnabled_12; }
	inline void set_mPreviouslyEnabled_12(bool value)
	{
		___mPreviouslyEnabled_12 = value;
	}

	inline static int32_t get_offset_of_mPressed_13() { return static_cast<int32_t>(offsetof(VirtualButtonBehaviour_t1436326451, ___mPressed_13)); }
	inline bool get_mPressed_13() const { return ___mPressed_13; }
	inline bool* get_address_of_mPressed_13() { return &___mPressed_13; }
	inline void set_mPressed_13(bool value)
	{
		___mPressed_13 = value;
	}

	inline static int32_t get_offset_of_mHandlers_14() { return static_cast<int32_t>(offsetof(VirtualButtonBehaviour_t1436326451, ___mHandlers_14)); }
	inline List_1_t365750880 * get_mHandlers_14() const { return ___mHandlers_14; }
	inline List_1_t365750880 ** get_address_of_mHandlers_14() { return &___mHandlers_14; }
	inline void set_mHandlers_14(List_1_t365750880 * value)
	{
		___mHandlers_14 = value;
		Il2CppCodeGenWriteBarrier((&___mHandlers_14), value);
	}

	inline static int32_t get_offset_of_mLeftTop_15() { return static_cast<int32_t>(offsetof(VirtualButtonBehaviour_t1436326451, ___mLeftTop_15)); }
	inline Vector2_t2156229523  get_mLeftTop_15() const { return ___mLeftTop_15; }
	inline Vector2_t2156229523 * get_address_of_mLeftTop_15() { return &___mLeftTop_15; }
	inline void set_mLeftTop_15(Vector2_t2156229523  value)
	{
		___mLeftTop_15 = value;
	}

	inline static int32_t get_offset_of_mRightBottom_16() { return static_cast<int32_t>(offsetof(VirtualButtonBehaviour_t1436326451, ___mRightBottom_16)); }
	inline Vector2_t2156229523  get_mRightBottom_16() const { return ___mRightBottom_16; }
	inline Vector2_t2156229523 * get_address_of_mRightBottom_16() { return &___mRightBottom_16; }
	inline void set_mRightBottom_16(Vector2_t2156229523  value)
	{
		___mRightBottom_16 = value;
	}

	inline static int32_t get_offset_of_mUnregisterOnDestroy_17() { return static_cast<int32_t>(offsetof(VirtualButtonBehaviour_t1436326451, ___mUnregisterOnDestroy_17)); }
	inline bool get_mUnregisterOnDestroy_17() const { return ___mUnregisterOnDestroy_17; }
	inline bool* get_address_of_mUnregisterOnDestroy_17() { return &___mUnregisterOnDestroy_17; }
	inline void set_mUnregisterOnDestroy_17(bool value)
	{
		___mUnregisterOnDestroy_17 = value;
	}

	inline static int32_t get_offset_of_mVirtualButton_18() { return static_cast<int32_t>(offsetof(VirtualButtonBehaviour_t1436326451, ___mVirtualButton_18)); }
	inline VirtualButton_t386166510 * get_mVirtualButton_18() const { return ___mVirtualButton_18; }
	inline VirtualButton_t386166510 ** get_address_of_mVirtualButton_18() { return &___mVirtualButton_18; }
	inline void set_mVirtualButton_18(VirtualButton_t386166510 * value)
	{
		___mVirtualButton_18 = value;
		Il2CppCodeGenWriteBarrier((&___mVirtualButton_18), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VIRTUALBUTTONBEHAVIOUR_T1436326451_H
#ifndef WIREFRAMEBEHAVIOUR_T1831066704_H
#define WIREFRAMEBEHAVIOUR_T1831066704_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.WireframeBehaviour
struct  WireframeBehaviour_t1831066704  : public VuforiaMonoBehaviour_t1150221792
{
public:
	// UnityEngine.Material Vuforia.WireframeBehaviour::lineMaterial
	Material_t340375123 * ___lineMaterial_4;
	// System.Boolean Vuforia.WireframeBehaviour::ShowLines
	bool ___ShowLines_5;
	// UnityEngine.Color Vuforia.WireframeBehaviour::LineColor
	Color_t2555686324  ___LineColor_6;
	// UnityEngine.Material Vuforia.WireframeBehaviour::mLineMaterial
	Material_t340375123 * ___mLineMaterial_7;

public:
	inline static int32_t get_offset_of_lineMaterial_4() { return static_cast<int32_t>(offsetof(WireframeBehaviour_t1831066704, ___lineMaterial_4)); }
	inline Material_t340375123 * get_lineMaterial_4() const { return ___lineMaterial_4; }
	inline Material_t340375123 ** get_address_of_lineMaterial_4() { return &___lineMaterial_4; }
	inline void set_lineMaterial_4(Material_t340375123 * value)
	{
		___lineMaterial_4 = value;
		Il2CppCodeGenWriteBarrier((&___lineMaterial_4), value);
	}

	inline static int32_t get_offset_of_ShowLines_5() { return static_cast<int32_t>(offsetof(WireframeBehaviour_t1831066704, ___ShowLines_5)); }
	inline bool get_ShowLines_5() const { return ___ShowLines_5; }
	inline bool* get_address_of_ShowLines_5() { return &___ShowLines_5; }
	inline void set_ShowLines_5(bool value)
	{
		___ShowLines_5 = value;
	}

	inline static int32_t get_offset_of_LineColor_6() { return static_cast<int32_t>(offsetof(WireframeBehaviour_t1831066704, ___LineColor_6)); }
	inline Color_t2555686324  get_LineColor_6() const { return ___LineColor_6; }
	inline Color_t2555686324 * get_address_of_LineColor_6() { return &___LineColor_6; }
	inline void set_LineColor_6(Color_t2555686324  value)
	{
		___LineColor_6 = value;
	}

	inline static int32_t get_offset_of_mLineMaterial_7() { return static_cast<int32_t>(offsetof(WireframeBehaviour_t1831066704, ___mLineMaterial_7)); }
	inline Material_t340375123 * get_mLineMaterial_7() const { return ___mLineMaterial_7; }
	inline Material_t340375123 ** get_address_of_mLineMaterial_7() { return &___mLineMaterial_7; }
	inline void set_mLineMaterial_7(Material_t340375123 * value)
	{
		___mLineMaterial_7 = value;
		Il2CppCodeGenWriteBarrier((&___mLineMaterial_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WIREFRAMEBEHAVIOUR_T1831066704_H
#ifndef WIREFRAMETRACKABLEEVENTHANDLER_T2143753312_H
#define WIREFRAMETRACKABLEEVENTHANDLER_T2143753312_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.WireframeTrackableEventHandler
struct  WireframeTrackableEventHandler_t2143753312  : public VuforiaMonoBehaviour_t1150221792
{
public:
	// Vuforia.TrackableBehaviour Vuforia.WireframeTrackableEventHandler::mTrackableBehaviour
	TrackableBehaviour_t1113559212 * ___mTrackableBehaviour_4;

public:
	inline static int32_t get_offset_of_mTrackableBehaviour_4() { return static_cast<int32_t>(offsetof(WireframeTrackableEventHandler_t2143753312, ___mTrackableBehaviour_4)); }
	inline TrackableBehaviour_t1113559212 * get_mTrackableBehaviour_4() const { return ___mTrackableBehaviour_4; }
	inline TrackableBehaviour_t1113559212 ** get_address_of_mTrackableBehaviour_4() { return &___mTrackableBehaviour_4; }
	inline void set_mTrackableBehaviour_4(TrackableBehaviour_t1113559212 * value)
	{
		___mTrackableBehaviour_4 = value;
		Il2CppCodeGenWriteBarrier((&___mTrackableBehaviour_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WIREFRAMETRACKABLEEVENTHANDLER_T2143753312_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2800 = { sizeof (TrackableSource_t2567074243), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2800[1] = 
{
	TrackableSource_t2567074243::get_offset_of_U3CTrackableSourcePtrU3Ek__BackingField_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2801 = { sizeof (Tracker_t2709586299), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2801[1] = 
{
	Tracker_t2709586299::get_offset_of_U3CIsActiveU3Ek__BackingField_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2802 = { sizeof (TrackerManager_t1703337244), -1, sizeof(TrackerManager_t1703337244_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2802[5] = 
{
	TrackerManager_t1703337244_StaticFields::get_offset_of_mInstance_0(),
	TrackerManager_t1703337244::get_offset_of_mStateManager_1(),
	TrackerManager_t1703337244::get_offset_of_mTrackers_2(),
	TrackerManager_t1703337244::get_offset_of_mTrackerCreators_3(),
	TrackerManager_t1703337244::get_offset_of_mTrackerNativeDeinitializers_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2803 = { sizeof (U3CU3Ec_t1451390621), -1, sizeof(U3CU3Ec_t1451390621_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2803[5] = 
{
	U3CU3Ec_t1451390621_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_t1451390621_StaticFields::get_offset_of_U3CU3E9__8_0_1(),
	U3CU3Ec_t1451390621_StaticFields::get_offset_of_U3CU3E9__8_1_2(),
	U3CU3Ec_t1451390621_StaticFields::get_offset_of_U3CU3E9__8_2_3(),
	U3CU3Ec_t1451390621_StaticFields::get_offset_of_U3CU3E9__8_3_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2804 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2805 = { sizeof (UserDefinedTargetBuildingBehaviour_t4262637471), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2805[11] = 
{
	UserDefinedTargetBuildingBehaviour_t4262637471::get_offset_of_mObjectTracker_4(),
	UserDefinedTargetBuildingBehaviour_t4262637471::get_offset_of_mLastFrameQuality_5(),
	UserDefinedTargetBuildingBehaviour_t4262637471::get_offset_of_mCurrentlyScanning_6(),
	UserDefinedTargetBuildingBehaviour_t4262637471::get_offset_of_mWasScanningBeforeDisable_7(),
	UserDefinedTargetBuildingBehaviour_t4262637471::get_offset_of_mCurrentlyBuilding_8(),
	UserDefinedTargetBuildingBehaviour_t4262637471::get_offset_of_mWasBuildingBeforeDisable_9(),
	UserDefinedTargetBuildingBehaviour_t4262637471::get_offset_of_mOnInitializedCalled_10(),
	UserDefinedTargetBuildingBehaviour_t4262637471::get_offset_of_mHandlers_11(),
	UserDefinedTargetBuildingBehaviour_t4262637471::get_offset_of_StopTrackerWhileScanning_12(),
	UserDefinedTargetBuildingBehaviour_t4262637471::get_offset_of_StartScanningAutomatically_13(),
	UserDefinedTargetBuildingBehaviour_t4262637471::get_offset_of_StopScanningWhenFinshedBuilding_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2806 = { sizeof (VideoBackgroundBehaviour_t1552899074), -1, sizeof(VideoBackgroundBehaviour_t1552899074_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2806[8] = 
{
	VideoBackgroundBehaviour_t1552899074::get_offset_of_mClearBuffers_4(),
	VideoBackgroundBehaviour_t1552899074::get_offset_of_mSkipStateUpdates_5(),
	VideoBackgroundBehaviour_t1552899074::get_offset_of_mVuforiaARController_6(),
	VideoBackgroundBehaviour_t1552899074::get_offset_of_mCamera_7(),
	VideoBackgroundBehaviour_t1552899074::get_offset_of_mBackgroundBehaviour_8(),
	VideoBackgroundBehaviour_t1552899074_StaticFields::get_offset_of_mFrameCounter_9(),
	VideoBackgroundBehaviour_t1552899074_StaticFields::get_offset_of_mRenderCounter_10(),
	VideoBackgroundBehaviour_t1552899074::get_offset_of_mDisabledMeshRenderers_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2807 = { sizeof (VideoBackgroundManager_t2198727358), -1, sizeof(VideoBackgroundManager_t2198727358_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2807[8] = 
{
	VideoBackgroundManager_t2198727358::get_offset_of_mClippingMode_1(),
	VideoBackgroundManager_t2198727358::get_offset_of_mMatteShader_2(),
	VideoBackgroundManager_t2198727358::get_offset_of_mVideoBackgroundEnabled_3(),
	VideoBackgroundManager_t2198727358::get_offset_of_mTexture_4(),
	VideoBackgroundManager_t2198727358::get_offset_of_mVideoBgConfigChanged_5(),
	VideoBackgroundManager_t2198727358::get_offset_of_mNativeTexturePtr_6(),
	VideoBackgroundManager_t2198727358_StaticFields::get_offset_of_mInstance_7(),
	VideoBackgroundManager_t2198727358_StaticFields::get_offset_of_mPadlock_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2808 = { sizeof (VirtualButton_t386166510), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2808[7] = 
{
	VirtualButton_t386166510::get_offset_of_mName_0(),
	VirtualButton_t386166510::get_offset_of_mID_1(),
	VirtualButton_t386166510::get_offset_of_mArea_2(),
	VirtualButton_t386166510::get_offset_of_mIsEnabled_3(),
	VirtualButton_t386166510::get_offset_of_mParentImageTarget_4(),
	VirtualButton_t386166510::get_offset_of_mParentDataSet_5(),
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2809 = { sizeof (Sensitivity_t3045829715)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2809[4] = 
{
	Sensitivity_t3045829715::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2810 = { sizeof (VirtualButtonBehaviour_t1436326451), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2810[15] = 
{
	0,
	VirtualButtonBehaviour_t1436326451::get_offset_of_mName_5(),
	VirtualButtonBehaviour_t1436326451::get_offset_of_mSensitivity_6(),
	VirtualButtonBehaviour_t1436326451::get_offset_of_mHasUpdatedPose_7(),
	VirtualButtonBehaviour_t1436326451::get_offset_of_mPrevTransform_8(),
	VirtualButtonBehaviour_t1436326451::get_offset_of_mPrevParent_9(),
	VirtualButtonBehaviour_t1436326451::get_offset_of_mSensitivityDirty_10(),
	VirtualButtonBehaviour_t1436326451::get_offset_of_mPreviousSensitivity_11(),
	VirtualButtonBehaviour_t1436326451::get_offset_of_mPreviouslyEnabled_12(),
	VirtualButtonBehaviour_t1436326451::get_offset_of_mPressed_13(),
	VirtualButtonBehaviour_t1436326451::get_offset_of_mHandlers_14(),
	VirtualButtonBehaviour_t1436326451::get_offset_of_mLeftTop_15(),
	VirtualButtonBehaviour_t1436326451::get_offset_of_mRightBottom_16(),
	VirtualButtonBehaviour_t1436326451::get_offset_of_mUnregisterOnDestroy_17(),
	VirtualButtonBehaviour_t1436326451::get_offset_of_mVirtualButton_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2811 = { sizeof (WebCamARController_t3718642882), -1, sizeof(WebCamARController_t3718642882_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2811[7] = 
{
	WebCamARController_t3718642882::get_offset_of_RenderTextureLayer_1(),
	WebCamARController_t3718642882::get_offset_of_mDeviceNameSetInEditor_2(),
	WebCamARController_t3718642882::get_offset_of_mFlipHorizontally_3(),
	WebCamARController_t3718642882::get_offset_of_mWebCamImpl_4(),
	WebCamARController_t3718642882::get_offset_of_mWebCamTexAdaptorProvider_5(),
	WebCamARController_t3718642882_StaticFields::get_offset_of_mInstance_6(),
	WebCamARController_t3718642882_StaticFields::get_offset_of_mPadlock_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2812 = { sizeof (U3CU3Ec_t3582055403), -1, sizeof(U3CU3Ec_t3582055403_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2812[2] = 
{
	U3CU3Ec_t3582055403_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_t3582055403_StaticFields::get_offset_of_U3CU3E9__7_0_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2813 = { sizeof (EyewearCalibrationProfileManager_t947793426), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2814 = { sizeof (EyewearUserCalibrator_t2926839199), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2815 = { sizeof (WireframeBehaviour_t1831066704), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2815[4] = 
{
	WireframeBehaviour_t1831066704::get_offset_of_lineMaterial_4(),
	WireframeBehaviour_t1831066704::get_offset_of_ShowLines_5(),
	WireframeBehaviour_t1831066704::get_offset_of_LineColor_6(),
	WireframeBehaviour_t1831066704::get_offset_of_mLineMaterial_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2816 = { sizeof (WireframeTrackableEventHandler_t2143753312), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2816[1] = 
{
	WireframeTrackableEventHandler_t2143753312::get_offset_of_mTrackableBehaviour_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2817 = { sizeof (U3CPrivateImplementationDetailsU3E_t3057255368), -1, sizeof(U3CPrivateImplementationDetailsU3E_t3057255368_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2817[1] = 
{
	U3CPrivateImplementationDetailsU3E_t3057255368_StaticFields::get_offset_of_U3898C2022A0C02FCE602BF05E1C09BD48301606E5_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2818 = { sizeof (__StaticArrayInitTypeSizeU3D24_t3517759979)+ sizeof (RuntimeObject), sizeof(__StaticArrayInitTypeSizeU3D24_t3517759979 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2819 = { sizeof (U3CModuleU3E_t692745552), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2820 = { sizeof (AnalyticsEventTracker_t2285229262), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2820[2] = 
{
	AnalyticsEventTracker_t2285229262::get_offset_of_m_Trigger_4(),
	AnalyticsEventTracker_t2285229262::get_offset_of_m_EventPayload_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2821 = { sizeof (U3CTimedTriggerU3Ec__Iterator0_t3813435494), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2821[4] = 
{
	U3CTimedTriggerU3Ec__Iterator0_t3813435494::get_offset_of_U24this_0(),
	U3CTimedTriggerU3Ec__Iterator0_t3813435494::get_offset_of_U24current_1(),
	U3CTimedTriggerU3Ec__Iterator0_t3813435494::get_offset_of_U24disposing_2(),
	U3CTimedTriggerU3Ec__Iterator0_t3813435494::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2822 = { sizeof (AnalyticsEventTrackerSettings_t480422680), -1, sizeof(AnalyticsEventTrackerSettings_t480422680_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2822[2] = 
{
	AnalyticsEventTrackerSettings_t480422680_StaticFields::get_offset_of_paramCountMax_0(),
	AnalyticsEventTrackerSettings_t480422680_StaticFields::get_offset_of_triggerRuleCountMax_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2823 = { sizeof (AnalyticsEventParam_t2480121928), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2823[5] = 
{
	AnalyticsEventParam_t2480121928::get_offset_of_m_RequirementType_0(),
	AnalyticsEventParam_t2480121928::get_offset_of_m_GroupID_1(),
	AnalyticsEventParam_t2480121928::get_offset_of_m_Tooltip_2(),
	AnalyticsEventParam_t2480121928::get_offset_of_m_Name_3(),
	AnalyticsEventParam_t2480121928::get_offset_of_m_Value_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2824 = { sizeof (RequirementType_t3584265503)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2824[4] = 
{
	RequirementType_t3584265503::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2825 = { sizeof (AnalyticsEventParamListContainer_t587083383), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2825[1] = 
{
	AnalyticsEventParamListContainer_t587083383::get_offset_of_m_Parameters_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2826 = { sizeof (StandardEventPayload_t1629891255), -1, sizeof(StandardEventPayload_t1629891255_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2826[6] = 
{
	StandardEventPayload_t1629891255::get_offset_of_m_IsEventExpanded_0(),
	StandardEventPayload_t1629891255::get_offset_of_m_StandardEventType_1(),
	StandardEventPayload_t1629891255::get_offset_of_standardEventType_2(),
	StandardEventPayload_t1629891255::get_offset_of_m_Parameters_3(),
	StandardEventPayload_t1629891255_StaticFields::get_offset_of_m_EventData_4(),
	StandardEventPayload_t1629891255::get_offset_of_m_Name_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2827 = { sizeof (TrackableField_t1772682203), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2827[3] = 
{
	TrackableField_t1772682203::get_offset_of_m_ValidTypeNames_2(),
	TrackableField_t1772682203::get_offset_of_m_Type_3(),
	TrackableField_t1772682203::get_offset_of_m_EnumType_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2828 = { sizeof (TrackablePropertyBase_t2121532948), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2828[2] = 
{
	TrackablePropertyBase_t2121532948::get_offset_of_m_Target_0(),
	TrackablePropertyBase_t2121532948::get_offset_of_m_Path_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2829 = { sizeof (ValueProperty_t1868393739), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2829[11] = 
{
	ValueProperty_t1868393739::get_offset_of_m_EditingCustomValue_0(),
	ValueProperty_t1868393739::get_offset_of_m_PopupIndex_1(),
	ValueProperty_t1868393739::get_offset_of_m_CustomValue_2(),
	ValueProperty_t1868393739::get_offset_of_m_FixedType_3(),
	ValueProperty_t1868393739::get_offset_of_m_EnumType_4(),
	ValueProperty_t1868393739::get_offset_of_m_EnumTypeIsCustomizable_5(),
	ValueProperty_t1868393739::get_offset_of_m_CanDisable_6(),
	ValueProperty_t1868393739::get_offset_of_m_PropertyType_7(),
	ValueProperty_t1868393739::get_offset_of_m_ValueType_8(),
	ValueProperty_t1868393739::get_offset_of_m_Value_9(),
	ValueProperty_t1868393739::get_offset_of_m_Target_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2830 = { sizeof (PropertyType_t4040930247)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2830[4] = 
{
	PropertyType_t4040930247::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2831 = { sizeof (AnalyticsTracker_t731021378), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2831[5] = 
{
	AnalyticsTracker_t731021378::get_offset_of_m_EventName_4(),
	AnalyticsTracker_t731021378::get_offset_of_m_Dict_5(),
	AnalyticsTracker_t731021378::get_offset_of_m_PrevDictHash_6(),
	AnalyticsTracker_t731021378::get_offset_of_m_TrackableProperty_7(),
	AnalyticsTracker_t731021378::get_offset_of_m_Trigger_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2832 = { sizeof (Trigger_t4199345191)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2832[8] = 
{
	Trigger_t4199345191::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2833 = { sizeof (TrackableProperty_t3943537984), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2833[2] = 
{
	0,
	TrackableProperty_t3943537984::get_offset_of_m_Fields_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2834 = { sizeof (FieldWithTarget_t3058750293), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2834[6] = 
{
	FieldWithTarget_t3058750293::get_offset_of_m_ParamName_0(),
	FieldWithTarget_t3058750293::get_offset_of_m_Target_1(),
	FieldWithTarget_t3058750293::get_offset_of_m_FieldPath_2(),
	FieldWithTarget_t3058750293::get_offset_of_m_TypeString_3(),
	FieldWithTarget_t3058750293::get_offset_of_m_DoStatic_4(),
	FieldWithTarget_t3058750293::get_offset_of_m_StaticString_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2835 = { sizeof (TriggerBool_t501031542)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2835[4] = 
{
	TriggerBool_t501031542::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2836 = { sizeof (TriggerLifecycleEvent_t3193146760)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2836[9] = 
{
	TriggerLifecycleEvent_t3193146760::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2837 = { sizeof (TriggerOperator_t3611898925)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2837[9] = 
{
	TriggerOperator_t3611898925::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2838 = { sizeof (TriggerType_t105272677)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2838[5] = 
{
	TriggerType_t105272677::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2839 = { sizeof (TriggerListContainer_t2032715483), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2839[1] = 
{
	TriggerListContainer_t2032715483::get_offset_of_m_Rules_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2840 = { sizeof (EventTrigger_t2527451695), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2840[12] = 
{
	EventTrigger_t2527451695::get_offset_of_m_IsTriggerExpanded_0(),
	EventTrigger_t2527451695::get_offset_of_m_Type_1(),
	EventTrigger_t2527451695::get_offset_of_m_LifecycleEvent_2(),
	EventTrigger_t2527451695::get_offset_of_m_ApplyRules_3(),
	EventTrigger_t2527451695::get_offset_of_m_Rules_4(),
	EventTrigger_t2527451695::get_offset_of_m_TriggerBool_5(),
	EventTrigger_t2527451695::get_offset_of_m_InitTime_6(),
	EventTrigger_t2527451695::get_offset_of_m_RepeatTime_7(),
	EventTrigger_t2527451695::get_offset_of_m_Repetitions_8(),
	EventTrigger_t2527451695::get_offset_of_repetitionCount_9(),
	EventTrigger_t2527451695::get_offset_of_m_TriggerFunction_10(),
	EventTrigger_t2527451695::get_offset_of_m_Method_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2841 = { sizeof (OnTrigger_t4184125570), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2842 = { sizeof (TrackableTrigger_t621205209), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2842[2] = 
{
	TrackableTrigger_t621205209::get_offset_of_m_Target_0(),
	TrackableTrigger_t621205209::get_offset_of_m_MethodPath_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2843 = { sizeof (TriggerMethod_t582536534), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2844 = { sizeof (TriggerRule_t1946298321), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2844[4] = 
{
	TriggerRule_t1946298321::get_offset_of_m_Target_0(),
	TriggerRule_t1946298321::get_offset_of_m_Operator_1(),
	TriggerRule_t1946298321::get_offset_of_m_Value_2(),
	TriggerRule_t1946298321::get_offset_of_m_Value2_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2845 = { sizeof (U3CModuleU3E_t692745553), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2846 = { sizeof (DefaultInitializationErrorHandler_t3109936861), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2846[9] = 
{
	DefaultInitializationErrorHandler_t3109936861::get_offset_of_mErrorText_4(),
	DefaultInitializationErrorHandler_t3109936861::get_offset_of_mErrorOccurred_5(),
	0,
	DefaultInitializationErrorHandler_t3109936861::get_offset_of_bodyStyle_7(),
	DefaultInitializationErrorHandler_t3109936861::get_offset_of_headerStyle_8(),
	DefaultInitializationErrorHandler_t3109936861::get_offset_of_footerStyle_9(),
	DefaultInitializationErrorHandler_t3109936861::get_offset_of_bodyTexture_10(),
	DefaultInitializationErrorHandler_t3109936861::get_offset_of_headerTexture_11(),
	DefaultInitializationErrorHandler_t3109936861::get_offset_of_footerTexture_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2847 = { sizeof (DefaultTrackableEventHandler_t1588957063), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2847[3] = 
{
	DefaultTrackableEventHandler_t1588957063::get_offset_of_mTrackableBehaviour_4(),
	DefaultTrackableEventHandler_t1588957063::get_offset_of_m_PreviousStatus_5(),
	DefaultTrackableEventHandler_t1588957063::get_offset_of_m_NewStatus_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2848 = { sizeof (U3CModuleU3E_t692745554), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2849 = { sizeof (AudioManager_t3267510698), -1, sizeof(AudioManager_t3267510698_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2849[3] = 
{
	AudioManager_t3267510698_StaticFields::get_offset_of_instance_4(),
	AudioManager_t3267510698::get_offset_of_mixerGroup_5(),
	AudioManager_t3267510698::get_offset_of_sounds_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2850 = { sizeof (U3CPlayU3Ec__AnonStorey0_t2108231134), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2850[1] = 
{
	U3CPlayU3Ec__AnonStorey0_t2108231134::get_offset_of_sound_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2851 = { sizeof (AnimationEventGameObjectActive_t1825876594), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2852 = { sizeof (Flicker_t3044644596), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2852[9] = 
{
	Flicker_t3044644596::get_offset_of_m_flickerDelay_4(),
	Flicker_t3044644596::get_offset_of_m_flickerAmount_5(),
	Flicker_t3044644596::get_offset_of_m_startFlickerDelay_6(),
	Flicker_t3044644596::get_offset_of_m_meshRenderer_7(),
	Flicker_t3044644596::get_offset_of_m_useShader_8(),
	Flicker_t3044644596::get_offset_of_m_amount_9(),
	Flicker_t3044644596::get_offset_of_m_timer_10(),
	Flicker_t3044644596::get_offset_of_m_startTimer_11(),
	Flicker_t3044644596::get_offset_of_m_startFlicker_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2853 = { sizeof (FollowCamera_t4234496323), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2853[2] = 
{
	FollowCamera_t4234496323::get_offset_of_m_cameraTransform_4(),
	FollowCamera_t4234496323::get_offset_of_m_offset_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2854 = { sizeof (FPSController_t1027197541), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2854[3] = 
{
	FPSController_t1027197541::get_offset_of_targetFPS_4(),
	FPSController_t1027197541::get_offset_of_frequency_5(),
	FPSController_t1027197541::get_offset_of_fps_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2855 = { sizeof (U3CFPSU3Ec__Iterator0_t1547198559), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2855[8] = 
{
	U3CFPSU3Ec__Iterator0_t1547198559::get_offset_of_U3ClastFrameCountU3E__1_0(),
	U3CFPSU3Ec__Iterator0_t1547198559::get_offset_of_U3ClastTimeU3E__1_1(),
	U3CFPSU3Ec__Iterator0_t1547198559::get_offset_of_U3CtimeSpanU3E__1_2(),
	U3CFPSU3Ec__Iterator0_t1547198559::get_offset_of_U3CframeCountU3E__1_3(),
	U3CFPSU3Ec__Iterator0_t1547198559::get_offset_of_U24this_4(),
	U3CFPSU3Ec__Iterator0_t1547198559::get_offset_of_U24current_5(),
	U3CFPSU3Ec__Iterator0_t1547198559::get_offset_of_U24disposing_6(),
	U3CFPSU3Ec__Iterator0_t1547198559::get_offset_of_U24PC_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2856 = { sizeof (HitDetection_t1652923020), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2856[11] = 
{
	HitDetection_t1652923020::get_offset_of_m_bubblePopEvent_4(),
	HitDetection_t1652923020::get_offset_of_m_freezePopEvent_5(),
	HitDetection_t1652923020::get_offset_of_m_upAssignmentEvent_6(),
	HitDetection_t1652923020::get_offset_of_m_killEnemy_7(),
	HitDetection_t1652923020::get_offset_of_m_energy_8(),
	HitDetection_t1652923020::get_offset_of_m_maxEnergy_9(),
	HitDetection_t1652923020::get_offset_of_m_hit_10(),
	HitDetection_t1652923020::get_offset_of_m_ray_11(),
	HitDetection_t1652923020::get_offset_of_m_DGADpressAssignment_12(),
	HitDetection_t1652923020::get_offset_of_m_resetPenType_13(),
	HitDetection_t1652923020::get_offset_of_m_resetPenTypeUI_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2857 = { sizeof (ImageDetected_t3895480982), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2857[7] = 
{
	ImageDetected_t3895480982::get_offset_of_targetScenes_4(),
	ImageDetected_t3895480982::get_offset_of_targetGameLogic_5(),
	ImageDetected_t3895480982::get_offset_of_m_ImageDetected_6(),
	ImageDetected_t3895480982::get_offset_of_m_GlobalImageDetected_7(),
	ImageDetected_t3895480982::get_offset_of_m_ImageLost_8(),
	ImageDetected_t3895480982::get_offset_of_m_GlobalImageLost_9(),
	ImageDetected_t3895480982::get_offset_of_mTrackableBehaviour_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2858 = { sizeof (MaterialwColorHolder_t2420114684), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2858[2] = 
{
	MaterialwColorHolder_t2420114684::get_offset_of_m_mat_0(),
	MaterialwColorHolder_t2420114684::get_offset_of_m_color_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2859 = { sizeof (MeshHolder_t304490584), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2859[2] = 
{
	MeshHolder_t304490584::get_offset_of_m_mesh_0(),
	MeshHolder_t304490584::get_offset_of_m_texture_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2860 = { sizeof (Mover_t2250641681), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2860[6] = 
{
	Mover_t2250641681::get_offset_of_m_maxDistance_4(),
	Mover_t2250641681::get_offset_of_m_startingPos_5(),
	Mover_t2250641681::get_offset_of_m_endPos_6(),
	Mover_t2250641681::get_offset_of_m_movingSpeed_7(),
	Mover_t2250641681::get_offset_of_m_finalSpeed_8(),
	Mover_t2250641681::get_offset_of_m_isReachedEnd_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2861 = { sizeof (OnTouchSpawn_t2434404655), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2861[5] = 
{
	OnTouchSpawn_t2434404655::get_offset_of_objectToSpawn_4(),
	OnTouchSpawn_t2434404655::get_offset_of_objectLifetime_5(),
	OnTouchSpawn_t2434404655::get_offset_of_eventsToFire_6(),
	OnTouchSpawn_t2434404655::get_offset_of_clicksToActivate_7(),
	OnTouchSpawn_t2434404655::get_offset_of_accumulatedClicks_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2862 = { sizeof (ParticleSpawner_t2092424095), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2862[4] = 
{
	ParticleSpawner_t2092424095::get_offset_of_m_particles_4(),
	ParticleSpawner_t2092424095::get_offset_of_m_delayTimer_5(),
	ParticleSpawner_t2092424095::get_offset_of_m_timer_6(),
	ParticleSpawner_t2092424095::get_offset_of_m_totalParticleTime_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2863 = { sizeof (Scaler_t1764693111), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2863[2] = 
{
	Scaler_t1764693111::get_offset_of_m_maxSize_4(),
	Scaler_t1764693111::get_offset_of_m_scaleSpeed_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2864 = { sizeof (SceneTransition_t1138091307), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2864[7] = 
{
	SceneTransition_t1138091307::get_offset_of_nextSceneName_4(),
	SceneTransition_t1138091307::get_offset_of_fadeColor_5(),
	SceneTransition_t1138091307::get_offset_of_fadeTime_6(),
	SceneTransition_t1138091307::get_offset_of_autoChange_7(),
	SceneTransition_t1138091307::get_offset_of_fader_8(),
	SceneTransition_t1138091307::get_offset_of_currAlpha_9(),
	SceneTransition_t1138091307::get_offset_of_endAlpha_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2865 = { sizeof (TextMeshEditor_t1187695439), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2865[2] = 
{
	TextMeshEditor_t1187695439::get_offset_of_m_textMesh_4(),
	TextMeshEditor_t1187695439::get_offset_of_m_text_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2866 = { sizeof (TextureSwitch_t147972937), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2866[4] = 
{
	TextureSwitch_t147972937::get_offset_of_m_switchTimer_4(),
	TextureSwitch_t147972937::get_offset_of_m_timer_5(),
	TextureSwitch_t147972937::get_offset_of_m_meshRenderer_6(),
	TextureSwitch_t147972937::get_offset_of_m_switch_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2867 = { sizeof (Tweener_t3008954595), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2867[45] = 
{
	Tweener_t3008954595::get_offset_of_startOnEnable_4(),
	Tweener_t3008954595::get_offset_of_startRepeatOnEnable_5(),
	Tweener_t3008954595::get_offset_of_disableOnEndTween_6(),
	Tweener_t3008954595::get_offset_of_autoEndTween_7(),
	Tweener_t3008954595::get_offset_of_setToStart_8(),
	Tweener_t3008954595::get_offset_of_tweenPosition_9(),
	Tweener_t3008954595::get_offset_of_tweenPositionRepeat_10(),
	Tweener_t3008954595::get_offset_of_startPosition_11(),
	Tweener_t3008954595::get_offset_of_endPosition_12(),
	Tweener_t3008954595::get_offset_of_currPosition_13(),
	Tweener_t3008954595::get_offset_of_positionEaseType_14(),
	Tweener_t3008954595::get_offset_of_positionTweenDuration_15(),
	Tweener_t3008954595::get_offset_of_positionTweenTarget_16(),
	Tweener_t3008954595::get_offset_of_tweenRotation_17(),
	Tweener_t3008954595::get_offset_of_tweenRotationRepeat_18(),
	Tweener_t3008954595::get_offset_of_startRotation_19(),
	Tweener_t3008954595::get_offset_of_endRotation_20(),
	Tweener_t3008954595::get_offset_of_currRotation_21(),
	Tweener_t3008954595::get_offset_of_rotationEaseType_22(),
	Tweener_t3008954595::get_offset_of_rotationTweenDuration_23(),
	Tweener_t3008954595::get_offset_of_rotationTweenTarget_24(),
	Tweener_t3008954595::get_offset_of_tweenScale_25(),
	Tweener_t3008954595::get_offset_of_tweenScaleRepeat_26(),
	Tweener_t3008954595::get_offset_of_startScale_27(),
	Tweener_t3008954595::get_offset_of_endScale_28(),
	Tweener_t3008954595::get_offset_of_currScale_29(),
	Tweener_t3008954595::get_offset_of_scaleEaseType_30(),
	Tweener_t3008954595::get_offset_of_scaleTweenDuration_31(),
	Tweener_t3008954595::get_offset_of_scaleTweenTarget_32(),
	Tweener_t3008954595::get_offset_of_tweenColor_33(),
	Tweener_t3008954595::get_offset_of_tweenColorRepeat_34(),
	Tweener_t3008954595::get_offset_of_startColor_35(),
	Tweener_t3008954595::get_offset_of_endColor_36(),
	Tweener_t3008954595::get_offset_of_currColor_37(),
	Tweener_t3008954595::get_offset_of_colorEaseType_38(),
	Tweener_t3008954595::get_offset_of_colorTweenDuration_39(),
	Tweener_t3008954595::get_offset_of_colorTweenTarget_40(),
	Tweener_t3008954595::get_offset_of_tweenAlpha_41(),
	Tweener_t3008954595::get_offset_of_tweenAlphaRepeat_42(),
	Tweener_t3008954595::get_offset_of_startAlpha_43(),
	Tweener_t3008954595::get_offset_of_endAlpha_44(),
	Tweener_t3008954595::get_offset_of_currAlpha_45(),
	Tweener_t3008954595::get_offset_of_alphaEaseType_46(),
	Tweener_t3008954595::get_offset_of_alphaTweenDuration_47(),
	Tweener_t3008954595::get_offset_of_alphaTweenTarget_48(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2868 = { sizeof (Dialogue_t118236717), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2868[1] = 
{
	Dialogue_t118236717::get_offset_of_sentences_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2869 = { sizeof (DialogueManager_t3506686710), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2869[2] = 
{
	DialogueManager_t3506686710::get_offset_of_dialogueText_4(),
	DialogueManager_t3506686710::get_offset_of_sentences_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2870 = { sizeof (U3CTypeSentenceU3Ec__Iterator0_t2432906426), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2870[8] = 
{
	U3CTypeSentenceU3Ec__Iterator0_t2432906426::get_offset_of_sentence_0(),
	U3CTypeSentenceU3Ec__Iterator0_t2432906426::get_offset_of_U24locvar0_1(),
	U3CTypeSentenceU3Ec__Iterator0_t2432906426::get_offset_of_U24locvar1_2(),
	U3CTypeSentenceU3Ec__Iterator0_t2432906426::get_offset_of_U3CletterU3E__1_3(),
	U3CTypeSentenceU3Ec__Iterator0_t2432906426::get_offset_of_U24this_4(),
	U3CTypeSentenceU3Ec__Iterator0_t2432906426::get_offset_of_U24current_5(),
	U3CTypeSentenceU3Ec__Iterator0_t2432906426::get_offset_of_U24disposing_6(),
	U3CTypeSentenceU3Ec__Iterator0_t2432906426::get_offset_of_U24PC_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2871 = { sizeof (DialogueTrigger_t3849820820), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2871[1] = 
{
	DialogueTrigger_t3849820820::get_offset_of_dialogue_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2872 = { sizeof (ObjectState_t1202103565), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2872[6] = 
{
	ObjectState_t1202103565::get_offset_of_m_affectFever_4(),
	ObjectState_t1202103565::get_offset_of_m_affectCombo_5(),
	ObjectState_t1202103565::get_offset_of_m_FeverAmount_6(),
	ObjectState_t1202103565::get_offset_of_m_typeAmount_7(),
	ObjectState_t1202103565::get_offset_of_m_comboAmount_8(),
	ObjectState_t1202103565::get_offset_of_m_type_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2873 = { sizeof (TYPE_t2057018713)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2873[4] = 
{
	TYPE_t2057018713::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2874 = { sizeof (StateWithMaterials_t3107498960), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2874[2] = 
{
	StateWithMaterials_t3107498960::get_offset_of_m_state_0(),
	StateWithMaterials_t3107498960::get_offset_of_m_matList_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2875 = { sizeof (EventListener_t3984233865), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2875[2] = 
{
	EventListener_t3984233865::get_offset_of_GameEvent_4(),
	EventListener_t3984233865::get_offset_of_Response_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2876 = { sizeof (GameEvent_t4069578135), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2876[1] = 
{
	GameEvent_t4069578135::get_offset_of_Event_Listeners_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2877 = { sizeof (DGADGameLogic_t3582670137), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2877[17] = 
{
	DGADGameLogic_t3582670137::get_offset_of_m_energyIncrement_6(),
	DGADGameLogic_t3582670137::get_offset_of_m_currEnergy_7(),
	DGADGameLogic_t3582670137::get_offset_of_m_maxEnergy_8(),
	DGADGameLogic_t3582670137::get_offset_of_m_currHealth_9(),
	DGADGameLogic_t3582670137::get_offset_of_m_maxHealth_10(),
	DGADGameLogic_t3582670137::get_offset_of_m_assignLevel_11(),
	DGADGameLogic_t3582670137::get_offset_of_m_destroyAllEnemies_12(),
	DGADGameLogic_t3582670137::get_offset_of_m_spawnEnemies_13(),
	DGADGameLogic_t3582670137::get_offset_of_m_resetAssignmentStage_14(),
	DGADGameLogic_t3582670137::get_offset_of_m_resetPenType_15(),
	DGADGameLogic_t3582670137::get_offset_of_spawnTimer_16(),
	DGADGameLogic_t3582670137::get_offset_of_baseSpawnTime_17(),
	DGADGameLogic_t3582670137::get_offset_of_waveTimer_18(),
	DGADGameLogic_t3582670137::get_offset_of_baseWaveTime_19(),
	DGADGameLogic_t3582670137::get_offset_of_wave_20(),
	DGADGameLogic_t3582670137::get_offset_of_mons_21(),
	DGADGameLogic_t3582670137::get_offset_of_canSpawn_22(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2878 = { sizeof (GDTGameLogic_t4243140947), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2878[32] = 
{
	GDTGameLogic_t4243140947::get_offset_of_currTimer_6(),
	GDTGameLogic_t4243140947::get_offset_of_currFever_7(),
	GDTGameLogic_t4243140947::get_offset_of_maxTimer_8(),
	GDTGameLogic_t4243140947::get_offset_of_maxFever_9(),
	GDTGameLogic_t4243140947::get_offset_of_currHealth_10(),
	GDTGameLogic_t4243140947::get_offset_of_currCombo_11(),
	GDTGameLogic_t4243140947::get_offset_of_currScore_12(),
	GDTGameLogic_t4243140947::get_offset_of_maxScore_13(),
	GDTGameLogic_t4243140947::get_offset_of_maxHealth_14(),
	0,
	GDTGameLogic_t4243140947::get_offset_of_feverBubbleSpawnTime_16(),
	GDTGameLogic_t4243140947::get_offset_of_isFeverMode_17(),
	GDTGameLogic_t4243140947::get_offset_of_isPowerUpSpawned_18(),
	GDTGameLogic_t4243140947::get_offset_of_spawnTimer_19(),
	GDTGameLogic_t4243140947::get_offset_of_spawnTime_20(),
	GDTGameLogic_t4243140947::get_offset_of_baseSpawnTime_21(),
	GDTGameLogic_t4243140947::get_offset_of_minSpawnTime_22(),
	GDTGameLogic_t4243140947::get_offset_of_freezeDuration_23(),
	GDTGameLogic_t4243140947::get_offset_of_freezeTimer_24(),
	GDTGameLogic_t4243140947::get_offset_of_hurtTime_25(),
	GDTGameLogic_t4243140947::get_offset_of_hurtTimer_26(),
	GDTGameLogic_t4243140947::get_offset_of_spawnBubble_27(),
	GDTGameLogic_t4243140947::get_offset_of_spawnFeverBubble_28(),
	GDTGameLogic_t4243140947::get_offset_of_spawnPowerUp_29(),
	GDTGameLogic_t4243140947::get_offset_of_destroyRedBubble_30(),
	GDTGameLogic_t4243140947::get_offset_of_destroyAllBubble_31(),
	GDTGameLogic_t4243140947::get_offset_of_hurtStart_32(),
	GDTGameLogic_t4243140947::get_offset_of_hurtEnd_33(),
	GDTGameLogic_t4243140947::get_offset_of_feverStart_34(),
	GDTGameLogic_t4243140947::get_offset_of_feverEnd_35(),
	GDTGameLogic_t4243140947::get_offset_of_freezeStart_36(),
	GDTGameLogic_t4243140947::get_offset_of_freezeEnd_37(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2879 = { sizeof (GameLogicState_t909530467)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2879[8] = 
{
	GameLogicState_t909530467::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2880 = { sizeof (IGameLogic_t3343192399), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2880[2] = 
{
	IGameLogic_t3343192399::get_offset_of_gameLogicState_4(),
	IGameLogic_t3343192399::get_offset_of_miniGameManager_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2881 = { sizeof (BoolVariable_t415785710), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2882 = { sizeof (ByteVariable_t2671702712), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2883 = { sizeof (DoubleVariable_t2292910480), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2884 = { sizeof (FloatVariable_t761998486), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2885 = { sizeof (GameObjectList_t328636558), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2886 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2886[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2887 = { sizeof (GenericReference_t1852046461), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2887[1] = 
{
	GenericReference_t1852046461::get_offset_of_m_UseConstantVariable_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2888 = { sizeof (FloatReference_t1374219879), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2888[2] = 
{
	FloatReference_t1374219879::get_offset_of_m_Variable_1(),
	FloatReference_t1374219879::get_offset_of_m_ConstantVariable_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2889 = { sizeof (IntReference_t2842414034), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2889[2] = 
{
	IntReference_t2842414034::get_offset_of_m_Variable_1(),
	IntReference_t2842414034::get_offset_of_m_ConstantVariable_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2890 = { sizeof (ByteReference_t441371523), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2890[2] = 
{
	ByteReference_t441371523::get_offset_of_m_Variable_1(),
	ByteReference_t441371523::get_offset_of_m_ConstantVariable_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2891 = { sizeof (DoubleReference_t1729311906), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2891[2] = 
{
	DoubleReference_t1729311906::get_offset_of_m_Variable_1(),
	DoubleReference_t1729311906::get_offset_of_m_ConstantVariable_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2892 = { sizeof (LongReference_t3770171985), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2892[2] = 
{
	LongReference_t3770171985::get_offset_of_m_Variable_1(),
	LongReference_t3770171985::get_offset_of_m_ConstantVariable_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2893 = { sizeof (ShortReference_t1700820959), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2893[2] = 
{
	ShortReference_t1700820959::get_offset_of_m_Variable_1(),
	ShortReference_t1700820959::get_offset_of_m_ConstantVariable_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2894 = { sizeof (UintReference_t2491003739), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2894[2] = 
{
	UintReference_t2491003739::get_offset_of_m_Variable_1(),
	UintReference_t2491003739::get_offset_of_m_ConstantVariable_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2895 = { sizeof (BoolReference_t2741665770), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2895[2] = 
{
	BoolReference_t2741665770::get_offset_of_m_Variable_1(),
	BoolReference_t2741665770::get_offset_of_m_ConstantVariable_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2896 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2896[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2897 = { sizeof (IntVariable_t1706759505), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2898 = { sizeof (LongVariable_t812939517), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2899 = { sizeof (MaterialList_t2938811163), -1, 0, 0 };
#ifdef __clang__
#pragma clang diagnostic pop
#endif
