﻿using UnityEngine;

[CreateAssetMenu(menuName = "Variables/List/ScriptableObject")]
public class ScriptableObjectList : GenericList<ScriptableObject> { }
