﻿using System.Collections;
using UnityEngine;

public abstract class GenericVariable<T> : ScriptableObject {

    [SerializeField]
    private T 
        m_value;

    public T GetVariable()
    {
        return m_value;
    }

    public void SetVariable(T _value)
    {
        m_value = _value;
    }
}
