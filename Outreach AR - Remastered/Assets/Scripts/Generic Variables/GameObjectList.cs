﻿using UnityEngine;

[CreateAssetMenu(menuName ="Variables/List/GameObject")]
public class GameObjectList : GenericList<GameObject> { }
