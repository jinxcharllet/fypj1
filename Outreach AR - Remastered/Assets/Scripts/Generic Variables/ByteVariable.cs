﻿using UnityEngine;

[CreateAssetMenu(menuName = "Variables/Byte")]
public class ByteVariable : GenericVariable<byte> { }
