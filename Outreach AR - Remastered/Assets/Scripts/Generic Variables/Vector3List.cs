﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Variables/List/Vector3 List")]
public class Vector3List : GenericList<Vector3>
{
}
