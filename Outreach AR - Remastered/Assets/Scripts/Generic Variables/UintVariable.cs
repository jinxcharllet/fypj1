﻿using UnityEngine;

[CreateAssetMenu(menuName = "Variables/Uint")]
public class UintVariable : GenericVariable<uint> { }
