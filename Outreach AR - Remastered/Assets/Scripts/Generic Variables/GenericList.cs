﻿using System.Collections.Generic;
using UnityEngine;

public abstract class GenericList<T> : ScriptableObject
{
    [SerializeField]
    private List<T>
        m_value = new List<T>();

    public List<T> GetList()
    {
        return m_value;
    }
}
