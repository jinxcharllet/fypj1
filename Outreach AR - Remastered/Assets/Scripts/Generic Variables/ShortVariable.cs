﻿using UnityEngine;

[CreateAssetMenu(menuName = "Variables/Short")]
public class ShortVariable : GenericVariable<short> { }
