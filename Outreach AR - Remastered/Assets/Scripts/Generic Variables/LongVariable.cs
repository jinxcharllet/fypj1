﻿using UnityEngine;

[CreateAssetMenu(menuName = "Variables/Long")]
public class LongVariable : GenericVariable<long> { }
