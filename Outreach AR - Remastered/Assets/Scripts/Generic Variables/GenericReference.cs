﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class GenericReference
{
    [SerializeField]
    public bool
        m_UseConstantVariable;
}

[Serializable]
public class FloatReference : GenericReference
{
    [SerializeField]
    private FloatVariable
        m_Variable;

    [SerializeField]
    private float
        m_ConstantVariable;

    public float Value
    {
        get { return ((m_UseConstantVariable) ? m_ConstantVariable : m_Variable.GetVariable()); }
    }

}

[Serializable]
public class IntReference : GenericReference
{
    [SerializeField]
    private IntVariable
        m_Variable;

    [SerializeField]
    private int
        m_ConstantVariable;

    public int Value
    {
        get { return ((m_UseConstantVariable) ? m_ConstantVariable : m_Variable.GetVariable()); }
    }

}

[Serializable]
public class ByteReference : GenericReference
{
    [SerializeField]
    private ByteVariable
        m_Variable;

    [SerializeField]
    private byte
        m_ConstantVariable;

    public byte Value
    {
        get { return ((m_UseConstantVariable) ? m_ConstantVariable : m_Variable.GetVariable()); }
    }

}

[Serializable]
public class DoubleReference : GenericReference
{
    [SerializeField]
    private DoubleVariable
        m_Variable;

    [SerializeField]
    private double
        m_ConstantVariable;

    public double Value
    {
        get { return ((m_UseConstantVariable) ? m_ConstantVariable : m_Variable.GetVariable()); }
    }

}

[Serializable]
public class LongReference : GenericReference
{
    [SerializeField]
    private LongVariable
        m_Variable;

    [SerializeField]
    private long
        m_ConstantVariable;

    public long Value
    {
        get { return ((m_UseConstantVariable) ? m_ConstantVariable : m_Variable.GetVariable()); }
    }

}

[Serializable]
public class ShortReference : GenericReference
{
    [SerializeField]
    private ShortVariable
        m_Variable;

    [SerializeField]
    private short
        m_ConstantVariable;

    public short Value
    {
        get { return ((m_UseConstantVariable) ? m_ConstantVariable : m_Variable.GetVariable()); }
    }

}

[Serializable]
public class UintReference : GenericReference
{
    [SerializeField]
    private UintVariable
        m_Variable;

    [SerializeField]
    private uint
        m_ConstantVariable;

    public uint Value
    {
        get { return ((m_UseConstantVariable) ? m_ConstantVariable : m_Variable.GetVariable()); }
    }
}

[Serializable]
public class BoolReference : GenericReference
{
    [SerializeField]
    private BoolVariable
        m_Variable;

    [SerializeField]
    private bool
        m_ConstantVariable;

    public bool Value
    {
        get { return ((m_UseConstantVariable) ? m_ConstantVariable : m_Variable.GetVariable()); }
    }

}