﻿using UnityEngine;

[CreateAssetMenu(menuName = "Variables/List/Material")]
public class MaterialList : GenericList<Material> { }
