﻿using UnityEngine;

[CreateAssetMenu(menuName = "Variables/List/Mesh Holder List")]
public class MeshHolderList : GenericList<MeshHolder> { }
