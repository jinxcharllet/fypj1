﻿using UnityEngine;

[CreateAssetMenu(menuName = "Variables/List/Material With Color List")]
public class MatwColorList : GenericList<MaterialwColorHolder> { }
