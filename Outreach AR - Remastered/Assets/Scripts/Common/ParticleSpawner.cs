﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParticleSpawner : MonoBehaviour
{
    [SerializeField]
    private List<GameObject>
        m_particles;

    [SerializeField]
    private FloatReference
        m_delayTimer;

    private float
        m_timer, 
        m_totalParticleTime;

	void Start ()
    {
        m_timer = m_delayTimer.Value;
        m_totalParticleTime = 0;

        foreach(GameObject go in m_particles)
        {
            ParticleSystem ps = go.GetComponent<ParticleSystem>();

            if (ps.main.duration > m_totalParticleTime)
                m_totalParticleTime = ps.main.duration + ps.main.startLifetime.constantMax;
        }
    }
	
	void Update ()
    {
        if (m_timer <= 0)
        {
            foreach(GameObject go in m_particles)
            {
                GameObject temp = Instantiate(go, gameObject.transform);
                Destroy(temp, m_totalParticleTime);
            }

            m_timer = m_delayTimer.Value;
        }
        else
            m_timer -= Time.deltaTime;
	}
}
