﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class Tweener : MonoBehaviour
{
    [Header("User Preferences")]
    public bool startOnEnable = true;
    public bool startRepeatOnEnable = false;
    public bool disableOnEndTween = true;
    public bool autoEndTween = false;
    [Tooltip("Default inactive values to start values or not")]
    public bool setToStart = false;

    [Header("Position")]
    public bool tweenPosition = false;
    public bool tweenPositionRepeat = false;
    public Vector3 startPosition = Vector3.zero;
    public Vector3 endPosition = new Vector3(10, 10, 10);
    public Vector3 currPosition = Vector3.zero;
    public Ease positionEaseType = Ease.Linear;
    public float positionTweenDuration = 0.2f;
    public Transform positionTweenTarget;

    [Header("Rotation")]
    public bool tweenRotation = false;
    public bool tweenRotationRepeat = false;
    public Vector3 startRotation = Vector3.zero;
    public Vector3 endRotation = Vector3.zero;
    public Vector3 currRotation = Vector3.zero;
    public Ease rotationEaseType = Ease.Linear;
    public float rotationTweenDuration = 0.2f;
    public Transform rotationTweenTarget;

    [Header("Scale")]
    public bool tweenScale = false;
    public bool tweenScaleRepeat = false;
    public Vector3 startScale = Vector3.zero;
    public Vector3 endScale = new Vector3(1, 1, 1);
    public Vector3 currScale = Vector3.zero;
    public Ease scaleEaseType = Ease.Linear;
    public float scaleTweenDuration = 0.2f;
    public Transform scaleTweenTarget;

    [Header("Color")]
    public bool tweenColor = false;
    public bool tweenColorRepeat = false;
    public Color startColor = Color.black;
    public Color endColor = Color.white;
    public Color currColor = Color.white;
    public Ease colorEaseType = Ease.Linear;
    public float colorTweenDuration = 0.2f;
    public Image colorTweenTarget;

    [Header("Alpha")]
    public bool tweenAlpha = false;
    public bool tweenAlphaRepeat = false;
    public float startAlpha = 0.0f;
    public float endAlpha = 1.0f;
    public float currAlpha = 0.0f;
    public Ease alphaEaseType = Ease.Linear;
    public float alphaTweenDuration = 0.2f;
    public Image alphaTweenTarget;

    private void Awake()
    {
        if (tweenPosition)
            if (positionTweenTarget == null)
                positionTweenTarget = gameObject.GetComponent<Transform>();
        if (tweenRotation)
            if (rotationTweenTarget == null)
                rotationTweenTarget = gameObject.GetComponent<Transform>();
        if (tweenScale)
            if (scaleTweenTarget == null)
                scaleTweenTarget = gameObject.GetComponent<Transform>();

        ResetValues();
        UpdateTween();
        
    }

    private void OnEnable()
    {
        if (startOnEnable)
        {
            if (startRepeatOnEnable)
                StartTweenRepeat();
            else
                StartTween();
        }
    }

    private void OnDisable()
    {
        ResetValues();
    }

    private void Update()
    {
        UpdateTween();
    }

    public void StartTweenRepeat()
    {
        RepeatStart();
    }

    private void RepeatStart()
    {
        if (tweenPositionRepeat)
        {
            DOTween.To(() => currPosition, x => currPosition = x, endPosition, positionTweenDuration).SetEase(positionEaseType).OnComplete(RepeatEnd);
        }

        if (tweenRotationRepeat)
        {
            DOTween.To(() => currRotation, x => currRotation = x, endRotation, rotationTweenDuration).SetEase(rotationEaseType).OnComplete(RepeatEnd);
        }

        if (tweenScaleRepeat)
        {
            DOTween.To(() => currScale, x => currScale = x, endScale, scaleTweenDuration).SetEase(scaleEaseType).OnComplete(RepeatEnd);
        }

        if (tweenColorRepeat)
        {
            DOTween.To(() => currColor, x => currColor = x, endColor, colorTweenDuration).SetEase(colorEaseType).OnComplete(RepeatEnd);
        }

        if (tweenAlphaRepeat)
        {
            DOTween.To(() => currAlpha, x => currAlpha = x, endAlpha, alphaTweenDuration).SetEase(alphaEaseType).OnComplete(RepeatEnd);
        }
    }

    private void RepeatEnd()
    {
        if (tweenPositionRepeat)
        {
            DOTween.To(() => currPosition, x => currPosition = x, startPosition, positionTweenDuration).SetEase(positionEaseType).OnComplete(RepeatStart);
        }

        if (tweenRotationRepeat)
        {
            DOTween.To(() => currRotation, x => currRotation = x, startRotation, rotationTweenDuration).SetEase(rotationEaseType).OnComplete(RepeatStart);
        }

        if (tweenScaleRepeat)
        {
            DOTween.To(() => currScale, x => currScale = x, startScale, scaleTweenDuration).SetEase(scaleEaseType).OnComplete(RepeatStart);
        }

        if (tweenColorRepeat)
        {
            DOTween.To(() => currColor, x => currColor = x, startColor, colorTweenDuration).SetEase(colorEaseType).OnComplete(RepeatStart);
        }

        if (tweenAlphaRepeat)
        {
            DOTween.To(() => currAlpha, x => currAlpha = x, startAlpha, alphaTweenDuration).SetEase(alphaEaseType).OnComplete(RepeatStart);
        }
    }

    public void UpdateTween()
    {
        if (tweenPosition)
        {
            positionTweenTarget.localPosition = currPosition;
        }

        if (tweenRotation)
        {
            rotationTweenTarget.localEulerAngles = new Vector3(currRotation.x, currRotation.y, currRotation.z);
        }

        if (tweenScale)
        {
            scaleTweenTarget.localScale = currScale;
        }

        if (tweenColor)
        {
            currColor.a = colorTweenTarget.color.a;
            colorTweenTarget.color = currColor;
        }

        if (tweenAlpha)
        {
            Color color = new Color(alphaTweenTarget.color.r, alphaTweenTarget.color.g, alphaTweenTarget.color.b, currAlpha);
            alphaTweenTarget.color = color;
        }
    }

    public void StartTween()
    {
        if (tweenPosition)
        {
            DOTween.To(() => currPosition, x => currPosition = x, endPosition, positionTweenDuration).SetEase(positionEaseType).OnComplete(OnStartTweenEnd);
        }

        if (tweenRotation)
        {
            DOTween.To(() => currRotation, x => currRotation = x, endRotation, rotationTweenDuration).SetEase(rotationEaseType).OnComplete(OnStartTweenEnd);
        }

        if (tweenScale)
        {
            DOTween.To(() => currScale, x => currScale= x, endScale, scaleTweenDuration).SetEase(scaleEaseType).OnComplete(OnStartTweenEnd);
        }

        if (tweenColor)
        {
            DOTween.To(() => currColor, x => currColor = x, endColor, colorTweenDuration).SetEase(colorEaseType).OnComplete(OnStartTweenEnd);
        }

        if(tweenAlpha)
        {
            DOTween.To(() => currAlpha, x => currAlpha = x, endAlpha, alphaTweenDuration).SetEase(alphaEaseType).OnComplete(OnStartTweenEnd);
        }
    }

    public void EndTween()
    {
        if (tweenPosition)
        {
            DOTween.To(() => currPosition, x => currPosition = x, startPosition, positionTweenDuration).SetEase(positionEaseType).OnComplete(OnEndTweenEnd);
        }
        
        if (tweenRotation)
        {
            DOTween.To(() => currRotation, x => currRotation = x, startRotation, rotationTweenDuration).SetEase(rotationEaseType).OnComplete(OnEndTweenEnd);
        }
        
        if (tweenScale)
        {
            DOTween.To(() => currScale, x => currScale = x, startScale, scaleTweenDuration).SetEase(scaleEaseType).OnComplete(OnEndTweenEnd);
        }
        
        if (tweenColor)
        {
            DOTween.To(() => currColor, x => currColor = x, startColor, colorTweenDuration).SetEase(colorEaseType).OnComplete(OnEndTweenEnd);
        }
        
        if(tweenAlpha)
        {
            DOTween.To(() => currAlpha, x => currAlpha = x, startAlpha, alphaTweenDuration).SetEase(alphaEaseType).OnComplete(OnEndTweenEnd);
        }
    }

    public void ResetValues()
    {
        if (setToStart)
            SetToStartValue();
        else
            SetToEndValue();
    }

    public void SetToStartValue()
    {
        if (tweenPosition)
        {
            currPosition = startPosition;
        }

        if (tweenRotation)
        {
            currRotation = startRotation;
        }

        if (tweenScale)
        {
            currScale = startScale;
        }

        if (tweenColor)
        {
            currColor = startColor;
        }

        if(tweenAlpha)
        {
            currAlpha = startAlpha;
        }
    }

    public void SetToEndValue()
    {
        if (tweenPosition)
        {
            currPosition = endPosition;
        }

        if (tweenRotation)
        {
            currRotation = endRotation;
        }

        if (tweenScale)
        {
            currScale = endScale;
        }

        if (tweenColor)
        {
            currColor = endColor;
        }

        if(tweenAlpha)
        {
            currAlpha = endAlpha;
        }
    }
    
    private void OnEndTweenEnd()
    {
        if (disableOnEndTween)
            gameObject.SetActive(false);
    }

    private void OnStartTweenEnd()
    {
        gameObject.SetActive(true);

        if (autoEndTween)
            EndTween();
    }

}
