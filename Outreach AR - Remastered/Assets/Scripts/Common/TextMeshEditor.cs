﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class TextMeshEditor : MonoBehaviour
{
    private TextMeshProUGUI
        m_textMesh;

    [SerializeField]
    private IntReference
        m_text;

    public void Start()
    {
        m_textMesh = GetComponent<TextMeshProUGUI>();
    }

    // Update is called once per frame
    void Update ()
    {
		if(!m_textMesh.text.Equals(m_text.Value.ToString()))
        {
            m_textMesh.text = m_text.Value.ToString();
        }
	}
}
