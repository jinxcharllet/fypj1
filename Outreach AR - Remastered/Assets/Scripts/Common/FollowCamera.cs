﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowCamera : MonoBehaviour
{
    private Transform
        m_cameraTransform;

    [SerializeField]
    private Vector3
        m_offset;


    private void Start()
    {
        m_cameraTransform = Camera.main.transform;
    }

    void Update ()
    {
        transform.LookAt(transform.position + m_cameraTransform.rotation * Vector3.forward,
             m_cameraTransform.rotation * Vector3.up);

        transform.localEulerAngles = new Vector3(transform.localEulerAngles.x + m_offset.x, transform.localEulerAngles.y + m_offset.y, transform.localEulerAngles.z + m_offset.z);
	}
}
