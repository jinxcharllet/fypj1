﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TextureSwitch : MonoBehaviour
{
    [SerializeField]
    private FloatReference
        m_switchTimer;

    private float
        m_timer;

    [SerializeField]
    private MeshRenderer
        m_meshRenderer;

    private bool
        m_switch;

	void Awake ()
    {
        m_timer = m_switchTimer.Value;
        m_switch = false;
        if(m_meshRenderer == null)
            m_meshRenderer = GetComponent<MeshRenderer>();
    }
	
	// Update is called once per frame
	void Update ()
    {
        if (m_switchTimer.Value > 0)
        {
            if (m_timer <= 0)
            {
                m_switch = !m_switch;
                m_meshRenderer.material.SetFloat("_OffSet", ((m_switch) ? 1 : 0));
                m_timer = m_switchTimer.Value;
            }
            else
                m_timer -= Time.deltaTime;
        }
    }

    
    public void ChangeTexture(bool _switch)
    {
        m_switch = _switch;
        m_meshRenderer.material.SetFloat("_OffSet", ((m_switch) ? 1 : 0));
    }

    public void SwapTexture()
    {
        m_switch = !m_switch;
        m_meshRenderer.material.SetFloat("_OffSet", ((m_switch) ? 1 : 0));
    }
}
