﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Vuforia;

public class ImageDetected : MonoBehaviour, ITrackableEventHandler {

    public List<GameObject> targetScenes;

    public IGameLogic targetGameLogic;

    [SerializeField]
    private GameEvent
        m_ImageDetected, m_GlobalImageDetected;
    
    [SerializeField]
    private GameEvent
        m_ImageLost, m_GlobalImageLost;

    private TrackableBehaviour mTrackableBehaviour;

    // Use this for initialization
    void Start () {
        mTrackableBehaviour = GetComponent<TrackableBehaviour>();
        if (mTrackableBehaviour)
        {
            mTrackableBehaviour.RegisterTrackableEventHandler(this);
        }
        SetTargetScenesActive(false);
    }
	
    public void OnTrackableStateChanged(TrackableBehaviour.Status previousStatus, TrackableBehaviour.Status newStatus)
    {
        if (newStatus == TrackableBehaviour.Status.DETECTED ||
            newStatus == TrackableBehaviour.Status.TRACKED ||
            newStatus == TrackableBehaviour.Status.EXTENDED_TRACKED)
        {
            //if(Debug.isDebugBuild)
                Debug.Log("Trackable " + mTrackableBehaviour.TrackableName + " found");

            ChangeGameLogic();
            m_ImageDetected.InvokeAllListeners();
            m_GlobalImageDetected.InvokeAllListeners();
            SetTargetScenesActive(true);
        }
        else
        {
            //if (Debug.isDebugBuild)
                Debug.Log("Trackable " + mTrackableBehaviour.TrackableName + " lost");

            m_ImageLost.InvokeAllListeners();
            m_GlobalImageLost.InvokeAllListeners();
            SetTargetScenesActive(false);
        }
    }

    public void SetTargetScenesActive(bool _active)
    {
        foreach(GameObject go in targetScenes)
        {
            if(go != null)
                go.SetActive(_active);
        }
    }

    public void ChangeGameLogic()
    {
        GameObject.Find("MiniGameManager").GetComponent<MiniGameManager>().ChangeGameLogic(targetGameLogic);
    }
}
