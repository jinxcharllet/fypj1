﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Displays FPS.
/// </summary>
public class FPSController : MonoBehaviour
{
    public int targetFPS = 10000;

    private float frequency = 1.0f;
    private string fps;

    
    void Start()
    {
        Application.targetFrameRate = targetFPS;
        StartCoroutine(FPS());
    }

    private IEnumerator FPS()
    {
        while(true)
        {
            // Capture frame-per-second
            int lastFrameCount = Time.frameCount;
            float lastTime = Time.realtimeSinceStartup;
            yield return new WaitForSeconds(frequency);
            float timeSpan = Time.realtimeSinceStartup - lastTime;
            int frameCount = Time.frameCount - lastFrameCount;

            // Display it
            fps = string.Format("FPS: {0}", Mathf.RoundToInt(frameCount / timeSpan));
        }
    }


    void OnGUI()
    {
        GUI.Label(new Rect(Screen.width - 100, 10, 150, 20), fps);
    }
}
