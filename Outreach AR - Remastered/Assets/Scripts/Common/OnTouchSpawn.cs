﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;

public class OnTouchSpawn : MonoBehaviour {

    public GameObject objectToSpawn;
    public float objectLifetime = 5.0f;

    public UnityEvent eventsToFire;

    [SerializeField]
    private int clicksToActivate = 1;

    [SerializeField]
    private int accumulatedClicks = 0;

    void Update()
    {
        //Mouse input
        if (Input.GetMouseButtonDown(0))
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit Hit;

            if (!EventSystem.current.IsPointerOverGameObject())
            {
                if (Physics.Raycast(ray, out Hit))
                {
                    if (Hit.collider.gameObject == gameObject)
                    {
                        OnInteract();
                    }
                }
            }
        }

        //Touch
        if (Input.touchCount > 0)
        {
            for (int i = 0; i < Input.touchCount; ++i)
            {
                Ray ray = Camera.main.ScreenPointToRay(Input.touches[i].position);
                RaycastHit Hit;


                if (EventSystem.current.IsPointerOverGameObject(Input.touches[i].fingerId))
                {
                    switch (Input.touches[i].phase)
                    {
                        case TouchPhase.Began:
                            {
                                if (Physics.Raycast(ray, out Hit))
                                {
                                    if (Hit.collider.gameObject == gameObject)
                                    {
                                        OnInteract();
                                    }
                                }
                            }
                            break;
                        case TouchPhase.Moved:
                            break;
                        case TouchPhase.Stationary:
                            break;
                        case TouchPhase.Ended:
                            break;
                        case TouchPhase.Canceled:
                            break;
                    }
                }
            }
        }
    }


    public void OnInteract()
    {
        GameLogicState gameState = GameObject.Find("MiniGameManager").GetComponent<MiniGameManager>().GetGameLogic().GetGameState();
        if (gameState != GameLogicState.GameIdle)
            return;

        accumulatedClicks += 1;
        if (accumulatedClicks >= clicksToActivate)
        {
            accumulatedClicks = 0;
        }
        else
        {
            return;
        }

        //Instantiate Gameobjects
        GameObject go = Instantiate(objectToSpawn);
        go.transform.SetParent(gameObject.transform);
        go.transform.localPosition = Vector3.zero;
        go.transform.localScale = new Vector3(1, 1, 1);
        go.transform.localRotation = Quaternion.identity;

        if(objectLifetime>0)
            Destroy(go, objectLifetime);

        //Invoke game event
        eventsToFire.Invoke();
    }
}
