﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Scaler : MonoBehaviour
{
    [SerializeField]
    private FloatReference
        m_maxSize;

    [SerializeField]
    [Range(0, 10)]
    private float
        m_scaleSpeed;

    private void OnEnable()
    {
        gameObject.transform.localScale = Vector3.zero;
    }

    void Update ()
    {
        if (gameObject.transform.localScale.x != m_maxSize.Value)
        {
            float temp = Vector3.Slerp(gameObject.transform.localScale, new Vector3(m_maxSize.Value, m_maxSize.Value, m_maxSize.Value), m_scaleSpeed * Time.unscaledDeltaTime).x;
            gameObject.transform.localScale = new Vector3(temp, temp, temp);
        }
    }
}
