﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Flicker : MonoBehaviour
{
    [SerializeField]
    private FloatReference
        m_flickerDelay,
        m_flickerAmount,
        m_startFlickerDelay;

    [SerializeField]
    private MeshRenderer
        m_meshRenderer;

    [SerializeField]
    private bool
        m_useShader;

    private float
        m_amount,
        m_timer,
        m_startTimer;

    private bool
        m_startFlicker;

    void Awake()
    {
        if(m_meshRenderer == null)
            m_meshRenderer = GetComponent<MeshRenderer>();
    }

    void Start()
    {
        m_amount = 0;
        m_startFlicker = false;
        m_timer = 0;
        m_startTimer = 0;
    }

    void Update()
    {
        if (m_startFlickerDelay.Value > 0 && !m_startFlicker)
        {
            m_startTimer -= Time.unscaledDeltaTime;

            if (m_startTimer <= 0)
                StartFlicker();
        }

        if (m_startFlicker)
        {
            m_timer -= Time.unscaledDeltaTime;

            if (m_amount <= 0)
            {
                m_startFlicker = false;
                if(!m_useShader)
                    m_meshRenderer.enabled = true;
                else
                    m_meshRenderer.material.SetFloat("_OffSet", 0);

                if (m_startFlickerDelay.Value > 0)
                {
                    m_startTimer = m_startFlickerDelay.Value;
                }

                return;
            }

            if (m_timer <= 0)
            {
                m_timer = m_flickerDelay.Value;

                if (m_amount % 2 == 0)
                {
                    if (!m_useShader)
                        m_meshRenderer.enabled = false;
                    else
                        m_meshRenderer.material.SetFloat("_OffSet", 1);
                }
                else
                {
                    if (!m_useShader)
                        m_meshRenderer.enabled = true;
                    else
                        m_meshRenderer.material.SetFloat("_OffSet", 0);
                }

                --m_amount;
            }
        }
    }

    public void StartFlicker()
    {
        m_amount = m_flickerAmount.Value * 2;
        m_startFlicker = true;
        m_timer = m_flickerDelay.Value;
    }
}
