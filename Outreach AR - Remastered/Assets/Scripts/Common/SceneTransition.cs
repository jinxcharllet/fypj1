﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using DG.Tweening;

public class SceneTransition : MonoBehaviour {
    
    [Header("Load Next Scene")]
    public string nextSceneName;
    public Color fadeColor = new Color(0,0,0);
    public float fadeTime = 1.0f;

    [Header("Automatically Change Scene")]
    public bool autoChange = false;
    public Image fader; 

    private float currAlpha = 0.0f;
    private float endAlpha = 1.0f;

    private void Start()
    {
        if (autoChange)
        {
            DOTween.To(() => currAlpha, x => currAlpha = x, endAlpha, fadeTime).SetEase(Ease.Linear).OnComplete(LoadScene);
        }
    }

    private void Update()
    {
        Color color = new Color(fader.color.r, fader.color.g, fader.color.b, currAlpha);
        fader.color = color;
    }

    public void LoadScene()
    {
        SceneManager.LoadScene(nextSceneName);
    }
}
