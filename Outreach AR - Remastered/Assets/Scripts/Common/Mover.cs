﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Mover : MonoBehaviour
{
    [SerializeField]
    private FloatReference
            m_maxDistance;

    private Vector3
        m_startingPos, 
        m_endPos;

    [SerializeField]
    [Range(0, 0.001f)]
    private float
        m_movingSpeed;

    private float
        m_finalSpeed;

    private bool
        m_isReachedEnd;

    private void Awake()
    {
        m_startingPos = gameObject.transform.localPosition;
    }

    private void Start()
    {
        m_endPos = new Vector3(gameObject.transform.localPosition.x + m_maxDistance.Value, gameObject.transform.localPosition.y, gameObject.transform.localPosition.z);
        m_isReachedEnd = false;
    }

    private void OnEnable()
    {
        gameObject.transform.localPosition = m_startingPos;
        m_finalSpeed = 0;
    }

    void Update()
    {
        m_finalSpeed += Time.unscaledDeltaTime * m_movingSpeed;

        if (!m_isReachedEnd)
        {
            if (Vector3.Distance(transform.localPosition, m_endPos) < 0.5f)
            {
                m_isReachedEnd = true;
                m_finalSpeed = 0;
            }
        }
        else
        {
            if (Vector3.Distance(transform.localPosition, m_startingPos) < 0.5f)
            {
                m_isReachedEnd = false;
                m_finalSpeed = 0;
            }
        }

        if (!m_isReachedEnd)
            gameObject.transform.localPosition = Vector3.Lerp(gameObject.transform.localPosition, m_endPos, m_finalSpeed);
        else
            gameObject.transform.localPosition = Vector3.Lerp(gameObject.transform.localPosition, m_startingPos, m_finalSpeed);

    }
}
