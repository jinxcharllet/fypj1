﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class MeshHolder
{
    [SerializeField]
    private Mesh
        m_mesh;

    [SerializeField]
    private Material
        m_texture;

    public Mesh GetMesh()
    {
        return m_mesh;
    }

    public Material GetMaterial()
    {
        return m_texture;
    }
}
