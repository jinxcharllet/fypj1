﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class MaterialwColorHolder
{
    [SerializeField]
    private Material
        m_mat;

    [SerializeField]
    private Color
        m_color;

    public Color GetColor()
    {
        return m_color;
    }

    public Material GetMaterial()
    {
        return m_mat;
    }
}

