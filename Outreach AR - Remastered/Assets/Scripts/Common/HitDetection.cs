﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HitDetection : MonoBehaviour
{
    [SerializeField]
    private GameEvent
        m_bubblePopEvent,
        m_freezePopEvent,
        m_upAssignmentEvent,
        m_killEnemy;

    [SerializeField]
    private FloatReference
        m_energy, 
        m_maxEnergy;

    private RaycastHit
        m_hit;

    private Ray
        m_ray;

    private bool
        m_DGADpressAssignment;

    [SerializeField]
    private GameEvent
        m_resetPenType, 
        m_resetPenTypeUI;

    private void Start()
    {
        m_DGADpressAssignment = false;
    }

    void Update()
    {
        m_ray = Camera.main.ScreenPointToRay(Input.mousePosition);

        if (Input.GetMouseButtonDown(0))
        {
            if (Physics.Raycast(m_ray, out m_hit))
            {
                if (m_hit.collider != null)
                {
                    switch (m_hit.collider.gameObject.tag)
                    {
                        case "Bubble":
                            m_bubblePopEvent.InvokeSpecificListner(m_hit.collider.gameObject.GetInstanceID());
                            break;

                        case "Freeze":
                            m_freezePopEvent.InvokeSpecificListner(m_hit.collider.gameObject.GetInstanceID());
                            break;

                        case "Assignment":
                            {
                                if (m_DGADpressAssignment)
                                {
                                    AudioManager.instance.Play("asn lv up");
                                    m_upAssignmentEvent.InvokeAllListeners();
                                    //ResetPenType();
                                }
                            }
                            break;
                        case "Enemy":
                            {
                                if (!m_DGADpressAssignment)
                                {
                                    AudioManager.instance.Play("shoot");
                                    m_killEnemy.InvokeSpecificListner(m_hit.collider.gameObject.GetInstanceID());
                                }
                            }
                            break;
                        default:
                            break;
                    }
                }
            }
        }

        //Touch
        if (Input.touchCount > 0)
        {
            for (int i = 0; i < Input.touchCount; ++i)
            {
                Ray ray = Camera.main.ScreenPointToRay(Input.touches[i].position);
                RaycastHit Hit;

                switch (Input.touches[i].phase)
                {
                    case TouchPhase.Began:
                        {
                            if (Physics.Raycast(ray, out Hit))
                            {
                                if (Hit.collider != null)
                                {
                                    switch (Hit.collider.gameObject.tag)
                                    {
                                        case "Bubble":
                                            m_bubblePopEvent.InvokeSpecificListner(Hit.collider.gameObject.GetInstanceID());
                                            break;

                                        case "Freeze":
                                            m_freezePopEvent.InvokeSpecificListner(Hit.collider.gameObject.GetInstanceID());
                                            break;

                                        case "Assignment":
                                            {
                                                if (m_DGADpressAssignment)
                                                {
                                                    m_upAssignmentEvent.InvokeAllListeners();
                                                    //ResetPenType();
                                                }
                                            }
                                            break;
                                        case "Enemy":
                                            {
                                                if (!m_DGADpressAssignment)
                                                    m_killEnemy.InvokeSpecificListner(m_hit.collider.gameObject.GetInstanceID());
                                            }
                                            break;
                                        default:
                                            break;
                                    }
                                }
                            }
                        }
                        break;
                    case TouchPhase.Moved:
                        break;
                    case TouchPhase.Stationary:
                        break;
                    case TouchPhase.Ended:
                        break;
                    case TouchPhase.Canceled:
                        break;
                }
            }
        }
    }

    public void ResetPenType()
    {
        m_DGADpressAssignment = false;
        m_resetPenTypeUI.InvokeAllListeners();
    }

    public void TogglePenType()
    {
        AudioManager.instance.Play("change gun");
        m_DGADpressAssignment = !m_DGADpressAssignment;
    }
}
