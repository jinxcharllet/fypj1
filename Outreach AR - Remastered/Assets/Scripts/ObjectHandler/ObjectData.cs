﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = ("Object Data"))]
public class ObjectData : ScriptableObject
{
    [SerializeField]
    private List<StateWithMaterials>
           m_dataList;

    public List<StateWithMaterials> GetDataList()
    {
        return m_dataList;
    }
}
