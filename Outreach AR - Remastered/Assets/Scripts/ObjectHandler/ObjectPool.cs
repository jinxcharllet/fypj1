﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectPool : MonoBehaviour
{
    private static ObjectPool
        m_instance;

    [SerializeField]
    private Transform
        m_GDTParent,
        m_DGADParent;

    [SerializeField]
    [Range(0, 100)]
    private uint
        m_amountToSpawn;

    public static ObjectPool GetInstance()
    {
        return m_instance;
    }

    [SerializeField]
    private GameObjectList
        m_prefabList;

    private Dictionary<int, List<GameObject>>
        m_objectDic = new Dictionary<int, List<GameObject>>();
    
    public void Awake()
    {
        if (m_instance != null && m_instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            m_instance = this;
        }

        foreach (GameObject go in m_prefabList.GetList())
        {
            if (!m_objectDic.ContainsKey(go.GetInstanceID()))
                m_objectDic.Add(go.GetInstanceID(), new List<GameObject>());

            for (int i = 0; i < m_amountToSpawn; ++i)
            {
                GameObject temp = Instantiate(go);
                m_objectDic[go.GetInstanceID()].Add(temp);

                if (go.name.Contains("GDT"))
                {
                    temp.transform.parent = m_GDTParent;
                }
                else if (go.name.Contains("DGAD"))
                {
                    temp.transform.parent = m_DGADParent;
                }
                else if (go.name.Contains("Future"))
                {

                }

                temp.SetActive(false);
            }
        }
    }

    public GameObject GetObject(GameObject _ID)
    {
        if (!m_objectDic.ContainsKey(_ID.GetInstanceID()))
            return null;

        foreach(GameObject go in m_objectDic[_ID.GetInstanceID()])
        {
            if (!go.activeSelf)
            {
                go.SetActive(true);
                return go;
            }
        }

        for (int i = 0; i < m_amountToSpawn; ++i)
        {
            GameObject temp = Instantiate(_ID);
            m_objectDic[_ID.GetInstanceID()].Add(temp);

            if (temp.name.Contains("GDT"))
            {
                temp.transform.parent = m_GDTParent;
            }
            else if (temp.name.Contains("DGAD"))
            {
                temp.transform.parent = m_DGADParent;
            }
            else if (temp.name.Contains("Future"))
            {

            }

            temp.SetActive(false);
        }

        return GetObject(_ID);
    }
}