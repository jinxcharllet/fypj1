﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;

public class InteractiveObject : MonoBehaviour
{
    [SerializeField]
    private GameEvent interactionEvent;

    [SerializeField]
    private bool isSpecific=false;
    
    [SerializeField]
    private List<UnityEvent> eventListeners;

    [SerializeField]
    private int clicksToActivate = 1;

    [SerializeField]
    private int accumulatedClicks = 0;

    void Update()
    {
        //Mouse input
        if (Input.GetMouseButtonDown(0))
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit Hit;

            if (!EventSystem.current.IsPointerOverGameObject())
            {
                if (Physics.Raycast(ray, out Hit))
                {
                    if (Hit.collider.gameObject == gameObject)
                    {
                        OnInteract();
                    }
                }
            }
        }
    
        //Touch
        if (Input.touchCount >0)
        {
            for (int i = 0; i < Input.touchCount; ++i)
            {
                Ray ray = Camera.main.ScreenPointToRay(Input.touches[i].position);
                RaycastHit Hit;


                if (EventSystem.current.IsPointerOverGameObject(Input.touches[i].fingerId))
                {
                    switch (Input.touches[i].phase)
                    {
                        case TouchPhase.Began:
                            {
                                if (Physics.Raycast(ray, out Hit))
                                {
                                    if (Hit.collider.gameObject == gameObject)
                                    {
                                        OnInteract();
                                    }
                                }
                            }
                            break;
                        case TouchPhase.Moved:
                            break;
                        case TouchPhase.Stationary:
                            break;
                        case TouchPhase.Ended:
                            break;
                        case TouchPhase.Canceled:
                            break;
                    }
                }
            }
        }
    }

    
    public void OnInteract()
    {
        GameLogicState gameState = GameObject.Find("MiniGameManager").GetComponent<MiniGameManager>().GetGameLogic().GetGameState();
        if (gameState != GameLogicState.GameIdle)
            return;

        accumulatedClicks += 1;
        if(accumulatedClicks >= clicksToActivate)
        {
            accumulatedClicks = 0;
        }
        else
        {
            return;
        }

        if (interactionEvent != null)
        {
            if (!isSpecific)
                interactionEvent.InvokeAllListeners();
            else
                interactionEvent.InvokeSpecificListner(gameObject.GetInstanceID());
        }
        foreach (UnityEvent ue in eventListeners)
        {
            ue.Invoke();
        }
        AudioManager.instance.Play("Dialogue Tap");

        
    }
}
