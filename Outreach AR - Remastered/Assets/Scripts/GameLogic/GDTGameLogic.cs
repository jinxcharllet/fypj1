﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName ="GameLogic/GDT")]
public class GDTGameLogic : IGameLogic
{
    //Game Specific variables
    [SerializeField]
    private FloatVariable currTimer, currFever;
    [SerializeField]
    private FloatVariable maxTimer, maxFever;
    [SerializeField]
    private UintVariable currHealth, currCombo, currScore, maxScore;
    [SerializeField]
    private UintVariable maxHealth;
    
    public const float feverDecreaseRate = 5.0f; //Per seconds

    //Fever Mode
    public float feverBubbleSpawnTime = 0.5f;
    private bool isFeverMode = false;

    //Power Up Spawning
    private bool isPowerUpSpawned = false;

    //Bubble Spawning
    private float spawnTimer = 0;
    private float spawnTime = 0;
    public float baseSpawnTime = 2.0f;
    public float minSpawnTime = 0.5f;

    //Freeze Powerup
    public float freezeDuration = 5.0f;
    private float freezeTimer = 0.0f;

    //Player Hurt
    public float hurtTime = 0.3f;
    private float hurtTimer = 0.0f;

    //Game Specific Events
    [SerializeField]
    private GameEvent spawnBubble, spawnFeverBubble, spawnPowerUp, destroyRedBubble, destroyAllBubble;

    [SerializeField]
    private GameEvent hurtStart, hurtEnd, feverStart, feverEnd, freezeStart, freezeEnd;

    //Game Logic State Functions
    public override void GameIdle()
    {

    }
    public override void GameStart()
    {
        //Reset variables
        currTimer.SetVariable(maxTimer.GetVariable());
        currFever.SetVariable(0.0f);
        currHealth.SetVariable(maxHealth.GetVariable());
        currCombo.SetVariable(0);
        currScore.SetVariable(0);

        destroyAllBubble.InvokeAllListeners();

        spawnTimer = 0.0f;
        freezeTimer = 0.0f;
        hurtTimer = 0.0f;

        isPowerUpSpawned = false;
        isFeverMode = false;

        hurtEnd.InvokeAllListeners();
        feverEnd.InvokeAllListeners();
        freezeEnd.InvokeAllListeners();

        Time.timeScale = 1.0f;

        //Request for state change to play
        //miniGameManager.GamePlay();
    }
    public override void GamePlay()
    {
        //Hurt UI
        if(hurtTimer > 0)
        {
            hurtTimer -= Time.unscaledDeltaTime;
            if(hurtTimer<= 0)
            {
                hurtEnd.InvokeAllListeners();
            }
        }

        //Frozen Powerup
        if(freezeTimer > 0)
        {
            freezeTimer -= Time.unscaledDeltaTime;
            if(freezeTimer<= 0)
            {
                freezeEnd.InvokeAllListeners();
            }
            Time.timeScale = 0.1f;
            return;
        }
        else
        {
            Time.timeScale = 1.0f;
        }
        
        // Decrease fever bar overtime
        currFever.SetVariable(Mathf.Clamp((currFever.GetVariable() - feverDecreaseRate * Time.deltaTime), 0, maxFever.GetVariable()));

        // Decrease timer overtime
        currTimer.SetVariable(Mathf.Clamp((currTimer.GetVariable() - Time.deltaTime), 0, maxTimer.GetVariable()));

        // Gameover when health or time runs out
        if (currTimer.GetVariable() <= 0 || currHealth.GetVariable() <= 0)
        {
            GameOver();
        }

        //Check for fever mode
        if (currFever.GetVariable() >= maxFever.GetVariable() && !isFeverMode)
        {
            isFeverMode = true;
            feverStart.InvokeAllListeners();
        }
        else if (currFever.GetVariable() <= 0 && isFeverMode)
        {
            isFeverMode = false;
            feverEnd.InvokeAllListeners();
        }

        //Power ups
        if(!isPowerUpSpawned && currTimer.GetVariable() > 1 && (int)currTimer.GetVariable() % 20 == 0)
        {
            SpawnPowerUp();
            isPowerUpSpawned = true;
        }
        if(isPowerUpSpawned)
        {
            if ((int)currTimer.GetVariable() % 20 != 0)
                isPowerUpSpawned = false;
        }

        // Spawn the bubbles
        if (isFeverMode)
        {
            SpawnFeverBubble();
        }
        else
        {
            SpawnBubble();
        }

        //Update high score
        if(currScore.GetVariable() > maxScore.GetVariable())
        {
            maxScore.SetVariable(currScore.GetVariable());
        }
    }
    public override void GameOver()
    {
        if (GetGameState() != GameLogicState.GameOver)
        {
            destroyAllBubble.InvokeAllListeners();
            freezeEnd.InvokeAllListeners();
            hurtEnd.InvokeAllListeners();
            feverEnd.InvokeAllListeners();
            SaveHighScore(currScore.GetVariable());
            miniGameManager.GameOver();
        }
    }
    public override void GameEnd()
    {
        destroyAllBubble.InvokeAllListeners();
        freezeEnd.InvokeAllListeners();
        hurtEnd.InvokeAllListeners();
        feverEnd.InvokeAllListeners();
        SaveHighScore(currScore.GetVariable());
        miniGameManager.GameIdle();
    }
    public override void GamePause()
    {
        if (GetGameState() != GameLogicState.GamePause)
        {
            miniGameManager.GamePause();
        }
        Time.timeScale = 0.0f;
    }
    public override void GameResume()
    {
        Time.timeScale = 1.0f;
        miniGameManager.GamePlay();
    }
    
    //Game Specific Functions
    private void SaveHighScore(uint _score)
    {
        uint oldHighestScore = maxScore.GetVariable();

        uint highestScore = (uint)Mathf.Max(_score, oldHighestScore);

        //Update the current highest score
        maxScore.SetVariable(highestScore);

        PlayerPrefs.SetInt("GDT_HighScore", (int)highestScore);
        PlayerPrefs.Save();
    }

    public void SpawnBubble()
    {
        float timeLeftConstant = currTimer.GetVariable() / maxTimer.GetVariable();
        spawnTime = baseSpawnTime * (minSpawnTime + timeLeftConstant);

        spawnTimer -= Time.deltaTime;
        if (spawnTimer <= 0)
        {
            for (int i = 0; i < miniGameManager.GDTFlowers.Count; ++i)
            {
                spawnBubble.InvokeSpecificListner(miniGameManager.GDTFlowers[Random.Range(0, miniGameManager.GDTFlowers.Count)].GetInstanceID());
            }
            spawnTimer = spawnTime;
        }
    }
    public void SpawnFeverBubble()
    {
        spawnTimer -= Time.deltaTime;
        if (spawnTimer <= 0)
        {
            for (int i = 0; i < miniGameManager.GDTFlowers.Count; ++i)
            {
                spawnFeverBubble.InvokeSpecificListner(miniGameManager.GDTFlowers[Random.Range(0, miniGameManager.GDTFlowers.Count)].GetInstanceID());
            }
            spawnTimer = feverBubbleSpawnTime;
        }
    }

    public void SpawnPowerUp()
    {
        for (int i = 0; i < miniGameManager.GDTPowerUpFlowers.Count; ++i)
        {
            spawnPowerUp.InvokeSpecificListner(miniGameManager.GDTPowerUpFlowers[Random.Range(0, miniGameManager.GDTPowerUpFlowers.Count)].GetInstanceID());
        }
    }

    public void OnBubblePop(ObjectState _state)
    {
        if (_state.AffectsCombo())
        {
            if (_state.GetComboAmount() == 0)
                currCombo.SetVariable(0);
            else
                currCombo.SetVariable((uint)(currCombo.GetVariable() + _state.GetComboAmount()));
        }

        if(_state.AffectsFever())
        {
            if (_state.GetFeverAmount() == 0)
                currFever.SetVariable(0);
            else
                currFever.SetVariable(currFever.GetVariable() + _state.GetFeverAmount());
        }

        switch(_state.GetObjectAffectType())
        {
            case ObjectState.TYPE.HEALTH:   
                {
                    if(currHealth.GetVariable() > 0)
                        currHealth.SetVariable((uint)(currHealth.GetVariable() + _state.GetTypeAmount()));

                    if(_state.GetTypeAmount() < 0)
                    {
                        OnHurt();
                    }
                }
                break;

            case ObjectState.TYPE.SCORE:
                {
                    currScore.SetVariable((uint)(currScore.GetVariable() + (_state.GetTypeAmount() * (currCombo.GetVariable() < 0 ?  1: currCombo.GetVariable()))));

                    int bubble_effect;
                    bubble_effect = Random.Range(0, 2);

                    if (bubble_effect == 0)
                        AudioManager.instance.Play("Good Bubble 1");
                    if (bubble_effect == 1)
                        AudioManager.instance.Play("Good Bubble 2");
                    if (bubble_effect == 2)
                        AudioManager.instance.Play("Good Bubble 3");
                }
                break;

            default:
                break;
        }

    }
    public void OnFreezePop()
    {
        freezeTimer = freezeDuration;
        freezeStart.InvokeAllListeners();
        AudioManager.instance.Play("Freeze Power Up");
    }
    public void OnHurt()
    {
        hurtTimer = hurtTime;
        hurtStart.InvokeAllListeners();
        AudioManager.instance.Play("Bad Bubble");
    }
}
