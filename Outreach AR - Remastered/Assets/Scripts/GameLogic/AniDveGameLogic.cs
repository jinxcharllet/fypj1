﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "GameLogic/AniDve")]

public class AniDveGameLogic : IGameLogic
{

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public override void GameIdle()
    {
    }
    public override void GameStart()
    {
    }
    public override void GamePlay()
    {
    }
    public override void GameOver()
    {
    }
    public override void GameEnd()
    {
    }
    public override void GamePause()
    {
    }
    public override void GameResume()
    {
    }
}
