﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Logical Game State for mini games. 
/// </summary>
public enum GameLogicState
{
    GameIdle = 0,
    GameStart,
    GamePlay,
    GameOver,
    GameEnd,
    GamePause,
    GameResume
}

/// <summary>
/// Interface class for gamelogic scriptable objects
/// </summary>
public abstract class IGameLogic : ScriptableObject {

    [SerializeField]
    private GameLogicState gameLogicState = GameLogicState.GameIdle;

    [SerializeField]
    public MiniGameManager miniGameManager;

    //Game State Functions
    public GameLogicState GetGameState()
    {
        return gameLogicState;
    }
    public void ChangeGameState(GameLogicState _newState)
    {
        if (gameLogicState == _newState)
            return;

        gameLogicState = _newState;
    }

    //Virtual functions

    /// <summary>
    /// Called once when switched to this logic
    /// </summary>
    public virtual void Init(MiniGameManager _gameManager) {
        miniGameManager = _gameManager;
        ChangeGameState(GameLogicState.GameIdle);
    }

    /// <summary>
    /// Updates to the game logic
    /// </summary>
    public virtual void Update() {

        switch (gameLogicState)
        {
            case GameLogicState.GameIdle:
                GameIdle();
                break;
            case GameLogicState.GameStart:
                GameStart();
                break;
            case GameLogicState.GamePlay:
                GamePlay();
                break;
            case GameLogicState.GameOver:
                GameOver();
                break;
            case GameLogicState.GameEnd:
                GameEnd();
                break;
            case GameLogicState.GamePause:
                GamePause();
                break;
            case GameLogicState.GameResume:
                GameResume();
                break;
            default:
                break;
        }
    }

    /// <summary>
    /// Called once when switching to another logic
    /// </summary>
    public virtual void Exit() {
        GameResume();
        GameEnd();
    }


    //Abstract functions

    /// <summary>
    /// GameIdle state. Used for menus etc.
    /// </summary>
    public abstract void GameIdle();

    /// <summary>
    /// GameStart state. Used for initialisation of the game, resetting variables etc.
    /// </summary>
    public abstract void GameStart();

    /// <summary>
    /// GamePlay state. Used for game win, lose draw conditions etc.
    /// </summary>
    public abstract void GamePlay();

    /// <summary>
    /// GameOver state. Used to show highscore, restart buttons etc.
    /// </summary>
    public abstract void GameOver();

    /// <summary>
    /// GameEnd state. Used for quitting the game, going back to menu etc. 
    /// </summary>
    public abstract void GameEnd();

    /// <summary>
    /// GamePause state. Self explanatory.
    /// </summary>
    public abstract void GamePause();

    /// <summary>
    /// GameResume state. Self explanatory.
    /// </summary>
    public abstract void GameResume();
}