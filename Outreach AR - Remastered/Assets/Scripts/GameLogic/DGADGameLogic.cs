﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "GameLogic/DGAD")]
public class DGADGameLogic : IGameLogic
{
    //Game Specific Variables
    [SerializeField]
    private FloatVariable
        m_energyIncrement,

        m_currEnergy,
        m_maxEnergy;

    [SerializeField]
    private UintVariable
        m_currHealth,
        m_maxHealth,
        m_maxScore,
        m_score;
   

    [SerializeField]
    private IntReference
        m_assignLevel;

    //Game Specific Events
    [SerializeField]
    private GameEvent
        m_destroyAllEnemies,
        m_spawnEnemies,
        m_resetAssignmentStage,
        m_resetPenType;

    //Game Specific Functions
    private float spawnTimer;
    public float baseSpawnTime;

    private float waveTimer;
    public float baseWaveTime;

    public int 
        wave, 
        mons;
    public bool
        canSpawn;


    public override void GameIdle()
    {
    }
    public override void GameStart()
    {
        //Reset variables
        m_currEnergy.SetVariable(0.0f);
        m_currHealth.SetVariable(m_maxHealth.GetVariable());
        m_score.SetVariable(0);
        m_maxScore.SetVariable((uint)PlayerPrefs.GetInt("DGAD_Highscore"));
        
        canSpawn = true;
        wave = 0;
        waveTimer = 0;
        spawnTimer = 0;
        mons = 0;
        m_resetAssignmentStage.InvokeAllListeners();
        m_destroyAllEnemies.InvokeAllListeners();
        m_resetPenType.InvokeAllListeners();

        Time.timeScale = 1.0f;
        
        //Request for state change to (play might not be needed)
        //miniGameManager.GamePlay();
    }
    public override void GamePlay()
    {
        if(canSpawn)
            SpawnEnemies();

        // Gameover when health or time runs out
        if (m_currHealth.GetVariable() <= 0)
        {
            AudioManager.instance.Play("lose game");
            GameOver();
        }
    }
    public override void GameOver()
    {
        if (GetGameState() != GameLogicState.GameOver)
        {
            miniGameManager.GameOver();
        }
    }
    public override void GameEnd()
    {
        m_destroyAllEnemies.InvokeAllListeners();
        miniGameManager.GameIdle();
    }
    public override void GamePause()
    {
        if (GetGameState() != GameLogicState.GamePause)
        {
            miniGameManager.GamePause();
        }
        Time.timeScale = 0.0f;
    }
    public override void GameResume()
    {
        Time.timeScale = 1.0f;
        miniGameManager.GamePlay();
    }

    public void SpawnEnemies()
    {
        spawnTimer -= Time.deltaTime;
        waveTimer -= Time.deltaTime;

        float assNum = ((m_assignLevel.Value <= 1) ? 1.5f : m_assignLevel.Value);

        if (waveTimer <= 0)
        {
            ++wave;
            mons += wave * 4;
            waveTimer = baseWaveTime / assNum;
        }

        if (mons > 0 && spawnTimer <= 0)
        {
            for (int temp = 0; temp < assNum && mons > 0; --mons, ++temp)
            {
                m_spawnEnemies.InvokeSpecificListner(miniGameManager.DGADSpawnpoints[Random.Range(0, miniGameManager.DGADSpawnpoints.Count)].GetInstanceID());
            }
            spawnTimer = baseSpawnTime / assNum;
        }
    }

    public void OnHurt()
    {
        m_currHealth.SetVariable(m_currHealth.GetVariable() - 1);
    }

    public void AddPoint()
    {
        //Add Score
        m_score.SetVariable(m_score.GetVariable() + 100);
        //Update high score
        if (m_score.GetVariable() > m_maxScore.GetVariable())
        {
            m_maxScore.SetVariable(m_score.GetVariable());
            PlayerPrefs.SetInt("DGAD_HighScore", (int)m_maxScore.GetVariable());
            PlayerPrefs.Save();
        }

        //Add Energy
        m_currEnergy.SetVariable(m_currEnergy.GetVariable() + m_energyIncrement.GetVariable());
        if (m_currEnergy.GetVariable() >= 100)
            m_currEnergy.SetVariable(100);
    }

    public void ResetEnergy()
    {
        m_currEnergy.SetVariable(0.0f);
    }

    public bool DecreaseEnergy(float _amount)
    {
        if (m_currEnergy.GetVariable() <= 0)
            return false;

        m_currEnergy.SetVariable(m_currEnergy.GetVariable() - _amount);
        return true;
    }
}