﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SplashController : MonoBehaviour {

    public List<GameObject> splashEffects;

    public void StartEffect(int _index)
    {
        splashEffects[_index].SetActive(true);
        splashEffects[_index].GetComponent<Tweener>().StartTween();
    }

    public void StopEffect(int _index)
    {
        splashEffects[_index].GetComponent<Tweener>().EndTween();
    }

    public void StopAllEffects()
    {
        foreach (GameObject go in splashEffects)
        {
            go.GetComponent<Tweener>().EndTween();
        }
    }
}
