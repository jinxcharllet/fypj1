﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ButtonFireEvent : MonoBehaviour {

    [SerializeField]
    public List<GameEvent> gameEventsToFire;

    public void FireAllEvent()
    {
        for (int i = 0; i < gameEventsToFire.Count; ++i)
        {
            gameEventsToFire[i].InvokeAllListeners();
        }
        AudioManager.instance.Play("Button");
    }
}
