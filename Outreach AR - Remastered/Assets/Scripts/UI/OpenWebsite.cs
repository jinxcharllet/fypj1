﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OpenWebsite : MonoBehaviour {

    public string url = "https://www.nyp.edu.sg/";

    public void OpenWebpage()
    {
        Application.OpenURL(url);
    }
}
