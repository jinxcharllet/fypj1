﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RainbowCycle : MonoBehaviour
{
    [SerializeField]
    private Image
        m_image;

    private Vector3
        m_rainbowColor;

    private bool
        m_changeR, 
        m_changeG, 
        m_changeB;

    [Range(0, 0.1f)]
    [SerializeField]
    private float
        m_speed;


    private void Start()
    {
        m_rainbowColor = Vector3.zero;
        m_changeR = m_changeG = m_changeB = false;
    }

    void Update ()
    {
		if(!m_changeR)
        {
            if (m_rainbowColor.x > 1)
                m_changeR = true;
        }
        else
        {
            if (m_rainbowColor.x < 0.5f)
                m_changeR = false;
        }

        if (!m_changeG)
        {
            if (m_rainbowColor.y > 1)
                m_changeG = true;
        }
        else
        {
            if (m_rainbowColor.y < 0)
                m_changeG = false;
        }


        if (!m_changeB)
        {
            if (m_rainbowColor.z > 1)
                m_changeB = true;
        }
        else
        {
            if (m_rainbowColor.z < 0)
                m_changeB = false;
        }

        if(Random.Range(0, 100) < 50)
        {
            m_rainbowColor.x += ((m_changeR) ? -m_speed : m_speed);
        }

        if (Random.Range(0, 100) < 50)
        {
            m_rainbowColor.y += ((m_changeG) ? -m_speed : m_speed);
        }

        if (Random.Range(0, 100) < 50)
        {
            m_rainbowColor.z += ((m_changeB) ? -m_speed : m_speed);
        }

        m_image.color = new Color(m_rainbowColor.x, m_rainbowColor.y, m_rainbowColor.z, m_image.color.a);
        
    }
}
