﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using TMPro;

public class RandomText : MonoBehaviour
{
    public TextMeshProUGUI textTarget;
    public RandomTextData textDatabase;
    
    public int currIndex = 0;

    //[SerializeField]
    //private List<UnityEvent> endDialogueCallbacks;
    //[SerializeField]
    //private List<UnityEvent> nextDialogueCallbacks;

    public void GetNextText()
    {
        SetText(textDatabase.GetText(currIndex));
        currIndex++;
    }

    public void ResetTextIndex()
    {
        currIndex = 0;
    }

    public void GetNewText()
    {
        SetText(textDatabase.GetRandomText());
    }

    
    private void SetText(string _newText)
    {
        textTarget.text = _newText;
    }

}
