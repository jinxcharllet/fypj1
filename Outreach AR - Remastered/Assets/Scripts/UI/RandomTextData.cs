﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "RandomText/RandomTextData")]
public class RandomTextData : ScriptableObject {

    [SerializeField]
    [TextArea(3,10)]
    private List<string> textDatabase;
	
    public string GetRandomText()
    {
        int random = Random.Range(0, textDatabase.Count);

        return textDatabase[random];
    }

    public string GetText(int _index)
    {
        return textDatabase[_index];
    }
}
