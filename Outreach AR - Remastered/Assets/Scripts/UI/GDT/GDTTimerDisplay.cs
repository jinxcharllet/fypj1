﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class GDTTimerDisplay : MonoBehaviour {

    [SerializeField]
    private FloatVariable currTimer;
    [SerializeField]
    private FloatVariable maxTimer;

    public Image timerBar;
    public TextMeshProUGUI timerText;

	void Update ()
    {
        UpdateTimer();	
	}

    public void UpdateTimer()
    {
        timerBar.fillAmount = currTimer.GetVariable() / maxTimer.GetVariable();

        timerText.text = string.Format("{0}", Mathf.RoundToInt(currTimer.GetVariable()));
    }
}
