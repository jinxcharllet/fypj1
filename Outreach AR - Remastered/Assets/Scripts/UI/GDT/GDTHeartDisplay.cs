﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Manages the displaying of hearts on the screen; displays according to the amount of health.
/// </summary>
public class GDTHeartDisplay : MonoBehaviour
{
    [SerializeField]
    private UintVariable currHealth;

    [SerializeField]
    private UintVariable maxHealth;

    [SerializeField]
    private List<Image> heartSprites;

    public GridLayoutGroup heartLayout;
    public Sprite filledHeart;
    public Sprite emptyHeart;

    private void Start()
    {
        for(uint i = 0; i < maxHealth.GetVariable(); ++i)
        {
            GameObject Heart = new GameObject();
            Heart.transform.SetParent(gameObject.transform);
            Heart.transform.localScale = new Vector3(1, 1, 1);
            Heart.name = "Heart";

            Heart.AddComponent<Image>();

            Image img = Heart.GetComponent<Image>();
            img.sprite = filledHeart;

            //Instantiate(Heart);
        }

        foreach(Image image in gameObject.GetComponentsInChildren<Image>())
        {
            heartSprites.Add(image);
        }
    }

    //REMOVE after event is made to handle the update of health
    private void Update()
    {
        UpdateHealthUI();
    }
    
    public void UpdateHealthUI()
    {
        for (int i = 0; i < heartSprites.Count; ++i)
        {
            if (i < currHealth.GetVariable())

                heartSprites[i].sprite = filledHeart;
            else
                heartSprites[i].sprite = emptyHeart;
        }
    }
}
