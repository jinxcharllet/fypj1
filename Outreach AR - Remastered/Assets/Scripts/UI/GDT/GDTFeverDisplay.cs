﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GDTFeverDisplay : MonoBehaviour {
    
    [SerializeField]
    private FloatReference currFever, maxFever;

    public List<GameObject> feverBars;
    public List<Image> feverBarImage;

    private void Start()
    {
        foreach(GameObject go in feverBars)
        {
            feverBarImage.Add(go.GetComponentInChildren<Image>());
        }
    }

    private void Update()
    {
        UpdateFeverBar();
    }

    public void UpdateFeverBar()
    {
        float percentage = currFever.Value / maxFever.Value;
        int partitions = feverBars.Count;
        float portion = 1.0f / partitions;

        for (int i = 0; i < feverBarImage.Count; ++i)
        {
            feverBarImage[i].fillAmount = percentage;
        }

        for (int i = 0; i < feverBars.Count;++i)
        {
            if (percentage >= i * portion)
                feverBars[i].SetActive(true);
            else
                feverBars[i].SetActive(false);
        }
    }
}
