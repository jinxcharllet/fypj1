﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InteractionShake : MonoBehaviour {

    private Renderer renderer;
    private bool isShaking = false;
    private float defaultSpeed = 20.0f;
    [SerializeField] private float shakeSpeed = 500;
    [SerializeField] private float shakeDuration = 1;

    private float shakeTimer;

    void Start () {
        renderer = GetComponent<Renderer>();
        renderer.material.shader = Shader.Find("Custom/UnlitSway");//Gets the Shader
        shakeTimer = shakeDuration;
        defaultSpeed = renderer.material.GetFloat("_Speed");
    }
	
	// Update is called once per frame
	void Update () {
        if (isShaking)
        {
            shakeTimer -= Time.deltaTime;
        }
        if (shakeTimer <= 0)
        {
            isShaking = false;
            shakeTimer = shakeDuration;
            renderer.material.SetFloat("_Speed", defaultSpeed);
        }
    }

    public void Shake()
    {
        renderer.material.SetFloat("_Speed", shakeSpeed);
        isShaking = true;
        AudioManager.instance.Play("Tree Rustling");

    }
}
