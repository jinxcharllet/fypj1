﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class GDTTextDisplay : MonoBehaviour
{
    [SerializeField]
    private UintVariable Variable;

    public string prefix = "";
    public string postfix = "";

    public TextMeshProUGUI textMeshPro;

    private void Update()
    {
        textMeshPro.text = string.Format( prefix + "{0}" + postfix, Variable.GetVariable());   
    }
}
