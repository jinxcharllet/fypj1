﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class GDTComboDisplay : MonoBehaviour {
    [SerializeField]
    private UintVariable currCombo;

    public TextMeshProUGUI comboText;

    void Update()
    {
        comboText.text = string.Format("Combo x{0}", currCombo.GetVariable());    
    }
}
