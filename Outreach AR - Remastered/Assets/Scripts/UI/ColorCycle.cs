﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColorCycle : MonoBehaviour {
    
    MeshRenderer meshRenderer;

    public float speed = 0.1f;

    Color color = Color.white;

    private bool fixedColor = true;

    public Color Color
    {
        get
        {
            return color;
        }

        set
        {
            color = value;
        }
    }

    public bool FixedColor
    {
        get
        {
            return fixedColor;
        }

        set
        {
            fixedColor = value;
        }
    }

    void Start()
    {
        speed = 0.1f;
        meshRenderer = GetComponent<MeshRenderer>();
    }

    void Update()
    {
        if (!FixedColor)
            meshRenderer.material.color = HSBColor.ToColor(new HSBColor(Mathf.PingPong(Time.time * speed, 1), 1, 1));
        else
            meshRenderer.material.color = Color;
    }
}
