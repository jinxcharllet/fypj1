﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class CrosshairToggle : MonoBehaviour {

    public GameObject energyCrosshair;
    public GameObject gunCrosshair;
    
	void Update () {

        ChangePosition(Input.mousePosition);
       
    }

    public void ChangePosition(Vector3 _newPos)
    {
        energyCrosshair.transform.position = _newPos;
        gunCrosshair.transform.position = _newPos;
    }
}
