﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AmmoToggle : MonoBehaviour {

    [SerializeField]
    private GameEvent toggleAmmo;

    [SerializeField]
    private FloatVariable currEnergy;

    [SerializeField]
    private FloatVariable maxEnergy;

    public GameObject enemyAmmo;
    public GameObject energyAmmo;
    public GameObject enemyAmmoCrosshair;
    public GameObject energyAmmoCrosshair;

    private void Start()
    {
        enemyAmmo.SetActive(true);
        energyAmmo.SetActive(false);
        enemyAmmoCrosshair.SetActive(true);
        energyAmmoCrosshair.SetActive(false);
    }

    public void ToggleAmmo()
    {
        //Energy Charged
        enemyAmmo.SetActive(!enemyAmmo.activeSelf);
        energyAmmo.SetActive(!energyAmmo.activeSelf);
        enemyAmmoCrosshair.SetActive(!enemyAmmoCrosshair.activeSelf);
        energyAmmoCrosshair.SetActive(!energyAmmoCrosshair.activeSelf);
        toggleAmmo.InvokeAllListeners();
    }

    public void ResetAmmo()
    {
        enemyAmmo.SetActive(true);
        energyAmmo.SetActive(false);
        enemyAmmoCrosshair.SetActive(true);
        energyAmmoCrosshair.SetActive(false);
    }
}
