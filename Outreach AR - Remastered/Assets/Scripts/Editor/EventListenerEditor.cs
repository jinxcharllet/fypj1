﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(EventListener))]
public class EventListenerEditor : Editor {

    EventListener editorTarget;

    private void OnEnable()
    {
        editorTarget = target as EventListener;
    }

    public override void OnInspectorGUI()
    {
        serializedObject.Update();

        EditorGUILayout.Space();
        EditorGUILayout.Space();

        if (GUILayout.Button("Add Event"))
        {
            ++serializedObject.FindProperty("GameEvent").arraySize;
            ++serializedObject.FindProperty("Response").arraySize;
        }

        if (GUILayout.Button("Remove Last Event"))
        {
            --serializedObject.FindProperty("GameEvent").arraySize;
            --serializedObject.FindProperty("Response").arraySize;
        }

        EditorGUILayout.Space();
        EditorGUILayout.Space();
        EditorGUILayout.Space();
        EditorGUILayout.Space();

        for (int i = 0; i < serializedObject.FindProperty("GameEvent").arraySize; ++i)
        {
            EditorGUILayout.PropertyField(serializedObject.FindProperty("GameEvent").GetArrayElementAtIndex(i));
            EditorGUILayout.PropertyField(serializedObject.FindProperty("Response").GetArrayElementAtIndex(i));
        }

        serializedObject.ApplyModifiedProperties();


    }
}
