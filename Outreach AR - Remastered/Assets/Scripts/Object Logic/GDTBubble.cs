﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GDTBubble : MonoBehaviour
{
    private ObjectState
        m_state;

    [SerializeField]
    private GameObject
        m_iconObject;

    [SerializeField]
    private ObjectData
        m_iconMat;

    [SerializeField]
    private FloatReference
        m_maxTimer,
        m_minTimer;

    [SerializeField]
    private GDTGameLogic
        m_gdtDataReference;

    private Rigidbody
        m_rb;

    private float
        m_timer;

	void Awake ()
    {
        m_rb = GetComponent<Rigidbody>();
    }
	
    public void OnStart(int _state)
    {
        m_rb.velocity = Vector3.zero;
        m_timer = Random.Range(m_minTimer.Value, m_maxTimer.Value);

        int temp = Random.Range(0, m_iconMat.GetDataList()[_state].m_matList.Count);
        m_iconObject.GetComponent<Renderer>().material = m_iconMat.GetDataList()[_state].m_matList[temp];
        //Debug.Log(m_state.name);
    }

    void Update ()
    {
        m_timer -= Time.deltaTime;

        if (m_timer <= 0)
            gameObject.SetActive(false);
	}

    private void FixedUpdate()
    {
        if(m_rb.velocity.x < 3 && m_rb.velocity.z < 3)
         m_rb.AddForce(new Vector3(0, 1, 0), ForceMode.Acceleration);
    }

    public void DestroyBubbleWithType(ObjectState _type)
    {
        if(m_state.name.Equals(_type.name))
            gameObject.SetActive(false);
    }

    public void PopBubble()
    {
        m_gdtDataReference.OnBubblePop(m_state);
        gameObject.SetActive(false);
    }

    public void SetState(ObjectState _state)
    {
        m_state = _state;
    }
}
