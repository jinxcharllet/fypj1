﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DGADAssignment : MonoBehaviour
{

    [SerializeField]
    private MeshHolderList
        m_meshList;

    [SerializeField]
    private GameObject
        m_assignmentUpParticle;

    [SerializeField]
    private FloatVariable
        m_assignmentProgress;

    [SerializeField]
    [Range(0, 100)]
    private int
        m_energyIncrement;

    private float
       m_totalParticleTime;

    private MeshFilter
        m_meshFilter;

    private MeshRenderer
        m_meshRenderer;

    private MeshCollider
        m_meshCollider;

    [SerializeField]
    private IntVariable
        m_stage;

    [SerializeField]
    private GameEvent
        m_winEvent;

    [SerializeField]
    private GameEvent
        m_killAllEnemy;

    [SerializeField]
    private DGADGameLogic
        m_dgadLogic;

	void Start ()
    {
        m_meshFilter = GetComponent<MeshFilter>();
        m_meshRenderer = GetComponent<MeshRenderer>();
        m_meshCollider = GetComponent<MeshCollider>();

        m_stage.SetVariable(0);
        m_assignmentProgress.SetVariable(0);

        m_meshFilter.mesh = m_meshList.GetList()[m_stage.GetVariable()].GetMesh();
        m_meshCollider.sharedMesh = m_meshList.GetList()[m_stage.GetVariable()].GetMesh();
        m_meshRenderer.material = m_meshList.GetList()[m_stage.GetVariable()].GetMaterial();
        
        ParticleSystem ps = m_assignmentUpParticle.GetComponent<ParticleSystem>();

        if (ps.main.duration > m_totalParticleTime)
            m_totalParticleTime = ps.main.duration + ps.main.startLifetime.constantMax;
    }

    private void FixedUpdate()
    {
        if (m_stage.GetVariable() >= m_meshList.GetList().Count)
        {
            AudioManager.instance.Play("win game");
            m_killAllEnemy.InvokeAllListeners();
            m_winEvent.InvokeAllListeners();
            m_dgadLogic.canSpawn = false;
            return;
        }

        if (m_assignmentProgress.GetVariable() >= 100)
        {
            m_assignmentProgress.SetVariable(m_assignmentProgress.GetVariable() - 100);
            GameObject temp = Instantiate(m_assignmentUpParticle, gameObject.transform);
            Destroy(temp, m_totalParticleTime);
            m_stage.SetVariable(m_stage.GetVariable() + 1);

            m_meshFilter.mesh = m_meshList.GetList()[m_stage.GetVariable()].GetMesh();
            m_meshRenderer.material = m_meshList.GetList()[m_stage.GetVariable()].GetMaterial();
        }
    }

    public void LevelUpAssignment()
    {
        if (m_dgadLogic.DecreaseEnergy(m_energyIncrement))
        {
            m_assignmentProgress.SetVariable(m_assignmentProgress.GetVariable() + m_energyIncrement);
            gameObject.transform.localScale = new Vector3(gameObject.transform.localScale.x * 0.8f, gameObject.transform.localScale.y * 0.8f, gameObject.transform.localScale.z * 0.8f);
        }
    }

    public void ResetAssignment()
    {
        if(m_meshFilter == null)
        {
            m_meshFilter = GetComponent<MeshFilter>();
            m_meshRenderer = GetComponent<MeshRenderer>();
            m_meshCollider = GetComponent<MeshCollider>();
        }

        GameObject temp = Instantiate(m_assignmentUpParticle, gameObject.transform);
        Destroy(temp, m_totalParticleTime);
        m_stage.SetVariable(0);
        m_assignmentProgress.SetVariable(0);

        m_meshFilter.mesh = m_meshList.GetList()[m_stage.GetVariable()].GetMesh();
        m_meshCollider.sharedMesh = m_meshList.GetList()[m_stage.GetVariable()].GetMesh();
        m_meshRenderer.material = m_meshList.GetList()[m_stage.GetVariable()].GetMaterial();

    }
}
