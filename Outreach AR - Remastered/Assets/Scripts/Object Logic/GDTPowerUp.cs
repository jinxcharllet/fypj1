﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GDTPowerUp : MonoBehaviour
{
    [SerializeField]
    private GDTGameLogic
      m_gdtDataReference;

    private Rigidbody
        m_rb;

    void Awake()
    {
        m_rb = GetComponent<Rigidbody>();
    }

    public void OnStart ()
    {
        m_rb.velocity = Vector3.zero;
    }
	
	void Update ()
    {
        if (gameObject.transform.position.y <= 0)
            gameObject.SetActive(false);

        gameObject.transform.LookAt(transform.position + m_rb.velocity.normalized);
        gameObject.transform.eulerAngles = new Vector3(gameObject.transform.eulerAngles.x, gameObject.transform.eulerAngles.y + 90, gameObject.transform.eulerAngles.z);

    }

    public void PopFreeze()
    {
        m_gdtDataReference.OnFreezePop();
        gameObject.SetActive(false);
    }
}
