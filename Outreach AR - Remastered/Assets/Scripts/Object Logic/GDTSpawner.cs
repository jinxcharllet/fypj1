﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GDTSpawner : BaseSpawner
{
    [SerializeField]
    private FloatReference
        m_shootForceMax,
        m_shootForceMin, 
        
        m_riggedChance;

    [SerializeField]
    private Animator
        m_animator;

    private uint
        m_amountToShoot;

    private bool
        m_isFever, 
        m_isPowerUp;

    protected void Start()
    {
        m_amountToShoot = 0;
        m_isFever = false;
        m_isPowerUp = false;
    }


    public void SpawnBubble()
    {
        ++m_amountToShoot;

        if (!m_animator.GetBool("startShooting"))
        {
            m_animator.SetBool("startShooting", true);
        }
    }

    public void SpawnFeverBubble()
    {
        m_isFever = true;
        ++m_amountToShoot;

        if (!m_animator.GetBool("startShooting"))
        {
            m_animator.SetBool("startShooting", true);
        }
    }

    public void SpawnPowerUp()
    {
        m_isPowerUp = true;
        ++m_amountToShoot;

        if (!m_animator.GetBool("startShooting"))
        {
            m_animator.SetBool("startShooting", true);
        }
    }

    public void InitializeBubble()
    {
        while (m_amountToShoot > 0)
        {
            GameObject temp = ObjectPool.GetInstance().GetObject(m_objectToSpawn);
            temp.transform.position = new Vector3(m_spawnPoint.transform.position.x, m_spawnPoint.transform.position.y, m_spawnPoint.transform.position.z);

            int randomNo = 0;

            if (m_isFever)
            {
                randomNo = m_data.GetDataList().Count - 1;
            }
            else
            {
                if(Random.Range(0, 100) < m_riggedChance.Value)
                    randomNo = 0;
                else
                    randomNo = 1;
            }

            if (m_isPowerUp)
            {
                GDTPowerUp temp_script = temp.GetComponent<GDTPowerUp>();

                temp_script.OnStart();

                Vector3 temp_vec = new Vector3(m_spawnPoint.transform.forward.normalized.x, m_spawnPoint.transform.forward.normalized.y, m_spawnPoint.transform.forward.normalized.z + Random.Range(-0.5f, 0.5f));
                temp.GetComponent<Rigidbody>().AddForce(temp_vec * Random.Range(m_shootForceMin.Value, m_shootForceMax.Value), ForceMode.Force);
            }
            else
            {
                int randomMat = Random.Range(0, m_data.GetDataList()[randomNo].m_matList.Count);

                GDTBubble temp_script = temp.GetComponent<GDTBubble>();

                temp.GetComponent<Renderer>().material = m_data.GetDataList()[randomNo].m_matList[randomMat];
                temp_script.SetState(m_data.GetDataList()[randomNo].m_state);
                temp_script.OnStart(randomNo);

                Vector3 temp_vec = new Vector3(m_spawnPoint.transform.forward.normalized.x, 0, m_spawnPoint.transform.forward.normalized.z + Random.Range(-0.5f, 0.5f));
                temp.GetComponent<Rigidbody>().AddForce(temp_vec * Random.Range(m_shootForceMin.Value, m_shootForceMax.Value), ForceMode.Force);
            }
            
            --m_amountToShoot;
        }

        if (m_animator.GetBool("startShooting"))
            m_animator.SetBool("startShooting", false);

        if (m_isFever)
            m_isFever = false;

        if (m_isPowerUp)
            m_isPowerUp = false;
    }
}
