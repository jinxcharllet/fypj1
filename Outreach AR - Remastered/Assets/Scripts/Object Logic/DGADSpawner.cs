﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DGADSpawner : BaseSpawner
{
    [SerializeField]
    private Vector3
        m_maxArea;

    [SerializeField]
    private Transform
        m_endPoint;

    public void SpawnEnemy()
    {
        GameObject temp = ObjectPool.GetInstance().GetObject(m_objectToSpawn);
        temp.transform.position = new Vector3(transform.position.x + Random.Range(-m_maxArea.x, m_maxArea.x), 
            transform.position.y + Random.Range(-m_maxArea.y, m_maxArea.y), 
            transform.position.z + Random.Range(-m_maxArea.z, m_maxArea.z));

        temp.GetComponent<DGADEnemy>().OnStart(m_endPoint.position);
    }
}
