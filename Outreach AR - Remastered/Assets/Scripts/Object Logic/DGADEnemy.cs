﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DGADEnemy : MonoBehaviour
{

    [SerializeField]
    private MatwColorList
        m_matList;

    [SerializeField]
    private GameObject
        m_deadParticle;

    [SerializeField]
    private DGADGameLogic
        m_dgadGameLogic;

    private MeshRenderer
        m_meshRenderer;

    private Rigidbody
        m_rb;

    private ColorCycle
        m_colCycle;

    private Vector3
        m_endPoint;

    [SerializeField]
    private GameEvent
        m_hurt;

    [SerializeField]
    private FloatReference
        m_speed;

    private float
      m_totalParticleTime;

    public void Awake()
    {
        m_meshRenderer = GetComponent<MeshRenderer>();
        m_rb = GetComponent<Rigidbody>();
        m_colCycle = GetComponent<ColorCycle>();

        ParticleSystem ps = m_deadParticle.GetComponent<ParticleSystem>();

        if (ps.main.duration > m_totalParticleTime)
            m_totalParticleTime = ps.main.duration + ps.main.startLifetime.constantMax;
    }

    public void OnStart(Vector3 _endPos)
    {
        int rand = Random.Range(0, m_matList.GetList().Count);
        m_meshRenderer.material = m_matList.GetList()[rand].GetMaterial();
        m_colCycle.FixedColor = true;
        m_colCycle.Color = m_matList.GetList()[rand].GetColor();
        m_endPoint = _endPos;
    }

    private void Update()
    {
        if (Vector3.Distance(transform.position, m_endPoint) < 4)
        {
            gameObject.SetActive(false);
            GameObject temp = Instantiate(m_deadParticle, gameObject.transform.position, gameObject.transform.rotation);
            Destroy(temp, m_totalParticleTime);
            m_dgadGameLogic.OnHurt();
            m_hurt.InvokeAllListeners();
            AudioManager.instance.Play("asn take dmg");
        }
    }

    public void KillWithPoints()
    {
        AudioManager.instance.Play("enemy death");
        m_dgadGameLogic.AddPoint();
        gameObject.SetActive(false);
        GameObject temp = Instantiate(m_deadParticle, gameObject.transform.position, gameObject.transform.rotation);
        Destroy(temp, m_totalParticleTime);
    }

    public void Kill()
    {
        AudioManager.instance.Play("enemy death");
        gameObject.SetActive(false);
        GameObject temp = Instantiate(m_deadParticle, gameObject.transform.position, gameObject.transform.rotation);
        Destroy(temp, m_totalParticleTime);
    }

    void FixedUpdate ()
    {
        Vector3 forward_temp = (m_endPoint - m_rb.transform.position).normalized;

        m_rb.MovePosition(m_rb.transform.position + (forward_temp * (m_speed.Value * Time.deltaTime)));

    }
}
