﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class BaseSpawner : MonoBehaviour
{
    [SerializeField]
    protected ObjectData
           m_data;

    [SerializeField]
    protected GameObject
        m_objectToSpawn,
        m_spawnPoint;
    
}
