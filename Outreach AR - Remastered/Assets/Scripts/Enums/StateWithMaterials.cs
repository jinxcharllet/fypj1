﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class StateWithMaterials
{
    public ObjectState m_state;
    public List<Material> m_matList = new List<Material>();
}
