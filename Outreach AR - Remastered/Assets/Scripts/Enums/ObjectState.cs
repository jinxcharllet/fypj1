﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Object State")]
public class ObjectState : ScriptableObject
{
    public enum TYPE
    {
        NONE,
        HEALTH,
        SCORE,
    }

    [SerializeField]
    private BoolReference
        m_affectFever,
        m_affectCombo;

    [SerializeField]
    private FloatReference
        m_FeverAmount;

    [SerializeField]
    private IntReference
        m_typeAmount,
        m_comboAmount;

    [SerializeField]
    private TYPE
        m_type;

    public TYPE GetObjectAffectType()
    {
        return m_type;
    }

    public bool AffectsFever()
    {
        return m_affectFever.Value;
    }

    public bool AffectsCombo()
    {
        return m_affectCombo.Value;
    }

    public float GetFeverAmount()
    {
        return m_FeverAmount.Value;
    }

    public int GetTypeAmount()
    {
        return m_typeAmount.Value;
    }

    public int GetComboAmount()
    {
        return m_comboAmount.Value;
    }
}
