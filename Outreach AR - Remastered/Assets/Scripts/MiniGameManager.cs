﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MiniGameManager : MonoBehaviour
{
    [SerializeField]
    public IGameLogic gameLogic;

    [SerializeField]
    public GameEvent gameIdle, gameStart, gamePlay, gameOver, gameEnd, gamePause, gameResume, gameStateChanged;

    [SerializeField]
    public List<GameObject>
        GDTFlowers = new List<GameObject>();

    [SerializeField]
    public List<GameObject>
        GDTPowerUpFlowers = new List<GameObject>();

    [SerializeField]
    public List<GameObject>
        DGADSpawnpoints = new List<GameObject>();


    
    void Start()
    {
        gameLogic.Init(this);
    }

    void Update()
    {
        gameLogic.Update();
    }
    
    //Game State Functions
    public void GameIdle()
    {
        gameStateChanged.InvokeAllListeners();
        gameIdle.InvokeAllListeners();
    }
    public void GameStart()
    {
        gameStateChanged.InvokeAllListeners();
        gameStart.InvokeAllListeners();
    }
    public void GamePlay()
    {
        gameStateChanged.InvokeAllListeners();
        gamePlay.InvokeAllListeners();
    }
    public void GameOver()
    {
        gameStateChanged.InvokeAllListeners();
        gameOver.InvokeAllListeners();
    }
    public void GameEnd()
    {
        gameStateChanged.InvokeAllListeners();
        gameEnd.InvokeAllListeners();
    }
    public void GamePause()
    {
        gameStateChanged.InvokeAllListeners();
        gamePause.InvokeAllListeners();
    }
    public void GameResume()
    {
        gameStateChanged.InvokeAllListeners();
        gameResume.InvokeAllListeners();
    }


    //Handle Events
    public void OnGameIdle()
    {
        gameLogic.ChangeGameState(GameLogicState.GameIdle);
    }
    public void OnGameStart()
    {
        gameLogic.ChangeGameState(GameLogicState.GameStart);
    }
    public void OnGamePlay()
    {
        gameLogic.ChangeGameState(GameLogicState.GamePlay);
    }
    public void OnGameOver()
    {
        gameLogic.ChangeGameState(GameLogicState.GameOver);
    }
    public void OnGameEnd()
    {
        gameLogic.ChangeGameState(GameLogicState.GameEnd);
    }
    public void OnGamePause()
    {
        gameLogic.ChangeGameState(GameLogicState.GamePause);
    }
    public void OnGameResume()
    {
        gameLogic.ChangeGameState(GameLogicState.GameResume);
    }
    


    //Game Logic Functions
    public IGameLogic GetGameLogic()
    {
        return gameLogic;
    }
    public void ChangeGameLogic(IGameLogic _gameLogic)
    {
        if (_gameLogic == null) return;

        if (_gameLogic.name == gameLogic.name)
        {
            return;
        }
        else
        {
            Debug.Log("Logic Changed: " + _gameLogic.name);
            gameLogic.Exit();
            gameLogic = _gameLogic;
            gameLogic.Init(this);
        }
    }
}
