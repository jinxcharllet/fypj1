﻿Shader "HoopAWolf/Celshader"
{
	Properties
	{
		_MainTex ("Texture", 2D) = "white" {}
		_Glossiness("Glossiness", float) = 32
		_RimAmount("Rim Amount", Range(0, 1)) = 0.716
		_RimThreshold("Rim Threshold", Range(0, 1)) = 0.1

		[HDR]
		_Color("Color", Color) = (0,0,0,1)
		_AmbientColor("Ambient Color", Color) = (0.4,0.4,0.4,1)
		_SpecularColor("Specular Color", Color) = (0.9,0.9,0.9,1)
		_RimColor("Rim Color", Color) = (1,1,1,1)
	}
		SubShader
		{
			Tags
			{
				"LightMode" = "ForwardBase"
				"PassFlags" = "OnlyDirectional"
			}

			LOD 100

			Pass
			{
				CGPROGRAM
				#pragma vertex vert
				#pragma fragment frag
				// make fog work
				#pragma multi_compile_fog
				#pragma multi_compile_fwdbase

				#include "UnityCG.cginc"
				#include "Lighting.cginc"
				#include "AutoLight.cginc"

			float
				_Glossiness, 
				_RimAmount, 
				_RimThreshold;

			sampler2D 
				_MainTex;

			float4
				_Color,
				_AmbientColor,
				_SpecularColor,
				_RimColor,

				_MainTex_ST;

			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
				float3 normal: NORMAL;
			};

			struct v2f
			{
				float2 uv : TEXCOORD0;
				float view_dir : TEXCOORD1;
				float4 pos : SV_POSITION;
				float3 world_normal : NORMAL;
				UNITY_FOG_COORDS(1)
				SHADOW_COORDS(2)
			};

			
			v2f vert (appdata v)
			{
				v2f o;
				o.pos = UnityObjectToClipPos(v.vertex);
				o.uv = TRANSFORM_TEX(v.uv, _MainTex);
				o.world_normal = UnityObjectToWorldNormal(v.normal);
				o.view_dir = WorldSpaceViewDir(v.vertex);
				UNITY_TRANSFER_FOG(o,o.vertex);
				TRANSFER_VERTEX_TO_FRAGMENT(o);
				return o;
			}
			
			fixed4 frag (v2f i) : SV_Target
			{
				float shadow = SHADOW_ATTENUATION(i);

				float3 normal = normalize(i.world_normal);
				float NdotL = dot(_WorldSpaceLightPos0, normal);
				float light_intensity = smoothstep(0, 0.01, NdotL * shadow);

				float3 view_dir = normalize(i.view_dir);
				float3 half_vector = normalize(_WorldSpaceLightPos0 + view_dir);
				float NdotH = dot(normal, half_vector);

				float specular_intensity = smoothstep(0.005, 0.01, pow(NdotH * light_intensity, _Glossiness * _Glossiness)) * _SpecularColor;

				float4 rim_dot = 1 - dot(view_dir, normal);

				float rim_intensity = rim_dot * pow(NdotL, _RimThreshold);

				float4 rim = smoothstep(_RimAmount - 0.01, _RimAmount + 0.01, rim_intensity) * _RimColor;

				// sample the texture
				fixed4 col = tex2D(_MainTex, i.uv);
				// apply fog
				UNITY_APPLY_FOG(i.fogCoord, col);
				return col * _Color * (_AmbientColor + (light_intensity * _LightColor0) + specular_intensity + rim);
			}
			ENDCG
		}

			UsePass "Legacy Shaders/VertexLit/SHADOWCASTER"
	}
}
