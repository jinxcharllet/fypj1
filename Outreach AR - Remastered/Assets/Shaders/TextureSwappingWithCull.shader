﻿Shader "HoopAWolf/TextureSwappingWithCull"
{
	Properties
	{
		_MainTex("Primary Texture", 2D) = "white" {}
		_SecondaryTex("Secondary Texture", 2D) = "white" {}
		_OffSet("OffSet", Range(0, 1)) = 0
		_Color("Color", Color) = (1, 1, 1, 1)
	}
		SubShader
		{
			Tags
			{
				"RenderType" = "Transparent"
				"Queue" = "Transparent"
			}
			LOD 100

			Pass
			{
				Blend SrcAlpha OneMinusSrcAlpha
				Cull Back
				ZWrite On

				CGPROGRAM
				#pragma vertex vert
				#pragma fragment frag
			// make fog work
			#pragma multi_compile_fog

			#include "UnityCG.cginc"

			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
			};

			struct v2f
			{
				float2 uv : TEXCOORD0;
				UNITY_FOG_COORDS(1)
				float4 vertex : SV_POSITION;
			};

			sampler2D
				_MainTex,
				_SecondaryTex;

			float
				_OffSet;

			float4
				_Color,

				_MainTex_ST,
				_SecondaryTex_ST;

			v2f vert(appdata v)
			{
				v2f o;
				o.vertex = UnityObjectToClipPos(v.vertex);
				o.uv = TRANSFORM_TEX(v.uv, _MainTex);
				UNITY_TRANSFER_FOG(o,o.vertex);
				return o;
			}

			fixed4 frag(v2f i) : SV_Target
			{
				// sample the texture
				fixed4 col = tex2D(_MainTex, i.uv);
				fixed4 sec_col = tex2D(_SecondaryTex, i.uv);

				fixed4 combined_col = (col * (1 - _OffSet)) + (sec_col * _OffSet);

				fixed4 total_col = combined_col * _Color;

				// apply fog
				UNITY_APPLY_FOG(i.fogCoord, total_col);
				return total_col;
			}
			ENDCG
		}
		}
}
