﻿Shader "OutReach/ToonShader"
{
	Properties
	{
		_MainTex ("Texture", 2D) = "white" {}

		_Color("Diffuse Material Color", Color) = (1,1,1,1)
		_UnlitColor("Unlit Color", Color) = (0.5,0.5,0.5,1)
		_DiffuseThreshold("Lighting Threshold", Range(-1.1,1)) = 0.1
		_SpecColor("Specular Material Color", Color) = (1,1,1,1)
		_Shininess("Shininess", Range(0.5,1)) = 1
		_OutlineThickness("Outline Thickness", Range(0,1)) = 0.1
	}
	SubShader
	{
		Tags 
		{ 
			"RenderType"="Opaque" 
			"LightMode" = "ForwardBase"
		}

		LOD 100

		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			// make fog work
			#pragma multi_compile_fog
			
			#include "UnityCG.cginc"

			sampler2D 
				_MainTex;

			float
				_Shininess, 
				_OutlineThickness, 
				_DiffuseThreshold;

			float4 
				_UnlitColor,
				_Color,
				_SpecColor,

				_MainTex_ST, 
				_LightColor0;

			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
				float3 normal : NORMAL;
			};

			struct v2f
			{
				float2 uv : TEXCOORD0;
				float4 vertex : SV_POSITION;
				float3 normalDir : TEXCOORD1;
				float4 lightDir : TEXCOORD2;
				float3 viewDir : TEXCOORD3;
				UNITY_FOG_COORDS(1)
			};

			
			v2f vert (appdata v)
			{
				v2f o;

				//normalDirection
				o.normalDir = normalize(mul(float4(v.normal, 0.0), unity_WorldToObject).xyz);

				//World position
				float4 posWorld = mul(unity_ObjectToWorld, v.vertex);

				//view direction
				o.viewDir = normalize(_WorldSpaceCameraPos.xyz - posWorld.xyz); //vector from object to the camera

				//light direction
				float3 fragmentToLightSource = (_WorldSpaceCameraPos.xyz - posWorld.xyz);
				o.lightDir = float4(
					normalize(lerp(_WorldSpaceLightPos0.xyz, fragmentToLightSource, _WorldSpaceLightPos0.w)),
					lerp(1.0, 1.0 / length(fragmentToLightSource), _WorldSpaceLightPos0.w)
					);

				o.vertex = UnityObjectToClipPos(v.vertex);
				o.uv = TRANSFORM_TEX(v.uv, _MainTex);
				UNITY_TRANSFER_FOG(o,o.vertex);
				return o;
			}
			
			fixed4 frag (v2f i) : COLOR
			{
				float nDotL = saturate(dot(i.normalDir, i.lightDir.xyz));
				//Diffuse threshold calculation
			 float diffuseCutoff = saturate((max(_DiffuseThreshold, nDotL) - _DiffuseThreshold) * 1000);

			//Specular threshold calculation
			float specularCutoff = saturate(max(_Shininess, dot(reflect(-i.lightDir.xyz, i.normalDir), i.viewDir)) - _Shininess) * 1000;

			//Calculate Outlines
			float outlineStrength = saturate((dot(i.normalDir, i.viewDir) - _OutlineThickness) * 1000);


			float3 ambientLight = (1 - diffuseCutoff) * _UnlitColor.xyz; //adds general ambient illumination
			float3 diffuseReflection = (1 - specularCutoff) * _Color.xyz * diffuseCutoff;
			float3 specularReflection = _SpecColor.xyz * specularCutoff;

			float3 combinedLight = (ambientLight + diffuseReflection) * outlineStrength + specularReflection;

			UNITY_APPLY_FOG(i.fogCoord, col);

			return tex2D(_MainTex, i.uv) * float4(combinedLight, 1.0);
			}
			ENDCG
		}
	}
}
