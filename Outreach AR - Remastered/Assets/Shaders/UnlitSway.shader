﻿Shader "Custom/UnlitSway"
{
	Properties
	{
		_MainTex ("Texture", 2D) = "white" {}
		_Speed("MoveSpeed", Range(20,500)) = 25 // speed of the swaying
		_Rigidness("Rigidness", Range(1,50)) = 25 // lower makes it look more "liquid" higher makes it look rigid
		_SwayMax("Sway Max", Range(0, 0.1)) = .005 // how far the swaying goes
		_YOffset("Y offset", float) = 0.5// y offset, below this is no animation
		_Color ("Main Color", Color) = (1,1,1,1)
	}
	SubShader
	{
		Tags { "RenderType"="Opaque" "DisableBatching" = "True" }
		LOD 100

		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			// make fog work
			#pragma multi_compile_fog
			
			#include "UnityCG.cginc"

			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
			};

			struct v2f
			{
				float2 uv : TEXCOORD0;
				UNITY_FOG_COORDS(1)
				float4 vertex : SV_POSITION;
			};

			sampler2D _MainTex;
			float4 _MainTex_ST;


			float _Speed;
			float _SwayMax;
			float _YOffset;
			float _Rigidness;
			fixed4 _Color;
			
			v2f vert (appdata v)
			{

				float3 localPos = v.vertex - mul(unity_ObjectToWorld, float4(0, 0, 0, 1)).xyz;
				//Swaying Codes
				//float3 wpos = mul(unity_ObjectToWorld, v.vertex).xyz;// world position
				float z = sin(localPos.x / _Rigidness + (_Time.x * _Speed)) *(v.vertex.y - _YOffset) * 5;// x axis movements
				float x = sin(localPos.z / _Rigidness + (_Time.x * _Speed)) *(v.vertex.y - _YOffset) * 5;// z axis movements
				v.vertex.x += step(0, v.vertex.y - _YOffset) * x * _SwayMax;// apply the movement if the vertex's y above the YOffset
				v.vertex.z += step(0, v.vertex.y - _YOffset) * z * _SwayMax;

				v2f o;
				o.vertex = UnityObjectToClipPos(v.vertex);
				o.uv = TRANSFORM_TEX(v.uv, _MainTex);
				UNITY_TRANSFER_FOG(o,o.vertex);
				return o;
			}
			
			fixed4 frag (v2f i) : SV_Target
			{
				// sample the texture
				fixed4 col = tex2D(_MainTex, i.uv) * _Color;
				// apply fog
				UNITY_APPLY_FOG(i.fogCoord, col);
				return col;
			}
			ENDCG
		}
	}
}
