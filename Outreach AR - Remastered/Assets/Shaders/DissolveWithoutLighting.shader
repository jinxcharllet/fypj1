﻿Shader "HoopAWolf/Dissolve/Without Lighting"
{
	Properties
	{
		_MainTexture("Main Texture", 2D) = "white" {}
		_DissolveTexture("Dissolve Texture", 2D) = "white" {}

		_Color("Color", Color) = (1, 1, 1, 1)
		_DissolveAmount("Dissolve Amount", Range(0, 1)) = 1

		_DissolveColorStart("Dissolve Color Start", Color) = (1, 1, 1, 1)
		_DissolveColorEnd("Dissolve Color End", Color) = (1, 1, 1, 1)
		_DissolveSize("Dissolve Size", Range(0, 0.1)) = 0
	}

		SubShader
		{

			Tags
			{
				"Queue" = "Transparent"
				"RenderType" = "Transparent"
				"LightMode" = "ForwardBase"
			}

			Pass
			{
				Blend SrcAlpha OneMinusSrcAlpha
				Cull Off
				

				CGPROGRAM

				#pragma vertex vertexFunc
				#pragma fragment fragmentFunc
				#pragma multi_compile_fwdbase

				#include "UnityCG.cginc"
				#include "AutoLight.cginc"


			//Vertex
			//Build the object
			// Vert, Normal, Color, UV
			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
				float3 normal : NORMAL;
			};

			struct v2f
			{
				float4 position : SV_POSITION;
				float2 uv : TEXCOORD0;
				float4 color : COLOR0;
			};

			sampler2D
				_MainTexture,
				_DissolveTexture;

			float4
				_Color,
				_DissolveColorStart,
				_DissolveColorEnd,

				_LightColor0,
				_MainTexture_ST;

			float
				_DissolveAmount,
				_DissolveSize;

			//Build Object
			v2f vertexFunc(appdata _input)
			{
				v2f output;

				output.position = UnityObjectToClipPos(_input.vertex);
				output.uv = TRANSFORM_TEX(_input.uv, _MainTexture);
				// first off, we need the normal to be in world space
				float3 normalDirection = mul(unity_ObjectToWorld, _input.normal);

				// we will only be dealing with a single directional light
				float3 lightDirection = normalize(_WorldSpaceLightPos0.xyz);
				float ndotl = dot(normalDirection, lightDirection);
				float3 diffuse = _LightColor0.xyz * max(0.0, ndotl);

				output.color = half4(diffuse, 1.0);
				return output;
			}

			//Fragment
			//Color it in
			fixed4 fragmentFunc(v2f _input) : SV_Target
			{
				float4 texColor = tex2D(_MainTexture, _input.uv) * _input.color;
				float disColor = tex2D(_DissolveTexture, _input.uv).r;

				if (disColor < _DissolveAmount)
					discard;

				if (disColor < texColor.a && disColor < _DissolveAmount + _DissolveSize)
					texColor = lerp(_DissolveColorStart, _DissolveColorEnd, (disColor - _DissolveAmount) / _DissolveSize);

				return texColor * _Color;
			}

			ENDCG
		}
		}
}